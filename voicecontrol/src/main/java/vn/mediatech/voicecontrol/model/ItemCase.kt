package vn.mediatech.voicecontrol.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class ItemCase(
    var id: String? = null,
    var title: String? = null,
    var itemCaseAnswerList: ArrayList<ItemCaseAnswer>? = null,
    var timeStartHHmm: String? = "00:00",
    var timeEndHHmm: String? = "23:59",
    var titleSpeak: String? = null
) : Parcelable {
}