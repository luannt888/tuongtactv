package vn.mediatech.voicecontrol.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemUtility(
    @SerializedName("tabid") val id: Int?,
    @SerializedName("avatar") val icon: String?,
    val iconRes: Int?,
    @SerializedName("title") var title: String? = null,
    @SerializedName("short_title") var shortTitle: String? = null
) : Parcelable
/*show_date: "2021-05-18",
id: "525",
avatar: "https://graph.facebook.com/2999532883651035/picture?type=large",
full_name: "Lê Việt Hoàng"*/