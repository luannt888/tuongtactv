package vn.mediatech.voicecontrol.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemCaseAnswer(
    var anserKey: Array<String>? = null,
    var questionNext: String? = null,
    var key: String? = null,
    var id: String? = null,
    var type: String? = null
) : Parcelable {
}