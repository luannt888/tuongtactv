package vn.mediatech.voicecontrol.voiceControl

/**
 * Created by stephenvinouze on 18/05/2017.
 */
enum class RecognitionStatus {
    SUCCESS,
    UNAVAILABLE
}