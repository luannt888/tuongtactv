package vn.mediatech.voicecontrol.voiceControl

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.media.AudioManager
import android.net.ConnectivityManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.util.DisplayMetrics
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import vn.mediatech.voicecontrol.R
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class VcUtils {
    companion object {
        var ENABLE_VOICE_CONTROLLER = false
        const val POST = 1
        const val GET = 2
        const val SUCCESS = 200
        const val REQUEST_CODE_PERMISSION_AUDIO = 853
        const val CODE = "status"
        const val MESSAGE = "msg"
        const val RESULT = "result"
        const val DATA = "data"
        const val SPEAK = "speak"
        const val UTILITY = "UTILITY"
        const val SUGGEST = "SUGGEST"
        const val LISTENING = "listening"
        const val HEIGHT = "HEIGHT"
        const val MORNING_TIME_INT = 1
        const val TIME_LISTEN_MAX = 2000
        const val AFTERNOON_TIME_INT = 2
        const val EVENING_TIME_INT = 3
        const val NIGHT_TIME_INT = 4

        const val MORNING_TIME_STR = "MORNING"
        const val AFTERNOON_TIME_STR = "AFTERNOON"
        const val EVENING_TIME_STR = "EVENING"
        const val NIGHT_TIME_STR = "NIGHT"
        fun isEmpty(data: String?): Boolean {
            return data == null || data.trim { it <= ' ' }.isEmpty()
        }

        fun isNetworkConnect(context: Context?): Boolean {
            if (context == null) {
                return false
            }
            val connMgr = context.applicationContext
                .getSystemService(Activity.CONNECTIVITY_SERVICE) as ConnectivityManager
                ?: return true
            val networkInfo = connMgr.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }

        fun getGreetingMessage(): String {
            val c = Calendar.getInstance()
            val timeOfDay = c.get(Calendar.HOUR_OF_DAY)

            return when (timeOfDay) {
                in 0..11 -> "Good Morning"
                in 12..15 -> "Good Afternoon"
                in 16..20 -> "Good Evening"
                in 21..23 -> "Good Night"
                else -> "Hello"
            }
        }

        fun getTimeSessionStringFromMilli(millis: Long): String? {
            val c = Calendar.getInstance()
            c.timeInMillis = millis
            val hours = c[Calendar.HOUR_OF_DAY]
            if (hours >= 1 && hours <= 12) {
                return MORNING_TIME_STR
            } else if (hours >= 12 && hours <= 16) {
                return AFTERNOON_TIME_STR
            } else if (hours >= 16 && hours <= 21) {
                return EVENING_TIME_STR
            } else if (hours >= 21 && hours <= 24) {
                return NIGHT_TIME_STR
            }
            return null
        }

        fun getTimeSessionIntFromMilli(millis: Long): Int? {
            val c = Calendar.getInstance()
            c.timeInMillis = millis
            val hours = c[Calendar.HOUR_OF_DAY]
            if (hours >= 1 && hours <= 12) {
                return MORNING_TIME_INT
            } else if (hours >= 12 && hours <= 16) {
                return AFTERNOON_TIME_INT
            } else if (hours >= 16 && hours <= 21) {
                return EVENING_TIME_INT
            } else if (hours >= 21 && hours <= 24) {
                return NIGHT_TIME_INT
            }
            return null
        }

        fun getHours(millis: Long): Int? {
            val c = Calendar.getInstance()
            c.timeInMillis = millis
            return try {
                c[Calendar.HOUR_OF_DAY]
            } catch (e: Exception) {
                null
            }
        }

        fun getCurrentHours(): Int? {
            return getHours(System.currentTimeMillis())
        }

        fun getMinute(millis: Long): Int? {
            try {
                val c = Calendar.getInstance()
                c.timeInMillis = millis
                return c[Calendar.MINUTE]
            } catch (e: Exception) {
                return null
            }
        }

        fun getHours(timeHHmm: String?): Int? {
            if (timeHHmm.isNullOrEmpty()) {
                return null
            }
            try {
                return timeHHmm.substring(0, timeHHmm.indexOf(":")).toInt()
            } catch (e: Exception) {
            }
            return null
        }

        fun getTimeMs(timeHHmm: String?): Long? {
            if (timeHHmm.isNullOrEmpty()) {
                return null
            }
            val c = Calendar.getInstance()
            val hour = getHours(timeHHmm)
            val min = getMinute(timeHHmm)
            if (hour == null || min == null) {
                return null
            }
            c.set(Calendar.HOUR_OF_DAY, hour)
            c.set(Calendar.MINUTE, min)
            return c.timeInMillis
        }

        fun getMinute(timeHHmm: String?): Int? {
            if (timeHHmm.isNullOrEmpty()) {
                return null
            }
            try {
                return timeHHmm.substring(timeHHmm.indexOf(":") + 1).toInt()
            } catch (e: Exception) {
            }
            return null
        }

        fun getCurrentMinute(): Int? {
            return getMinute(System.currentTimeMillis())
        }

        fun isInTime(timeStartHHmm: String?, timeEndHHmm: String?): Boolean {
            if (timeEndHHmm.isNullOrEmpty() || timeStartHHmm.isNullOrEmpty()) {
                return false
            }
            return isInTime(getTimeMs(timeStartHHmm), getTimeMs(timeEndHHmm))
        }

        fun isInTime(timeStartMs: Long?, timeEndMs: Long?): Boolean {
            if (timeStartMs == null || timeEndMs == null) {
                return false
            }
            val currentTimeMs = System.currentTimeMillis()
            return currentTimeMs >= timeStartMs && currentTimeMs < timeEndMs
        }

        fun setHtmlTextView(text: String, textView: TextView?) {
            if (textView == null) {
                return
            }
            textView.setText(getHtmlFormat(text + ""))
        }

        fun getStringHtmlColor(mString: String?, hexColor: String?): String? {
            if (hexColor == null || mString == null || !hexColor.startsWith("#")) {
                return mString
            }
            val string = "<span style=\"color: $hexColor\">$mString</span>"
            return string
        }

        fun getStringHtmlStype(
            mString: String?,
            hexColor: String?,
            isBold: Boolean,
            isUnderline: Boolean
        ): String? {
            if (mString == null) {
                return mString
            }
            var string: String = mString
            if (isBold) {
                string = "<b>$string</b>"
            }
            if (isUnderline) {
                string = "<u>$string</u>"
            }
            if (hexColor != null && hexColor.startsWith("#")) {
                string = "<span style=\"color: $hexColor\">$string</span>"
            }
            return string
        }

        fun setHtmlStypeTextView(
            textView: TextView?,
            mString: String?,
            hexColor: String?,
            isBold: Boolean,
            isUnderline: Boolean
        ) {
            if (mString == null || textView == null) {
                return
            }
            var string: String = mString
            if (isBold) {
                string = "<b>$string</b>"
            }
            if (isUnderline) {
                string = "<u>$string</u>"
            }
            if (hexColor != null && hexColor.startsWith("#")) {
                string = "<span style=\"color: $hexColor\">$string</span>"
            }
            setHtmlTextView(string, textView)
        }
//        fun loadImageCircle(
//            context: Context?, imageView: ImageView?, url: String?,
//            placeholderDrawableId: Int
//        ) {
//            if (context == null || imageView == null || url.isNullOrEmpty()) {
//                return
//            }
//            val options = if (placeholderDrawableId <= 0) {
//                RequestOptions().transform(CircleCrop())
//                    .priority(Priority.NORMAL)
//            } else {
//                RequestOptions().transform(CircleCrop())
//                    .placeholder(placeholderDrawableId)
//                    .error(placeholderDrawableId)
//                    .priority(Priority.NORMAL)
//            }
//            Glide.with(context).load(url).apply(options).into(imageView!!)
//        }
//
//        fun loadImageCircleClearCache(
//            context: Context?, imageView: ImageView?, url: String?,
//            placeholderDrawableId: Int
//        ) {
//            val options: RequestOptions
//            options = if (placeholderDrawableId <= 0) {
//                RequestOptions().transform(CircleCrop())
//                    .priority(Priority.NORMAL)
//            } else {
//                RequestOptions().transform(CircleCrop())
//                    .placeholder(placeholderDrawableId)
//                    .error(placeholderDrawableId)
//                    .priority(Priority.NORMAL).diskCacheStrategy(DiskCacheStrategy.NONE)
//                    .skipMemoryCache(true)
//            }
//            Glide.with(context!!).load(url).apply(options).into(imageView!!)
//        }
//
//        fun loadImage(context: Context?, imageView: ImageView?, url: String?) {
//            if (imageView == null || isEmpty(url) || context == null) {
//                return
//            }
//            val options: RequestOptions = RequestOptions()
//                .priority(Priority.IMMEDIATE)
//                .placeholder(R.drawable.img_loading)
//                .error(R.drawable.img_loading)
//            Glide.with(context).load(url).apply(options).into(imageView)
//        }
//
//        fun loadImage(context: Context?, imageView: ImageView?, res: Int) {
//            if (imageView == null || res == 0 || context == null) {
//                return
//            }
//            val options: RequestOptions = RequestOptions()
//                .priority(Priority.IMMEDIATE)
//                .placeholder(R.drawable.img_loading)
//                .error(R.drawable.img_loading)
//            Glide.with(context).load(res).apply(options).into(imageView)
//        }

        fun getColoredString(mString: String?, colorId: Int): Spannable {
            val spannable: Spannable = SpannableString(mString)
            spannable.setSpan(
                ForegroundColorSpan(colorId),
                0,
                spannable.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return spannable
        }

        fun convertDpToPixel(dp: Float, context: Context): Float {
            return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }


        fun getColoredSpannable(spannable: Spannable, colorId: Int): Spannable {
            val mSpannable: Spannable = spannable
            mSpannable.setSpan(
                ForegroundColorSpan(colorId),
                0,
                mSpannable.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return mSpannable
        }

        fun getDateTime(strDate: String?): Date {
//        val strDate = "2013-05-15T10:00:00-0700"
            val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
            val dateFormat2 = SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.ENGLISH)
            val date = try {
                dateFormat.parse(strDate)
            } catch (e: Exception) {
                try {
                    dateFormat2.parse(strDate)
                } catch (e: Exception) {
                    Date()
                }
            }
            return date
        }

        fun getTimeInMilliSeconds(strDate: String?): Long {
            try {
                if (strDate.isNullOrEmpty()) {
                    return -1L
                }
//        val strDate = "2013-05-15T10:00:00-0700"
                val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH)
                return dateFormat.parse(strDate).time
            } catch (e: Exception) {
                e.printStackTrace()
                return 0L
            }
        }

        fun getDateString(context: Context, strDate: String?): String {
            try {
                val date = getDateTime(strDate)
                val currentDate = Date()
                val dateStr = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(date)
                val dateCurrent = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(currentDate)
                return if (dateStr.equals(dateCurrent)) "Hôm nay" else dateStr
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun getCurrentDateStringFormatServer(): String {
            val currentDate = Date()
            val dateCurrent =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).format(currentDate)
            return dateCurrent
        }

        fun getDateStringFormatServer(milliseconds: Long): String {
            val currentDate = Date(milliseconds)
            val dateCurrent =
                SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH).format(currentDate)
            return dateCurrent
        }

        fun getTimeString(strDate: String?): String {
            try {
                val date = getDateTime(strDate)
                return SimpleDateFormat("HH:mm", Locale.ENGLISH).format(date)
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun getTextHtml(str: String?): String {
            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    return Html.fromHtml(str, Html.FROM_HTML_MODE_LEGACY).toString()
                } else {
                    return Html.fromHtml(str).toString()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                return ""
            }
        }

        fun moneyFormat(amount: String?): String {
            if (amount.isNullOrEmpty()) {
                return "0 Đ"
            }
            val formatter = DecimalFormat("###,###,###")
            return try {
                formatter.format(amount.toDouble()) + " Đ"
            } catch (e: Exception) {
                e.printStackTrace()
                "0 Đ"
            }
        }

        fun changeColorHTMLString(string: String?, hexColorString: String?): String {
            if (string.isNullOrEmpty()) {
                return ""
            }

//            val str = "&lt;font color='$hexColorString'&gt;" + string + "&lt;/font&gt;"
            val str = "<font color='$hexColorString'>" + string + "</font>"
            return str
        }

        fun getHtmlFormat(text: String?): Spanned? {
            val data = text ?: ""
            val textSpanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(data, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(data)
            }
            return textSpanned
        }

        fun convertSecondsToMmSs(seconds: Long): String {
            val s = seconds % 60
            val m = seconds / 60 % 60
            return String.format("%02d:%02d", m, s)
        }

        fun isShowKeyboard(context: Context): Boolean {
            val imm by lazy { context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager }
            val windowHeightMethod =
                InputMethodManager::class.java.getMethod("getInputMethodWindowVisibleHeight")
            val height = windowHeightMethod.invoke(imm) as Int
            return height > 0
        }

        fun setSoundMute(activity: Activity?, isMute: Boolean = true) {
            if (activity == null) {
                return
            }
            val muteConst = if (isMute) AudioManager.ADJUST_MUTE else AudioManager.ADJUST_UNMUTE
            val audioManager: AudioManager =
                activity.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
            audioManager.adjustVolume(muteConst, AudioManager.FLAG_PLAY_SOUND)
        }

        fun setSound(activity: Activity?, percent: Int?) {
            if (activity == null || percent == null || percent < 0 || percent > 100) {
                return
            }
            val audioManager: AudioManager =
                activity.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
            val maxSound = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
            val soundProgress = percent * maxSound / 100
            audioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                soundProgress,
                AudioManager.FLAG_PLAY_SOUND
            )
        }

        fun getCurrentPercentSoundVoulume(activity: Activity?): Int? {
            if (activity == null) {
                return null
            }
            val audioManager: AudioManager =
                activity.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
            val maxSound = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC)
            var currentSound = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC)
            return (currentSound * 100 / maxSound)
        }

        var currentPercentSound: Int? = -1
        fun setSoundWhenListening(
            activity: Activity?,
            isListen: Boolean = false,
            percent: Int? = 10
        ) {
/*            if (activity == null || percent == null || percent < 0 || percent > 100) {
                return
            }
            if (isListen) {
                if (currentPercentSound == -1 || getCurrentPercentSoundVoulume(activity) != percent) {
                    currentPercentSound = getCurrentPercentSoundVoulume(activity)
                }
//                if (currentPercentSound != null && currentPercentSound!! <= percent) {
//                    return
//                }
                setSound(activity, percent)
            } else {
                if (currentPercentSound == -1) {
                    return
                }
                setSound(activity, currentPercentSound)
            }*/
        }

        @Suppress("DEPRECATION")
        fun muteRecognition(activity: Activity?, mute: Boolean) {
            if (activity == null) {
                return
            }
            val audioManager: AudioManager =
                activity.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
            audioManager?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val flag = if (mute) AudioManager.ADJUST_MUTE else AudioManager.ADJUST_UNMUTE
                    it.adjustStreamVolume(AudioManager.STREAM_NOTIFICATION, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_ALARM, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_MUSIC, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_RING, flag, 0)
//                it.adjustStreamVolume(AudioManager.STREAM_SYSTEM, flag, 0)
                } else {
                    it.setStreamMute(AudioManager.STREAM_NOTIFICATION, mute)
//                it.setStreamMute(AudioManager.STREAM_ALARM, mute)
//                it.setStreamMute(AudioManager.STREAM_MUSIC, mute)
//                it.setStreamMute(AudioManager.STREAM_RING, mute)
//                it.setStreamMute(AudioManager.STREAM_SYSTEM, mute)
                }
            }
        }

        fun getRandomString(arr: Array<String>?): String? {
            if (arr == null || arr.size == 0) {
                return null
            }
            val list = arr.toCollection(ArrayList())
            Collections.shuffle(list)
            return list.get(0)
        }

        fun loadImageCircle(
            context: Context?, imageView: ImageView?, url: String?,
            placeholderDrawableId: Int
        ) {
            val options: RequestOptions
            options = if (placeholderDrawableId <= 0) {
                RequestOptions().transform(CircleCrop())
                    .priority(Priority.NORMAL)
            } else {
                RequestOptions().transform(CircleCrop())
                    .placeholder(placeholderDrawableId)
                    .error(placeholderDrawableId)
                    .priority(Priority.NORMAL)
            }
            Glide.with(context!!).load(url).apply(options).into(imageView!!)
        }

        fun loadDrawableImage(context: Context?, drawableId: Int, imageView: ImageView?) {
            if (imageView == null) {
                return
            }
            val options = RequestOptions()
                .priority(Priority.HIGH)
            Glide.with(context!!).load(drawableId).apply(options).into(imageView)
        }

        fun getNavigationBarHeight(context: Context, orientation: Int): Int {
            val resources: Resources = context.resources
            val id: Int = resources.getIdentifier(
                if (orientation == Configuration.ORIENTATION_PORTRAIT) "navigation_bar_height" else "navigation_bar_height_landscape",
                "dimen", "android"
            )
            return if (id > 0) {
                resources.getDimensionPixelSize(id)
            } else 0
        }

        fun isListeningBackground(activity: Activity?): Boolean {
            if (activity == null) {
                return false
            }
            val sharedPreferences: SharedPreferences = activity.getSharedPreferences(
                "VoiceController",
                Context.MODE_PRIVATE
            )
            return sharedPreferences.getBoolean("VOICE_CONTINUOUS", true)
        }

        fun setListeningContinuousBackground(activity: Activity?, isSet: Boolean = false) {
            if (activity == null) {
                return
            }
            if (!isSet) {
                KontinuousRecognitionManager.destroy()
            }
            val layoutRootIconItv = activity.findViewById<View>(R.id.layoutRootIconItv)
            if (layoutRootIconItv != null && layoutRootIconItv.visibility == View.VISIBLE) {
                activity.findViewById<View>(R.id.textStatusVc).visibility =
                    if (isSet) View.VISIBLE else View.GONE
            }
            val res =
                if (isSet) R.drawable.ic_listen_background_on_vc else R.drawable.ic_listen_background_off_vc
            if (activity is FragmentActivity) {
                val buttonListener =
                    activity.supportFragmentManager.findFragmentByTag(VoiceControlFragment::class.java.simpleName)?.view?.findViewById<ImageView>(
                        R.id.buttonListenBackgroundVc
                    )
                buttonListener?.setImageResource(res)
                val textView =
                    activity.supportFragmentManager.findFragmentByTag(VoiceControlFragment::class.java.simpleName)?.view?.findViewById<TextView>(
                        R.id.textListenBackground
                    )
                val strColor = if (isSet) "#F91000" else "#ffffff"
                textView?.setTextColor(Color.parseColor(strColor))
            }
            val sharedPreferences: SharedPreferences =
                activity.getSharedPreferences("VoiceController", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putBoolean("VOICE_CONTINUOUS", isSet)
            editor.apply()
        }
        fun setListeningBackgroundCurrent(activity: Activity?) {
            setListeningContinuousBackground(activity, isListeningBackground(activity))
        }
        fun isListeningAuto(activity: Activity?): Boolean {
            if (activity == null) {
                return false
            }
            val sharedPreferences: SharedPreferences = activity.getSharedPreferences(
                "VoiceController",
                Context.MODE_PRIVATE
            )
            return sharedPreferences.getBoolean("VOICE_CONTINUOUS_FRAGMENT", true)
        }

        fun setListeningAuto(activity: Activity?, isSet: Boolean = false) {
            if (activity == null) {
                return
            }
            if (activity is FragmentActivity) {
                val res =
                    if (isSet) R.drawable.ic_listen_auto_on_vc else R.drawable.ic_listen_auto_off_vc
                val buttonListenerAuto =
                    activity.supportFragmentManager.findFragmentByTag(VoiceControlFragment::class.java.simpleName)?.view?.findViewById<ImageView>(
                        R.id.imgListenAutoVc
                    )
                buttonListenerAuto?.setImageResource(res)
                val textView =
                    activity.supportFragmentManager.findFragmentByTag(VoiceControlFragment::class.java.simpleName)?.view?.findViewById<TextView>(
                        R.id.textListenAuto
                    )
                val strColor = if (isSet) "#2CA700" else "#ffffff"
                textView?.setTextColor(Color.parseColor(strColor))
            }
            val sharedPreferences: SharedPreferences =
                activity.getSharedPreferences("VoiceController", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putBoolean("VOICE_CONTINUOUS_FRAGMENT", isSet)
            editor.apply()
        }
        fun setListeningAutoCurrent(activity: Activity?){
            setListeningAuto(activity, isListeningAuto(activity))
        }
        fun showDialog(
            activity: Activity?,
            title: String?,
            message: String?,
            leftButtonTitle: String?,
            rightButtonTitle: String?,
            onDialogButtonListener: OnDialogButtonListener?,
            cancelable: Boolean = true
        ): AlertDialog? {
            if (activity == null || activity.isFinishing || activity.isDestroyed || message.isNullOrEmpty()) {
                return null
            }
            val buidler = AlertDialog.Builder(activity)
            buidler.setCancelable(cancelable)
            title?.let { buidler.setTitle(it) }
            buidler.setMessage(message)
            rightButtonTitle?.let {
                buidler.setPositiveButton(rightButtonTitle,
                    object : DialogInterface.OnClickListener {
                        override fun onClick(p0: DialogInterface?, p1: Int) {
                            onDialogButtonListener?.onRightButtonClick()
                        }
                    })
            }
            leftButtonTitle?.let {
                buidler.setNegativeButton(leftButtonTitle,
                    object : DialogInterface.OnClickListener {
                        override fun onClick(p0: DialogInterface?, p1: Int) {
                            onDialogButtonListener?.onLeftButtonClick()
                        }
                    })
            }
            val dialog = buidler.create()
            dialog.show()
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
                .setTextColor(Color.parseColor("#0277BD"));
            dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setTextColor(Color.parseColor("#0277BD"));
            return dialog
        }

        fun showDialogAskListeningBackground(activity: Activity?) {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return
            }
            val isSet = isListeningBackground(activity)
            val msg =
                if (isSet) "Tính năng \" iTV Chạy ngầm\" giúp cho khi ẩn giao diện điều khiển bạn chỉ cần nói \"iTV\" thì giao diện điều khiển sẽ hiện lên. Bạn có muốn tắt tính năng này không?" else "Bạn có muốn bật tính năng sẵn sàng nghe lệnh khi ẩn giao diện điều khiển không?"
            val confirm = if (isSet) "Tắt" else "Bật"
            showDialog(
                activity,
                null,
                msg,
                "Không",
                confirm,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                    }

                    override fun onRightButtonClick() {
                        setListeningContinuousBackground(activity, !isSet)
                    }
                })
        }

        fun showDialogAskListeningFragment(activity: Activity?) {
            if (activity == null || activity.isFinishing || activity.isDestroyed) {
                return
            }
            val isSet = isListeningAuto(activity)
            val msg =
                if (isSet) "Bạn có muốn tắt tính năng tự động nghe dạng hỏi đáp không?" else "Bạn có muốn bật tính năng tự động nghe dạng hỏi đáp không?"
            val confirm = if (isSet) "Tắt" else "Bật"
            showDialog(
                activity,
                null,
                msg,
                "Không",
                confirm,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                    }

                    override fun onRightButtonClick() {
                        if (isSet) {
                            val diaglog = showDialog(
                                activity,
                                null,
                                "Để điều khiển bằng giọng nói vui lòng nhấn micro rồi ra lệnh",
                                "Ok",
                                null,
                                null
                            )
                            Handler(Looper.getMainLooper()).postDelayed(
                                Runnable { diaglog?.dismiss() }, 2000
                            )
                        }
                        setListeningAuto(activity, !isSet)

                    }
                })
        }

        fun saveString(context: Context?, string: String?, key: String?) {
            if (context == null) {
                return
            }
            val sharedPreferences = context.getSharedPreferences(
                "VC_CONFIG",
                Context.MODE_PRIVATE
            )
            val editor = sharedPreferences.edit()
            editor.putString(key, string)
            editor.apply()
        }

        fun getString(context: Context?, key: String?): String? {
            if (context == null) {
                return null
            }
            val sharedPreferences = context.getSharedPreferences(
                "VC_CONFIG",
                Context.MODE_PRIVATE
            )
            return sharedPreferences.getString(key, null)
        }

        fun saveInt(context: Context?, i: Int, key: String?) {
            if (context == null) {
                return
            }
            val sharedPreferences = context.getSharedPreferences(
                "VC_CONFIG",
                Context.MODE_PRIVATE
            )
            val editor = sharedPreferences.edit()
            editor.putInt(key, i)
            editor.apply()
        }

        fun getInt(context: Context?, key: String?): Int {
            if (context == null) {
                return 0
            }
            val sharedPreferences = context.getSharedPreferences(
                "VC_CONFIG",
                Context.MODE_PRIVATE
            )
            return sharedPreferences.getInt(key, 0)
        }

        fun getBoolean(context: Context?, key: String?): Boolean {
            if (context == null) {
                return false
            }
            val sharedPreferences = context.getSharedPreferences(
                "VC_CONFIG",
                Context.MODE_PRIVATE
            )
            return sharedPreferences.getBoolean(key, false)
        }

        fun isContain(context: Context?, key: String?): Boolean {
            if (context == null) {
                return false
            }
            val sharedPreferences = context.getSharedPreferences(
                "VC_CONFIG",
                Context.MODE_PRIVATE
            )
            return sharedPreferences.contains(key)
        }

        fun saveBoolean(context: Context?, value: Boolean, key: String?) {
            if (context == null) {
                return
            }
            val sharedPreferences = context.getSharedPreferences(
                "VC_CONFIG",
                Context.MODE_PRIVATE
            )
            val editor = sharedPreferences.edit()
            editor.putBoolean(key, value)
            editor.apply()
        }
    }

    interface OnDialogButtonListener {
        fun onLeftButtonClick()
        fun onRightButtonClick()
    }
}