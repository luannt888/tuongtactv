package vn.mediatech.voicecontrol.listener

interface OnClickListener {
    fun onClick()
    fun onLongClick()
}