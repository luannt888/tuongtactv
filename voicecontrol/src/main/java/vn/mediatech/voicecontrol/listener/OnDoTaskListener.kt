package vn.mediatech.voicecontrol.listener

interface OnDoTaskListener {
    fun onDoTask(contentId: String?, contentType: String?)
}