package vn.mediatech.voicecontrol.log

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemLog(
    @SerializedName("id") val id: Int?,
    @SerializedName("message") val message: String?,
    @SerializedName("type") var type: Int  //1:Itv, 2:user
) : Parcelable
/*show_date: "2021-05-18",
id: "525",
avatar: "https://graph.facebook.com/2999532883651035/picture?type=large",
full_name: "Lê Việt Hoàng"*/