package vn.mediatech.istudiocafe.htextview;

public class CharacterDiffResult {
    public char c;
    public int fromIndex;
    public int moveIndex;
}
