package vn.mediatech.istudiocafe.voucher.chatsupport

import android.content.Context
import android.os.*
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_chat_support.*
import okhttp3.Response
import vn.mediatech.interactive.app.*
import vn.mediatech.interactive.service.SocketMangager
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.SharedPreferencesManager
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.VoucherAdapter


class ChatSupportFragment : Fragment() {
    private var loadMoreAble: Boolean = true
    private var isLoadMore: Boolean = false
    private var page: Int = 1
    private var limit: Int = 20
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var itemObj: ItemNews? = null
    var myHttpRequest: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    var itemList: ArrayList<ItemConversation> = ArrayList()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_support, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
//        initSocket()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.DATA, itemObj)
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
//        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
//        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main, imageBgMain)

    }

    val SOLVED = "Đã xử lý"
    val SOLVING = "Đang xử lý"
    val NOT_SOLVE = "Chưa xử lý"
    val ALL = "Tất cả"
    private fun initMenuStatus() {
        val menu = PopupMenu(context, layoutStatusSolve)
        menu.getMenu().add(ALL)
        menu.getMenu().add(NOT_SOLVE)
        menu.getMenu().add(SOLVING)
        menu.getMenu().add(SOLVED)
        menu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem?): Boolean {
                textFilter.text = item!!.title
                if (item!!.title.equals(ALL)) {
                    getData()
                } else {
                    getDataStatus(item!!.title.toString())
                }
                return true
            }
        })
        menu.show()
    }

    fun initData() {
//        val bundle = savedInstanceState ?: arguments ?: return
//        itemObj = bundle.getParcelable(Constant.DATA) ?: return
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        getData()
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
                layoutLoadMore.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("page", "${page}")
        requestParams.put("limit", "${limit}")
//        val api = String.format(Locale.ENGLISH, Constant.API_USE_VOUCHER_HISTORY, itemUser!!.id, itemObj!!.id)
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_CHAT_CONVERSATION_HISTORY)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
//                Loggers.e("MyHttpRequest_url", api);
//                Loggers.e("MyHttpRequest_result", responseString);
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                        layoutLoadMore.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun getDataStatus(type: String? = NOT_SOLVE) {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        progressBar.visibility = View.VISIBLE
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        var status = "1"
        if (type.equals(NOT_SOLVE)) {
            status = "1"
        } else if (type.equals(SOLVING)) {
            status = "2"
        } else {
            status = "3"
        }
        requestParams.put("status", status)
//        val api = String.format(Locale.ENGLISH, Constant.API_USE_VOUCHER_HISTORY, itemUser!!.id, itemObj!!.id)
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_CHAT_CONVERSATION_HISTORY)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getString(jsonObject, Constant.RESULT)
        val dataStr = JsonParser.getString(jsonObject, Constant.DATA)
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemConversation>>() {}.getType()
        activity?.runOnUiThread {
//            viewDisableScroll.visibility =View.VISIBLE
            recyclerView?.getRecycledViewPool()?.clear()
        }
//        itemList.clear()
        var newList: ArrayList<ItemConversation> = ArrayList()
        try {
            val list: ArrayList<ItemConversation>? = gson.fromJson(resultObj, gsonType)
//            list!!.reverse()
            newList.addAll(list!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
//        itemObj = JsonParser.parseItemVoucher(dataObj)

//        itemList.addAll(newList!!)
        if (newList.size < limit) {
            loadMoreAble = false
        }
        activity?.runOnUiThread {
            try {
                if (adapter != null) {
                    recyclerView?.stopScroll()
                    if (!isLoadMore) {
                        itemList.clear()
                    }
                    itemList.addAll(newList)
//                    recyclerView?.getRecycledViewPool()?.clear();
                    adapter?.notifyDataSetChanged()
                } else {
                    itemList.clear()
                    itemList.addAll(newList)
                    initAdapter()
                }
                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            isLoadMore = false
        }
    }

    fun setData() {
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
        }
    }

    var adapter: ItemConversationAdapter? = null
    private fun initAdapter() {
        adapter = ItemConversationAdapter(requireContext(), itemList)
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
//        recyclerView.scheduleLayoutAnimation()
        adapter!!.onItemClickListener = object : ItemConversationAdapter.OnItemClickListener {
            override fun onClick(item: ItemConversation, position: Int) {
                if (activity is ChatSupportActivity) {
                    if (itemList.size > position) {
                        itemList.get(position).count = 0
                    }
                    adapter?.notifyDataSetChanged()
                    val fragment = ChatConversationFragment()
                    val bundle = Bundle()
                    bundle.putParcelable(Constant.DATA, item)
                    fragment.arguments = bundle
                    fragment.fragmentCallBackListenner = object : FragmentCallBackListenner {
                        override fun onResume() {
                        }

                        override fun onDetach() {
                            if (itemList.size > position) {
                                itemList.get(position).count = 0
                            }
                            adapter?.notifyDataSetChanged()
                            initSocket()
//                            getData()
                            MyApplication.getInstance().dataManager.isChat = true
                        }
                    }
                    (activity as ChatSupportActivity).showFragment(fragment)
                }
            }

            override fun onLongClick(item: ItemConversation, position: Int) {
                val fragment = VoucherDialogFragment()
                val bundle = Bundle()
                bundle.putParcelableArrayList(Constant.DATA, item.listVoucher)
                val height = ScreenSize(requireActivity()).height - layoutActionbar.height
//                bundle.putInt(Constant.HEIGHT, height)
                fragment.arguments = bundle
                fragment.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
                    override fun onClick(itemObj: ItemVoucher, position: Int) {
                        fragment?.dismiss()
                        val chatConversationFragment = ChatConversationFragment()
                        val bundle = Bundle()
                        val itemConversation = item
                        itemConversation.voucherId = if (itemObj.id != null) itemObj.id.toString() else return
                        itemConversation.uniqueId = if (itemObj.uniqueId != null) itemObj.uniqueId.toString() else return
                        itemConversation.voucherName = if (itemObj.name != null) itemObj.name.toString() else ""
                        bundle.putParcelable(Constant.DATA, item)
                        chatConversationFragment.arguments = bundle
                        chatConversationFragment.fragmentCallBackListenner = object : FragmentCallBackListenner {
                            override fun onResume() {
                            }

                            override fun onDetach() {
                                initSocket()
                                getData()
                                MyApplication.getInstance().dataManager.isChat = true
                            }
                        }
                        (activity as ChatSupportActivity).showFragment(chatConversationFragment)
                    }

                }
                fragment.show(fragmentManager!!, "Voucher")
            }
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView2: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView2, dx, dy)
                if (isLoading || !loadMoreAble) {
//                    Loggers.e("VoucherFrg", "addOnScrollListener onScrolled_stopGet")
                    return
                }
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPosition()
                layoutRefresh.isEnabled = firstPos == 0

                val visibleItemCount = layoutManager.childCount
                val firstVisible = layoutManager.findFirstVisibleItemPosition()
                val totalItem = layoutManager.itemCount
                if (visibleItemCount + firstVisible >= totalItem - 4) {
                    isLoadMore = true
                    page++
                    layoutLoadMore.visibility = View.VISIBLE
//                    Loggers.e("VoucherFrg", "addOnScrollListener onScrolled_getData")
                    getData()
                }
            }
        })
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonBack.setOnClickListener {
            activity?.onBackPressed()
        }
        editSearch.setOnClickListener {
            var fragment = SearchSupportFragment()
            (activity as? ChatSupportActivity)?.showFragment(fragment)
        }
        layoutStatusSolve.setOnClickListener {
            initMenuStatus()
        }
        layoutRefresh.setOnRefreshListener {
            refreshData()
        }
    }

    private fun refreshData() {
        page = 1
        loadMoreAble = true
        isLoadMore = false
        adapter?.notifyDataSetChanged()
        progressBar.visibility = View.VISIBLE
        layoutRefresh.isRefreshing = false
        getData()
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        mSocketMangager?.close()
        super.onDestroyView()
    }

    private var port: String? = ""
    private val PORT: String = "wss://api.daugiatruyenhinh.com:8088/interactive?" // debug
    private var mSocketMangager: SocketMangager? = null
    private fun initSocket() {
        mSocketMangager = SocketMangager.getInstance()
        var listenner = (object : SocketMangager.MySocketListenner {
            override fun onConnected(response: Response) {
                Loggers.e("Socket", "onConnected")
            }

            override fun onMessage(string: String) {
                autoConnectCount = 0
                val data = EncryptUtil.base64Decode(string)
//                data = resume
                Loggers.e("Socket", "onMessage :" + data)
                if (data.isNullOrEmpty()) {
                    return
                }
                activity?.runOnUiThread {
                    try {
                        handleMessage(data)
                    } catch (e: Exception) {
                    }
                }
            }

            override fun onClosed(code: Int, reason: String) {
                Loggers.e("Socket", "onClosed" + reason)
                if (code == InteractiveConstant.CLOSE_CONNECT) {
                    return
                }
            }

            override fun onClosing(code: Int, reason: String) {
                Loggers.e("Socket", "onClosing" + reason)
                if (code == InteractiveConstant.CLOSE_CONNECT) {
                    return
                }
                tryConnect()
            }

            override fun onFailure(t: Throwable, response: Response?) {
                Loggers.e("Socket", "onFailure:" + t.message + " --- " + (if (response != null) (response.body.toString()) else ""))
                tryConnect()
            }
        })
//        mSocketMangager?.addSocketlistenner(ChatSupportFragment::class.java.canonicalName, listenner)
        mSocketMangager?.listenner = listenner
        val item = ServiceUtilTT.itemInteractiveConfig
        if (SharedPreferencesManager.isTest(context) && !MyApplication.getInstance().isEmpty(SharedPreferencesManager.getPortChat(context))) {
            port = SharedPreferencesManager.getPortChat(context).trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
        } else if (item != null && !item.portSupport.isNullOrEmpty()) {
            port = item.portSupport.trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
        } else {
//            port = mSocketMangager!!.url
//            if (port == null) {
//                port = PORT.trim() + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
//            }
        }
        if (!port.isNullOrEmpty()) {
            mSocketMangager?.run(port)
        }
    }

    var autoConnectCount: Int = 0
    private fun tryConnect() {
        if (autoConnectCount >= 0) {
            activity?.runOnUiThread {
                AppUtils.showDialog(activity, false, false, null, "Không thể kết nối đến máy chủ. Vui lòng kiểm tra và kết nối lại!", "Huỷ", "Kết nối", object : AppUtils.OnDialogButtonListener {
                    override fun onLeftButtonClick() {
//                        activity?.finish()
                    }

                    override fun onRightButtonClick() {
                        mSocketMangager?.reconnect()
                    }
                })
            }
        } else {
            autoConnectCount++
            mSocketMangager?.reconnect()
        }
    }

    val TYPE_ADMIN = 1
    val TYPE_USER = 0
    val TYPE_IS_TYPING = 2
    val TYPE_CANCEL_TYPING = 3
    private fun handleMessage(message: String) {
        val json = JsonParserUtil.getJsonObject(message)
        Loggers.e("Socket", "onMessageData :" + JsonParserUtil.getString(json, "data"))
//         json = JsonParserUtil.getJsonObject(startGame)
        if (json == null) {
            return
        }
        val service = JsonParserUtil.getString(json, InteractiveConstant.SERVICE)
        when (service) {
            InteractiveConstant.SERVICE_REGISTER -> {
            }
            InteractiveConstant.SERVICE_CHAT -> {
                val jsonMessageString = JsonParserUtil.getString(json, "message")
                val jsonMessage = JsonParserUtil.getJsonObject(jsonMessageString)
                val message = JsonParserUtil.getString(jsonMessage, "content")
                val imageUrl = JsonParserUtil.getString(jsonMessage, "image")
                val voucherId = JsonParserUtil.getString(jsonMessage, "voucherid")
                val uniqueId = JsonParserUtil.getString(jsonMessage, "unique_id")
                val voucherName = JsonParserUtil.getString(jsonMessage, "voucher_name")
                val cmd = JsonParserUtil.getString(json, "cmd")
                val jsonfrom = JsonParserUtil.getJsonObject(json, "from")
                val idfrom = JsonParserUtil.getString(jsonfrom, "ID")
                if (idfrom!!.equals(itemUser!!.id.toString())) {
                    return
                }
                val type = JsonParserUtil.getInt(jsonMessage, "type")
                if (type == TYPE_IS_TYPING || type == TYPE_CANCEL_TYPING) {
                    return
                }
                var isOldConversation = false
                var idCheck = cmd
                if ("admin".equals(cmd)) {
                    idCheck = idfrom
                }
                for (item in itemList) {
                    if (item.userId.equals(idCheck)) {
                        isOldConversation = true
                        if (item.count == null) {
                            item.count = 1
                        } else {
                            item.count = item.count!! + 1
                        }
                        if (!imageUrl.isNullOrEmpty()) {
                            item.message = "[Ảnh]"
                        }
                        if (!voucherId.isNullOrEmpty()) {
                            item.voucherId = voucherId
                        }
                        if (!uniqueId.isNullOrEmpty()) {
                            item.uniqueId = uniqueId
                        }
                        if (!voucherName.isNullOrEmpty()) {
                            item.voucherName = voucherName
                        }
                        if (!message.isNullOrEmpty()) {
                            item.message = message
                        }
                        itemList.remove(item)
                        itemList.add(0, item)
                        break
                    }
                }
                playVivration()
                if (!isDetached) {
                    if (isOldConversation) {
                        adapter!!.notifyDataSetChanged()
                    } else {
                        Handler(Looper.getMainLooper()).postDelayed({
                            getData()
                        }, 200)
                    }
                }
//                var type = TYPE_USER
//                if (!idfrom.equals(itemObj!!.userId)) {
//                    type = TYPE_ADMIN
//                }
//                val userName = itemUser!!.fullname ?: itemUser!!.phone
//                val itemVoucherChat = ItemChat("", itemObj!!.id, itemUser!!.id.toString(), userName, itemUser!!.avatar, "0", type, message, imageUrl, TextUtil.getCurrentDateString())
//                addItemChat(itemVoucherChat)
            }
        }
    }


    fun playVivration() {
        if (isDetached) {
            return
        }
        val v = requireContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
// Vibrate for 500 milliseconds
// Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v?.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            //deprecated in API 26
            v?.vibrate(500)
        }
    }

    override fun onResume() {
        super.onResume()
        MyApplication.getInstance().dataManager.isChat = true
        if (mSocketMangager == null || (mSocketMangager != null && !mSocketMangager!!.isConnecting) || !port.equals(mSocketMangager!!.url)) {
            initSocket()
        }
    }

    override fun onPause() {
        super.onPause()
        MyApplication.getInstance().dataManager.isChat = false
    }

    var isOnScreen = false

}