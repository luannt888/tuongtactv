package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_voucher_market.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import java.util.*

class VoucherMarketFragment : Fragment() {
    var isViewCreated: Boolean = false
    var voucherFragment: VoucherFragment? = null
    var voucherType: Int = 0
    var savedInstanceState: Bundle? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_voucher_market, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(Constant.VOUCHER_TYPE, voucherType)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        voucherType = bundle.getInt(Constant.VOUCHER_TYPE, Constant.VOUCHER_STORE)
        when (voucherType) {
            Constant.VOUCHER_STORE -> {
                layoutActionbarVoucherSearch.visibility = View.VISIBLE
                textAllVoucher.visibility = View.VISIBLE
                textActionbarTitle.visibility = View.GONE
            }
            Constant.MY_VOUCHER -> {
                layoutActionbarVoucherSearch.visibility = View.INVISIBLE
                textAllVoucher.visibility = View.GONE
                textActionbarTitle.visibility = View.VISIBLE
                val lp = tabLayout.layoutParams as LinearLayout.LayoutParams
                lp.topMargin = activity!!.resources.getDimensionPixelSize(R.dimen.dimen_15)
            }
        }
        initTab()
    }

    fun initTab() {
        when (voucherType) {
            Constant.VOUCHER_STORE -> {
                val tabOffer = tabLayout.newTab()
                tabOffer.text = getString(R.string.tab_offer)
                tabOffer.tag = Constant.VOUCHER_TYPE_OFFER
                tabLayout.addTab(tabOffer)

                val tabNewest = tabLayout.newTab()
                tabNewest.text = getString(R.string.tab_newest)
                tabNewest.tag = Constant.VOUCHER_STORE_TAB_NEWEST
                tabLayout.addTab(tabNewest)

                val tabPrice = tabLayout.newTab()
                tabPrice.text = getString(R.string.tab_price)
                tabPrice.tag = Constant.VOUCHER_STORE_TAB_PRICE
                tabLayout.addTab(tabPrice)
            }
            Constant.MY_VOUCHER -> {
                val tabNewest = tabLayout.newTab()
                tabNewest.text = getString(R.string.tab_newest)
                tabNewest.tag = Constant.MY_VOUCHER_TAB_NEWEST
                tabLayout.addTab(tabNewest)

                val tabPrice = tabLayout.newTab()
                tabPrice.text = getString(R.string.tab_price)
                tabPrice.tag = Constant.MY_VOUCHER_TAB_PRICE
                tabLayout.addTab(tabPrice)

                val tabUsed = tabLayout.newTab()
                tabUsed.text = getString(R.string.tab_used)
                tabUsed.tag = Constant.MY_VOUCHER_TAB_USED
                tabLayout.addTab(tabUsed)

                val tabDonated = tabLayout.newTab()
                tabDonated.text = getString(R.string.tab_donated)
                tabDonated.tag = Constant.MY_VOUCHER_TAB_DONATED
                tabLayout.addTab(tabDonated)
            }
        }

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab == null) {
                    return
                }
                handleOnPageSelected(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val rigtMargin = resources.getDimensionPixelSize(R.dimen.dimen_3)
            val leftMargin = if (i == 0) resources.getDimensionPixelSize(R.dimen.dimen_10) else rigtMargin
            p.setMargins(leftMargin, 0, rigtMargin, 0)
            tab.layoutParams = p
            tab.requestLayout()
        }

        handleOnPageSelected(0)
    }

    fun handleOnPageSelected(position: Int) {
        val tab = tabLayout.getTabAt(position)
        if (tab == null || tab.tag == null) {
            return
        }
        val tabTag = tab.tag as Int
        if (voucherFragment != null) {
//            voucherFragment!!.refreshData(tabTag)
            return
        }
        val bundle = Bundle()
        bundle.putInt(Constant.TYPE, tabTag)
        bundle.putInt(Constant.VOUCHER_TYPE, voucherType)
        voucherFragment = VoucherFragment()
        voucherFragment!!.arguments = bundle
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, voucherFragment!!)
        fragmentTransaction.commit()
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(voucherType == Constant.VOUCHER_TYPE_OFFER)
            }
        }
        editSearch.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).showSearchVoucherFragment()
            }
        }
        buttonMyVoucher.setOnClickListener {
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser == null) {
                if (activity is VoucherActivity) {
                    (activity as VoucherActivity).showLoginFragment()
                }
                return@setOnClickListener
            }
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).showMyVoucherFragment()
            }
        }
    }

}