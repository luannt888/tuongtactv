package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.fragment_detail_brand.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import java.util.*

class DetailBrandFragment : Fragment() {
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    lateinit var itemBrand: ItemBrand
    var brandId: Int = -1
    var brandName: String = ""

    var myHttpRequest: MyHttpRequest? = null
    var myHttpRequestRelate: MyHttpRequest? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail_brand, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(Constant.ID, brandId)
            putString(Constant.NAME, brandName)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        brandId = bundle.getInt(Constant.ID, -1)
        if (brandId == -1) {
            return
        }
        brandName = bundle.getString(Constant.NAME, "")
        textActionbarTitle.text = brandName
        getData()
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
                layoutContent.visibility = View.GONE
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
         if (isLoading|| isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("id", "$brandId")
        ServiceManager.getBrandDetail(activity!!, requestParams, object : ServiceManager.OnBrandDetailRequestListener {
            override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                this@DetailBrandFragment.myHttpRequest = myHttpRequest
            }

            override fun onResponse(isSuccess: Boolean, message: String?, itemObj: ItemBrand?) {
                if (isDetached) {
                    return
                }
                isLoading = false
                if (!isSuccess) {
                    activity?.runOnUiThread {
                        showErrorNetwork(message)
                    }
                    return
                }
                if (itemObj == null) {
                    activity?.runOnUiThread {
                        showErrorNetwork(activity!!.resources.getString(R.string.msg_empty_data))
                    }
                    return
                }
                isLoading = false
                activity?.runOnUiThread {
                    progressBar.visibility = View.GONE
                }
                itemBrand = itemObj
                activity!!.runOnUiThread {
                    try {
                        setData()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                getRelateData()
            }
        })
    }

    fun setData() {
        val imageHeight = ScreenSize(activity!!).width * 5 / 8
        val lp = imageThumbnail.layoutParams as LinearLayout.LayoutParams
        lp.height = imageHeight
        MyApplication.getInstance().loadImage(activity!!, imageThumbnail, itemBrand.image)
        MyApplication.getInstance().loadImage(activity!!, imageBrand, itemBrand.logo, true)
        MyApplication.getInstance().loadImage(activity!!, imageMap, itemBrand.map)
        textBrandName.text = itemBrand.name ?: ""
        textBrandPlace.text = itemBrand.location
        TextUtil.setHtmlTextView(itemBrand.description, textBrandDescription)

        textNotify.visibility = View.GONE
        layoutContent.visibility = View.VISIBLE
        progressBar.visibility = View.GONE
    }

    fun getRelateData() {
        val requestParams = RequestParams()
        requestParams.put("brandid", "$brandId")
        ServiceManager.getVoucherList(activity!!, ServiceUtilTT.validAPIDGTH(context,Constant.API_VOUCHER_STORE), requestParams, object : ServiceManager.OnVoucherRequestListener {
            override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                this@DetailBrandFragment.myHttpRequestRelate = myHttpRequest
            }

            override fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemVoucher>?) {
                if (isDetached) {
                    return
                }
                if (!isSuccess) {
                    activity?.runOnUiThread {
                        recyclerViewRelate.visibility = View.GONE
                        progressBarRelate.visibility = View.GONE
                        textNotifyRelate.visibility = View.VISIBLE
                    }
                    return
                }
                if (itemList == null || itemList.size == 0) {
                    activity?.runOnUiThread {
                        layoutEndow.visibility = View.GONE
                    }
                    return
                }
                try {
                    setRelateData(itemList)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    fun setRelateData(itemList: ArrayList<ItemVoucher>) {
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val numberColumn = 1
        val itemDecoration = SpacesItemDecoration(spacePx, numberColumn, false)
        val layoutManager = StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
        val adapter = VoucherAdapter(activity!!, itemList, VoucherAdapter.STYLE_LIST_STRING, numberColumn)
        activity?.runOnUiThread {
            adapter.showLayoutUser = true
            recyclerViewRelate.visibility = View.VISIBLE
            progressBarRelate.visibility = View.GONE
            if (recyclerViewRelate.itemDecorationCount == 0) {
                recyclerViewRelate.addItemDecoration(itemDecoration)
            }
            recyclerViewRelate.layoutManager = layoutManager
            recyclerViewRelate.adapter = adapter
            recyclerViewRelate.scheduleLayoutAnimation()

            adapter.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
                override fun onClick(itemObj: ItemVoucher, position: Int) {
                    if (activity is VoucherActivity) {
                        val bundle = Bundle()
                        bundle.putParcelable(Constant.DATA, itemObj)
                        (activity as VoucherActivity).showDetailVoucherFragment(bundle, null)
                    }
                }
            }
        }
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(false)
            }
        }
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        textNotifyRelate.setOnClickListener {
            textNotifyRelate.visibility = View.GONE
            progressBarRelate.visibility = View.VISIBLE
            getRelateData()
        }
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        myHttpRequestRelate?.cancel()
        super.onDestroyView()
    }
}