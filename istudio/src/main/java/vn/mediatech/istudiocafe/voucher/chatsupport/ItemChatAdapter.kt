package vn.mediatech.istudiocafe.voucher.chatsupport

import android.app.Activity
import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.item_chat.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.fragment.ImageZoomPagerDialog
import vn.mediatech.istudiocafe.model.ItemPhoto
import vn.mediatech.istudiocafe.util.ScreenSize

class ItemChatAdapter(val context: Context, val list: ArrayList<ItemChat>, val isAdmin: Boolean) : Adapter<ItemChatAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_chat,
                parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: ItemChat = list.get(position)
        if (item.content.isNullOrEmpty() && item.image.isNullOrEmpty()) {
            return
        }
        if (item.createdAt.isNullOrEmpty()) {
            holder.textDate.visibility = View.GONE
        } else {
            holder.textDate.text = item.createdAt
            holder.textDate.visibility = View.VISIBLE
        }
        if (item.content.isNullOrEmpty()) {
            holder.textMessage.visibility = View.GONE
        } else {
            holder.textMessage.text = item.content
            holder.textMessage.visibility = View.VISIBLE
        }

//        val TYPE_ADMIN = 1
//        val TYPE_USER = 0

        if ((isAdmin && item.type!! == 1) || (!isAdmin && item.type!! != 1)) {
            holder.layoutContent.background = ContextCompat.getDrawable(context, R.drawable.bg_layout_voucher_chat_right)
            holder.layoutRoot.gravity = Gravity.END
            holder.layoutContent.gravity = Gravity.END
//            val lp = holder.layoutContent.layoutParams as LinearLayout.LayoutParams
//            lp.leftMargin = context.resources.getDimensionPixelSize(R.dimen.dimen_80)
//            holder.layoutContent.layoutParams = lp
            holder.layoutRoot.setPadding(context.resources.getDimensionPixelSize(R.dimen.dimen_50), holder.layoutContent.paddingTop, holder.layoutContent.paddingRight, holder.layoutContent.paddingBottom)

        } else {
            holder.layoutContent.background = ContextCompat.getDrawable(context, R.drawable.bg_item_chat_left)
            holder.layoutRoot.gravity = Gravity.START
            holder.layoutContent.gravity = Gravity.START
//            val lp = holder.layoutContent.layoutParams as LinearLayout.LayoutParams
//            lp.rightMargin = context.resources.getDimensionPixelSize(R.dimen.dimen_80)
//            holder.layoutContent.layoutParams = lp
            holder.layoutRoot.setPadding(holder.layoutContent.paddingLeft, holder.layoutContent.paddingTop, context.resources.getDimensionPixelSize(R.dimen.dimen_50), holder.layoutContent.paddingBottom)

        }
        if (item.image.isNullOrEmpty()) {
            holder.imageThumbnail.visibility = View.GONE
        } else {
            holder.imageThumbnail.visibility = View.VISIBLE
//            MyApplication.getInstance().loadImageNoPlaceHolder(context, holder.imageThumbnail, item.image)
            MyApplication.getInstance().loadImageNoPlaceHolderChat(context, item.image, object : CustomTarget<Drawable?>() {
                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable?>?) {
                    val screenSize = ScreenSize(context as Activity)
                    val lp: ViewGroup.LayoutParams = holder.imageThumbnail.layoutParams
                    if (resource is BitmapDrawable) {
                        val bitmap = resource.bitmap
                        if (bitmap.height >= bitmap.width) {
                            lp.width = screenSize.width * 1 / 2
                        } else {
                            lp.width = screenSize.width * 4 / 5
                        }
                        lp.height = lp.width * bitmap.height / bitmap.width
                        holder.imageThumbnail.layoutParams = lp
                        holder.imageThumbnail.setImageDrawable(resource)
                    }
                    onItemClickListener?.onLoadImgFinish()
//                    holder.imageThumbnail.setImageDrawable(resource)
                }

                override fun onLoadCleared(placeholder: Drawable?) {
                }
            })
            holder.imageThumbnail.setOnClickListener {
                val fragment = ImageZoomPagerDialog()
                val list = ArrayList<ItemPhoto>()
                list.add(ItemPhoto(item.userName, item.image, item.createdAt))
                val bundle = Bundle()
                bundle.putParcelableArrayList(Constant.DATA, list)
                fragment.arguments = bundle
                fragment.show((context as? FragmentActivity)?.supportFragmentManager!!, "ImageZoom")
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {

        val layoutRoot = itemView.layoutRoot
        val layoutContent = itemView.layoutContent
        val textTime = itemView.imageThumbnail
        val textMessage = itemView.textMessage
        val textDate = itemView.textDate
        val imageThumbnail = itemView.imageThumbnail
    }

    interface OnItemClickListener {
        fun onClick(item: ItemConversation, position: Int)
        fun onLoadImgFinish()
    }

}