package vn.mediatech.istudiocafe.voucher.chatsupport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_chat_support_search.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.VoucherAdapter

class SearchSupportFragment : Fragment() {
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var itemObj: ItemNews? = null
    var myHttpRequest: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    var itemList: ArrayList<ItemConversation> = ArrayList()
    var keyword: String? = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chat_support_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.DATA, itemObj)
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
//        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
//        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main, imageBgMain)
    }

    fun initData() {
//        val bundle = savedInstanceState ?: arguments ?: return
//        itemObj = bundle.getParcelable(Constant.DATA) ?: return
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        if (itemUser == null) {
            activity?.onBackPressed()
            return

        }
        itemList = ArrayList()
//        for(i in 0 until 10){
//            val item = ItemConversation("" + i,"6","86", "Nguyen Van "+ i, "https://api.daugiatruyenhinh.com/media/daugiatruyenhinh.jpg","","","Xin chao toi la Nguyen Van " + i + ", Toi muon su dung voucher", "","Ngay 08 thang 03" )
//            itemList.add(item)
//        }
//        initAdapter()

    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun search() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
//        val api = String.format(Locale.ENGLISH, Constant.API_USE_VOUCHER_HISTORY, itemUser!!.id, itemObj!!.id)
        val api = ServiceUtilTT.validAPIDGTH(context,Constant.API_CHAT_SEARCH)
        val requestParams = RequestParams()
        requestParams.put("userid", itemUser!!.id.toString())
        requestParams.put("keyword", keyword)
        myHttpRequest!!.request(true, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getString(jsonObject, Constant.RESULT)
//        val dataStr = JsonParser.getString(jsonObject, Constant.DATA)
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemConversation>>() {}.getType()
        try {
            itemList = gson.fromJson(resultObj, gsonType)
        } catch (e: Exception) {
            e.printStackTrace()
            itemList = ArrayList()
        }
        if (itemList.size == 0) {
            activity?.runOnUiThread {
                textNotify.visibility = View.VISIBLE
            }
            return
        }
//        itemObj = JsonParser.parseItemVoucher(dataObj)
        activity?.runOnUiThread {
            try {
                initAdapter()
//                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    var isFirsAddLayoutChat = true
    fun setData() {
        if (isFirsAddLayoutChat) {
            isFirsAddLayoutChat = false
        }
    }

    private fun initAdapter() {
        var adapter = ItemConversationAdapter(requireContext(), itemList)
        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.scheduleLayoutAnimation()
        adapter.onItemClickListener = object : ItemConversationAdapter.OnItemClickListener {
            override fun onClick(item: ItemConversation, position: Int) {
                if (activity is ChatSupportActivity) {
                    val fragment = ChatConversationFragment()
                    val bundle = Bundle()
                    bundle.putParcelable(Constant.DATA, item)
                    fragment.arguments = bundle
                    (activity as ChatSupportActivity).showFragment(fragment)
                }
            }

            override fun onLongClick(item: ItemConversation, position: Int) {
                val fragment = VoucherDialogFragment()
                val bundle = Bundle()
                bundle.putParcelableArrayList(Constant.DATA, item.listVoucher)
                val height = ScreenSize(requireActivity()).height - layoutActionbar.height
                bundle.putInt(Constant.HEIGHT, height)
                fragment.arguments = bundle
                fragment.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
                    override fun onClick(itemObj: ItemVoucher, position: Int) {
                        val fragment = ChatConversationFragment()
                        val bundle = Bundle()
                        val itemConversation = item
                        itemConversation.voucherId = if (itemObj.id != null) itemObj.id.toString() else return
                        itemConversation.uniqueId = if (itemObj.uniqueId != null) itemObj.id.toString() else return
                        itemConversation.voucherName = if (itemObj.name != null) itemObj.id.toString() else ""
                        bundle.putParcelable(Constant.DATA, item)
                        fragment.arguments = bundle
                        fragment.fragmentCallBackListenner = object : FragmentCallBackListenner {
                            override fun onResume() {
                            }

                            override fun onDetach() {
                            }
                        }
                        (activity as ChatSupportActivity).showFragment(fragment)
                    }

                }
                fragment.show(fragmentManager!!, "Voucher")
            }

        }
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }


    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            search()
        }
        buttonBack.setOnClickListener {
            (activity as? BaseActivity)?.hideKeyboard(editSearch)
            activity?.onBackPressed()
        }
        editSearch.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                (activity as? BaseActivity)?.hideKeyboard(editSearch)
                submitSend()
                true
            } else false
        }
    }

    private fun submitSend() {
        keyword = editSearch.text.toString().trim()
        progressBar.visibility = View.VISIBLE
        search()
    }


    override fun onDestroyView() {
        myHttpRequest?.cancel()

        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
    }
}