package vn.mediatech.istudiocafe.voucher

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemBrand(
        @SerializedName("id") var id: Int?,
        @SerializedName("name") var name: String?,
        @SerializedName("description") var description: String?,
        @SerializedName("brand_image") var image: String?,
        @SerializedName("brand_logo") var logo: String?,
        @SerializedName("longitude") var longitude: String?,
        @SerializedName("latitude") var latitude: String?,
        @SerializedName("map") var map: String?,
        @SerializedName("location") var location: String?
) : Parcelable