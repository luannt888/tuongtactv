package vn.mediatech.istudiocafe.voucher.chatsupport

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemChat(@SerializedName("id") val id: String?,
                    @SerializedName("voucherid") val voucherId: String?,
                    @SerializedName("unique_id") var uniqueId: String?,
                    @SerializedName("userid") val userId: String?,
                    @SerializedName("user_name") val userName: String?,
                    @SerializedName("user_avatar") val userAvatar: String?,
                    @SerializedName("status") val status: String?,
                    @SerializedName("type") val type: Int?,
                    @SerializedName("content") val content: String?,
                    @SerializedName("image") val image: String?,
                    @SerializedName("created_at") val createdAt: String?) : Parcelable


