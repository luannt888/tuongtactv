package vn.mediatech.istudiocafe.voucher

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemBaseCategory(
        @SerializedName("id") var id: Int,
        @SerializedName("name") var name: String?,
        @SerializedName("icon") var image: String?,
        @SerializedName("style") var style: String,
        @SerializedName("isChecked") var isChecked: Boolean
) : Parcelable