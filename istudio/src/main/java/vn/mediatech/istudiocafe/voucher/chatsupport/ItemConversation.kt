package vn.mediatech.istudiocafe.voucher.chatsupport

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import vn.mediatech.istudiocafe.voucher.ItemVoucher

@Parcelize
//<<<<<<< HEAD
class ItemConversation(@SerializedName("ID") val id: String?,
                       @SerializedName("voucherid") var voucherId: String?,
                       @SerializedName("userid") val userId: String?,
                       @SerializedName("unique_id") var uniqueId: String?,
                       @SerializedName("fullname") val userName: String?,
                       @SerializedName("avatar") val userAvatar: String?,
//                        @SerializedName("status") val status: String?,
//                        @SerializedName("type") val type: String?,
                       @SerializedName("mess") var message: String?,
                       @SerializedName("image") val image: String?,
                       @SerializedName("date") val date: String?,
                       @SerializedName("voucher_name") var voucherName: String?,
                       @SerializedName("count") var count: Int?,
                       @SerializedName("list_voucher") var listVoucher: ArrayList<ItemVoucher>?) : Parcelable
//=======
//class ItemConversation(
//        @SerializedName("ID") val id: String?,
//        @SerializedName("unique_id") var uniqueId: Int?,
//        @SerializedName("voucherid") val voucherId: String?,
//        @SerializedName("userid") val userId: String?,
//        @SerializedName("fullname") val userName: String?,
//        @SerializedName("avatar") val userAvatar: String?,
//        @SerializedName("status") val status: String?,
//        @SerializedName("type") val type: String?,
//        @SerializedName("mess") var message: String?,
//        @SerializedName("image") val image: String?,
//        @SerializedName("date") val date: String?,
//        @SerializedName("count") var count: Int?
//) : Parcelable
//>>>>>>> master_interactive
