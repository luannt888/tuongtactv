package vn.mediatech.istudiocafe.voucher

import android.app.Activity
import android.graphics.Outline
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.item_checkbox.view.*
import kotlinx.android.synthetic.main.item_radiobutton.view.*
import vn.mediatech.istudiocafe.R
import java.util.*

class CheckboxAdapter(val activity: Activity, val itemList: ArrayList<ItemBaseCategory>, val style: Int) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    companion object {
        val STYLE_CHECKBOX = 1
        val STYLE_RADIO_BUTTON = 2
    }

    var onItemClickListener: OnItemClickListener? = null

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        return style
    }

    fun initViewSize(viewType: Int) {
    }

    fun setFullSpan(view: View, isFullSpan: Boolean) {
        if (view.layoutParams is StaggeredGridLayoutManager.LayoutParams) {
            (view.layoutParams as StaggeredGridLayoutManager.LayoutParams).isFullSpan = isFullSpan
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        initViewSize(viewType)
        if (viewType == STYLE_CHECKBOX) {
            val view = LayoutInflater.from(activity).inflate(R.layout.item_checkbox,
                    parent, false)
            setFullSpan(view, false)
            return CheckboxViewHolder(view)
        } else {
            val view = LayoutInflater.from(activity).inflate(R.layout.item_radiobutton,
                    parent, false)
            setFullSpan(view, false)
            return RadioButtonViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val itemObj: ItemBaseCategory = itemList.get(position)
        if (holder is CheckboxViewHolder) {
            bindDataCheckboxView(holder, itemObj, position)
        } else if (holder is RadioButtonViewHolder) {
            bindDataRadioButtonView(holder, itemObj, position)
        }
    }

    fun bindDataCheckboxView(holder: CheckboxViewHolder, itemObj: ItemBaseCategory, position: Int) {
        holder.checkbox.text = itemObj.name
        holder.checkbox.isChecked = itemObj.isChecked
    }

    fun bindDataRadioButtonView(holder: RadioButtonViewHolder, itemObj: ItemBaseCategory, position: Int) {
        holder.checkbox.text = itemObj.name
        holder.checkbox.isChecked = itemObj.isChecked
    }

    inner class CheckboxViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkbox = itemView.checkbox

        init {
            setRound(itemView)
            checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
                val itemObj = itemList.get(adapterPosition)
                itemObj.isChecked = isChecked
                onItemClickListener?.onClick(itemObj, adapterPosition)
            }
        }
    }


    inner class RadioButtonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkbox = itemView.radioButton

        init {
            setRound(itemView)
            checkbox.setOnClickListener {
                onItemClickListener?.onClick(itemList.get(adapterPosition), adapterPosition)
            }
        }
    }

    fun setRound(view: View) {
        view.setOutlineProvider(object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 1f * activity.resources.getDimensionPixelSize(R.dimen.dimen_5)
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        })
        view.setClipToOutline(true)
    }

    interface OnItemClickListener {
        fun onClick(itemObj: ItemBaseCategory, position: Int)
    }
}