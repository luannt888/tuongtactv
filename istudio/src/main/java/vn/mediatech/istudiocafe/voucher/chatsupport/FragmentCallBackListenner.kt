package vn.mediatech.istudiocafe.voucher.chatsupport

interface FragmentCallBackListenner {
    fun onResume()
    fun onDetach()
}