package vn.mediatech.istudiocafe.voucher.chatsupport

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_conversation.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.MyApplication

class ItemConversationAdapter(val context: Context, val list: ArrayList<ItemConversation>) : Adapter<ItemConversationAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_conversation,
                parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val itemObj: ItemConversation = list.get(position)
        MyApplication.getInstance().loadImageCircle(context, holder.imgAvatar, itemObj.userAvatar, R.drawable.ic_avatar)
        holder.textTime.setText(itemObj.date)
        holder.textName.setText(itemObj.userName)
        if (itemObj.count != null && itemObj.count!! > 0) {
            holder.textCountUnRead.text = itemObj.count.toString()
            holder.textCountUnRead.visibility = View.VISIBLE
            holder.textName.setTypeface(Typeface.DEFAULT_BOLD)
        } else {
            holder.textCountUnRead.visibility = View.GONE
            holder.textName.setTypeface(Typeface.DEFAULT)
        }
        holder.textMessage.setText(itemObj.message)
        holder.textVoucherName.setText(itemObj.voucherName)
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
        }
        holder.layoutRoot.setOnLongClickListener {
            onItemClickListener?.onLongClick(itemObj, position)
            true
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textTime = itemView.textTime
        val textMessage = itemView.textMessage
        val textName = itemView.textName
        val textCountUnRead = itemView.textCountUnRead
        val textVoucherName = itemView.textVoucherName
    }

    interface OnItemClickListener {
        fun onClick(item: ItemConversation, position: Int)
        fun onLongClick(item: ItemConversation, position: Int)
    }

}