package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_my_voucher.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.VoucherActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnItemClickUserVoucherListener
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.util.GeneralUtils
import vn.mediatech.istudiocafe.util.ServiceManager
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import java.util.*

class MyVoucherFragment : Fragment() {
    private var useVoucherFragment: UseVoucherFragment? = null
    private var detailVoucherFragment: DetailVoucherFragment? = null
    var myHttpRequest: MyHttpRequest? = null
    var isViewCreated: Boolean = false
    var voucherFragment: VoucherFragment? = null
    var voucherType: Int = 0
    var savedInstanceState: Bundle? = null
    lateinit var itemList: ArrayList<ItemBaseCategory>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        (activity as? VoucherActivity)?.setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)
    }

    fun initData() {
        layoutActionbarVoucherSearch.visibility = View.INVISIBLE
        textActionbarTitle.visibility = View.VISIBLE
        getTabData()
    }

    fun getTabData() {
        activity?.runOnUiThread {
            textNotify.visibility = View.GONE
            loadingTab.visibility = View.VISIBLE
        }
        ServiceManager.getBaseCategoryVoucher(
            requireActivity(),
            ServiceUtilTT.validAPIDGTH(context, Constant.API_VOUCHER_MY_TAB),
            object : ServiceManager.OnBaseCategoryRequestListener {
                override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                    this@MyVoucherFragment.myHttpRequest = myHttpRequest
                }

                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    itemList: ArrayList<ItemBaseCategory>?
                ) {
                    if (isDetached) {
                        return
                    }
                    activity?.runOnUiThread {
                        loadingTab.visibility = View.GONE
                    }
                    if (!isSuccess || itemList == null || itemList.size == 0) {
                        activity?.runOnUiThread {
                            textNotify.visibility = View.VISIBLE
                            textNotify.text = message
                        }
                        return
                    }
                    activity?.runOnUiThread {
                        tabLayout.visibility = View.VISIBLE
                        initTab(itemList)
                    }
                }
            })
    }

    fun initTab(list: ArrayList<ItemBaseCategory>) {
        itemList = list
        for (itemObj in itemList) {
            val tab = tabLayout.newTab()
            tab.text = itemObj.name
            tabLayout.addTab(tab)
        }
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab == null) {
                    return
                }
                handleOnPageSelected(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

        for (i in 0 until tabLayout.tabCount) {
            val tab = (tabLayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            val rightMargin = resources.getDimensionPixelSize(R.dimen.dimen_3)
            val leftMargin =
                if (i == 0) resources.getDimensionPixelSize(R.dimen.dimen_10) else rightMargin
            p.setMargins(leftMargin, 0, rightMargin, 0)
            tab.layoutParams = p
            tab.requestLayout()
        }
        handleOnPageSelected(0)
    }

    fun handleOnPageSelected(position: Int) {
        val itemObj = itemList.get(position)
        if (voucherFragment != null) {
            voucherFragment!!.refreshData(itemObj)
            return
        }
        val bundle = Bundle()
        bundle.putParcelable(Constant.DATA, itemObj)
        bundle.putInt(Constant.VOUCHER_TYPE, Constant.MY_VOUCHER)
        voucherFragment = VoucherFragment()
        voucherFragment!!.arguments = bundle
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, voucherFragment!!)
        fragmentTransaction.commit()
        voucherFragment?.onItemClickListener = object : VoucherAdapter.OnItemClickListener {
            override fun onClick(itemObj: ItemVoucher, position: Int) {
                if (activity !is VoucherActivity) {
                    val bundle = Bundle()
                    bundle.putParcelable(Constant.DATA, itemObj)
                    val onResultListener = object : OnResultListener {
                        override fun onResult(isSuccess: Boolean, msg: String?) {
                            refreshData()
                            detailVoucherFragment = null
                        }
                    }
                    detailVoucherFragment = DetailVoucherFragment()
                    detailVoucherFragment?.arguments = bundle
                    detailVoucherFragment?.onResultListener = onResultListener
                    GeneralUtils.showFragment(
                        childFragmentManager,
                        detailVoucherFragment,
                        R.id.frameLayoutFullscreen,
                        false,
                        true
                    )
                    detailVoucherFragment?.onItemClickUserVoucherListener =
                        object : OnItemClickUserVoucherListener {
                            override fun onClickUseVoucher(itemObj: ItemVoucher?, position: Int) {
                                val bundle = Bundle()
                                bundle.putParcelable(Constant.DATA, itemObj)
                                useVoucherFragment = UseVoucherFragment()
                                useVoucherFragment?.arguments = bundle
                                GeneralUtils.showFragment(
                                    childFragmentManager,
                                    useVoucherFragment,
                                    R.id.frameLayoutFullscreen,
                                    false,
                                    true
                                )
                            }
                        }
                }
            }
        }
    }

    fun back(): Boolean {
        if (useVoucherFragment != null) {
            val fragmentTRansaction = childFragmentManager.beginTransaction()
            fragmentTRansaction.remove(useVoucherFragment!!)
            fragmentTRansaction.commit()
            useVoucherFragment = null
            return true
        }
        if (detailVoucherFragment != null) {
            val fragmentTRansaction = childFragmentManager.beginTransaction()
            fragmentTRansaction.remove(detailVoucherFragment!!)
            fragmentTRansaction.commit()
            return true
        }
        return false
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).goBack(true)
            } else {
                activity?.onBackPressed()
            }
        }
        buttonVoucherStore.setOnClickListener {
            if (activity is VoucherActivity) {
                (activity as VoucherActivity).showVoucherStoreFragment()
            }
        }
        textNotify.setOnClickListener {
            getTabData()
        }
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }

    fun refreshData() {
        voucherFragment?.refreshData()
    }
}