package vn.mediatech.istudiocafe.voucher.chatsupport

import android.content.Context
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_chat.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.fragment.ImageZoomPagerDialog
import vn.mediatech.istudiocafe.model.ItemPhoto
import vn.mediatech.istudiocafe.model.ItemVoucherChat

class ItemVoucherChatAdapter(val context: Context, val list: ArrayList<ItemVoucherChat>) : Adapter<ItemVoucherChatAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_chat,
                parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item: ItemVoucherChat = list.get(position)
        if (item.content.isNullOrEmpty() && item.image.isNullOrEmpty()) {
            return
        }
        if (item.createdAt.isNullOrEmpty()) {
            holder.textDate.visibility = View.GONE
        } else {
            holder.textDate.text = item.createdAt
            holder.textDate.visibility = View.VISIBLE
        }
        if (item.content.isNullOrEmpty()) {
            holder.textMessage.visibility = View.GONE
        } else {
            holder.textMessage.text = item.content
            holder.textMessage.visibility = View.VISIBLE
        }

        when (item.type) {
            0 -> {
                holder.layoutContent.background = ContextCompat.getDrawable(context, R.drawable.bg_layout_voucher_chat_right)
                holder.layoutRoot.gravity = Gravity.END
                holder.layoutContent.gravity = Gravity.END
            }
            1 -> {
                holder.layoutContent.background = ContextCompat.getDrawable(context, R.drawable.bg_item_chat_left)
                holder.layoutRoot.gravity = Gravity.START
                holder.layoutContent.gravity = Gravity.START
            }
        }
        if (item.image.isNullOrEmpty()) {
            holder.imageThumbnail.visibility = View.GONE
        } else {
            holder.imageThumbnail.visibility = View.VISIBLE
            MyApplication.getInstance().loadImage(context, holder.imageThumbnail, item.image)
            holder.imageThumbnail.setOnClickListener {
                val fragment = ImageZoomPagerDialog()
                val list = ArrayList<ItemPhoto>()
                list.add(ItemPhoto(item.userName, item.image, item.createdAt))
                val bundle = Bundle()
                bundle.putParcelableArrayList(Constant.DATA, list)
                fragment.arguments = bundle
                fragment.show((context as? FragmentActivity)?.supportFragmentManager!!, "ImageZoom")
            }
        }


    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {

        val layoutRoot = itemView.layoutRoot
        val layoutContent = itemView.layoutContent
        val textTime = itemView.imageThumbnail
        val textMessage = itemView.textMessage
        val textDate = itemView.textDate
        val imageThumbnail = itemView.imageThumbnail
    }

    interface OnItemClickListener {
        fun onClick(item: ItemConversation, position: Int)
    }

}