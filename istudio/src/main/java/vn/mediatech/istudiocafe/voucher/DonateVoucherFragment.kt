package vn.mediatech.istudiocafe.voucher

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_donate_voucher.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnResultListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceManager
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.TextUtil
import java.util.*

class DonateVoucherFragment : BottomSheetDialogFragment() {
    var bottomSheetDialog: BottomSheetDialog? = null
    var savedInstanceState: Bundle? = null
    lateinit var itemObj: ItemVoucher
    var myHttpRequest: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    var onResultListener: OnResultListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_donate_voucher, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheetInternal: View? = (it as BottomSheetDialog).findViewById(R.id.design_bottom_sheet)
            bottomSheetInternal?.setBackgroundColor(ContextCompat.getColor(requireActivity(), R.color.transparent))
        }
        initUI()
        initData()
        initControl()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.DATA, itemObj)
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun initUI() {
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        itemObj = bundle.getParcelable(Constant.DATA) ?: return
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        if (itemUser == null) {
            dismiss()
            return
        }
        setData()
    }

    fun initControl() {
        editUserName.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEND || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                (activity as? BaseActivity)?.hideKeyboard(editUserName)
                send()
                true
            } else false
        }
        buttonSend.setOnClickListener {
            send()
        }
    }

    fun send() {
        val userName = editUserName.text.toString().trim()
        if (userName.isEmpty()) {
            (activity as BaseActivity).showDialog(requireActivity().getString(R.string.msg_user_name_empty))
            return
        }
        if (userName.length < 5) {
            (activity as BaseActivity).showDialog(requireActivity().getString(R.string.msg_user_name_invalid))
            return
        }
        getData()
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
                (activity as BaseActivity).hideLoadingDialog()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (!MyApplication.getInstance().isNetworkConnect) {
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        (activity as BaseActivity).showLoadingDialog(true, requireActivity().getString(R.string.msg_donate_title), object : OnCancelListener {
            override fun onCancel(isCancel: Boolean) {
                myHttpRequest?.cancel()
            }
        })
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val api = String.format(Locale.ENGLISH, ServiceUtilTT.validAPIDGTH(context,Constant.API_DONATE_VOUCHER), itemUser!!.id)
        val userName = editUserName.text.toString().trim()
        val requestParams = RequestParams()
        requestParams.put("voucherid", "${itemObj.id}")
        requestParams.put("unique_id", "${itemObj.uniqueId}")
        requestParams.put("phone_receive", userName)
        ServiceManager.requestBase(requireActivity(), ServiceUtilTT.validAPIDGTH(context,Constant.API_DONATE_VOUCHER), requestParams, object : ServiceManager.OnBaseRequestListener {
            override fun onPreRequest(myHttpRequest: MyHttpRequest?) {
                this@DonateVoucherFragment.myHttpRequest = myHttpRequest
            }

            override fun onResponse(isSuccess: Boolean, message: String?, responseString: String?) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    (activity as BaseActivity).hideLoadingDialog()
                }
                if (!isSuccess) {
                    activity?.runOnUiThread {
                        try {
                            textNotify.visibility = View.VISIBLE
                            textNotify.text = message
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    return
                }
                activity?.runOnUiThread {
                    (activity as BaseActivity).showDialogVoucher(false, false, null, R.string.notification, message, null, null, R.string.close, object: OnDialogButtonVoucherListener {
                        override fun onLeftButtonClick() {
                        }

                        override fun onCenterButtonClick() {
                        }

                        override fun onRightButtonClick() {
                            onResultListener?.onResult(true, message)
                            dismiss()
                        }

                    })
                }
            }
        })
    }

    fun setData() {
        val msg = itemObj.voucherDonate ?: ""
        TextUtil.setHtmlTextView(msg, textMessage)
//        val msg = String.format(Locale.ENGLISH, activity!!.getString(R.string.msg_donate_title, itemObj.name))
//        TextUtil.setHtmlTextView(msg, textMsg)
        val imageHeight = ScreenSize(activity!!).width * 5 / 8
        val lp = imageThumbnail.layoutParams as LinearLayout.LayoutParams
        lp.height = imageHeight
        MyApplication.getInstance().loadImage(activity!!, imageThumbnail, itemObj.image)
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }
}