package vn.mediatech.istudiocafe.voucher

interface OnDialogButtonVoucherListener {
    fun onLeftButtonClick()
    fun onCenterButtonClick()
    fun onRightButtonClick()
}