package vn.mediatech.istudiocafe.spriteView

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import androidx.core.graphics.drawable.toBitmap
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.util.CustomThreadPoolManager
import vn.mediatech.istudiocafe.util.GeneralUtils
import java.util.*
import kotlin.concurrent.schedule


class SpriteView(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    var image: Bitmap? = null
    var imagePlaceholder: Bitmap? = null
    var spriteWidth = 0.0
    var spriteHeight = 0.0
    var isFitXY = true
    var columns = 1
        set(value) {
            lastFrame = value * rows
            field = value
            if (image != null) {
                spriteWidth = image!!.width.toDouble() / columns
                spriteHeight = image!!.height.toDouble() / rows
            }
        }
    var rows = 1
        set(value) {
            lastFrame = columns * value
            field = value
            if (image != null) {
                spriteWidth = image!!.width.toDouble() / columns
                spriteHeight = image!!.height.toDouble() / rows
            }
        }

    var fps = columns * rows
    var lastFrame = columns * rows
    var isFixedRow = false
    var autoPlay = true
    val isRunning: Boolean
        get() {
            return running
        }

    //    var maxCycles = 0
//    var currentCycle = 0
    var stateChangeListener: StateChangeListener? = null

    var renderRow = 0
    var renderColumn = 0
    var currentFrame = 0
    private var running = false
    private val srcFrame = Rect()
    private val dstFrame = Rect()
    private var timer = Timer()

    init {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(
                attrs, R.styleable.SpriteView, 0, 0
            )
            image = a.getDrawable(R.styleable.SpriteView_src)?.toBitmap()
            imagePlaceholder = a.getDrawable(R.styleable.SpriteView_srcPlaceHolder)?.toBitmap()
            columns = a.getInt(R.styleable.SpriteView_columns, columns)
            rows = a.getInt(R.styleable.SpriteView_rows, rows)
            fps = a.getInt(R.styleable.SpriteView_fps, fps)
            lastFrame = a.getInt(R.styleable.SpriteView_lastFrame, lastFrame)
            renderRow = a.getInt(R.styleable.SpriteView_renderRow, renderRow)
            isFixedRow = a.getBoolean(R.styleable.SpriteView_isFixedRow, isFixedRow)
            autoPlay = a.getBoolean(R.styleable.SpriteView_autoPlay, autoPlay)
            isFitXY = a.getBoolean(R.styleable.SpriteView_fitXY, isFitXY)
            if (image != null && spriteWidth == 0.0) {
                spriteWidth = image!!.width.toDouble() / columns
                spriteHeight = image!!.height.toDouble() / rows
            }
//            if (image == null && imagePlaceholder != null) {
//                setScaleImage(imagePlaceholder!!.width.toDouble(), imagePlaceholder!!.height.toDouble())
//                invalidate()
//            }
            a.recycle()
            if (autoPlay) {
                start()
            }
        }
    }

    constructor(context: Context) : this(context, null)

    fun start(url: String?, updateSpriteDimetion: Boolean = false) {
        GeneralUtils.loadImageBitmapListener(
            context,
            url,
            object : MyApplication.OnLoadImageBitmapListener {
                override fun onResourceReady(resource: Bitmap?) {
                    image = resource
                    if (image != null) {
                        if (updateSpriteDimetion) {
                            columns = (image!!.width.toDouble() / (spriteWidth)).toInt()
                            rows = (image!!.height.toDouble() / (spriteHeight)).toInt()
                        }
                        Loggers.e(
                            "SPRITE",
                            url + "/" + image!!.width + "/" + image!!.height + "/" + columns + "/" + rows
                        )

                        start()
                    }
                }
            })
    }

    fun start() {
        stop()
        if (image != null) {
            spriteWidth = image!!.width.toDouble() / columns
            spriteHeight = image!!.height.toDouble() / rows
            timer.schedule(0, (1000 / fps).toLong()) {
                CustomThreadPoolManager.getsInstance().getThreadPoolExecutor(context)
                    .execute {
                        if (context == null) {
                            stop()
                            return@execute
                        }
                        running = true
                        post { invalidate() }
                        if (lastFrame == currentFrame) {
                            resetFrames()
//                    stop()
                        }
                        currentFrame++
//
                    }
                post {
                    stateChangeListener?.onStart(this@SpriteView)
                }
            }
        }
    }

    private fun setScaleImage(imageWidth: Double, imageHeight: Double) {
        if (isFitXY) {
            dstFrame.left = 0
            dstFrame.top = 0
            dstFrame.right = width
            dstFrame.bottom = height
        } else {
            if ((imageWidth / width) >= (imageHeight / height)) {
                dstFrame.right = width
                val mHeight = (width * (imageHeight / imageWidth)).toInt()
                dstFrame.top = (height - mHeight) / 2
                dstFrame.bottom = dstFrame.top + height
                dstFrame.left = 0
            } else {
                val mWidth = (height * (imageWidth / imageHeight)).toInt()
                dstFrame.left = (width - mWidth) / 2
                dstFrame.right = dstFrame.left + mWidth
                dstFrame.bottom = height
                dstFrame.top = 0
            }
        }
    }

    fun stop() {
        pause()
        if (currentFrame > 0)
            post {
                stateChangeListener?.onStop(this)
            }

        resetFrames()
        invalidate()
        running = false
//        currentCycle = 0
    }

    fun pause() {
        timer.cancel()
        timer = Timer()
    }

    fun resetFrames() {
        renderRow = if (isFixedRow) renderRow else 0
        renderColumn = 0
        currentFrame = 0
    }


    override fun onDraw(canvas: Canvas?) {
        if (image != null) {
            val srcX = renderColumn * spriteWidth
            val srcY = renderRow * spriteHeight
            srcFrame.left = srcX.toInt()
            srcFrame.top = srcY.toInt()
            srcFrame.right = (srcX + spriteWidth).toInt()
            srcFrame.bottom = (srcY + spriteHeight).toInt()
            setScaleImage(spriteWidth, spriteHeight)
            canvas?.drawBitmap(image!!, srcFrame, dstFrame, null)
            if (renderColumn == columns - 1 && !isFixedRow) {
                renderRow = ++renderRow % rows
            }
            renderColumn = ++renderColumn % columns

            post {
                stateChangeListener?.onUpdateFrame(this)
            }
        }
        if (image == null && imagePlaceholder != null) {
            setScaleImage(imagePlaceholder!!.width.toDouble(), imagePlaceholder!!.height.toDouble())
            srcFrame.top = 0
            srcFrame.left = 0
            srcFrame.right=imagePlaceholder!!.width
            srcFrame.bottom=imagePlaceholder!!.height
            canvas?.drawBitmap(imagePlaceholder!!, srcFrame, dstFrame, null)
        }
    }
}