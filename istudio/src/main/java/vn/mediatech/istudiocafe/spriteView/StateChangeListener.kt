package vn.mediatech.istudiocafe.spriteView

interface StateChangeListener {
    fun onUpdateFrame(view: SpriteView){}
    fun onStart(view: SpriteView){}
    fun onStop(view: SpriteView){}
}
