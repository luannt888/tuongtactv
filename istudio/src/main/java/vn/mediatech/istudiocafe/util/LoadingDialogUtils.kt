package vn.mediatech.istudiocafe.util

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.TextView
import kotlinx.android.synthetic.main.layout_dialog_tt.*
import vn.mediatech.istudiocafe.R

class LoadingDialogUtils {
    companion object {
        private var loadingDialog: Dialog? = null

        fun showLoadingDialog(
            context: Context,
            cancelable: Boolean,
            onCancelListener: OnCancelListener?
        ) {
            showLoadingDialog(context, cancelable, R.string.please_waiting, onCancelListener)
        }

        fun showLoadingDialog(
            context: Context,
            cancelable: Boolean,
            message: Int,
            onCancelListener: OnCancelListener?
        ) {
            showLoadingDialog(context, cancelable, context.getString(message), onCancelListener)
        }

        fun showLoadingDialog(
            context: Context,
            cancelable: Boolean,
            message: String?,
            onCancelListener: OnCancelListener?
        ) {
            if (loadingDialog != null) {
                loadingDialog!!.dismiss()
                loadingDialog!!.setCancelable(cancelable)
                loadingDialog!!.setCanceledOnTouchOutside(false)
                val textMessage: TextView = loadingDialog!!.findViewById(R.id.textMessage)
                if (message.isNullOrEmpty()) {
                    textMessage.visibility = View.GONE
                } else {
                    textMessage.text = message
                    textMessage.visibility = View.VISIBLE
                }
                try {
                    loadingDialog!!.show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                return
            }
            loadingDialog = Dialog(context)
            if (loadingDialog!!.window != null) {
                loadingDialog!!.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            }
            loadingDialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loadingDialog?.setCancelable(cancelable)
            loadingDialog?.setCanceledOnTouchOutside(false)
            loadingDialog?.setContentView(R.layout.layout_loading_tt)
            val textMessage: TextView = loadingDialog!!.findViewById(R.id.textMessage)
            if (message.isNullOrEmpty()) {
                textMessage.visibility = View.GONE
            } else {
                textMessage.text = message
                textMessage.visibility = View.VISIBLE
            }
            loadingDialog?.setOnCancelListener {
                onCancelListener?.onCancel(true)
            }
            try {
                loadingDialog?.show()
            } catch (e: Exception) {
            }
        }

        fun hideLoadingDialog() {
            loadingDialog?.dismiss()
        }

        fun setTextLoadingDialog(msg: String) {
            loadingDialog?.textMessage?.text = msg
        }
    }

    interface OnCancelListener {
        fun onCancel(isCancel: Boolean)
    }
}