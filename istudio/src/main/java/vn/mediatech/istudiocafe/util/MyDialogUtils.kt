package vn.mediatech.istudiocafe.util

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.text.method.LinkMovementMethod
import android.view.*
import android.widget.TextView
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.AppUtils.Companion.showDialog
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.model.ItemUser

class MyDialogUtils {
    companion object {
        fun showDialog(context: Context, message: String?) {
            showDialog(
                context,
                true,
                context.getString(R.string.notification),
                message,
                context.getString(R.string.close),
                null,
                null
            )
        }

        fun showDialog(
            context: Context,
            message: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ) {
            showDialog(
                context,
                true,
                context.getString(R.string.notification),
                message,
                context.getString(R.string.close),
                null,
                onDialogButtonListener
            )
        }

        fun showDialog(
            context: Context,
            cancelable: Boolean,
            message: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ) {
            showDialog(
                context,
                cancelable,
                context.getString(R.string.notification),
                message,
                context.getString(R.string.close),
                null,
                onDialogButtonListener
            )
        }

        fun showDialog(
            context: Context, cancelable: Boolean, titleId: Int, messageId: Int,
            leftButtonTitleId: Int, rightButtonTitleId: Int,
            dialogButtonListener: OnDialogButtonListener?
        ) {
            var message: String? = null
            if (messageId != 0) {
                message = context.getString(messageId)
            }
            showDialog(
                context,
                cancelable,
                titleId,
                message,
                leftButtonTitleId,
                rightButtonTitleId,
                dialogButtonListener
            )
        }

        fun showDialog(
            context: Context, cancelable: Boolean, titleId: Int, message: String?,
            leftButtonTitleId: Int, rightButtonTitleId: Int,
            dialogButtonListener: OnDialogButtonListener?
        ) {
            var title: String? = null
            var leftButtonTitle: String? = null
            var rightButtonTitle: String? = null
            if (titleId != 0) {
                title = context.getString(titleId)
            }
            if (leftButtonTitleId != 0) {
                leftButtonTitle = context.getString(leftButtonTitleId)
            }
            if (rightButtonTitleId != 0) {
                rightButtonTitle = context.getString(rightButtonTitleId)
            }
            showDialog(
                context,
                cancelable,
                title,
                message,
                leftButtonTitle,
                rightButtonTitle,
                dialogButtonListener
            )
        }

        fun showDialog(
            context: Context, cancelable: Boolean, title: String?, message: String?,
            leftButtonTitle: String?, rightButtonTitle: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): MyDialog {
            return showDialog(
                context,
                cancelable,
                false,
                title,
                message,
                leftButtonTitle,
                rightButtonTitle,
                onDialogButtonListener
            )
        }

        fun showDialog(
            context: Context,
            cancelable: Boolean,
            cancelableOutside: Boolean,
            title: String?,
            message: String?,
            leftButtonTitle: String?,
            rightButtonTitle: String?,
            onDialogButtonListener: OnDialogButtonListener?
        ): MyDialog {
            val dialog = MyDialog(context)
            dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val lp = WindowManager.LayoutParams()
            //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            lp.gravity = Gravity.CENTER
            lp.windowAnimations = R.style.DialogAnimationLeftRight
            dialog.window?.attributes = lp
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(cancelable)
            dialog.setCanceledOnTouchOutside(cancelableOutside)
            dialog.setContentView(R.layout.layout_dialog_tt)
            val screenSize = ScreenSize(context)
            dialog.window?.setLayout(
                screenSize.width * 9 / 10,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
            val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
            val textTitle: TextView = dialog.findViewById(R.id.textTitle)
            val textMessage: TextView = dialog.findViewById(R.id.textMessage)
            val buttonLeft: TextView = dialog.findViewById(R.id.buttonLeft)
            val buttonRight: TextView = dialog.findViewById(R.id.buttonRight)
            //        View viewDivider = dialog.findViewById(R.id.viewDivider);
            layoutRoot.clipToOutline = true
            layoutRoot.outlineProvider = object : ViewOutlineProvider() {
                override fun getOutline(view: View, outline: Outline) {
                    val cornerRadiusDP = 15f
                    outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
                }
            }
            if (!title.isNullOrEmpty()) {
                textTitle.text = title
            }
            if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
                TextUtil.setHtmlTextView(message, textMessage)
                textMessage.movementMethod = LinkMovementMethod.getInstance()
            }
            if (leftButtonTitle.isNullOrEmpty()) {
                buttonLeft.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonLeft.text = leftButtonTitle
                buttonLeft.visibility = View.VISIBLE
            }
            if (rightButtonTitle.isNullOrEmpty()) {
                buttonRight.visibility = View.GONE
                //            viewDivider.setVisibility(View.GONE);
            } else {
                buttonRight.text = rightButtonTitle
                buttonRight.visibility = View.VISIBLE
            }
            buttonLeft.setOnClickListener {
                dialog.dismiss()
                onDialogButtonListener?.onLeftButtonClick()
            }
            buttonRight.setOnClickListener {
                dialog.dismiss()
                onDialogButtonListener?.onRightButtonClick()
            }
            try {
                dialog.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return dialog
        }
    }

    interface OnDialogButtonListener {
        fun onLeftButtonClick()
        fun onRightButtonClick()
    }


}