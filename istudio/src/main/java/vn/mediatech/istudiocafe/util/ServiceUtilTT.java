package vn.mediatech.istudiocafe.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import vn.mediatech.interactive.service.OnClearFromRecentService;
import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.Loggers;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnBaseRequestListener;
import vn.mediatech.istudiocafe.listener.OnGetConfigInteractiveListener;
import vn.mediatech.istudiocafe.listener.OnItemNewsListener;
import vn.mediatech.istudiocafe.listener.OnLoginFormListener;
import vn.mediatech.istudiocafe.listener.OnRequireShowMainInteractiveListener;
import vn.mediatech.istudiocafe.listener.OnResultListener;
import vn.mediatech.istudiocafe.listener.OnResultRequestListener;
import vn.mediatech.istudiocafe.model.ItemInteractiveConfig;
import vn.mediatech.istudiocafe.model.ItemNews;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ServiceUtilTT {
    public static void addLike(Context context, String id) {
        if (MyApplication.getInstance().isEmpty(id)) {
            return;
        }
        RequestParams requestParams = new RequestParams();
        requestParams.put("article_id", id);
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        myHttpRequest.request(true, validAPI(Constant.API_LIKE), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                    }
                });
    }

    public static void addShare(Context context, String id) {
        if (MyApplication.getInstance().isEmpty(id)) {
            return;
        }
        RequestParams requestParams = new RequestParams();
        requestParams.put("id", id);
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        myHttpRequest.request(false, validAPI(Constant.API_SHARE), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                    }
                });
    }

    public static void addShareLinkApp(Context context) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("type", "share_app");
        requestParams.put("article_id", "-1");
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        myHttpRequest.request(false, validAPI(Constant.API_SHARE), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                    }
                });
    }

    public static void addLikeLinkApp(Context context) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("type", "like");
        requestParams.put("article_id", "-1");
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        myHttpRequest.request(false, validAPI(Constant.API_SHARE), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                    }
                });
    }

    public static MyHttpRequest getNameUser(Context context, String userId, OnResultRequestListener onResultListener) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("userid", userId);
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        myHttpRequest.request(false, validAPIDGTH(context, Constant.API_GET_NAME_USER), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (onResultListener != null && context != null) {
                            onResultListener.onResult(false, context.getString(R.string.msg_network_error), null);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
                        if (jsonObject == null) {
                            if (onResultListener != null && context != null) {
                                onResultListener.onResult(false, context.getString(R.string.msg_not_data), null);
                            }
                            return;
                        }
                        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
                        String msg = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
                        String result = JsonParser.Companion.getString(jsonObject, Constant.RESULT);
                        String avatar = JsonParser.Companion.getString(jsonObject, "avatar");
                        if (errorCode != Constant.SUCCESS) {
                            if (onResultListener != null && context != null) {
                                onResultListener.onResult(false, result, null);
                            }
                            return;
                        }
                        if (result == null || result.trim().equals("")) {
                            if (onResultListener != null && context != null) {
                                onResultListener.onResult(false, context.getString(R.string.msg_not_data), null);
                            }
                            return;
                        }

                        if (onResultListener != null && context != null) {
                            onResultListener.onResult(true, result, avatar);
                        }
                    }
                });
        return myHttpRequest;
    }

    public static void addAdsView(Context context, int id, String type) {
        if (!type.equals(Constant.TYPE_VIEW) && !type.equals(Constant.TYPE_CLICK)) {
            return;
        }
        RequestParams requestParams = new RequestParams();
        requestParams.put("id", String.valueOf(id));
        requestParams.put("type", type);
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        myHttpRequest.request(false, validAPI(Constant.API_ADS_VIEW), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                    }
                });
    }

    public static void autoLogin(@NonNull final Context context,
                                 final OnLoginFormListener loginFormListener) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Constant.CONFIG,
                Context.MODE_PRIVATE);
//        String username = sharedPreferences.getString(Constant.USERNAME, null);
        String token = sharedPreferences.getString(Constant.ACCESS_TOKEN, null);
        if (MyApplication.getInstance().isEmpty(token)) {
            if (loginFormListener != null) {
                loginFormListener.onLogin(false);
            }
            return;
        }

        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        RequestParams requestParams = new RequestParams();
        requestParams.put("access_token", token);
        myHttpRequest.request(false, ServiceUtilTT.validAPIDGTH(context, Constant.API_USER_AUTO_LOGIN)
                , requestParams, new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (loginFormListener != null) {
                            loginFormListener.onLogin(false);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        handleDataLogin(context, responseString, loginFormListener);
                    }
                });
    }

    private static void handleDataLogin(@NonNull Context context, String responseString,
                                        OnLoginFormListener loginFormListener) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            if (loginFormListener != null) {
                loginFormListener.onLogin(false);
            }
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            if (loginFormListener != null) {
                loginFormListener.onLogin(false);
            }
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            SharedPreferencesManager.saveUser(context, null, null);
            if (loginFormListener != null) {
                loginFormListener.onLogin(false);
            }
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
        if (itemUser == null) {
            if (loginFormListener != null) {
                loginFormListener.onLogin(false);
            }
            return;
        }
        SharedPreferencesManager.saveUser(context, itemUser.getPhone(), itemUser.getAccessToken());
        if (loginFormListener != null) {
            loginFormListener.onLogin(true);
        }
    }

    public static void getEventLive(@NonNull Activity activity,
                                    OnItemNewsListener onItemNewsListener) {
        MyHttpRequest myHttpRequest = new MyHttpRequest(activity);
        myHttpRequest.request(false, validAPIDGTH(activity, Constant.API_EVENT_LIVE), null,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (onItemNewsListener != null) {
                            onItemNewsListener.onResponse(false,
                                    activity.getString(R.string.msg_network_error), null);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        handleDataEventLive(activity, responseString, onItemNewsListener);
                    }
                });
    }

    private static void handleDataEventLive(@NonNull Activity activity, String responseString,
                                            OnItemNewsListener onItemNewsListener) {
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            if (onItemNewsListener != null) {
                onItemNewsListener.onResponse(false,
                        activity.getString(R.string.msg_empty_data), null);
            }
            return;
        }

        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode != Constant.SUCCESS) {
            if (onItemNewsListener != null) {
                onItemNewsListener.onResponse(false, message, null);
            }
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
        JSONArray dataArr = JsonParser.Companion.getJsonArray(resultObj, Constant.DATA);
        ArrayList<ItemNews> itemList = JsonParser.Companion.parseItemNewsList(activity, dataArr);
        if (onItemNewsListener != null) {
            onItemNewsListener.onResponse(true, message, itemList);
        }
    }

    public static void updateUserProfile(@NonNull Activity activity, RequestParams requestParams,
                                         OnBaseRequestListener onBaseRequestListener) {
        MyHttpRequest myHttpRequest = new MyHttpRequest(activity);
        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(activity, Constant.API_USER_UPDATE_PROFILE),
                requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (onBaseRequestListener != null) {
                            onBaseRequestListener.onResponse(false,
                                    activity.getString(R.string.msg_network_error), null);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        handleDataUpdateUserProfile(activity, responseString,
                                onBaseRequestListener);
                    }
                });
    }

    private static void handleDataUpdateUserProfile(@NonNull Activity activity,
                                                    String responseString,
                                                    OnBaseRequestListener onBaseRequestListener) {
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            if (onBaseRequestListener != null) {
                onBaseRequestListener.onResponse(false,
                        activity.getString(R.string.msg_empty_data), responseString);
            }
            return;
        }

        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode != Constant.SUCCESS) {
            if (onBaseRequestListener != null) {
                onBaseRequestListener.onResponse(false, message, null);
            }
            return;
        }
//        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
//        JSONArray dataArr = JsonParser.Companion.getJsonArray(resultObj, Constant.DATA);
//        ArrayList<ItemNews> itemList = JsonParser.Companion.parseItemNewsList(activity, dataArr);
        if (onBaseRequestListener != null) {
            onBaseRequestListener.onResponse(true, message, responseString);
        }
    }

    public static void deleteNotification(@NonNull Activity activity, String id,
                                          OnBaseRequestListener onBaseRequestListener) {
        ItemUser itemUser = MyApplication.getInstance().getDataManager().getItemUser();
        if (itemUser == null) {
            if (onBaseRequestListener != null) {
                onBaseRequestListener.onResponse(false, "", "");
            }
            return;
        }
        MyHttpRequest myHttpRequest = new MyHttpRequest(activity);
        RequestParams requestParams = new RequestParams();
        requestParams.put("id", id);
        requestParams.put("access_token", itemUser.getAccessToken());
        myHttpRequest.request(false, validAPI(Constant.API_NOTIFICATION_DELETE), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (onBaseRequestListener != null) {
                            onBaseRequestListener.onResponse(false,
                                    activity.getString(R.string.msg_network_error), null);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (onBaseRequestListener != null) {
                            onBaseRequestListener.onResponse(true, "", responseString);
                        }
                    }
                });
    }

    public static void addChatUseVoucher(@NonNull Activity activity, String voucherId, String content,
                                         OnResultListener onResultListener) {
        ItemUser itemUser = MyApplication.getInstance().getDataManager().getItemUser();
        if (itemUser == null) {
            return;
        }
        String userName;
        if (!MyApplication.getInstance().isEmpty(itemUser.getFullname())) {
            userName = itemUser.getFullname();
        } else {
            userName = itemUser.getPhone();
        }
        MyHttpRequest myHttpRequest = new MyHttpRequest(activity);
        RequestParams requestParams = new RequestParams();
        requestParams.put("userid", String.valueOf(itemUser.getId()));
        requestParams.put("id", voucherId);
        requestParams.put("chanel", "istudio");
        requestParams.put("content", content);
        requestParams.put("user_name", userName);
        requestParams.put("user_avatar", itemUser.getAvatar());

        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(activity, Constant.API_USE_VOUCHER_ADD), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (onResultListener != null) {
                            onResultListener.onResult(false,
                                    activity.getString(R.string.msg_network_error));
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
                        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
                        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
                        if (onResultListener != null) {
                            onResultListener.onResult(errorCode == Constant.SUCCESS, message);
                        }
                    }
                });
    }


    public static String validAPI(String api) {
        String baseUrl = MyApplication.getInstance().getBaseApiUrl();
        return baseUrl + api;
    }

    public static String validAPIDGTH(Context context, String api) {
        String baseUrl;
        baseUrl = Constant.BASE_URL_DGTH;
        if (context != null && SharedPreferencesManager.isTest(context) && !MyApplication.getInstance().isEmpty(SharedPreferencesManager.getBaseURL(context))) {
            baseUrl = SharedPreferencesManager.getBaseURL(context);
        }
        return baseUrl + api;
    }

    public static int calculatorPercent(int number1, int number2) {
        int percent = number2 == 0 ? 0 : (int) Math.round(number1 * 100f / number2);
        if (percent <= 0) {
            percent = -100;
        } else if (percent < 100) {
            percent -= 100;
        } else if (percent == 100) {
            percent = 0;
        }
        return percent;
    }

    public static void getConfigInteractive(Context context, String appId,
                                            OnGetConfigInteractiveListener listenerGetConfig, OnGetConfigInteractiveListener listenerCheckIsInteractive, int timePeriodMsInteractive) {
        if (context == null) {
            return;
        }
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        String api = Constant.API_CONFIG_INTERACTIVE_APP;
        RequestParams requestParams = new RequestParams();
        requestParams.put("appid", appId);
        myHttpRequest.request(false, api, requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (listenerGetConfig != null) {
                            listenerGetConfig.onResponse(false,
                                    context.getString(R.string.msg_network_error), null);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        responseString = responseString.trim();
                        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
                        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
                        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
                        if (errorCode != Constant.SUCCESS) {
                            if (listenerGetConfig != null) {
                                listenerGetConfig.onResponse(true, message, null);
                            }
                            return;
                        }
                        Gson gson = new Gson();
                        String result = JsonParser.Companion.getString(jsonObject, Constant.RESULT);
                        ItemInteractiveConfig item = null;
                        try {
                            item = gson.fromJson(result, ItemInteractiveConfig.class);
                        } catch (JsonSyntaxException e) {
                            e.printStackTrace();
                        }

                        if (listenerGetConfig != null) {
                            if (item != null) {
//                                item.setTime_period(20);
                                if (item.getTime_period() != null) {
                                    item.setTime_period(item.getTime_period() * 1000);
                                }
                            }
                            itemInteractiveConfig = item;
                            listenerGetConfig.onResponse(true, message, item);
                            isGetConfigFirst = true;
//                            if (timePeriodMsInteractive <= 0 || listenerCheckIsInteractive == null) {
//                                return;
//                            }
//                            if (item != null && Constant.TYPE_VISIBLE.equals(item.getTypeShowHome()) && Constant.TYPE_VISIBLE.equals(item.getTypeShowDetail())) {
//                                startTimerRequestCheckIsInteractive(context, listenerCheckIsInteractive, timePeriodMsInteractive);
//                            } else {
//                                if (timer != null) {
//                                    timer.cancel();
//                                }
//                                listenerCheckIsInteractive.onResponse(false, "0", null);
//                            }
                        }
                    }
                });
    }

    public static void getIsInteractive(Context context,
                                        OnGetConfigInteractiveListener listener) {
        if (context == null) {
            return;
        }
        MyHttpRequest myHttpRequest = new MyHttpRequest(context);
        String api = Constant.API_CHECK_INTERACTIVE_SHOW;
        RequestParams requestParams = new RequestParams();
//        requestParams.put("appid", appId);
        myHttpRequest.request(false, api, null,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (listener != null) {
                            listener.onResponse(false,
                                    context.getString(R.string.msg_network_error), null);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (listener != null) {
                            listener.onResponse(true,
                                    responseString, null);
                        }
                    }
                });
    }

    public static Timer timer, timerGetConfig;
    public static ItemInteractiveConfig itemInteractiveConfig;

    public static void startTimerRequestCheckIsInteractive(Context context, OnGetConfigInteractiveListener listener, int timePeriodMs) {
        if (timer != null) {
            timer.cancel();
        }
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    getIsInteractive(context, listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, timePeriodMs);
    }

    public static void startTimerRequestConfig(@NonNull Context context, String appId,
                                               OnGetConfigInteractiveListener listenerGetConfig, OnGetConfigInteractiveListener listenerCheckIsInteractive, int timePeriodMsGetConfig, int timePeriodMsInteractive) {
        stopRequestConfig();
        InteractiveConstant.CHANEL_ID = appId;
        timerGetConfig = new Timer();
        if (timePeriodMsGetConfig > 0) {
            timerGetConfig.schedule(new TimerTask() {
                @Override
                public void run() {
                    try {
                        Loggers.e("TIME_REQUEST", timePeriodMsGetConfig);
                        getConfigInteractive(context, appId, listenerGetConfig, listenerCheckIsInteractive, timePeriodMsInteractive);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, 0, timePeriodMsGetConfig);
        }
//        else if (timePeriodMsGetConfig == 0){
//            try {
//                getConfigInteractive(context, appId, listenerGetConfig, listenerCheckIsInteractive, timePeriodMsInteractive);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
    }

    private static int timeRequestConfig;

    public static void startTimerRequestConfig(@NonNull Context context, String appId,
                                               OnGetConfigInteractiveListener listenerGetConfig, int timePeriodMsGetConfig) {
        timeRequestConfig = timePeriodMsGetConfig;
        startTimerRequestConfig(context, appId, new OnGetConfigInteractiveListener() {
            @Override
            public void onResponse(boolean isSuccess, @Nullable String message, @Nullable ItemInteractiveConfig item) {
                if (listenerGetConfig != null) {
                    listenerGetConfig.onResponse(isSuccess, message, item);
                }

                if (item != null && item.getTime_period() != null && item.getTime_period() != timeRequestConfig) {
                    stopRequestConfig();
                    if (item.getTime_period() >= 0)
                        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                restartConfig(item);
                            }
                        }, item.getTime_period());

                }
            }

            private void restartConfig(@NotNull ItemInteractiveConfig item) {
                startTimerRequestConfig(getApplicationContext(), appId, this, item.getTime_period());
            }
        }, null, timePeriodMsGetConfig, 0);
    }

    public static void stopRequestConfig() {
        try {
            if (timerGetConfig != null) {
                timerGetConfig.cancel();
                timerGetConfig.purge();
                timerGetConfig = null;
            }
            if (timer != null) {
                timer.cancel();
                timer.purge();
                timer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isShowMainInteractiveFragment() {
        if (itemInteractiveConfig != null && Constant.TYPE_VISIBLE.equals(itemInteractiveConfig.getTypeShowDetail())) {
            return true;
        } else return false;
    }

    public static boolean isEnableShowIconHome() {
        if (itemInteractiveConfig != null && Constant.TYPE_VISIBLE.equals(itemInteractiveConfig.getTypeShowDetail()) && Constant.TYPE_VISIBLE.equals(itemInteractiveConfig.getTypeShowHome())) {
            return true;
        } else return false;
    }

    public static boolean isShowIcon = true, isShowInteractiveNotify, isGetConfigFirst;
    public static boolean isHidebyButtonClose = false;
    private static long TIME_DELAY_CLOSE = 60000;
    private static String urlImg;

    public static void setConfig(Activity activity, String appId, ViewGroup layoutParent, int timePeriodMsGetConfig, OnGetConfigInteractiveListener onGetConfigInteractiveListener, OnRequireShowMainInteractiveListener onRequireShowMainInteractiveListener) {
        if (activity == null || activity.isDestroyed() || activity.isFinishing()) {
            return;
        }
        Intent intent = new Intent(activity, OnClearFromRecentService.class);
        activity.startService(intent);
        View layoutIcon = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_icon_home_interactive, layoutParent, false);
        RelativeLayout layoutStickerInteractive = layoutIcon.findViewById(R.id.layoutStickerInteractive);
        ImageView imageSticker = layoutIcon.findViewById(R.id.imageStickerTT);
        ImageView buttonClose = layoutIcon.findViewById(R.id.buttonCLoseTT);
        imageSticker.setOnClickListener(v -> {
            if (onRequireShowMainInteractiveListener != null) {
                isShowInteractiveNotify = false;
                onRequireShowMainInteractiveListener.onRequireShowMainInteractive();
            }
        });
        buttonClose.setOnClickListener(v -> {
            layoutStickerInteractive.setVisibility(View.GONE);
            isHidebyButtonClose = true;
            new Handler(Looper.getMainLooper()).postDelayed(() -> isHidebyButtonClose = false, TIME_DELAY_CLOSE);
        });
        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            layoutParent.addView(layoutIcon);
        }, 200);
        ServiceUtilTT.startTimerRequestConfig(getApplicationContext(), appId, new OnGetConfigInteractiveListener() {
            @Override
            public void onResponse(boolean isSuccess, @org.jetbrains.annotations.Nullable String message, @org.jetbrains.annotations.Nullable ItemInteractiveConfig item) {
                if (activity.isDestroyed() || activity.isFinishing() || !isSuccess) {
                    return;
                }
                activity.runOnUiThread(() -> {
                    if (isShowInteractiveNotify && onRequireShowMainInteractiveListener != null) {
                        isShowInteractiveNotify = false;
                        onRequireShowMainInteractiveListener.onRequireShowMainInteractive();
                    }
                    setShowIcon(activity);
                    onGetConfigInteractiveListener.onResponse(isSuccess, message, item);

                });

            }
        }, timePeriodMsGetConfig);
    }

    public static void setShowIcon(Activity activity) {
        if (activity == null || activity.isDestroyed() || activity.isFinishing() || activity.findViewById(R.id.layoutStickerInteractive) == null) {
            return;
        }
        if (ServiceUtilTT.isEnableShowIconHome() && !ServiceUtilTT.itemInteractiveConfig.getIcon().equals(urlImg)) {
            urlImg = ServiceUtilTT.itemInteractiveConfig.getIcon();
            try {
                MyApplication.getInstance().loadImage(activity, (ImageView) activity.findViewById(R.id.imageStickerTT), urlImg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (ServiceUtilTT.isEnableShowIconHome() && isShowIcon && !isHidebyButtonClose) {
            activity.findViewById(R.id.layoutStickerInteractive).setVisibility(View.VISIBLE);
        } else {
            activity.findViewById(R.id.layoutStickerInteractive).setVisibility(View.GONE);
        }
    }

    public static void setShowIcon(Activity activity, boolean isShow) {
        isShowIcon = isShow;
        setShowIcon(activity);
    }
}