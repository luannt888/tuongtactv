package vn.mediatech.istudiocafe.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.ActivityInfo
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.view.View
import android.view.WindowInsetsController
import android.webkit.WebChromeClient
import android.widget.FrameLayout

@Suppress("DEPRECATION")
@SuppressLint("SourceLockedOrientationActivity")
class MyChromeClient(private val context: Activity) : WebChromeClient() {
    private var customView: View? = null
    private var customViewCallback: CustomViewCallback? = null
    private var originalOrientation = 0
    private var originalSystemUiVisibility = 0
    override fun getDefaultVideoPoster(): Bitmap? {
        return if (customView == null) {
            super.getDefaultVideoPoster()
        } else BitmapFactory.decodeResource(context.resources, 2130837573)
    }

    override fun onHideCustomView() {
        (context.window.decorView as FrameLayout).removeView(customView)
        customView = null
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            context.window.decorView.setSystemUiVisibility(originalSystemUiVisibility)
        } else {
            context.window.setDecorFitsSystemWindows(true)
            context.window.insetsController!!.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_SWIPE
        }
        context.requestedOrientation = originalOrientation
        customViewCallback!!.onCustomViewHidden()
        customViewCallback = null
        context.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onHideCustomView()
    }

    override fun onShowCustomView(view: View, callback: CustomViewCallback) {
        if (customView != null) {
            onHideCustomView()
            return
        }
        customView = view
        originalSystemUiVisibility = context.window.decorView.systemUiVisibility
        originalOrientation = context.requestedOrientation
        customViewCallback = callback
        (context.window.decorView as FrameLayout).addView(customView, FrameLayout.LayoutParams(-1, -1))
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            context.window.decorView.setSystemUiVisibility(3846 or View.SYSTEM_UI_FLAG_LAYOUT_STABLE)
        } else {
            context.window.setDecorFitsSystemWindows(false)
            context.window.insetsController!!.systemBarsBehavior = WindowInsetsController.BEHAVIOR_SHOW_BARS_BY_SWIPE
        }
        context.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
        super.onShowCustomView(view, callback)
    }
}