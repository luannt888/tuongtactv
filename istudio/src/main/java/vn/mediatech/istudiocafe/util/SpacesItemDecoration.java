package vn.mediatech.istudiocafe.util;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;
    private int spanCount;
    private boolean isHorizoltalListview;

    public SpacesItemDecoration(int space, int numberColumn, boolean isHorizoltalListview) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = isHorizoltalListview;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    public SpacesItemDecoration(int space, int numberColumn) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = false;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
        if (spanCount == 1) {
            if (isHorizoltalListview) {
                if (position == 0) {
                    outRect.left = space;
                }
                outRect.right = space;
                outRect.top = space;
                outRect.bottom = space;
            } else {//Listview 1 column
                if (position == 0) {
                    outRect.top = space;
                }
                outRect.left = space;
//                outRect.bottom = space / 2;
                outRect.bottom = space;
                outRect.right = space;
            }
            return;
        }
        if (!isHorizoltalListview) {
//            if (position >= 0) {
            int column = position % spanCount; // item column
            outRect.left = space - column * space / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * space / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
            if (position < spanCount) { // top edge
                outRect.top = space;
            }
            outRect.bottom = space; // item bottom
//            }
        } else {
//            if (position >= 0) {
            int column = position % spanCount; // item column
            if (position <= spanCount - 1) {
                outRect.left = space;
                outRect.right = space / spanCount;
            } else {
                outRect.left = space / spanCount;
                outRect.right = space / spanCount;
            }
            outRect.bottom = space;
        }
    }
}