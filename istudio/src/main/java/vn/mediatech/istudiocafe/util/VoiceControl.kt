package vn.mediatech.istudiocafe.util

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.media.AudioManager
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.google.gson.Gson
import vn.go.utility.fragment.calendar.CalendarFragment
import vn.go.utility.fragment.covid.CovidVnFragment
import vn.go.utility.fragment.currency.CurrencyFragment
import vn.go.utility.fragment.gold.GoldFragment
import vn.go.utility.fragment.weather.WeatherFragment
import vn.go.utility.listener.OnLifeCycleListener
import vn.go.utility.model.ItemCategory
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.activity.SplashActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.voicecontrol.adapter.ItemUtilityAdapter
import vn.mediatech.voicecontrol.listener.OnClickListener
import vn.mediatech.voicecontrol.listener.OnDoTaskListener
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.voicecontrol.listener.RecognitionCallback
import vn.mediatech.voicecontrol.model.ItemCase
import vn.mediatech.voicecontrol.model.ItemCaseAnswer
import vn.mediatech.voicecontrol.model.ItemUtility
import vn.mediatech.voicecontrol.voiceControl.*

class VoiceControl(
    val activity: FragmentActivity,
    val isFirstInit: Boolean = false,
    val oniTVListener: OnITVListener? = null,
    val iTVHeightInPx: Int? = 0,
    val isStatusBarMargin: Boolean = false
) {
    var layoutIconVoice: View? = null
    var msg: String? = null
    var itemCaseList: ArrayList<ItemCase>? = null
    var voiceControlFragment: VoiceControlFragment? = null

    companion object {
        const val TYPE_TAB_HOME = 1
        const val TYPE_TAB_NEWS = 2
        const val TYPE_TAB_TV = 3
        const val TYPE_TAB_RADIO = 4
        const val TYPE_TAB_VIDEO = 5
        const val TYPE_TAB_AUDIO = 6
        const val TYPE_SEARCH = 7
    }

    var onResultListener: VoiceControllerManager.OnResultListener? =
        object : VoiceControllerManager.OnResultListener {
            override fun onPartialResults(str: String?) {
            }

            override fun onResult(str: String?) {
                navigationFollowVoiceResult(str, voiceControlFragment)
            }

            override fun onRmsChanged(p0: Float) {
            }
        }
    var onDoTaskListener = object : OnDoTaskListener {
        override fun onDoTask(contentId: String?, contentType: String?) {
            if (contentId == null) {
                navigationFollowVoiceResult(contentType, voiceControlFragment)
            } else {
                val itemNews = ItemNews()
                itemNews.id = contentId
                itemNews.contentType = contentType
                oniTVListener?.onGoDetailNews(itemNews)
            }
        }
    }
    var itemCase: ItemCase? = null
    fun initItemCaseList() {
        val list = ArrayList<ItemCaseAnswer>()
        if (list.size == 0) return
        /*list.add(
            ItemCaseAnswer(
                arrayOf("tương tác", "đấu giá", "truyền hình"),
                " Ok, Tôi sẽ mở ngay!",
                "tương tác"
            )
        )*/
        list.add(ItemCaseAnswer(arrayOf("tivi"), " Ok, Tôi sẽ mở ngay!", "tivi"))
        list.add(ItemCaseAnswer(arrayOf("radio"), " Ok, Tôi sẽ mở ngay!", "radio"))
        list.add(ItemCaseAnswer(arrayOf("video"), " Ok, Tôi sẽ mở ngay!", "video"))
//        list.add(ItemCaseAnswer(arrayOf("diễn đàn"), " Ok, Tôi sẽ mở ngay!", "diễn đàn"))
        list.add(ItemCaseAnswer(arrayOf("tin tức", "news"), " Ok, Tôi sẽ mở ngay!", "tin tức"))
        itemCase = ItemCase(
            "2",
            "Hiện đang có một số chương trình sau: <br/>1. Tivi<br/>2. Radio <br/>3. Video <br/>4. Tin tức <br/> Bạn có muốn xem gì không?",
            list, null, null
        )
        val gson = Gson()
        val str = gson.toJson(itemCase)
        Log.e("GSON", str)
        if (itemCase != null) {
            showVoiceControlFragment(itemCase)
            itemCase = null
            return
        }
        if (itemCaseList != null) {
            return
        }
        itemCaseList = ArrayList()
        /*itemCaseList?.add(
            ItemCase(
                "1",
                "Xin chào buổi sáng, đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tương tác"), null,
                "00:00",
                "11:30"
            )
        )*/
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào, sáng nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"), null,
                "00:00",
                "11:30"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào, sáng nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"), null,
                "00:00",
                "11:30"
            )
        )
        /*itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào,bạn ăn sáng chưa? Có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"), null,
                "00:00",
                "11:30"
            )
        )*/

        /* itemCaseList?.add(
             ItemCase(
                 "1",
                 "Chúc bạn có buổi trưa vui vẻ, đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                 createListItemCaseAnswerYesNo(
                     "tương tác"
                 ), null,
                 "11:30",
                 "15:00"
             )
         )*/
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào, trưa nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"), null,
                "11:30",
                "15:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào, trưa nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"), null,
                "11:30",
                "15:00"
            )
        )
        /*itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào,trưa nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"), null,
                "11:30",
                "15:00"
            )
        )*/

        /*itemCaseList?.add(
            ItemCase(
                "1",
                "Xin chào, chiều nay đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tương tác"),
                "15:00",
                "18:00"
            )
        )*/
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào, chiều nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"),
                "15:00",
                "18:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào, chiều nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"),
                "15:00",
                "18:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào, chiều nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"),
                "15:00",
                "18:00"
            )
        )

        /*itemCaseList?.add(
            ItemCase(
                "1",
                "Xin chào,tối nay đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tương tác"),
                "18:00",
                "23:59"
            )
        )*/
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào,tối nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"),
                "18:00",
                "23:59"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào,tối nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"),
                "18:00",
                "23:59"
            )
        )
        /*itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào,tối nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"),
                "18:00",
                "23:59"
            )
        )*/

        itemCaseList?.shuffle()
        for (item in itemCaseList!!) {
            if (VcUtils.isInTime(item.timeStartHHmm, item.timeEndHHmm)) {
                showVoiceControlFragment(item)
                return
            }
        }
    }

    private fun createListItemCaseAnswerYesNo(
        key: String? = null,
        contentId: String? = null,
        contentType: String? = null
    ): ArrayList<ItemCaseAnswer> {
        val itemCaseAnswerList: ArrayList<ItemCaseAnswer> = ArrayList()
        itemCaseAnswerList.add(
            ItemCaseAnswer(
                arrayOf("có", "ok", "đồng ý", "mở", "yes"),
                key, "Ok,Tôi sẽ mở ngay!"
            )
        )
        itemCaseAnswerList.add(
            ItemCaseAnswer(
                arrayOf("không", "bỏ", "huỷ", "cancel"),
                null,
                "Ok, xin cảm ơn!"
            )
        )
        return itemCaseAnswerList
    }

    val UTILITY_VOICE_CONTROL = 0
    val UTILITY_COVID = 1
    val UTILITY_WEATHER = 2
    val UTILITY_CALENDAR = 3
    val UTILITY_GOLD = 4
    val UTILITY_CURRENCY = 5

    //    val UTILITY_PETROL = 6
//    val UTILITY_BIKE = 7
//    val UTILITY_CAR = 8
//    val UTILITY_VIETLOT = 9
    private val itemUtilityList = ArrayList<ItemUtility>()
    private val itemUtilityInsideList = ArrayList<ItemUtility>()
    fun initVoiceManger() {
        if (!VcUtils.ENABLE_VOICE_CONTROLLER) {
            return
        }
        if (!MyRequestPermissions.hasPermissions(
                activity,
                arrayOf(Manifest.permission.RECORD_AUDIO)
            )
        ) {
            MyRequestPermissions.requestPermission(
                activity,
                arrayOf(Manifest.permission.RECORD_AUDIO),
                object : MyRequestPermissions.OnMyRequestPermissionListener {
                    override fun onPermissionGranted() {
                        initVoiceManger()
                    }

                    override fun onPermissionDenied() {
                    }

                    override fun onError() {
                    }
                })
            return
        }
        val countShowMsg = VcUtils.getInt(activity, "MSG")
        var isClickIcon = false
        if (isFirstInit && countShowMsg < 1) {
            msg =
                "Xin chào! Vui lòng click icon iTV và giữ nguyên 3 giây để điều khiển bằng giọng nói!"
        }
        val count = countShowMsg + 1
        VcUtils.saveInt(activity, count, "MSG")
        if (layoutIconVoice != null) {
            return
        }
        var msgVoice: String? = null
        if (!msg.isNullOrEmpty()) {
            msgVoice =
                "Bạn hãy nói theo từ khoá gợi ý. Ví dụ bạn muốn xem tivi hãy nói tivi khi icon micro xanh và có tín hiệu chạy. Cảm ơn"
        }

        itemUtilityList.add(
            ItemUtility(
                UTILITY_COVID,
                "",
                R.drawable.ic_util_covid,
                "Số liệu Covid-19",
                "Covid-19"
            )
        )
        itemUtilityList.add(
            ItemUtility(
                UTILITY_WEATHER,
                "",
                R.drawable.ic_util_weather,
                "Thời tiết"
            )
        )
        itemUtilityList.add(
            ItemUtility(
                UTILITY_CALENDAR,
                "",
                R.drawable.ic_util_calendar,
                "Lịch vạn niên",
                "Lịch Việt"
            )
        )
        itemUtilityList.add(ItemUtility(UTILITY_GOLD, "", R.drawable.ic_util_gold, "Giá vàng"))
        itemUtilityList.add(
            ItemUtility(
                UTILITY_CURRENCY,
                "",
                R.drawable.ic_util_currency,
                "Ngoại tệ"
            )
        )
        itemUtilityList.add(
            ItemUtility(
                UTILITY_VOICE_CONTROL,
                "",
                R.drawable.bg_ic_utility_voice_vc,
                "Điều khiển bằng giọng nói",
                "iTV"
            )
        )

        itemUtilityInsideList.addAll(itemUtilityList)
        itemUtilityInsideList.removeAt(itemUtilityInsideList.size - 1)

        layoutIconVoice = VoiceControllerManager.showIconMenuVoice(
            activity,
            msg,
            56F,
            10000L,
            object : OnClickListener {
                override fun onClick() {
                }

                override fun onLongClick() {
                    showUtilityVertical(false)
                    showVoiceControlFragment(true, msgVoice)
                    if (!msgVoice.isNullOrEmpty()) {
                        isClickIcon = true
                        msgVoice = null
                    }
                }
            }, true, itemUtilityList, object : ItemUtilityAdapter.OnItemClickListener {
                override fun onClick(itemObject: ItemUtility, position: Int) {
                    Log.e("UTILITY", "layoutIconVoice onClick" + position + "/" + itemObject.title)
                    clickItemUtility(itemObject)
                }

                override fun onClickOut(itemObject: ItemUtility, position: Int) {
                }
            })
        if (VoiceControllerManager.isShowing) {
            VoiceControllerManager.isShowing = false
            showVoiceControlFragment(false)
        }
        if (isFirstInit) {
            if (!msg.isNullOrEmpty()) {
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        if (!isClickIcon) {
                            initItemCaseList()
                        }
                    }, 10000
                )
                return
            }
            if (!isClickIcon) {
                initItemCaseList()
            }
        }
    }

    private fun clickItemUtility(itemObject: ItemUtility) {
        val onLifeCycleListener = object : OnLifeCycleListener {
            override fun onResume() {
                oniTVListener?.onUtilShowing(true)
                oniTVListener?.onPlayPlayer(false)
            }

            override fun onStop() {
                oniTVListener?.onUtilShowing(false)
            }

            override fun onDetach() {
                oniTVListener?.onPlayPlayer(true)
            }
        }
        showUtilityVertical(false)
        oniTVListener?.onPlayPlayer(false)
        oniTVListener?.onUtilShowing(true)
        val frameLayout: FrameLayout =
            activity.findViewById(R.id.frameLayoutITV) ?: return
        val containerId = R.id.frameLayoutITV
        voiceControlFragment?.dismiss()
        val itemCategory = ItemCategory(itemObject.id, itemObject.id, itemObject.title, null, null)
        when (itemObject.id) {
            UTILITY_COVID -> {
                itemCategory.name = "Thống kê số liệu Covid"
                val fragment = CovidVnFragment.newInstance(itemCategory, isStatusBarMargin)
                fragment.onLifeCycleListener = onLifeCycleListener
                showFragment(activity, containerId, fragment)
            }
            UTILITY_WEATHER -> {
                val fragment = WeatherFragment.newInstance(itemCategory, isStatusBarMargin)
                fragment.onLifeCycleListener = onLifeCycleListener
                showFragment(activity, containerId, fragment)
            }
            UTILITY_CALENDAR -> {
                val fragment = CalendarFragment.newInstance(itemCategory, null, isStatusBarMargin)
                fragment.onLifeCycleListener = onLifeCycleListener
                showFragment(activity, containerId, fragment)
            }
            UTILITY_GOLD -> {
                val fragment = GoldFragment.newInstance(itemCategory, isStatusBarMargin)
                fragment.onLifeCycleListener = onLifeCycleListener
                showFragment(activity, containerId, fragment)
            }
            UTILITY_CURRENCY -> {
                val fragment = CurrencyFragment.newInstance(itemCategory, isStatusBarMargin)
                fragment.onLifeCycleListener = onLifeCycleListener
                showFragment(activity, containerId, fragment)
            }
            UTILITY_VOICE_CONTROL -> {
                showVoiceControlFragment(true)
            }
            else -> {
                Toast.makeText(
                    activity,
                    "Hiện chưa có tính năng này!",
                    Toast.LENGTH_SHORT
                ).show()
                oniTVListener?.onUtilShowing(false)
            }
        }
    }

    var currentFragment: Fragment? = null
    fun showFragment(
        activity: FragmentActivity,
        containerId: Int,
        fragment: Fragment
    ) {
        voiceControlFragment?.dismiss()
        if (currentFragment != null) {
            removeFragment(activity, currentFragment!!)
        }
        currentFragment = fragment
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(
            R.anim.fragment_right_to_left_util,
            R.anim.fragment_left_to_right_util,
            R.anim.fragment_left_to_right_util,
            R.anim.fragment_left_to_right_util
        )
        if (containerId > 0) {
            transaction.add(containerId, fragment, fragment::class.java.simpleName)
        } else {
            transaction.add(fragment, fragment::class.java.simpleName)
        }

        transaction.addToBackStack(null)
        transaction.commitAllowingStateLoss()
    }

    fun removeFragment(
        activity: FragmentActivity,
        fragment: Fragment
    ) {
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(
            R.anim.fragment_right_to_left_util,
            R.anim.fragment_left_to_right_util,
            R.anim.fragment_left_to_right_util,
            R.anim.fragment_left_to_right_util
        )
        transaction.remove(fragment)
        transaction.commitAllowingStateLoss()
    }

    val suggestList = arrayOf(
        "Trang chủ",
        "Home",
        "Tin tức", "Thời tiết", "Giá vàng", "Lịch", "Nổi bật", "Thời sự",
//            "Diễn đàn",
            "Tương tác",
            "Đấu giá",
            "Đấu giá truyền hình",
        "Truyền hình",
        "Video",
        "Tin tức",
            "Trang cá nhân",
            "Kho quà",
            "Voucher",
        "Tắt app",
        "Thoát app",
        "Back",
        "Quay lại",
        "Tăng âm lượng",
        "Giảm âm lượng",
        "Im lặng",
        "Mute"
    )

    fun showVoiceControlFragment(itemCase: ItemCase?) {
        if (!VcUtils.ENABLE_VOICE_CONTROLLER || itemCase == null) {
            return
        }
        oniTVListener?.onPlayPlayer(false)
        voiceControlFragment?.dismiss()
        voiceControlFragment =
            VoiceControlFragment.showFragment(
                activity,
                itemCase,
                suggestList,
                onResultListener,
                itemUtilityInsideList,
                object : ItemUtilityAdapter.OnItemClickListener {
                    override fun onClick(itemObject: ItemUtility, position: Int) {
                        clickItemUtility(itemObject)
                    }

                    override fun onClickOut(itemObject: ItemUtility, position: Int) {
                    }
                },
                true,
                true,
                0
            )
        voiceControlFragment?.onDoTaskListener = onDoTaskListener
        voiceControlFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onDismiss() {
                voiceControlFragment = null
                startListenContinuos()
            }

            override fun onShow() {
                kontinuousRecognitionManager?.destroyRecognizer()
                super.onShow()
            }
        }
    }

    fun showVoiceControlFragment(
        isSetListening: Boolean = false,
        msg: String? = null
    ) {
        if (!VcUtils.ENABLE_VOICE_CONTROLLER) {
            return
        }
        var message = msg
        if (message.isNullOrEmpty()) {
            message = getRandomString(
                arrayOf(
                    "Xin chào, iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?"
//                    ,
//                    "iTV xin nghe !",
//                    "iTV có thể hỗ trợ gì cho bạn?",
//                    "iTV lắng nghe!",
//                    "Tôi là iTV, rất vui được hỗ trợ cho bạn?"
                )
            )
        }
        oniTVListener?.onPlayPlayer(false)
        if (currentFragment != null) {
            removeFragment(activity, currentFragment!!)
        }

        voiceControlFragment = VoiceControlFragment
            .showFragment(
                activity,
                message,
                onResultListener,
                isSetListening,
                true, suggestList,
                itemUtilityInsideList,
                object : ItemUtilityAdapter.OnItemClickListener {
                    override fun onClick(itemObject: ItemUtility, position: Int) {
                        clickItemUtility(itemObject)
                        Log.e("UTILITY", "VCFragment onClick" + position + "/" + itemObject.title)
                    }

                    override fun onClickOut(itemObject: ItemUtility, position: Int) {
                    }
                }, iTVHeightInPx ?: 0
            )
        voiceControlFragment?.onDoTaskListener = onDoTaskListener
        voiceControlFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onDismiss() {
                oniTVListener?.onUtilShowing(false)
                voiceControlFragment = null
                startListenContinuos()
                oniTVListener?.onPlayPlayer(true)
            }

            override fun onShow() {
                oniTVListener?.onPlayPlayer(false)
                kontinuousRecognitionManager?.destroyRecognizer()
                super.onShow()
            }
        }
    }

    private fun getItemUtility(id: Int): ItemUtility? {
        for (itemObj in itemUtilityList) {
            if (itemObj.id == id) {
                return itemObj
            }
        }
        return null
    }

    private val iTVConfirmList =
        arrayOf(
            "iTV mở ngay đây",
            "Sau đây là kết quả",
            "iTV tìm thấy kết quả sau",
            "iTV tìm thấy thông tin sau",
            "OK. Có ngay đây"
        )

    @SuppressLint("RestrictedApi")
    private fun navigationFollowVoiceResult(
        str: String?,
        fragment: VoiceControlFragment?
    ) {
        if (str.isNullOrEmpty()) {
            return
        }
//        val key = GeneralUtils.convertNonAccentVietnamese(str).toString().lowercase()
        val key = str.lowercase()
        val iTVConfirm = getRandomString(iTVConfirmList)
        Loggers.e("VoiceControl", "key = $key")
        with(key) {
            when {
//                contains("tìm kiếm") -> {
//                    Loggers.e("VoiceControl", "keyword = $key")
//                    var keyword = key.replace("tìm kiếm tin tức", "")
//                    keyword = keyword.replace("tìm kiếm tin", "")
//                    keyword = keyword.replace("tìm kiếm", "")
//                    if (keyword.isNotEmpty()) {
//                        fragment?.speakWithDismiss(iTVConfirm, Runnable {
//                            oniTVListener?.onSwitchAppHandle(TYPE_SEARCH, keyword)
//                        })
//                    } else {
//                        Loggers.e("VoiceControl", "keyword empty")
//                        fragment?.speakWithTyping(
//                            getRandomString(
//                                arrayOf(
//                                    "Bạn có thể diễn đạt lại không?"
//                                )
//                            ), true
//                        )
//                    }
//                }
                //START: Utility
                contains("covid") || contains("corona") || contains("virus") || contains("vi rút") -> {
                    val itemObj = getItemUtility(UTILITY_COVID)
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        itemObj?.let {
                            clickItemUtility(it)
                        }
                    })
                }
                contains("thời tiết") || contains("nhiệt độ") || contains("độ ẩm") -> {
                    val itemObj = getItemUtility(UTILITY_WEATHER)
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        itemObj?.let {
                            clickItemUtility(it)
                        }
                    })
                }
                contains("lịch âm") || contains("lịch dương") || contains("lịch vạn niên") || contains(
                    "lịch việt"
                ) || contains("lịch hôm nay") || contains("lịch tháng") || contains("ngày âm") || contains(
                    "ngày dương"
                ) || contains("lịch 2021") || contains("lịch 2022") || contains("xem lịch") -> {
                    val itemObj = getItemUtility(UTILITY_CALENDAR)
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        itemObj?.let {
                            clickItemUtility(it)
                        }
                    })
                }
                contains("giá vàng") || contains("chỉ vàng") || contains("nhẫn vàng") || contains("sjc") || contains(
                    "doji"
                ) || contains("pnj") || contains("bảo tín minh châu") -> {
                    val itemObj = getItemUtility(UTILITY_GOLD)
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        itemObj?.let {
                            clickItemUtility(it)
                        }
                    })
                }
                contains("ngoại tệ") || contains("ngoại hối") || contains("tỷ giá") || contains("đô la") || contains(
                    "euro"
                ) || contains(
                    "bảng anh"
                ) -> {
                    val itemObj = getItemUtility(UTILITY_CURRENCY)
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        itemObj?.let {
                            clickItemUtility(it)
                        }
                    })
                }
                //END: Utility
                //START: HANDLE APP
                contains("trang chủ") || key.contains("home") -> {
                    MainActivity.typeShowNotify = Constant.TYPE_HOME
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        oniTVListener?.onSwitchAppHandle(TYPE_TAB_HOME, key)
                    })
                }
                contains("video") -> {
                    MainActivity.typeShowNotify = Constant.TYPE_VIDEO
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        oniTVListener?.onSwitchAppHandle(TYPE_TAB_VIDEO, key)
                    })
                }
                contains("tin tức") || key.contains("news") || key.contains("tin bài") || key.contains(
                    "bài viết"
                ) || key.contains("thời sự")
                -> {
                    MainActivity.typeShowNotify = Constant.TYPE_NEWS
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        oniTVListener?.onSwitchAppHandle(TYPE_TAB_NEWS, key)
                    })
                }
//                contains("diễn đàn") -> {
//                    MainActivity.typeShowNotify = Constant.TYPE_FORUM
//                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
//                        oniTVListener?.onSwitchAppHandle(TYPE_TAB_NEWS, key)
//                    })
//                }
                contains("đấu giá") || key.contains("truyền hình") || key.contains("tương tác") -> {
                    MainActivity.typeShowNotify = Constant.TYPE_INTERACTIVE
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        oniTVListener?.onSwitchAppHandle(TYPE_TAB_NEWS, key)
                    })
                }
                contains("tài khoản") || key.contains("cá nhân") || key.contains("personal")
                -> {
                    MainActivity.typeShowNotify = Constant.TYPE_PERSONAL
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        oniTVListener?.onSwitchAppHandle(TYPE_TAB_NEWS, key)
                    })
                }
                contains("kho quà") || key.contains("quà") || key.contains("voucher")
                        || key.contains("vốt chơ") || key.contains("vâu chờ")
                -> {
                    fragment?.speakWithDismiss(iTVConfirm, Runnable {
                        GeneralUtils.openMyVoucher(activity)
                    })
                }
                //END: HANDLE APP

                //START: OTHER
                contains("trở lại") || contains("trở về") || contains("quay lại") || contains("quay về") || contains(
                    "back"
                ) -> {
//                    activity.onBackPressed()
//                    activity.dispatchKeyEvent(KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_BACK))
                    fragment?.dismiss()
                }
                contains("kết thúc") || contains("tắt áp") || contains("tắt app") || contains("close app") || contains(
                    "tắt máy"
                ) || contains("thoát áp") || contains("thoát app") || contains("thoát ứng dụng") || contains(
                    "đóng ứng dụng"
                ) || contains("đóng app") || contains("đóng ứng dụng")
                -> {
                    fragment?.dismiss()
                    activity.finishAndRemoveTask();
                }
                contains("ẩn áp") || contains("ẩn app") || contains("hide app") || contains("hide áp") || contains(
                    "màn hình chính"
                ) -> {
                    activity.startActivity(Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
                    fragment?.dismiss()
                    oniTVListener?.onPlayPlayer(true)
                }
                contains("tăng âm lượng") || contains("tăng tiếng") || contains("to lên") || contains(
                    "mở tiếng"
                ) || contains("to tiếng") -> {
                    val audioManager: AudioManager =
                        activity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
                    audioManager.adjustVolume(
                        AudioManager.ADJUST_RAISE,
                        AudioManager.FLAG_PLAY_SOUND
                    );
                    fragment?.speakWithTyping("Đã tăng âm lượng", true)
                }
                contains("giảm âm lượng") || contains("nhỏ âm lượng") || contains("nhỏ đi") || contains(
                    "giảm tiếng"
                ) || contains("nhỏ tiếng") -> {
                    val audioManager: AudioManager =
                        activity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
                    audioManager.adjustVolume(
                        AudioManager.ADJUST_LOWER,
                        AudioManager.FLAG_PLAY_SOUND
                    );
                    fragment?.speakWithTyping("Đã giảm âm lượng", true)
                }
                contains("im lặng") || contains("câm") || contains("mute") || contains("tắt tiếng") || contains(
                    "tắt âm"
                ) -> {
                    val audioManager: AudioManager =
                        activity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
                    audioManager.adjustVolume(AudioManager.ADJUST_MUTE, AudioManager.FLAG_SHOW_UI);
                    fragment?.speakWithTyping("Đã tắt âm thanh", true)
                }
                contains("tắt trợ lý") || contains("tắt chạy ngầm itv") || contains(
                    "stop itv"
                ) || contains("tạm dừng itv") || contains("tắt chạy ngầm") || contains("tắt itv chạy ngầm") || contains(
                    "đóng chạy ngầm"
                ) || contains("đóng itv chạy ngầm") -> {
                    VcUtils.setListeningContinuousBackground(activity, false)
                    fragment?.speakWithTyping("Bạn đã tắt tính năng \" iTV Chạy ngầm\".", true)
                }
                contains("bật trợ lý") || contains("bật chạy ngầm iTV") || contains("bật itv") || contains(
                    "mở itv"
                ) || contains("mở trợ lý") || contains("bật chạy ngầm") || contains("itv chạy ngầm") || contains(
                    "mở chạy ngầm"
                ) -> {
                    VcUtils.setListeningContinuousBackground(activity, true)
                    fragment?.speakWithTyping(
                        "Bạn đã bật tính năng \" iTV Chạy ngầm\". Khi ẩn giao diện điều khiển bạn chỉ cần nói \"iTV\" thì giao diện điều khiển sẽ hiện lên.",
                        true
                    )
                }
                contains("bật tự động nghe") || contains("nghe tự động") || contains("nghe liên tục") || contains(
                    "bật trò chuyện"
                ) || contains("bật luôn nghe") -> {
                    VcUtils.setListeningAuto(activity, true)
                    fragment?.speakWithTyping(
                        "Bạn đã bật tính năng \"Tự động nghe\". Chế độ này giúp iTV trò chuyện liên tục với bạn cho đến khi hiển thị kết quả. iTV có thể giúp gì cho bạn?",
                        true
                    )
                }
                contains("tắt tự động nghe") || contains("tắt nghe tự động") || contains("tắt nghe liên tục") || contains(
                    "tắt trò chuyện"
                ) || contains("tắt luôn nghe") || contains("đóng nghe tự động") || contains("đóng tự động nghe") -> {
                    VcUtils.setListeningAuto(activity, false)
                    fragment?.speakWithTyping(
                        "Bạn đã tắt tính năng \"Tự động nghe\". Hãy nhấn biểu tượng mic ở phía trên bên trái màn hình để ra lệnh cho iTV nhé!",
                        true
                    )
                }
                //END: OTHER
                else -> {
                    fragment ?: return
                    if (fragment.handResultCase(
                            key,
                            arrayOf("xin chào", "chào", "chào bạn", "hello"),
                            "Xin chào, iTV có thể giúp gì cho bạn?"
                        )
                    ) {
                        return
                    }
                    if (fragment.handResultCase(
                            key,
                            arrayOf("cà phê", "sữa", "coffee"),
                            "Không có rồi. Bạn ra cửa hàng mua nhé!"
                        )
                    ) {
                        return
                    }
                    if (fragment.handResultCase(
                            key,
                            arrayOf("lui", "ẩn", "biến", "cút", "bye", "tắt itv"),
                            getRandomString(
                                arrayOf(
//                                "Vâng, cảm ơn Bạn",
//                                "Ok",
                                    "Bạn cần hỗ trợ gì hãy gọi iTV nhé!"
                                )
                            ), false, true
                        )
                    ) {
                        return
                    }
                    if (fragment.handResultCase(
                            key,
                            arrayOf("atv", "itv"),
                            getRandomString(
                                arrayOf(
                                    "iTV có thể giúp gì cho bạn?",
                                    "iTV có thể hỗ trợ gì cho bạn?"
                                )
                            ), true
                        )
                    ) {
                        return
                    }
                    fragment.speakWithTyping(
                        getRandomString(
                            arrayOf(
                                "Yêu cầu này chưa được thực hiện. Thử lại từ khoá khác nhé!",
                                "Bạn có thể diễn đạt lại không?"
                            )
                        ), true
                    )
                }
            }
        }

    }

    fun hideIconVoiceController() {
        layoutIconVoice?.visibility = View.INVISIBLE
    }

    fun showIconVoiceController() {
        layoutIconVoice?.visibility = View.VISIBLE
    }

    var kontinuousRecognitionManager
            : KontinuousRecognitionManager? = null

    fun startListenContinuos() {
        if (activity is SplashActivity || !VcUtils.ENABLE_VOICE_CONTROLLER) {
            return
        }
        if (!VcUtils.isListeningBackground(activity)) {
            kontinuousRecognitionManager?.muteRecognition(false)
            return
        }
        if (voiceControlFragment != null) {
            return
        }
        kontinuousRecognitionManager?.destroyRecognizer()
        kontinuousRecognitionManager = KontinuousRecognitionManager(
            activity,
            arrayOf("itv", "atv"),
            false,
            object : RecognitionCallback {
                override fun onKeywordDetected() {
                    val str = getRandomString(
                        arrayOf(
//                            "iTV xin nghe!",
                            "Xin chào, iTV có thể giúp gì cho bạn?",
                            "iTV có thể giúp gì cho bạn?",
                            "iTV có thể giúp gì cho bạn?",
                            "iTV có thể giúp gì cho bạn?"
                        )
                    )
                    activity.runOnUiThread { showVoiceControlFragment(true, str) }
                }
            })
        kontinuousRecognitionManager?.createRecognizer()
        kontinuousRecognitionManager?.startRecognition()
    }

    fun pauseVoiceControl() {
        kontinuousRecognitionManager?.destroyRecognizer()
        VoiceControllerManager.instance?.stopSpeak()
    }

    fun getRandomString(arr: Array<String>?): String? {
        if (arr.isNullOrEmpty()) {
            return null
        }
        val list = arr.toCollection(ArrayList())
        list.shuffle()
        return list[0]
    }

    private fun showUtilityVertical(isShow: Boolean = true) {
        val visibilityInt = if (isShow) View.VISIBLE else View.GONE
        layoutIconVoice?.findViewById<View>(R.id.recyclerViewMenu)?.visibility = visibilityInt
        layoutIconVoice?.findViewById<View>(R.id.viewOverlay)?.visibility = visibilityInt
    }

    fun removeUtilityFragment(): Boolean {
        if (currentFragment != null) {
            removeFragment(activity, currentFragment!!)
            return true
        }
        return false
    }

    interface OnITVListener {
        fun onGoDetailNews(itemObj: ItemNews)
        fun onPlayPlayer(isPlay: Boolean)
        fun onSwitchAppHandle(type: Int, keyword: String?)
        fun onUtilShowing(isShowing: Boolean)
    }
}
/*
class VoiceControl(
    val activity: FragmentActivity,
    val isFirstInit: Boolean = false,
    val oniTVListener: OniTVListener? = null
) {
    var layoutIconVoice: View? = null
    var msg: String? = null
    var itemCaseList: ArrayList<ItemCase>? = null
    var voiceControlFragment: VoiceControlFragment? = null

    companion object {
        const val TYPE_TAB_HOME = 1
        const val TYPE_TAB_NEWS = 2
        const val TYPE_TAB_TV = 3
        const val TYPE_TAB_RADIO = 4
        const val TYPE_TAB_VIDEO = 5
        const val TYPE_TAB_AUDIO = 6
        const val TYPE_SEARCH = 7
        const val TYPE_TAB_FORUM = 8
    }

    var onResultListener: VoiceControllerManager.OnResultListener? =
        object : VoiceControllerManager.OnResultListener {
            override fun onPartialResults(str: String?) {
            }

            override fun onResult(str: String?) {
                navigationFollowVoiceResult(str, voiceControlFragment)
            }

            override fun onRmsChanged(p0: Float) {
            }
        }
    var onDoTaskListener = object : OnDoTaskListener {
        override fun onDoTask(contentId: String?, contentType: String?) {
            if (contentId == null) {
                navigationFollowVoiceResult(contentType, voiceControlFragment)
            } else {
                val itemNews = ItemNews()
                itemNews.id = contentId
                itemNews.contentType = contentType
                oniTVListener?.onGoDetailNews(itemNews)
            }

        }
    }
    var itemCase: ItemCase? = null
    fun initItemCaseList() {
        val list = ArrayList<ItemCaseAnswer>()
        if (list.size == 0) return
        list.add(
            ItemCaseAnswer(
                arrayOf("tương tác", "đấu giá", "truyền hình"),
                " Ok, Tôi sẽ mở ngay!",
                "tương tác"
            )
        )
        list.add(ItemCaseAnswer(arrayOf("video"), " Ok, Tôi sẽ mở ngay!", "video"))
        list.add(ItemCaseAnswer(arrayOf("diễn đàn"), " Ok, Tôi sẽ mở ngay!", "diễn đàn"))
        list.add(ItemCaseAnswer(arrayOf("tin tức", "news"), " Ok, Tôi sẽ mở ngay!", "tin tức"))
        itemCase = ItemCase(
            "2",
            "Hiện đang có một số chương trình sau: <br/>1. Diễn đàn<br/>2. Tương tác <br/>3. Video <br/>4. Tin tức <br/> Bạn có muốn xem gì không?",
            list, null, null
        )
        val gson = Gson()
        val str = gson.toJson(itemCase)
        Log.e("GSON", str)
        if (itemCase != null) {
            showVoiceControlFragment(itemCase)
            itemCase = null
            return
        }
        if (itemCaseList != null) {
            return
        }
        itemCaseList = ArrayList()
        itemCaseList?.add(
            ItemCase(
                "1",
                "Xin chào buổi sáng, đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tương tác"), null,
                "00:00",
                "11:30"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào, sáng nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"), null,
                "00:00",
                "11:30"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào, sáng nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"), null,
                "00:00",
                "11:30"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào,bạn ăn sáng chưa? Có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"), null,
                "00:00",
                "11:30"
            )
        )


        itemCaseList?.add(
            ItemCase(
                "1",
                "Chúc bạn có buổi trưa vui vẻ, đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo(
                    "tương tác"
                ), null,
                "11:30",
                "15:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào, trưa nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"), null,
                "11:30",
                "15:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào, trưa nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"), null,
                "11:30",
                "15:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào,trưa nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"), null,
                "11:30",
                "15:00"
            )
        )

        itemCaseList?.add(
            ItemCase(
                "1",
                "Xin chào, chiều nay đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tương tác"),
                "15:00",
                "18:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào, chiều nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"),
                "15:00",
                "18:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào, chiều nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"),
                "15:00",
                "18:00"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào, chiều nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"),
                "15:00",
                "18:00"
            )
        )

        itemCaseList?.add(
            ItemCase(
                "1",
                "Xin chào,tối nay đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tương tác"),
                "18:00",
                "23:59"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "2",
                "Xin chào,tối nay có một số video nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("video"),
                "18:00",
                "23:59"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "3",
                "Xin chào,tối nay có một số tin tức nổi bật, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("tin tức"),
                "18:00",
                "23:59"
            )
        )
        itemCaseList?.add(
            ItemCase(
                "4",
                "Xin chào,tối nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
                createListItemCaseAnswerYesNo("diễn đàn"),
                "18:00",
                "23:59"
            )
        )

        Collections.shuffle(itemCaseList)
        for (item in itemCaseList!!) {
            if (VcUtils.isInTime(item.timeStartHHmm, item.timeEndHHmm)) {
                showVoiceControlFragment(item)
                return
            }
        }
    }

    private fun createListItemCaseAnswerYesNo(
        key: String? = null,
        contentId: String? = null,
        contentType: String? = null
    ): ArrayList<ItemCaseAnswer> {
        val itemCaseAnswerList: ArrayList<ItemCaseAnswer> = ArrayList()
        itemCaseAnswerList.add(
            ItemCaseAnswer(
                arrayOf("có", "ok", "đồng ý", "mở", "yes"),
                key, "Ok,Tôi sẽ mở ngay!"
            )
        )
        itemCaseAnswerList.add(
            ItemCaseAnswer(
                arrayOf("không", "bỏ", "huỷ", "cancel"),
                null,
                "Ok, xin cảm ơn!"
            )
        )
        return itemCaseAnswerList
    }

    val UTILITY_VOICE_CONTROL = 0
    val UTILITY_COVID = 1
    val UTILITY_WEATHER = 2
    val UTILITY_CALENDAR = 3
    val UTILITY_GOLD = 4
    val UTILITY_CURRENCY = 5

    //    val UTILITY_PETROL = 6
//    val UTILITY_BIKE = 7
//    val UTILITY_CAR = 8
//    val UTILITY_VIETLOT = 9
    val itemUtilityList = ArrayList<ItemUtility>()
    fun initVoiceManger() {
        if (!VcUtils.ENABLE_VOICE_CONTROLLER) {
            return
        }
        if (!MyRequestPermissions.hasPermissions(
                activity,
                arrayOf(Manifest.permission.RECORD_AUDIO)
            )
        ) {
            MyRequestPermissions.requestPermission(
                activity,
                arrayOf(Manifest.permission.RECORD_AUDIO),
                object : MyRequestPermissions.OnMyRequestPermissionListener {
                    override fun onPermissionGranted() {
                        initVoiceManger()
                    }

                    override fun onPermissionDenied() {
                    }

                    override fun onError() {
                    }
                })
            return
        }
        val countShowMsg = VcUtils.getInt(activity, "MSG")
        var isClickIcon = false
        if (isFirstInit && countShowMsg < 1) {
            msg =
                "Xin chào! Vui lòng click icon iTV và giữ nguyên 3 giây để điều khiển bằng giọng nói!"
        }
        val count = countShowMsg + 1
        VcUtils.saveInt(activity, count, "MSG")
        if (layoutIconVoice != null) {
            return
        }
        var msgVoice: String? = null
        if (!msg.isNullOrEmpty()) {
            msgVoice =
                "Bạn hãy nói theo từ khoá gợi ý. Ví dụ bạn muốn xem đấu giá truyền hình hãy nói truyền hình khi icon micro xanh và có tín hiệu chạy. Cảm ơn"
        }

        itemUtilityList.add(
            ItemUtility(
                UTILITY_COVID,
                "",
                R.drawable.ic_util_covid,
                "Số liệu Covid",
                "Covid"
            )
        )
        itemUtilityList.add(
            ItemUtility(
                UTILITY_WEATHER,
                "",
                R.drawable.ic_util_weather,
                "Thời tiết"
            )
        )
        itemUtilityList.add(
            ItemUtility(
                UTILITY_CALENDAR,
                "",
                R.drawable.ic_util_calendar,
                "Lịch vạn niên",
                "Lịch"
            )
        )
        itemUtilityList.add(ItemUtility(UTILITY_GOLD, "", R.drawable.ic_util_gold, "Giá vàng"))
        itemUtilityList.add(
            ItemUtility(
                UTILITY_CURRENCY,
                "",
                R.drawable.ic_util_currency,
                "Ngoại tệ"
            )
        )
        itemUtilityList.add(
            ItemUtility(
                UTILITY_VOICE_CONTROL,
                "",
                R.drawable.bg_ic_utility_voice_vc,
                "Điều khiển bằng giọng nói"
            )
        )

//        itemList.add(ItemUtility(UTILITY_CAR, "", R.drawable.ic_car, "Giá xe hơi"))
//        itemList.add(ItemUtility(UTILITY_PETROL, "", R.drawable.ic_petrol_station, "Xăng dầu"))
//        itemList.add(ItemUtility(UTILITY_RATES, "", R.drawable.ic_exchange_rates, "Tỉ giá"))
//        itemList.add(ItemUtility(UTILITY_VIETLOT, "", R.drawable.ic_vietlot, "Vietlot"))
//        itemList.add(
//            ItemUtility(
//                UTILITY_VOICE_CONTROL,
//                "",
//                R.drawable.bg_ic_utility_voice_vc,
//                "Điều khiển bằng giọng nói"
//            )
//        )
        layoutIconVoice = VoiceControllerManager.showIconMenuVoice(
            activity,
            msg,
            56F,
            10000L,
            object : OnClickListener {
                override fun onClick() {
                }

                override fun onLongClick() {
                    showUtilityVertical(false)
                    showVoiceControlFragment(true, msgVoice)
                    if (!msgVoice.isNullOrEmpty()) {
                        isClickIcon = true
                        msgVoice = null
                    }
                }
            }, true, itemUtilityList, object : ItemUtilityAdapter.OnItemClickListener {
                override fun onClick(itemObject: ItemUtility, position: Int) {
                    Log.e("UTILITY", "onClick" + position + "/" + itemObject.title)
                    clickItemUtility(itemObject)
                }

                override fun onClickOut(itemObject: ItemUtility, position: Int) {
                }
            })
        if (VoiceControllerManager.isShowing) {
            VoiceControllerManager.isShowing = false
            showVoiceControlFragment(false)
        }
        if (isFirstInit) {
            if (!msg.isNullOrEmpty()) {
                Handler(Looper.getMainLooper()).postDelayed(
                    {
                        if (!isClickIcon) {
                            initItemCaseList()
                        }
                    }, 10000
                )
                return
            }
            if (!isClickIcon) {
                initItemCaseList()
            }
        }
    }

    fun showUtilityVertical(isShow: Boolean = true) {
        val visibilityInt = if (isShow) View.VISIBLE else View.GONE
        layoutIconVoice?.findViewById<View>(R.id.recyclerViewMenu)?.visibility = visibilityInt
        layoutIconVoice?.findViewById<View>(R.id.viewOverlay)?.visibility = visibilityInt
    }

    private fun clickItemUtility(itemObject: ItemUtility) {
        oniTVListener?.onPlayPlayer(false)
        oniTVListener?.onUtilShowing(true)
        val frameLayout: FrameLayout =
            activity.findViewById(R.id.frameLayoutDialog) ?: return
        val containerId = R.id.frameLayoutDialog
        voiceControlFragment?.dismiss()
        val itemCategory = ItemCategory(itemObject.id, itemObject.id, itemObject.title, null, null)
        when (itemObject.id) {
            UTILITY_COVID -> {
                itemCategory.name = "Thống kê số liệu Covid"
                val fragment = CovidVnFragment.newInstance(itemCategory)
                fragment.onLifeCycleListener = object : OnLifeCycleListener {
                    override fun onDestroyView() {
                        oniTVListener?.onUtilShowing(false)
                    }
                }
                showFragment(activity, containerId, fragment)
            }
            UTILITY_WEATHER -> {
                val fragment = WeatherFragment.newInstance(itemCategory)
                fragment.onLifeCycleListener = object : OnLifeCycleListener {
                    override fun onDestroyView() {
                        oniTVListener?.onUtilShowing(false)
                    }
                }
                showFragment(activity, containerId, fragment)
            }
            UTILITY_CALENDAR -> {
                val fragment = CalendarFragment.newInstance(itemCategory)
                fragment.onLifeCycleListener = object : OnLifeCycleListener {
                    override fun onDestroyView() {
                        oniTVListener?.onUtilShowing(false)
                    }
                }
                showFragment(activity, containerId, fragment)
            }
            UTILITY_GOLD -> {
                val fragment = GoldFragment.newInstance(itemCategory)
                fragment.onLifeCycleListener = object : OnLifeCycleListener {
                    override fun onDestroyView() {
                        oniTVListener?.onUtilShowing(false)
                    }
                }
                showFragment(activity, containerId, fragment)
            }
            UTILITY_CURRENCY -> {
                val fragment = CurrencyFragment.newInstance(itemCategory)
                fragment.onLifeCycleListener = object : OnLifeCycleListener {
                    override fun onDestroyView() {
                        oniTVListener?.onUtilShowing(false)
                    }
                }
                showFragment(activity, containerId, fragment)
            }
            UTILITY_VOICE_CONTROL -> {
                showVoiceControlFragment(true)
            }
            else -> {
                Toast.makeText(
                    activity,
                    "Hiện chưa có tính năng này!",
                    Toast.LENGTH_SHORT
                ).show()
                oniTVListener?.onUtilShowing(false)
            }
        }
    }


    var currentFragment: Fragment? = null
    fun showFragment(
        activity: FragmentActivity,
        containerId: Int,
        fragment: Fragment
    ) {
        voiceControlFragment?.dismiss()
        if (currentFragment != null) {
            removeFragment(activity, currentFragment!!)
        }
        currentFragment = fragment
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(
            vn.go.utility.R.anim.fragment_right_to_left_util,
            vn.go.utility.R.anim.fragment_left_to_right_util,
            vn.go.utility.R.anim.fragment_left_to_right_util,
            vn.go.utility.R.anim.fragment_left_to_right_util
        )
        if (containerId > 0) {
            transaction.add(containerId, fragment, fragment::class.java.simpleName)
        } else {
            transaction.add(fragment, fragment::class.java.simpleName)
        }

        transaction.addToBackStack(null)
        transaction.commitAllowingStateLoss()
    }

    fun removeFragment(
        activity: FragmentActivity,
        fragment: Fragment
    ) {
        val transaction = activity.supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(
            vn.go.utility.R.anim.fragment_right_to_left_util,
            vn.go.utility.R.anim.fragment_left_to_right_util,
            vn.go.utility.R.anim.fragment_left_to_right_util,
            vn.go.utility.R.anim.fragment_left_to_right_util
        )
        transaction.remove(fragment)
        transaction.commitAllowingStateLoss()
    }

    fun removeUtilityFragment():Boolean {
        if(currentFragment== null){
            return false
        }
        removeFragment(activity, currentFragment!!)
        return true
    }

    val suggestList = arrayOf(
        "Trang chủ",
        "Home",
        "Tin tức", "Thời tiết", "Giá vàng", "Lịch", "Nổi bật", "Thời sự",
//            "Diễn đàn",
//            "Tương tác",
//            "Đấu giá",
//            "Đấu giá truyền hình",
        "Truyền hình",
        "Video",
        "Tin tức",
//            "Trang cá nhân",
//            "Kho quà",
//            "Voucher",
        "Tắt app",
        "Thoát app",
        "Back",
        "Quay lại",
        "Tăng âm lượng",
        "Giảm âm lượng",
        "Im lặng",
        "Mute"
    )

    fun showVoiceControlFragment(itemCase: ItemCase?) {
        if (!VcUtils.ENABLE_VOICE_CONTROLLER || itemCase == null) {
            return
        }
        oniTVListener?.onPlayPlayer(false)
        voiceControlFragment?.dismiss()
        voiceControlFragment =
            VoiceControlFragment.showFragment(activity, itemCase, suggestList, onResultListener)
        voiceControlFragment?.onDoTaskListener = onDoTaskListener
        voiceControlFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onDismiss() {
                voiceControlFragment = null
                startListenContinuos()
            }

            override fun onShow() {
                kontinuousRecognitionManager?.destroyRecognizer()
                super.onShow()
            }
        }
    }

    fun showVoiceControlFragment(isSetListening: Boolean = false, msg: String? = null) {
        if (!VcUtils.ENABLE_VOICE_CONTROLLER) {
            return
        }
        oniTVListener?.onPlayPlayer(false)
        var message = msg
        if (message.isNullOrEmpty()) {
            message = getRandomString(
                arrayOf(
                    "Xin chào, iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?",
                    "iTV có thể giúp gì cho bạn?"
//                    ,
//                    "iTV xin nghe !",
//                    "iTV có thể hỗ trợ gì cho bạn?",
//                    "iTV lắng nghe!",
//                    "Tôi là iTV, rất vui được hỗ trợ cho bạn?"
                )
            )
        }
        oniTVListener?.onPlayPlayer(false)
        if (currentFragment != null) {
            removeFragment(activity, currentFragment!!)
        }

        voiceControlFragment = VoiceControlFragment
            .showFragment(
                activity,
                message,
                onResultListener,
                isSetListening,
                true, suggestList,
                itemUtilityList,
                object : ItemUtilityAdapter.OnItemClickListener {
                    override fun onClick(itemObject: ItemUtility, position: Int) {
                        clickItemUtility(itemObject)
                        Log.e("UTILITY", "onClick" + position + "/" + itemObject.title)
                    }

                    override fun onClickOut(itemObject: ItemUtility, position: Int) {
                    }
                })
        voiceControlFragment?.onDoTaskListener = onDoTaskListener
        voiceControlFragment?.onShowDismissListener = object : OnShowDismissListener {
            override fun onDismiss() {
                oniTVListener?.onUtilShowing(false)
                voiceControlFragment = null
                startListenContinuos()
            }

            override fun onShow() {
                kontinuousRecognitionManager?.destroyRecognizer()
                super.onShow()
            }
        }
    }

    private fun getItemUtility(id: Int): ItemUtility? {
        for (itemObj in itemUtilityList) {
            if (itemObj.id == id) {
                return itemObj
            }
        }
        return null
    }

    private fun navigationFollowVoiceResult(
        str: String?,
        fragment: VoiceControlFragment?
    ) {
        if (str.isNullOrEmpty()) {
            return
        }
//        val key = GeneralUtils.convertNonAccentVietnamese(str).toString().lowercase()
        val key = str.lowercase()

        with(key) {
            when {
                //START: Utility
                contains("covid") || contains("corona") || contains("virus") || contains("vi rút") -> {
                    val itemObj = getItemUtility(UTILITY_COVID)
                    itemObj?.let {
                        clickItemUtility(it)
                    }
                }
                contains("thời tiết") || contains("nhiệt độ") || contains("độ ẩm") -> {
                    val itemObj = getItemUtility(UTILITY_WEATHER)
                    itemObj?.let {
                        clickItemUtility(it)
                    }
                }
                contains("lịch âm") || contains("lịch dương") || contains("lịch vạn niên") || contains(
                    "lịch việt"
                ) || contains("lịch hôm nay") || contains("lịch tháng") || contains("ngày âm") || contains(
                    "ngày dương"
                ) || contains("lịch 2021") || contains("lịch 2022") || contains("xem lịch") -> {
                    val itemObj = getItemUtility(UTILITY_CALENDAR)
                    itemObj?.let {
                        clickItemUtility(it)
                    }
                }
                contains("giá vàng") || contains("chỉ vàng") || contains("nhẫn vàng") || contains("sjc") || contains(
                    "doji"
                ) || contains("pnj") || contains("bảo tín minh châu") -> {
                    val itemObj = getItemUtility(UTILITY_GOLD)
                    itemObj?.let {
                        clickItemUtility(it)
                    }
                }
                contains("ngoại tệ") || contains("ngoại hối") || contains("tỷ giá") || contains("đô la") || contains(
                    "euro"
                ) || contains(
                    "bảng anh"
                ) -> {
                    val itemObj = getItemUtility(UTILITY_CURRENCY)
                    itemObj?.let {
                        clickItemUtility(it)
                    }
                }
                //END: Utility
                //START: HANDLE APP
                contains("trang chủ") || key.contains("home") -> {
                    oniTVListener?.onSwitchAppHandle(TYPE_TAB_HOME, key)
                    MainActivity.typeShowNotify = Constant.TYPE_HOME
                    navigationToMainActivity(fragment)
                }
                contains("video") -> {
                    oniTVListener?.onSwitchAppHandle(TYPE_TAB_VIDEO, key)
                    MainActivity.typeShowNotify = Constant.TYPE_VIDEO
                    navigationToMainActivity(fragment)
                }
                contains("tin tức") || key.contains("news") || key.contains("tin bài") || key.contains(
                    "bài viết"
                ) || key.contains("thời sự")
                -> {
                    oniTVListener?.onSwitchAppHandle(TYPE_TAB_NEWS, key)
                    MainActivity.typeShowNotify = Constant.TYPE_NEWS
                    navigationToMainActivity(fragment)
                }
                contains("diễn đàn") -> {
                    oniTVListener?.onSwitchAppHandle(TYPE_TAB_FORUM, key)
                    MainActivity.typeShowNotify = Constant.TYPE_FORUM
                    navigationToMainActivity(fragment)
                }
                contains("đấu giá") || key.contains("truyền hình") || key.contains("tương tác") -> {
//                    oniTVListener?.onSwitchAppHandle(TYPE_INTERACTIVE, key)
                    MainActivity.typeShowNotify = Constant.TYPE_INTERACTIVE
                    navigationToMainActivity(fragment)
                }
//                contains("tivi") || contains("truyền hình") || contains("television") || contains("kênh Nghệ An") || contains(
//                    "kênh ntv"
//                ) || contains("trực tiếp") || contains("thời sự")
//                -> {
//                    fragment?.dismiss()
//                    oniTVListener?.onSwitchAppHandle(TYPE_TAB_TV, key)
//                }
//                contains("radio") || contains("phát thanh") -> {
//                    fragment?.dismiss()
//                    oniTVListener?.onSwitchAppHandle(TYPE_TAB_RADIO, key)
//                }
                contains("tài khoản") || key.contains("cá nhân") || key.contains("personal")
                -> {
                    MainActivity.typeShowNotify = Constant.TYPE_PERSONAL
                    navigationToMainActivity(fragment)
                }
                contains("kho quà") || key.contains("quà") || key.contains("voucher")
                        || key.contains("vốt chơ") || key.contains("vâu chờ")
                -> {
                    fragment?.dismiss()
                    GeneralUtils.openMyVoucher(activity)
                    return
                }
                //END: HANDLE APP

                //START: OTHER
                contains("trở lại") || contains("trở về") || contains("quay lại") || contains("quay về") || contains(
                    "back"
                ) -> {
                    activity.onBackPressed()
                }
                contains("kết thúc") || contains("tắt áp") || contains("tắt app") || contains("close app") || contains(
                    "tắt máy"
                ) || contains("thoát áp") || contains("thoát app") || contains("thoát ứng dụng") || contains(
                    "đóng ứng dụng"
                ) || contains("đóng app") || contains("đóng ứng dụng")
                -> {
                    fragment?.dismiss()
                    activity.finishAndRemoveTask();
                }
                contains("ẩn áp") || contains("ẩn app") || contains("hide app") || contains("hide áp") || contains(
                    "àn hình chính"
                ) -> {
                    activity.startActivity(Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
                    fragment?.dismiss()
                    oniTVListener?.onPlayPlayer(true)
                }
                contains("tăng âm lượng") || contains("tăng tiếng") || contains("to lên") || contains(
                    "mở tiếng"
                ) || contains("to tiếng") -> {
                    val audioManager: AudioManager =
                        activity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
                    audioManager.adjustVolume(
                        AudioManager.ADJUST_RAISE,
                        AudioManager.FLAG_PLAY_SOUND
                    );
                    fragment?.speakWithTyping("Đă tăng âm lượng", true)
                }
                contains("giảm âm lượng") || contains("nhỏ âm lượng") || contains("nhỏ đi") || contains(
                    "giảm tiếng"
                ) || contains("nhỏ tiếng") -> {
                    val audioManager: AudioManager =
                        activity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
                    audioManager.adjustVolume(
                        AudioManager.ADJUST_LOWER,
                        AudioManager.FLAG_PLAY_SOUND
                    );
                    fragment?.speakWithTyping("Đă giảm âm lượng", true)
                }
                contains("im lặng") || contains("câm") || contains("mute") || contains("tắt tiếng") || contains(
                    "tắt âm"
                ) -> {
                    val audioManager: AudioManager =
                        activity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
                    audioManager.adjustVolume(AudioManager.ADJUST_MUTE, AudioManager.FLAG_SHOW_UI);
                    fragment?.speakWithTyping("Đă tắt âm thanh", true)
                }
                contains("tắt trợ lý") || contains("tắt chạy ngầm itv") || contains("tắt itv") || contains(
                    "stop itv"
                ) || contains("tạm dừng itv") -> {
                    VcUtils.setListeningContinuousBackground(activity, false)
                    fragment?.speakWithDismiss("Đã tắt tính năng sẵn sàng nhận lệnh bằng giọng nói khi ẩn giao diện điều khiển")
                }
                contains("bật trợ lý") || contains("bật chạy ngầm iTV") || contains("bật itv") || contains(
                    "mở itv"
                ) || contains("mở trợ lý") -> {
                    VcUtils.setListeningContinuousBackground(activity, true)
                    fragment?.speakWithTyping(
                        "Đã bật tính năng sẵn sàng nhận lệnh bằng giọng nói. iTV có thể giúp gì cho bạn?",
                        true
                    )
                }
                //END: OTHER
                else -> {
                    fragment ?: return
                    if (fragment.handResultCase(
                            key,
                            arrayOf("xin chào", "chào", "chào bạn", "hello"),
                            "Xin chào, iTV có thể giúp gì cho bạn?"
                        )
                    ) {
                        return
                    }
                    if (fragment.handResultCase(
                            key,
                            arrayOf("cà phê", "sữa", "coffee"),
                            "Không có rồi. Bạn ra cửa hàng mua nhé!"
                        )
                    ) {
                        return
                    }
                    if (fragment.handResultCase(
                            key,
                            arrayOf("lui", "ẩn", "biến", "cút", "bye"),
                            getRandomString(
                                arrayOf(
//                                "Vâng, cảm ơn Bạn",
//                                "Ok",
                                    "Bạn cần hỗ trợ gì hãy gọi iTV nhé!"
                                )
                            ), false, true
                        )
                    ) {
                        return
                    }
                    if (fragment.handResultCase(
                            key,
                            arrayOf("atv", "itv"),
                            getRandomString(
                                arrayOf(
                                    "iTV xin nghe",
                                    "Ok, tôi đây. Bạn cần hỗ trợ gì?"
                                )
                            ), true
                        )
                    ) {
                        return
                    }
                    fragment.speakWithTyping(
                        getRandomString(
                            arrayOf(
                                "Yêu cầu này chưa được thực hiện. Thử lại từ khoá khác nhé!",
                                "Bạn có thể diễn đạt lại không?"
                            )
                        ), true
                    )
                }
            }
        }

//            fragment?.clearTextResult()
    }

    private fun navigationToMainActivity(fragment: VoiceControlFragment?) {
//        try {
//            fragment?.dismiss()
//        } catch (e: Exception) {
//        }
//        if (baseActivity !is MainActivity) {
//            gotoActivity(MainActivity::class.java, Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        } else {
//            baseActivity.showNotify()
//        }
    }

    fun hideIconVoiceController() {
        layoutIconVoice?.visibility = View.INVISIBLE
    }

    fun showIconVoiceController() {
        layoutIconVoice?.visibility = View.VISIBLE
    }

    var kontinuousRecognitionManager
            : KontinuousRecognitionManager? = null

    fun startListenContinuos() {
        if (activity is SplashActivity || !VcUtils.ENABLE_VOICE_CONTROLLER) {
            return
        }
        if (!VcUtils.isListeningBackground(activity)) {
            kontinuousRecognitionManager?.muteRecognition(false)
            return
        }
        if (voiceControlFragment != null) {
            return
        }
        kontinuousRecognitionManager?.destroyRecognizer()
        kontinuousRecognitionManager = KontinuousRecognitionManager(
            activity,
            arrayOf("itv", "atv"),
            false,
            object : RecognitionCallback {
                override fun onKeywordDetected() {
                    val str = getRandomString(
                        arrayOf(
//                            "iTV xin nghe!",
                            "Xin chào, iTV có thể giúp gì cho bạn?",
                            "iTV có thể giúp gì cho bạn?",
                            "iTV có thể giúp gì cho bạn?",
                            "iTV có thể giúp gì cho bạn?"
                        )
                    )
                    activity.runOnUiThread { showVoiceControlFragment(true, str) }
                }
            })
        kontinuousRecognitionManager?.createRecognizer()
        kontinuousRecognitionManager?.startRecognition()
    }

    fun pauseVoiceControl() {
        kontinuousRecognitionManager?.destroyRecognizer()
        VoiceControllerManager.instance?.stopSpeak()
    }

    fun getRandomString(arr: Array<String>?): String? {
        if (arr.isNullOrEmpty()) {
            return null
        }
        val list = arr.toCollection(ArrayList())
        list.shuffle()
        return list[0]
    }

    interface OniTVListener {
        fun onGoDetailNews(itemObj: ItemNews)
        fun onPlayPlayer(isPlay: Boolean)
        fun onSwitchAppHandle(type: Int, keyword: String?)
        fun onUtilShowing(isShowing: Boolean)
    }
}*/
