package vn.mediatech.istudiocafe.util

import android.util.Base64

object EncryptUtil {
    fun base64Encode(data: String): String {
        return String(Base64.encode(data.toByteArray(), Base64.DEFAULT)).trim()
    }

    fun base64Decode(data: String): String {
        return String(Base64.decode(data, Base64.DEFAULT)).trim()
    }
}