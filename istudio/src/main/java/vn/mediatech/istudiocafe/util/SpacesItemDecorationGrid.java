package vn.mediatech.istudiocafe.util;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

public class SpacesItemDecorationGrid extends RecyclerView.ItemDecoration {
    private int space;
    private int spanCount;
    private boolean isHorizoltalListview;

    public SpacesItemDecorationGrid(int space, int numberColumn,
                                    boolean isHorizoltalListview) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = isHorizoltalListview;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    public SpacesItemDecorationGrid(int space, int numberColumn) {
        this.space = space;
        this.spanCount = numberColumn;
        this.isHorizoltalListview = false;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
//        int column = position % spanCount;
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
//        int viewType = -1;
        int spanIndex = -1;
        if (layoutManager instanceof StaggeredGridLayoutManager) {
//            viewType = layoutManager.getItemViewType(view);
            StaggeredGridLayoutManager.LayoutParams lp =
                    (StaggeredGridLayoutManager.LayoutParams) view.getLayoutParams();
            spanIndex = lp.getSpanIndex();
        }
//        Loggers.e("getItemOffsetsGrid_" + spanCount, "position = " + position + " _ column = "
//        + column + " _ viewType = " + viewType + " _ spanIndex = " + spanIndex);
        outRect.bottom = space;
//        if (viewType == Constant.STYLE_GRIDVIEW) {
        if (spanIndex == 0) {
            outRect.left = space;
            outRect.right = space;
        } else {
            outRect.left = 0;
            outRect.right = space;
        }
        if (position == 0 || position == 1) {
            outRect.top = space;
        }
//        }
//        else {
//            if (spanIndex == 0) {
//                outRect.top = space;
//            }
//            outRect.left = space;
//            outRect.right = space;
//        }
    }
}