package vn.mediatech.istudiocafe.util;

import android.app.ActivityManager;
import android.content.Context;


import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import vn.mediatech.istudiocafe.app.Loggers;

public class CustomThreadPoolManager {
    private static CustomThreadPoolManager sInstance = null;
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static final int KEEP_ALIVE_TIME = 1;
    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;

    private final ThreadPoolExecutor threadPoolExecutor;

    // The class is used as a singleton
    static {
        KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
        sInstance = new CustomThreadPoolManager();
    }

    // Made constructor private to avoid the class being initiated from outside
    private CustomThreadPoolManager() {
        threadPoolExecutor = new ThreadPoolExecutor(NUMBER_OF_CORES,
                NUMBER_OF_CORES * 2,
                KEEP_ALIVE_TIME,
                KEEP_ALIVE_TIME_UNIT,
                new LinkedBlockingQueue<Runnable>());
    }

    public synchronized static CustomThreadPoolManager getsInstance() {
        if (sInstance == null) {
            sInstance = new CustomThreadPoolManager();
        }
        return sInstance;
    }

    public ThreadPoolExecutor getThreadPoolExecutor(Context context) {
//        setThreadPoolExecutor(context);
        return threadPoolExecutor;

    }

    public static double getFreeMemory(Context context) {
        ActivityManager.MemoryInfo mi = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(mi);
        double freeSize = mi.availMem / 0x100000L;
        Loggers.e("FreeMemory", freeSize + "");
        return freeSize;
    }

    public static boolean isLowMemory(Context context) {
        if (getFreeMemory(context) < 500) {
            return true;
        } else {
            return false;
        }
    }

    private void setThreadPoolExecutor(Context context) {
        if (context == null) {
            return;
        }
        double freeSize = getFreeMemory(context);
        Loggers.e("FREERAM", freeSize + "");
        int corePoolSize = 1;
        if (freeSize > 800) {
            threadPoolExecutor.setCorePoolSize(NUMBER_OF_CORES);
            threadPoolExecutor.setMaximumPoolSize(NUMBER_OF_CORES * 2);
        } else {
            if (freeSize <= 800 && freeSize > 700) {
                corePoolSize = 6;
            } else if (freeSize <= 700 && freeSize > 600) {
                corePoolSize = 5;
            } else if (freeSize <= 600 && freeSize > 500) {
                corePoolSize = 4;
            } else if (freeSize <= 500 && freeSize > 400) {
                corePoolSize = 3;
            } else if (freeSize <= 400 && freeSize > 200) {
                corePoolSize = 2;
            }
            threadPoolExecutor.setCorePoolSize(corePoolSize);
            threadPoolExecutor.setMaximumPoolSize(corePoolSize);
        }
        Loggers.e("FREERAM", threadPoolExecutor.getCorePoolSize() + "/" + threadPoolExecutor.getMaximumPoolSize() + "/" + freeSize);
    }
}