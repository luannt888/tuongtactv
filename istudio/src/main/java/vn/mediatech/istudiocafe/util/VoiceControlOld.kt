//package vn.mediatech.istudiocafe.util
//
//import android.Manifest
//import android.content.Intent
//import android.media.AudioManager
//import android.os.Handler
//import android.os.Looper
//import android.util.Log
//import android.view.View
//import android.widget.FrameLayout
//import android.widget.Toast
//import androidx.appcompat.app.AppCompatActivity
//import androidx.fragment.app.Fragment
//import androidx.fragment.app.FragmentActivity
//import com.google.gson.Gson
//import vn.mediatech.istudiocafe.R
//import vn.mediatech.istudiocafe.activity.BaseActivity
//import vn.mediatech.istudiocafe.activity.MainActivity
//import vn.mediatech.istudiocafe.activity.SplashActivity
//import vn.mediatech.istudiocafe.app.Constant
//import vn.mediatech.istudiocafe.model.ItemNews
//import vn.mediatech.utility.fragment.BaseFragment
//import vn.mediatech.utility.fragment.calendar.CalendarFragment
//import vn.mediatech.utility.fragment.covid.CovidVnFragment
//import vn.mediatech.utility.fragment.weather.WeatherFragment
//import vn.mediatech.utility.model.ItemCategory
//import vn.mediatech.voicecontrol.adapter.ItemUtilityAdapter
//import vn.mediatech.voicecontrol.listener.OnClickListener
//import vn.mediatech.voicecontrol.listener.OnDoTaskListener
//import vn.mediatech.voicecontrol.listener.OnShowDismissListener
//import vn.mediatech.voicecontrol.listener.RecognitionCallback
//import vn.mediatech.voicecontrol.model.ItemCase
//import vn.mediatech.voicecontrol.model.ItemCaseAnswer
//import vn.mediatech.voicecontrol.model.ItemUtility
//import vn.mediatech.voicecontrol.voiceControl.*
//import java.util.*
//import kotlin.collections.ArrayList
//
//class VoiceControlOld(val baseActivity: BaseActivity) {
//    var layoutIconVoice: View? = null
//    var msg: String? = null
//    var itemCaseList: ArrayList<ItemCase>? = null
//    var voiceControlFragment: VoiceControlFragment? = null
//    var onResultListener: VoiceControllerManager.OnResultListener? =
//        object : VoiceControllerManager.OnResultListener {
//            override fun onPartialResults(str: String?) {
//            }
//
//            override fun onResult(str: String?) {
//                navigationFollowVoiceResult(str, voiceControlFragment)
//            }
//
//            override fun onRmsChanged(p0: Float) {
//            }
//        }
//    var onDoTaskListener = object : OnDoTaskListener {
//        override fun onDoTask(contentId: String?, contentType: String?) {
//
//            if (contentId == null) {
//                navigationFollowVoiceResult(contentType, voiceControlFragment)
//            } else {
//                val itemNews = ItemNews()
//                itemNews.id = contentId
//                itemNews.contentType = contentType
//                baseActivity.goDetailNewsActivity(itemNews)
//            }
//
//        }
//    }
//    var itemCase: ItemCase? = null
//    fun initItemCaseList() {
//        val list = ArrayList<ItemCaseAnswer>()
//        if (list.size == 0) return
//        list.add(
//            ItemCaseAnswer(
//                arrayOf("tương tác", "đấu giá", "truyền hình"),
//                " Ok, Tôi sẽ mở ngay!",
//                "tương tác"
//            )
//        )
//        list.add(ItemCaseAnswer(arrayOf("video"), " Ok, Tôi sẽ mở ngay!", "video"))
//        list.add(ItemCaseAnswer(arrayOf("diễn đàn"), " Ok, Tôi sẽ mở ngay!", "diễn đàn"))
//        list.add(ItemCaseAnswer(arrayOf("tin tức", "news"), " Ok, Tôi sẽ mở ngay!", "tin tức"))
//        itemCase = ItemCase(
//            "2",
//            "Hiện đang có một số chương trình sau: <br/>1. Diễn đàn<br/>2. Tương tác <br/>3. Video <br/>4. Tin tức <br/> Bạn có muốn xem gì không?",
//            list, null, null
//        )
//        val gson = Gson()
//        val str = gson.toJson(itemCase)
//        Log.e("GSON", str)
//        if (itemCase != null) {
//            showVoiceControlFragment(itemCase)
//            itemCase = null
//            return
//        }
//        if (itemCaseList != null) {
//            return
//        }
//        itemCaseList = ArrayList()
//        itemCaseList?.add(
//            ItemCase(
//                "1",
//                "Xin chào buổi sáng, đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("tương tác"), null,
//                "00:00",
//                "11:30"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "2",
//                "Xin chào, sáng nay có một số video nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("video"), null,
//                "00:00",
//                "11:30"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "3",
//                "Xin chào, sáng nay có một số tin tức nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("tin tức"), null,
//                "00:00",
//                "11:30"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "4",
//                "Xin chào,bạn ăn sáng chưa? Có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("diễn đàn"), null,
//                "00:00",
//                "11:30"
//            )
//        )
//
//
//        itemCaseList?.add(
//            ItemCase(
//                "1",
//                "Chúc bạn có buổi trưa vui vẻ, đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo(
//                    "tương tác"
//                ), null,
//                "11:30",
//                "15:00"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "2",
//                "Xin chào, trưa nay có một số video nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("video"), null,
//                "11:30",
//                "15:00"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "3",
//                "Xin chào, trưa nay có một số tin tức nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("tin tức"), null,
//                "11:30",
//                "15:00"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "4",
//                "Xin chào,trưa nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("diễn đàn"), null,
//                "11:30",
//                "15:00"
//            )
//        )
//
//        itemCaseList?.add(
//            ItemCase(
//                "1",
//                "Xin chào, chiều nay đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("tương tác"),
//                "15:00",
//                "18:00"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "2",
//                "Xin chào, chiều nay có một số video nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("video"),
//                "15:00",
//                "18:00"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "3",
//                "Xin chào, chiều nay có một số tin tức nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("tin tức"),
//                "15:00",
//                "18:00"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "4",
//                "Xin chào, chiều nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("diễn đàn"),
//                "15:00",
//                "18:00"
//            )
//        )
//
//        itemCaseList?.add(
//            ItemCase(
//                "1",
//                "Xin chào,tối nay đang có chương trình trực tiếp đấu giá truyền hình rất hay, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("tương tác"),
//                "18:00",
//                "23:59"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "2",
//                "Xin chào,tối nay có một số video nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("video"),
//                "18:00",
//                "23:59"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "3",
//                "Xin chào,tối nay có một số tin tức nổi bật, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("tin tức"),
//                "18:00",
//                "23:59"
//            )
//        )
//        itemCaseList?.add(
//            ItemCase(
//                "4",
//                "Xin chào,tối nay có một chủ đề mới trên diễn đàn, bạn có muốn xem không?",
//                createlistItemCaseAnserYesNo("diễn đàn"),
//                "18:00",
//                "23:59"
//            )
//        )
//
//        Collections.shuffle(itemCaseList)
//        for (item in itemCaseList!!) {
//            if (VcUtils.isInTime(item.timeStartHHmm, item.timeEndHHmm)) {
//                showVoiceControlFragment(item)
//                return
//            }
//        }
//    }
//
//    private fun createlistItemCaseAnserYesNo(
//        key: String? = null,
//        contentId: String? = null,
//        contentType: String? = null
//    ): ArrayList<ItemCaseAnswer> {
//        var itemCaseAnswerList: ArrayList<ItemCaseAnswer> = ArrayList()
//        itemCaseAnswerList.add(
//            ItemCaseAnswer(
//                arrayOf("có", "ok", "đồng ý", "mở", "yes"),
//                key, "Ok,Tôi sẽ mở ngay!"
//            )
//        )
//        itemCaseAnswerList.add(
//            ItemCaseAnswer(
//                arrayOf("không", "bỏ", "huỷ", "cancel"),
//                null,
//                "Ok, xin cảm ơn!"
//            )
//        )
//
//        return itemCaseAnswerList
//    }
//
//    val UTILITY_CALENDAR = 1
//    val UTILITY_COVID = 2
//    val UTILITY_WEATHER = 3
//    val UTILITY_CAR = 4
//    val UTILITY_BIKE = 5
//    val UTILITY_VOICE_CONTROL = 6
//    val UTILITY_PETROL = 7
//    val UTILITY_RATES = 8
//    val UTILITY_VIETLOT = 9
//    val itemList = ArrayList<ItemUtility>()
//    fun initVoiceManger() {
//        if (!VcUtils.ENABLE_VOICE_CONTROLLER) {
//            return
//        }
//        if (baseActivity is SplashActivity) {
//            return
//        }
//        if (!MyRequestPermissions.hasPermissions(
//                baseActivity,
//                arrayOf(Manifest.permission.RECORD_AUDIO)
//            )
//        ) {
//            MyRequestPermissions.requestPermission(
//                baseActivity,
//                arrayOf(Manifest.permission.RECORD_AUDIO),
//                object : MyRequestPermissions.OnMyRequestPermissionListener {
//                    override fun onPermissionGranted() {
//                        initVoiceManger()
//                    }
//
//                    override fun onPermissionDenied() {
//                    }
//
//                    override fun onError() {
//                    }
//                })
//            return
//        }
//        val countShowMsg = VcUtils.getInt(baseActivity, "MSG")
//        var isClickIcon = false
//        if (baseActivity is MainActivity && countShowMsg < 1) {
//            msg =
//                "Xin chào! Vui lòng click icon ITV và giữ nguyên 3 giây để điều khiển bằng giọng nói!"
//        }
//        val count = countShowMsg + 1
//        VcUtils.saveInt(baseActivity, count, "MSG")
//        if (layoutIconVoice != null) {
//            return
//        }
//        var msgVoice: String? = null
//        if (!msg.isNullOrEmpty()) {
//            msgVoice =
//                "Bạn hãy nói theo từ khoá gợi ý. Ví dụ bạn muốn xem đấu giá truyền hình hãy nói đấu giá truyền hình sau tiếng bíp. Cảm ơn"
//        }
//
//        itemList.add(
//            ItemUtility(
//                UTILITY_CALENDAR,
//                "",
//                R.drawable.ic_itv_calendar,
//                "Lịch vạn niên",
//                "Lịch"
//            )
//        )
//        itemList.add(ItemUtility(UTILITY_COVID, "", R.drawable.ic_itv_covid, "Số liệu Covid", "Covid"))
//        itemList.add(ItemUtility(UTILITY_WEATHER, "", R.drawable.ic_itv_weather, "Thời tiết"))
////        itemList.add(ItemUtility(UTILITY_BIKE, "", R.drawable.ic_itv_bike, "Giá xe máy"))
////        itemList.add(ItemUtility(UTILITY_CAR, "", R.drawable.ic_itv_car, "Giá xe hơi"))
////        itemList.add(ItemUtility(UTILITY_PETROL, "", R.drawable.ic_itv_petrol_station, "Xăng dầu"))
////        itemList.add(ItemUtility(UTILITY_RATES, "", R.drawable.ic_itv_exchange_rates, "Tỉ giá"))
////        itemList.add(ItemUtility(UTILITY_VIETLOT, "", R.drawable.ic_itv_vietlot, "Vietlot"))
////        itemList.add(
////            ItemUtility(
////                UTILITY_VOICE_CONTROL,
////                "",
////                R.drawable.bg_ic_utility_voice_vc,
////                "Điều khiển bằng giọng nói"
////            )
////        )
//        layoutIconVoice = VoiceControllerManager.showIconMenuVoice(
//            baseActivity,
//            msg,
//            56F,
//            10000L,
//            object : OnClickListener {
//                override fun onClick() {
//                }
//
//                override fun onLongClick() {
//                    showVoiceControlFragment(true, msgVoice)
//                    if (!msgVoice.isNullOrEmpty()) {
//                        isClickIcon = true
//                        msgVoice = null
//                    }
//                }
//            }, true, itemList, object : ItemUtilityAdapter.OnItemClickListener {
//                override fun onClick(itemObject: ItemUtility, position: Int) {
//                    Log.e("UTILITY", "onClick" + position + "/" + itemObject.title)
//                    clickItemUtility(itemObject)
//                }
//
//                override fun onClickOut(itemObject: ItemUtility, position: Int) {
//                }
//            })
//        if (VoiceControllerManager.isShowing) {
//            VoiceControllerManager.isShowing = false
//            showVoiceControlFragment(false)
//        }
//        if (baseActivity is MainActivity) {
//            if (!msg.isNullOrEmpty()) {
//                Handler(Looper.getMainLooper()).postDelayed(
//                    {
//                        if (!isClickIcon) {
//                            initItemCaseList()
//                        }
//                    }, 10000
//                )
//                return
//            } else {
//                if (!isClickIcon) {
//                    initItemCaseList()
//                }
//            }
//        }
//    }
//
//    private fun clickItemUtility(itemObject: ItemUtility) {
////        (baseActivity as? MainActivity)?.playPlayer(false)
//        voiceControlFragment?.dismiss()
//        when (itemObject.id) {
////            UTILITY_BIKE -> {
////
////            }
////            UTILITY_CAR -> {
////
////            }
//            UTILITY_CALENDAR -> {
//                showCalendar()
//            }
//            UTILITY_COVID -> {
//                showCovid()
//            }
//            UTILITY_VOICE_CONTROL -> {
//                showVoiceControlFragment(true)
//            }
//            UTILITY_WEATHER -> {
//                showWeather()
//            }
//            else -> {
//                Toast.makeText(
//                    baseActivity,
//                    "Hiện chưa có tính năng này!",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//        }
//    }
//
//    private fun showWeather() {
//        val fragment = WeatherFragment()
//        showFragmentUtility(fragment)
//    }
//
//    private fun showCovid() {
//        val fragment = CovidVnFragment.newInstance(
//            ItemCategory(
//                1,
//                1,
//                "Thống kê số liệu Covid",
//                null,
//                null
//            )
//        )
//        showFragmentUtility(fragment)
//    }
//
//    private fun showCalendar() {
//        val fragment = CalendarFragment()
//        showFragmentUtility(fragment)
//    }
//
//    fun showFragmentUtility(utilityFragment: BaseFragment?) {
//        if (baseActivity.isFinishing || baseActivity.isDestroyed || utilityFragment == null) {
//            return
//        }
////        (baseActivity as? MainActivity)?.playPlayer(false)
//        val frameLayout: FrameLayout =
//            baseActivity.findViewById<FrameLayout>(R.id.frameLayoutDialog) ?: return
//        val containerId = R.id.frameLayoutDialog
//        utilityFragment.
//        showFragment(baseActivity, containerId, utilityFragment)
//    }
//
//    var currentFragment: Fragment? = null
//    fun showFragment(
//        activity: FragmentActivity,
//        containerId: Int,
//        fragment: Fragment
//    ) {
//        voiceControlFragment?.dismiss()
//        if (currentFragment != null) {
//            removeFragment(activity, currentFragment!!)
//        }
//        currentFragment = fragment
//        val transaction = activity.supportFragmentManager.beginTransaction()
//        transaction.setCustomAnimations(
//            vn.mediatech.utility.R.anim.fragment_right_to_left_util,
//            vn.mediatech.utility.R.anim.fragment_left_to_right_util,
//            vn.mediatech.utility.R.anim.fragment_left_to_right_util,
//            vn.mediatech.utility.R.anim.fragment_left_to_right_util
//        )
//        if (containerId > 0) {
//            transaction.add(containerId, fragment, fragment::class.java.simpleName)
//        } else {
//            transaction.add(fragment, fragment::class.java.simpleName)
//        }
//
//        transaction.addToBackStack(null)
//    }
//
//    fun removeFragment(
//        activity: FragmentActivity,
//        fragment: Fragment
//    ) {
//        val transaction = activity.supportFragmentManager.beginTransaction()
//        transaction.setCustomAnimations(
//            vn.mediatech.utility.R.anim.fragment_right_to_left_util,
//            vn.mediatech.utility.R.anim.fragment_left_to_right_util,
//            vn.mediatech.utility.R.anim.fragment_left_to_right_util,
//            vn.mediatech.utility.R.anim.fragment_left_to_right_util
//        )
//        transaction.remove(fragment)
//        transaction.commitAllowingStateLoss()
//    }
//
//    fun showVoiceControlFragment(itemCase: ItemCase?) {
//        if (!VcUtils.ENABLE_VOICE_CONTROLLER || itemCase == null) {
//            return
//        }
////        (baseActivity as? MainActivity)?.playPlayer(false)
//        VoiceControlFragment.showFragment(baseActivity, itemCase, onResultListener)
//        voiceControlFragment?.onDoTaskListener = onDoTaskListener
//        voiceControlFragment?.onShowDismissListener = object : OnShowDismissListener {
//            override fun onDismiss() {
//                startListenContinuos()
//            }
//
//            override fun onShow() {
//                kontinuousRecognitionManager?.destroyRecognizer()
//                super.onShow()
//            }
//        }
//    }
//
//    fun showVoiceControlFragment(isSetListening: Boolean = false, msg: String? = null) {
//        if (!VcUtils.ENABLE_VOICE_CONTROLLER) {
//            return
//        }
//        var message = msg
//        if (message.isNullOrEmpty()) {
//            message = getRandomString(
//                arrayOf(
//                    "Xin chào, iTV có thể giúp gì cho bạn?",
//                    "iTV có thể giúp gì cho bạn?",
//                    "iTV có thể giúp gì cho bạn?",
//                    "iTV có thể giúp gì cho bạn?",
//                    "iTV có thể giúp gì cho bạn?"
////                    ,
////                    "iTV xin nghe !",
////                    "iTV có thể hỗ trợ gì cho bạn?",
////                    "iTV lắng nghe!",
////                    "Tôi là iTV, rất vui được hỗ trợ cho bạn?"
//                )
//            )
//        }
//
//        if (currentFragment != null) {
//            removeFragment(baseActivity, currentFragment!!)
//        }
////        (baseActivity as? MainActivity)?.playPlayer(false)
//        var suggestList = arrayOf(
//            "Trang chủ",
//            "Home",
//            "Tin tức",
//            "Diễn đàn",
//            "Tương tác",
//            "Đấu giá",
//            "Đấu giá truyền hình",
//            "Truyền hình",
//            "Video",
//            "Tin tức",
//            "Trang cá nhân",
//            "Kho quà",
//            "Voucher",
//            "Tắt app",
//            "Thoát app",
//            "Back",
//            "Quay lại",
//            "Tăng âm lượng",
//            "Giảm âm lượng",
//            "Im lặng",
//            "Mute"
//        )
//        voiceControlFragment = VoiceControlFragment
//            .showFragment(
//                baseActivity,
//                message,
//                onResultListener,
//                isSetListening,
//                true, suggestList,
//                itemList,
//                object : ItemUtilityAdapter.OnItemClickListener {
//                    override fun onClick(itemObject: ItemUtility, position: Int) {
//                        clickItemUtility(itemObject)
//                        Log.e("UTILITY", "onClick" + position + "/" + itemObject.title)
//                    }
//
//                    override fun onClickOut(itemObject: ItemUtility, position: Int) {
//                    }
//                })
//        voiceControlFragment?.onDoTaskListener = onDoTaskListener
//        voiceControlFragment?.onShowDismissListener = object : OnShowDismissListener {
//            override fun onDismiss() {
//                startListenContinuos()
//            }
//
//            override fun onShow() {
//                kontinuousRecognitionManager?.destroyRecognizer()
//                super.onShow()
//            }
//        }
//    }
//
//    private fun navigationFollowVoiceResult(
//        str: String?,
//        fragment: VoiceControlFragment?
//    ) {
//        if (str.isNullOrEmpty()) {
//            return
//        }
////        val key = GeneralUtils.convertNonAccentVietnamese(str).toString().lowercase()
//        val key = str.lowercase()
//        if (key.contains("trang chủ") || key.contains("home")) {
//            MainActivity.typeShowNotify = Constant.TYPE_HOME
//            navigationToMainActivity(fragment)
//        } else if (key.contains("video")) {
//            MainActivity.typeShowNotify = Constant.TYPE_VIDEO
//            navigationToMainActivity(fragment)
//        } else if (key.contains("tin tức") || key.contains("news") || key.contains("tin bài") || key.contains(
//                "bài viết"
//            ) || key.contains("thời sự")
//        ) {
//            MainActivity.typeShowNotify = Constant.TYPE_NEWS
//            navigationToMainActivity(fragment)
//        } else if (key.contains("diễn đàn")) {
//            MainActivity.typeShowNotify = Constant.TYPE_FORUM
//            navigationToMainActivity(fragment)
//        } else if (key.contains("đấu giá") || key.contains("truyền hình") || key.contains(
//                "tương tác"
//            )
//        ) {
//            MainActivity.typeShowNotify = Constant.TYPE_INTERACTIVE
//            navigationToMainActivity(fragment)
//        } else if (key.contains("tài khoản") || key.contains("cá nhân") || key.contains("personal")
//        ) {
//            MainActivity.typeShowNotify = Constant.TYPE_PERSONAL
//            navigationToMainActivity(fragment)
//        } else if (key.contains("kho quà") || key.contains("quà") || key.contains("voucher")
//            || key.contains("vốt chơ") || key.contains("vâu chờ")
//        ) {
//            fragment?.dismiss()
//            GeneralUtils.openMyVoucher(baseActivity)
//            return
//        } else
//            if (key.contains("thời tiết") || key.contains("weather") || key.contains("sunny") || key.contains(
//                    "rain"
//                ) || key.contains("wet") || key.contains("bão") || key.contains("có mưa không") || key.contains(
//                    "có nắng không"
//                ) || key.contains(
//                    "ẩm ướt"
//                )
//            ) {
//                fragment?.dismiss()
//                showWeather()
//                return
//            } else if (key.contains("covid") || key.contains("dịch bệnh") || key.contains("corona") || key.contains(
//                    "corrona"
//                ) || key.contains("sars") || key.contains(
//                    "cov"
//                )
//            ) {
//                fragment?.dismiss()
//                showCovid()
//                return
//            } else if (key.contains("lịch vạn niên") || key.contains("ngày tốt") || key.contains("ngày xấu") || key.contains(
//                    "calendar"
//                ) || key.contains("âm lịch") || key.contains("lịch")
//            ) {
//                fragment?.dismiss()
//                showCalendar()
//                return
//            }else if (key.contains("trở lại") || key.contains("trở về") || key.contains("back")
//                || key.contains("quay lại")
//            ) {
//                baseActivity.onBackPressed()
//                return
//            } else if (key.contains("kết thúc") || key.contains("tắt áp") || key.contains("tắt app") || key.contains(
//                    "close app"
//                ) || key.contains("tắt máy") || key.contains("thoát áp") || key.contains("thoát") || key.contains(
//                    "thoát app"
//                )
//            ) {
//                fragment?.dismiss()
//                baseActivity.finishAndRemoveTask();
//                return
//            } else if (key.contains("ẩn ap") || key.contains("ẩn app") || key.contains("hide app") || key.contains(
//                    "màn hình chính"
//                )
//            ) {
//                baseActivity.startActivity(Intent(Intent.ACTION_MAIN).addCategory(Intent.CATEGORY_HOME));
//                fragment?.dismiss()
//                return
//            } else if (key.contains("tăng âm lượng") || key.contains("to lên") || key.contains("mở tiếng")
//                || key.contains("tăng tiếng") || key.contains("to tiếng")
//            ) {
//                val audioManager: AudioManager =
//                    baseActivity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
//                audioManager.adjustVolume(AudioManager.ADJUST_RAISE, AudioManager.FLAG_PLAY_SOUND);
//                fragment?.speakWithTyping("Đă tăng âm lượng", true)
//                return
//            } else if (key.contains("giảm âm lượng") || key.contains("nhỏ âm lượng") || key.contains(
//                    "nhỏ đi"
//                ) || key.contains(
//                    "giảm tiếng"
//                ) || key.contains("nhỏ tiếng")
//            ) {
//                val audioManager: AudioManager =
//                    baseActivity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
//                audioManager.adjustVolume(AudioManager.ADJUST_LOWER, AudioManager.FLAG_PLAY_SOUND);
//                fragment?.speakWithTyping("Đă giảm âm lượng", true)
//                return
//            } else if (key.contains("im lặng") || key.contains("câm") || key.contains("mute") || key.contains(
//                    "tắt tiếng"
//                ) || key.contains("tắt âm")
//            ) {
//                val audioManager: AudioManager =
//                    baseActivity.applicationContext.getSystemService(AppCompatActivity.AUDIO_SERVICE) as AudioManager
//                audioManager.adjustVolume(AudioManager.ADJUST_MUTE, AudioManager.FLAG_SHOW_UI);
//                fragment?.speakWithTyping("Đă tắt âm thanh", true)
//                return
//            } else if (key.contains("tắt trợ lý") || key.contains("tắt chạy ngầm itv") || key.contains(
//                    "tắt itv"
//                )
//            ) {
//                VcUtils.setListeningContinuousBackground(baseActivity, false)
//                fragment?.speakWithDismiss("Đã tắt tính năng sẵn sàng nhận lệnh bằng giọng nói. Để bật lại hãy nhấn và giữ icon để ra lệnh.")
//                return
//            } else if (key.contains("bật trợ lý") || key.contains("bật chạy ngầm iTV") || key.contains(
//                    "bật itv"
//                )
//            ) {
//                VcUtils.setListeningContinuousBackground(baseActivity, true)
//                fragment?.speakWithTyping(
//                    "Đã bật tính năng sẵn sàng nhận lệnh bằng giọng nói.iTV có thể giúp gì cho bạn?",
//                    true
//                )
//                return
//            } else {
//                fragment ?: return
//                if (fragment.handResultCase(
//                        key,
//                        arrayOf("xin chào", "chào", "chào bạn", "hello"),
//                        "Xin chào, iTV có thể giúp gì cho bạn?"
//                    )
//                ) {
//                    return
//                }
//                if (fragment.handResultCase(
//                        key,
//                        arrayOf("cà phê", "sữa", "coffee"),
//                        "Không có rồi. Bạn ra cửa hàng mua nhé!"
//                    )
//                ) {
//                    return
//                }
//                if (fragment.handResultCase(
//                        key,
//                        arrayOf("lui", "ẩn", "biến", "cút", "bye"),
//                        getRandomString(
//                            arrayOf(
////                                "Vâng, cảm ơn Bạn",
////                                "Ok",
//                                "Bạn cần hỗ trợ gì hãy gọi iTV nhé!"
//                            )
//                        ), false, true
//                    )
//                ) {
//                    return
//                }
//                if (fragment.handResultCase(
//                        key,
//                        arrayOf("atv", "itv"),
//                        getRandomString(
//                            arrayOf(
//                                "iTV xin nghe",
//                                "Ok, tôi đây. Bạn cần hỗ trợ gì?"
//                            )
//                        ), true
//                    )
//                ) {
//                    return
//                }
//                fragment.speakWithTyping(
//                    getRandomString(
//                        arrayOf(
//                            "Yêu cầu này chưa được thực hiện. Thử lại từ khoá khác nhé!",
//                            "Bạn có thể diễn đạt lại không?"
//                        )
//                    ), true
//                )
////            fragment?.clearTextResult()
//            }
//    }
//
//    private fun navigationToMainActivity(fragment: VoiceControlFragment?) {
//        try {
//            fragment?.dismiss()
//        } catch (e: Exception) {
//        }
//        if (baseActivity !is MainActivity) {
//            baseActivity.gotoActivity(MainActivity::class.java, Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        } else {
//            baseActivity.showNotify()
//        }
//    }
//
//    fun hideIconVoiceController() {
//        layoutIconVoice?.visibility = View.INVISIBLE
//    }
//
//    fun showIconVoiceController() {
//        layoutIconVoice?.visibility = View.VISIBLE
//    }
//
//    var kontinuousRecognitionManager
//            : KontinuousRecognitionManager? = null
//
//    fun startListenContinuos() {
//        if (baseActivity is SplashActivity || !VcUtils.ENABLE_VOICE_CONTROLLER) {
//            return
//        }
//        if (!VcUtils.isListeningContinuousBackground(baseActivity)) {
//            kontinuousRecognitionManager?.muteRecognition(false)
//            return
//        }
//        if(VoiceControlFragment.currentFragment!= null){
//            return
//        }
//        kontinuousRecognitionManager?.destroyRecognizer()
//        kontinuousRecognitionManager = KontinuousRecognitionManager(
//            baseActivity,
//            arrayOf("itv", "atv"),
//            false,
//            object : RecognitionCallback {
//                override fun onKeywordDetected() {
//                    val str = getRandomString(
//                        arrayOf(
////                            "iTV xin nghe!",
//                            "Xin chào, iTV có thể giúp gì cho bạn?",
//                            "iTV có thể giúp gì cho bạn?",
//                            "iTV có thể giúp gì cho bạn?",
//                            "iTV có thể giúp gì cho bạn?"
//                        )
//                    )
//                    baseActivity.runOnUiThread { showVoiceControlFragment(true, str) }
//                }
//            })
//        kontinuousRecognitionManager?.createRecognizer()
//        kontinuousRecognitionManager?.startRecognition()
//    }
//
//    fun pauseVoiceControl() {
//        kontinuousRecognitionManager?.destroyRecognizer()
//        VoiceControllerManager.instance?.stopSpeak()
//    }
//
//    fun getRandomString(arr: Array<String>?): String? {
//        if (arr == null || arr.size == 0) {
//            return null
//        }
//        val list = arr.toCollection(ArrayList())
//        Collections.shuffle(list)
//        return list.get(0)
//    }
//}