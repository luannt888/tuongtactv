package vn.mediatech.istudiocafe.util;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpacesItemDecorationOTT extends RecyclerView.ItemDecoration {
    private int space;
    private int spanCount;

    public SpacesItemDecorationOTT(int space, int numberColumn) {
        this.space = space;
        this.spanCount = numberColumn;
        if (spanCount <= 0) {
            spanCount = 1;
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        int position = parent.getChildAdapterPosition(view); // item position
//        int spanCount = 2;
        if (position >= 0) {
            int column = position % spanCount; // item column
            outRect.left = space - column * space / spanCount; // spacing - column * ((1f / spanCount) * spacing)
            outRect.right = (column + 1) * space / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
            if (position < spanCount) { // top edge
                outRect.top = space;
            }
            outRect.bottom = space; // item bottom
        } else {
            outRect.left = 0;
            outRect.right = 0;
            outRect.top = 0;
            outRect.bottom = 0;
        }
       /* outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
        outRect.top = space;*/
    }
}