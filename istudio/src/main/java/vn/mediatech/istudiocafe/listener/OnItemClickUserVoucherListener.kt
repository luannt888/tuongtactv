package vn.mediatech.istudiocafe.listener

import vn.mediatech.istudiocafe.voucher.ItemVoucher

interface OnItemClickUserVoucherListener {
    fun onClickUseVoucher(itemObj: ItemVoucher?, position: Int)
}