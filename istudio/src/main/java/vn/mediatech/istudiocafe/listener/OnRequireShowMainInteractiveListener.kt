package vn.mediatech.istudiocafe.listener

import vn.mediatech.istudiocafe.model.ItemInteractiveConfig


interface OnRequireShowMainInteractiveListener {
    fun onRequireShowMainInteractive()
}