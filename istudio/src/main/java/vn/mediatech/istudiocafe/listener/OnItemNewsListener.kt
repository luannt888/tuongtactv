package vn.mediatech.istudiocafe.listener

import vn.mediatech.istudiocafe.model.ItemNews

interface OnItemNewsListener {
    fun onResponse(isSuccess: Boolean, message: String?, itemList: ArrayList<ItemNews>?)
}