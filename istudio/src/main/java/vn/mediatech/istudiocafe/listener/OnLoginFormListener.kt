package vn.mediatech.istudiocafe.listener

interface OnLoginFormListener {
    fun onLogin(hasLogin: Boolean)
}