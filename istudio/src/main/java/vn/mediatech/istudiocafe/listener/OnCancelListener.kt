package vn.mediatech.istudiocafe.listener

interface OnCancelListener {
    fun onCancel(isCancel: Boolean)
}