package vn.mediatech.istudiocafe.listener;

public interface OnPlayerStateChangeListener {
    void onStateEnd();

    void onStateReady();

    void onPlayerError();
}