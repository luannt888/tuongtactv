package vn.mediatech.istudiocafe.listener

interface OnSimpleListviewListener {
    fun onItemClick(position: Int, title: String?)
}