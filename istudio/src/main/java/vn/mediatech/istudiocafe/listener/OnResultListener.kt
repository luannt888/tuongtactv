package vn.mediatech.istudiocafe.listener

interface OnResultListener {
    fun onResult(isSuccess: Boolean, msg: String?)
}