package vn.mediatech.istudiocafe.listener

interface OnShowListener {
    fun onViewCreated()
    fun onResume()
    fun onPause()
    fun onStop()
}