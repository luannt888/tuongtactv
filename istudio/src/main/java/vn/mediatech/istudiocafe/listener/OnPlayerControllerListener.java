package vn.mediatech.istudiocafe.listener;

public interface OnPlayerControllerListener {
    void onShow();

    void onHide();
}