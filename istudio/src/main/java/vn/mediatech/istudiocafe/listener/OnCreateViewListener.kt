package vn.mediatech.istudiocafe.listener

interface OnCreateViewListener {
    fun onCreated()
}