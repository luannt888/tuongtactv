package vn.mediatech.istudiocafe.listener

interface OnKeyboardVisibilityListener {
    fun onVisibilityChanged(visible: Boolean)
}