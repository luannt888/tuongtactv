package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener;
import vn.mediatech.istudiocafe.listener.OnKeyboardVisibilityListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;

public class UserEnterNewPasswordActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonConfirm;
    private EditText editCurrentPassword, editPassword, editConfirmPassword;
    private MyHttpRequest myHttpRequest;
    private boolean resultOk = false;
    private boolean isChangePasswordScreen;
    private TextView textScreenUserDescription;
    private ItemUser itemUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_enter_new_password_tt);
        initUI();
        initData();
        initControl();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editCurrentPassword = findViewById(R.id.editCurrentPassword);
        editPassword = findViewById(R.id.editPassword);
        editConfirmPassword = findViewById(R.id.editConfirmPassword);
        buttonConfirm = findViewById(R.id.buttonConfirm);
        textScreenUserDescription = findViewById(R.id.textScreenUserDescription);
    }

    private void initData() {
        itemUser = MyApplication.getInstance().getDataManager().getItemUser();
        if (itemUser == null) {
            showDialogUser(false, getString(R.string.notification),
                    getString(R.string.msg_empty_data), null, getString(R.string.close),
                    new OnDialogButtonListener() {
                        @Override
                        public void onLeftButtonClick() {
                        }

                        @Override
                        public void onRightButtonClick() {
                            finish();
                        }
                    });
            return;
        }
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
        isChangePasswordScreen = bundle.getBoolean(Constant.IS_CHANGE_PASSWORD_SCREEN, false);
        if (isChangePasswordScreen) {
            textScreenUserDescription.setText(R.string.item_person_change_password);
//            editCurrentPassword.setVisibility(View.VISIBLE);
        }
    }

    private void validForm() {
        getData();
    }

    private void getData() {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        String accessToken = itemUser.getAccessToken();
        if (MyApplication.getInstance().isEmpty(accessToken)) {
            showDialogUser(getString(R.string.msg_empty_data));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        String password = editPassword.getText().toString().trim();
        String passwordConfirm = editConfirmPassword.getText().toString().trim();
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        requestParams.put("password", password);
        requestParams.put("confirm_password", passwordConfirm);
        requestParams.put("access_token", accessToken);
        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(this,Constant.API_USER_CHANGE_PASSSWORD),
                requestParams, new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                showDialogUser(getString(R.string.msg_network_error));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                handleData(responseString);
                            }
                        });
                    }
                });
    }

    private void handleData(String responseString) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode != Constant.SUCCESS) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        showToast(message);
        resultOk = true;
        finish();
    }

    private void validButtonConfirm() {
        /*if (isChangePasswordScreen) {
            String currentPassword = editCurrentPassword.getText().toString().trim();
        }*/
        String password = editPassword.getText().toString().trim();
        if (password.length() < 6) {
            buttonConfirm.setEnabled(false);
            return;
        }
        String passwordConfirm = editConfirmPassword.getText().toString().trim();
        if (passwordConfirm.length() < 6) {
            buttonConfirm.setEnabled(false);
            return;
        }
        buttonConfirm.setEnabled(password.equals(passwordConfirm));
    }

    private void initControl() {
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPassword);
                resultOk = true;
                finish();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPassword);
                validForm();
            }
        });
        setOnKeyboardVisibilityListener(new OnKeyboardVisibilityListener() {
            @Override
            public void onVisibilityChanged(boolean visible) {
                textScreenUserDescription.setVisibility(visible ? View.GONE : View.VISIBLE);
            }
        });
    }

    private void showCancelDialog() {
        showDialogUser(true, getString(R.string.notification),
                getString(R.string.msg_cancel_register), getString(R.string.cancel),
                getString(R.string.close), new OnDialogButtonListener() {
                    @Override
                    public void onLeftButtonClick() {
                        resultOk = true;
                        if (bundleResult == null) {
                            bundleResult = new Bundle();
                        }
                        bundleResult.putInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SCREEN);
                        finish();
                    }

                    @Override
                    public void onRightButtonClick() {

                    }
                });
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    private Bundle bundleResult;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            resultOk = true;
            bundleResult = data != null ? data.getExtras() : null;
            finish();
        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
            if (bundleResult == null) {
                bundleResult = new Bundle();
            }
            bundleResult.putInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SUCCESS);
            intent.putExtras(bundleResult);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            hideKeyboard(editPassword);
            resultOk = true;
            finish();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}