package vn.mediatech.istudiocafe.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.*
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.widget.TextViewCompat
import kotlinx.android.synthetic.main.activity_detail_product_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.user.UserLoginActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.swipebacklayout.SwipeBackLayout
import vn.mediatech.istudiocafe.swipebacklayout.app.SwipeBackActivity
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.TextUtil
import kotlin.math.roundToInt

class DetailProductActivity : SwipeBackActivity() {
    var itemObj: ItemNews? = null
    var myHttpRequest: MyHttpRequest? = null
    var isUpdateCart = false
    var firstQuantity = 0

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(Constant.DATA, itemObj)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_product_tt)
        initSwipeBack()
        initUI()
        initData(savedInstanceState)
        initControl()
    }

    override fun onResume() {
        super.onResume()
        trackScreen(getString(R.string.fa_detail_menu))
    }

    fun initSwipeBack() {
        val edgeFlag: Int = SwipeBackLayout.EDGE_LEFT
        swipeBackLayout.setEdgeTrackingEnabled(edgeFlag)
        swipeBackLayout.setEdgeSize(ScreenSize(this).width / 9)
//        mSwipeBackLayout.setTouchFullScreen(true);
    }

    @SuppressLint("RestrictedApi", "WrongConstant")
    fun initUI() {
        val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout.LayoutParams
        lp.height = ScreenSize(this).height * 2 / 5
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            textTotalPrice.setLines(1)
//            textTotalPrice.setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM)
            textTotalPrice.setAutoSizeTextTypeWithDefaults(TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM)
        }
    }

    fun initData(savedInstanceState: Bundle?) {
        val bundle = savedInstanceState ?: intent.extras
        itemObj = bundle?.getParcelable(Constant.DATA)
        if (itemObj == null) {
            finish()
            return
        }
        setData()
        getData()
    }

    fun showErrorNetwork(message: String) {
        runOnUiThread {
            textNotify.text = message
            textNotify.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
        }
    }

    fun getData() {
        if (!MyApplication.getInstance().isNetworkConnect) {
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
//        progressBar.visibility = View.VISIBLE
//        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(this)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("id", "${itemObj!!.id}")
        myHttpRequest!!.request(false, ServiceUtilTT.validAPI(Constant.API_PRODUCT_DETAIL), requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isFinishing || isDestroyed) {
                    return
                }
//                isLoading = false
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isFinishing || isDestroyed) {
                    return
                }
                handleData(responseString)
                runOnUiThread {
                    progressBar.visibility = View.GONE
                }
//                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
        itemObj = JsonParser.parseItemNews(this, resultObj, null)
        if (itemObj == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        runOnUiThread {
            layoutContent.visibility = View.VISIBLE
            setData()
        }

        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            runOnUiThread {
                rate.rating = itemObj!!.rate!!
                val currentRate = (itemObj!!.rate!!).roundToInt() - 1
                if (currentRate >= 0 && currentRate < rate.childCount) {
                    val rotation = AnimationUtils.loadAnimation(this, R.anim.rotation)
                    rate.getChildAt(currentRate).startAnimation(rotation)
                    /*val scaleDown = AnimationUtils.loadAnimation(this, R.anim.scale_down)
                    val scaleUp = AnimationUtils.loadAnimation(this, R.anim.scale_up)
                    rate.getChildAt(currentRate).startAnimation(scaleUp)
                    rate.getChildAt(currentRate).startAnimation(scaleDown)*/
                }
            }
        }, 500)
    }

    fun setData() {
        textTitle.text = itemObj?.name
        textPriceInfo.text = TextUtil.formatMoney(itemObj!!.basePrice)
//        rate.rating = itemObj!!.rate!!
        TextUtil.setHtmlTextView(itemObj?.description, textDescription)
        if (itemObj!!.reviews.isNullOrEmpty()) {
            textReviews.visibility = View.GONE
        } else {
            textReviews.text = itemObj!!.reviews
            textReviews.visibility = View.VISIBLE
        }
//        val totalPrice = itemObj!!.basePrice * (textQuantity.text.toString().toInt())
//        textTotalPrice.text = TextUtil.formatMoney(totalPrice)
        MyApplication.getInstance().loadImage(this, imageThumbnail, itemObj?.image)
        initUserReviewsData()

        //check product exist in cart
        val itemCartList = MyApplication.getInstance().dataManager.itemCartList
        for (item in itemCartList) {
            if (item.id.equals(itemObj!!.id)) {
                textQuantity.animateText("${item.quantity}")
                buttonOrder.text = getString(R.string.update_cart)
                isUpdateCart = true
                textQuantity.text = "${item.quantity}"
//                val totalPrice = itemObj!!.basePrice * item.quantity
//                Loggers.e("checkQuantity_C", "quantity = ${item.quantity} , totalPrice = $totalPrice")
//                textTotalPrice.animateText(TextUtil.formatMoney(totalPrice))
                break
            }
        }
        checkQuantity(0)
        firstQuantity = textQuantity.text.toString().toInt()
        checkDescriptionLine()
    }

    fun initUserReviewsData() {
        val itemUserReviewList = itemObj!!.itemUserReviewList
        if (itemUserReviewList == null) {
            return
        }
        for (i in 0 until itemUserReviewList.size) {
            val itemUserReview = itemUserReviewList.get(i)
            val paddingStart = resources.getDimensionPixelSize(R.dimen.dimen_25) * i
            val image = ImageView(this)
            image.scaleType = ImageView.ScaleType.CENTER_CROP
            image.setBackgroundResource(R.drawable.bg_user_review)
            image.layoutParams = RelativeLayout.LayoutParams(80, 80)
            image.x = paddingStart * 1f
            MyApplication.getInstance().loadImageCircle(this, image, itemUserReview.avatar)
            layoutImageReviews.addView(image)
        }
    }

    fun addToCart() {
        val itemUser = MyApplication.getInstance().dataManager.itemUser
        if (itemUser == null) {
            gotoActivityForResult(UserLoginActivity::class.java, Constant.CODE_LOGIN)
            return
        }
        val itemCartList = MyApplication.getInstance().dataManager.itemCartList
        if (buttonOrder.text.toString().equals(getString(R.string.back))) {
            for (item in itemCartList) {
                if (item.id.equals(itemObj!!.id)) {
                    itemCartList.remove(item)
                    break
                }
            }
            supportFinishAfterTransition()
            return
        }
        val quantity = textQuantity.text.toString().toInt()
        var flag = false
        for (item in itemCartList) {
            if (item.id.equals(itemObj!!.id)) {
                item.quantity = quantity
                flag = true
                break
            }
        }
        if (flag) {
            supportFinishAfterTransition()
            return
        }
        val itemObjCopy = itemObj!!.copy()
        itemObjCopy.quantity = quantity
        itemCartList.add(itemObjCopy)
        supportFinishAfterTransition()
    }

    fun checkQuantity(number: Int) {
        var quantity = textQuantity.text.toString().toInt()
        quantity += number
        if (isUpdateCart) {
            buttonOrder.text = if (quantity <= 0) getString(R.string.back) else getString(R.string.update_cart)
        }
        val minQuantity = if (isUpdateCart) 0 else 1
        if (quantity < minQuantity || quantity > itemObj!!.maxQuantity!!) {
            return
        }
//        textQuantity.text = "$quantity"
        /*textQuantity.setAnimationListener(object : AnimationListener {
            override fun onAnimationEnd(hTextView: HTextView?) {
                Loggers.e("checkQuantity_B", "quantity = $quantity")
                val totalPrice = itemObj!!.basePrice * quantity
                textTotalPrice.animateText(TextUtil.formatMoney(totalPrice))
            }
        })*/
        Loggers.e("checkQuantity_A", "quantity = $quantity")
        textQuantity.animateText("$quantity")
        val totalPrice = itemObj!!.basePrice * quantity
        textTotalPrice.animateText(TextUtil.formatMoney(totalPrice))
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            supportFinishAfterTransition()
        }
        buttonIncrease.setOnClickListener {
            checkQuantity(1)
        }
        buttonDecrease.setOnClickListener {
            checkQuantity(-1)
        }
        buttonOrder.setOnClickListener {
            addToCart()
        }
        textDescription.setOnClickListener {
            toogleDescription()
        }

        textViewMore.setOnClickListener {
            toogleDescription()
        }
    }

    fun toogleDescription() {
        if (textViewMore.visibility == View.VISIBLE) {
            textDescription.maxLines = Int.MAX_VALUE
            textViewMore.visibility = View.GONE
        } else {
            val currentLine = textDescription.lineCount
            if (currentLine > MAX_LINE) {
                textDescription.maxLines = MAX_LINE
                textViewMore.visibility = View.VISIBLE
            }
        }
    }

    val MAX_LINE = 10//6
    fun checkDescriptionLine() {
        textDescription.post(Runnable {
            val currentLine = textDescription.lineCount
            if (currentLine <= MAX_LINE) {
                textViewMore.visibility = View.GONE
                return@Runnable
            }
            textDescription.maxLines = MAX_LINE
            textViewMore.visibility = View.VISIBLE
        })
//        var maxLine = textDescription.maxLines
//        maxLine = if (maxLine != 8) 8 else Int.MAX_VALUE// default line = 4
    }

    override fun finish() {
        if (isUpdateCart) {
            val currentQuatity = textQuantity.text.toString().toInt()
            if (currentQuatity != firstQuantity) {
                val intent = Intent()
                setResult(RESULT_OK, intent)
            }
        }
        super.finish()
    }

    override fun onDestroy() {
        myHttpRequest?.cancel()
        super.onDestroy()
    }
}