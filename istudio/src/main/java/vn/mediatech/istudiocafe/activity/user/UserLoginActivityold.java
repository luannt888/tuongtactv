//package vn.mediatech.istudiocafe.activity.user;
//
//import android.app.Dialog;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Color;
//import android.graphics.Outline;
//import android.graphics.drawable.ColorDrawable;
//import android.os.Bundle;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.Gravity;
//import android.view.View;
//import android.view.ViewOutlineProvider;
//import android.view.Window;
//import android.view.WindowManager;
//import android.view.inputmethod.EditorInfo;
//import android.widget.AdapterView;
//import android.widget.ArrayAdapter;
//import android.widget.CheckBox;
//import android.widget.EditText;
//import android.widget.ImageView;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import androidx.annotation.Nullable;
//
//import com.facebook.AccessToken;
//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.google.android.gms.auth.api.signin.GoogleSignIn;
//import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
//import com.google.android.gms.common.api.ApiException;
//import com.google.android.gms.tasks.Task;
//import com.google.firebase.auth.FirebaseAuth;
//
//import org.json.JSONObject;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//
//import vn.mediatech.istudiocafe.interactive.app.InteractiveConstant;
//import vn.mediatech.istudiocafe.R;
//import vn.mediatech.istudiocafe.activity.BaseActivity;
//import vn.mediatech.istudiocafe.app.Constant;
//import vn.mediatech.istudiocafe.app.Loggers;
//import vn.mediatech.istudiocafe.app.MyApplication;
//import vn.mediatech.istudiocafe.listener.OnCancelListener;
//import vn.mediatech.istudiocafe.listener.OnResultListener;
//import vn.mediatech.istudiocafe.listener.OnResultRequestListener;
//import vn.mediatech.istudiocafe.model.ItemUser;
//import vn.mediatech.istudiocafe.service.MyHttpRequest;
//import vn.mediatech.istudiocafe.service.RequestParams;
//import vn.mediatech.istudiocafe.util.GeneralUtils;
//import vn.mediatech.istudiocafe.util.JsonParser;
//import vn.mediatech.istudiocafe.util.MyDialog;
//import vn.mediatech.istudiocafe.util.ScreenSize;
//import vn.mediatech.istudiocafe.util.ServiceUtil;
//import vn.mediatech.istudiocafe.util.SharedPreferencesManager;
//import vn.mediatech.istudiocafe.util.TextUtil;
//
//public class UserLoginActivityold extends BaseActivity {
//    private ImageView buttonBack, imageFacebook;
//    private TextView buttonRegisterAccount, buttonLoginFacebook, buttonConfirm,
//            buttonForgotPassword;
//    private EditText editPhone, editPassword;
//    private MyHttpRequest myHttpRequest;
//    private CallbackManager callbackManager;
//    private FirebaseAuth mAuth;
//    private String loginType = Constant.TYPE_REGISTER_FORM;
//    private String accessTokenSocial = "";
//    private String userIdSocial = "";
//    private String emailSocial = "";
//    private ItemUser itemUserTemp = new ItemUser();
//    private boolean resultOk = false;
//    private String tv;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_user_login_tt);
//        MyApplication.getInstance().getDataManager().setItemUserTemp(null);
//        initUI();
//        initData();
//        initControl();
////        showFormDialog("ThanhNguyen872", "abecd3", null, null, null);
////        showMoreInfoUserDialog();
//    }
//
//    private void initUI() {
//        setFullStatusTransparent();
//        buttonBack = findViewById(R.id.buttonBack);
//        editPhone = findViewById(R.id.editPhone);
//        editPassword = findViewById(R.id.editPassword);
//        buttonConfirm = findViewById(R.id.buttonConfirm);
//        buttonRegisterAccount = findViewById(R.id.buttonRegisterAccount);
//        buttonForgotPassword = findViewById(R.id.buttonForgotPassword);
//        buttonLoginFacebook = findViewById(R.id.buttonLoginFacebook);
//        imageFacebook = findViewById(R.id.imageFacebook);
//        if (!Constant.IS_TEST) {
//            findViewById(R.id.layoutFacebook).setVisibility(View.VISIBLE);
//            findViewById(R.id.layoutLoginNomal).setVisibility(View.GONE);
//        } else {
//            findViewById(R.id.layoutFacebook).setVisibility(View.GONE);
//            findViewById(R.id.layoutLoginNomal).setVisibility(View.VISIBLE);
//        }
//        TextUtil.setHtmlTextView(getString(R.string.user_form_register), buttonRegisterAccount);
////        setBackgroudActionbarFragment(rootView);
//    }
//
//    private void initData() {
//        Bundle bundle = getIntent().getExtras();
//        if (bundle == null) {
//            return;
//        }
//        boolean isRegisterScreen = bundle.getBoolean(Constant.IS_REGISTER, false);
//        if (isRegisterScreen) {
//            goRegisterScreen();
//        }
//    }
//
//    private void validForm() {
//        String username = editPhone.getText().toString().trim();
//        if (username.isEmpty()) {
//            editPhone.requestFocus();
//            showToast(R.string.msg_username_empty);
//            return;
//        }
//        String password = editPassword.getText().toString().trim();
//        if (password.isEmpty()) {
//            editPassword.requestFocus();
//            showToast(R.string.msg_password_empty);
//            return;
//        }
//        loginType = Constant.TYPE_REGISTER_FORM;
//        login(username, password);
//    }
//
//    private void login(String username, String password) {
//        login(username, password, null, null, null, null, null);
//    }
//
//    private void login(String username, String password, String gender, String job, String address, String presenterType, String presentervalue) {
//        if (!MyApplication.getInstance().isNetworkConnect()) {
//            showDialogUser(getString(R.string.msg_network_error));
//            return;
//        }
//        showLoadingDialog(true, null, new OnCancelListener() {
//            @Override
//            public void onCancel(boolean isCancel) {
//                if (myHttpRequest != null) {
//                    myHttpRequest.cancel();
//                }
//            }
//        });
//        if (myHttpRequest == null) {
//            myHttpRequest = new MyHttpRequest(this);
//        } else {
//            myHttpRequest.cancel();
//        }
//        RequestParams requestParams = new RequestParams();
//        String api = Constant.API_USER_LOGIN;
//        boolean isPostMethod = true;
//        if (loginType.equals(Constant.TYPE_REGISTER_FORM)) {
//            api = Constant.API_USER_LOGIN;
//            requestParams.put("phone", username);
//            requestParams.put("password", password);
//        } else if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK)) {
//           /* if (getApplication().getPackageName().equals("vn.mediatech.istudiocafe")) {
//                api = Constant.API_USER_LOGIN_SOCIAL;
//            } else {
//                api = Constant.API_USER_LOGIN_SOCIAL_NONE_PHONE;
//            }*/
//            api = Constant.API_USER_LOGIN_SOCIAL_NONE_PHONE;
//            requestParams.put("social_id", userIdSocial);
//            requestParams.put("email", emailSocial);
//            requestParams.put("access_token", accessTokenSocial);
//            if (itemUserTemp != null) {
//                requestParams.put("fullname", itemUserTemp.getFullname());
//            }
//        } else if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK_2)) {
//            api = Constant.API_USER_LOGIN_FACEBOOK_FORUM;
//            requestParams.put("access_token", accessTokenSocial);
//            requestParams.put("userID", userIdSocial);
//            requestParams.put("email", emailSocial);
//            requestParams.put("type", "mb");
//            requestParams.put("username", username);
//            requestParams.put("password", password);
//            requestParams.put("gender", gender);
//            requestParams.put("address", address);
//            requestParams.put("job", job);
//            requestParams.put("channel", InteractiveConstant.CHANEL_ID);
//            requestParams.put("presenter_type", presenterType);
//            requestParams.put("presenter_value", presentervalue);
//            if (itemUserTemp != null) {
//                requestParams.put("first_name", itemUserTemp.getFullname());
//                requestParams.put("last_name", itemUserTemp.getFullname());
//            }
//        } else if (loginType.equals(Constant.TYPE_REGISTER_GOOGLE)) {
//            api = Constant.API_USER_LOGIN_SOCIAL;
//            requestParams.put("social_id", userIdSocial);
//            requestParams.put("email", emailSocial);
//            requestParams.put("access_token", accessTokenSocial);
//        }
//        requestParams.put("type", loginType);
//        myHttpRequest.request(isPostMethod, ServiceUtil.validAPIDGTH(this, api), requestParams,
//                new MyHttpRequest.ResponseListener() {
//                    @Override
//                    public void onFailure(int statusCode) {
//                        if (isFinishing() || isDestroyed()) {
//                            return;
//                        }
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                hideLoadingDialog();
//                                showDialogUser(getString(R.string.msg_network_error));
//                            }
//                        });
//                    }
//
//                    @Override
//                    public void onSuccess(int statusCode, String responseString) {
//                        if (isFinishing() || isDestroyed()) {
//                            return;
//                        }
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                hideLoadingDialog();
//                                handleDataLogin(responseString, username, password);
//                            }
//                        });
//                    }
//                });
//    }
//
//    Boolean isTestShowDialog = true;
//
//    private void handleDataLogin(String responseString, String username, String password) {
//        if (MyApplication.getInstance().isEmpty(responseString)) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    showDialogUser(getString(R.string.msg_empty_data));
//                }
//            });
//            return;
//        }
//        responseString = responseString.trim();
//        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
//        if (jsonObject == null) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    showDialogUser(getString(R.string.msg_empty_data));
//                }
//            });
//            return;
//        }
//        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
//        if (errorCode == Constant.CODE_ENTER_PHONE) {
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.TYPE, loginType);
//            gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
//            return;
//        }
//        if (errorCode == Constant.CODE_VERIFY_OTP) {
//            JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
//            int expire = JsonParser.Companion.getInt(resultObj, "expire");
//            String phone = JsonParser.Companion.getString(resultObj, "phone");
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.TYPE, loginType);
//            bundle.putInt(Constant.DATA, expire);
//            bundle.putString(Constant.PHONE, phone);
//            gotoActivityForResult(UserOTPConfirmActivity.class, bundle, Constant.CODE_LOGIN);
//            return;
//        }
//        if (errorCode == Constant.CODE_REGISTER_FORM) {
//            Bundle bundle = new Bundle();
//            bundle.putString(Constant.TYPE, loginType);
//            gotoActivityForResult(UserRegisterActivity.class, bundle, Constant.CODE_LOGIN);
//            return;
//        }
//        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
//        if (errorCode != Constant.SUCCESS) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    showDialogUser(message);
//                }
//            });
//            return;
//        }
//        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
//        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
//        if (itemUser == null) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    showDialogUser(getString(R.string.msg_empty_data));
//                }
//            });
//            return;
//        }
//        if (isTestShowDialog) {
//            isTestShowDialog = false;
//            itemUser.setLoginStatus("1");
//        }
//        if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK_2) && "1".equals(itemUser.getLoginStatus())) {
////            if (itemUser.getUserNameForum() == null || itemUser.getPasswordForum() == null) {
////                return;
////            }
//            showFormDialog(itemUser.getUserNameForum(), itemUser.getPasswordForum(), itemUser.getGender(), itemUser.getAddress(), itemUser.getJob());
//            return;
//        }
//        resultOk = true;
//        SharedPreferencesManager.saveUser(this, itemUser.getPhone(), itemUser.getAccessToken());
//        finish();
//    }
//
//    private void showFormDialog(String mUserName, String mPassword, String mGender, String mAddress, String mJob) {
//        UserRegisterFormFragment userRegisterFormFragment = new UserRegisterFormFragment();
//        Bundle bundle = new Bundle();
//        bundle.putString(Constant.USERNAME, mUserName);
//        bundle.putString(Constant.PASSWORD, mPassword);
//        bundle.putString(Constant.GENDER, mGender);
//        bundle.putString(Constant.ADDRESS, mAddress);
//        bundle.putString(Constant.JOB, mJob);
//        userRegisterFormFragment.setArguments(bundle);
//        userRegisterFormFragment.show(getSupportFragmentManager(), "UserRegisterFormFragment");
////        userRegisterFormFragment.setOnShowDismissListener(new UserRegisterFormFragment.OnShowDismissListener() {
////            @Override
////            public void onDismiss(boolean isSuccess, @org.jetbrains.annotations.Nullable String userName, @org.jetbrains.annotations.Nullable String password, @org.jetbrains.annotations.Nullable String gender, @org.jetbrains.annotations.Nullable String job, @org.jetbrains.annotations.Nullable String address) {
////                if (isSuccess) {
//////                    login(userName, password, gender, job, address, mPresenterType, mPresenterValue);
////                    Dialog dialogMoreInfo = showMoreInfoUserDialog();
////                    dialogMoreInfo.setOnDismissListener(new DialogInterface.OnDismissListener() {
////                        @Override
////                        public void onDismiss(DialogInterface dialog) {
////                            Loggers.e("INFOLOGIN", userName + "/" + password + "/" + gender + "/" + job + "/" + address + "/" + mPresenterType + "/" + mPresenterValue);
////                            login(userName, password, gender, job, address, mPresenterType, mPresenterValue);
////
////                        }
////                    });
////                }
//////                else {
//////                    login(mUserName, mPassword, mGender, mJob, mAddress);
//////                }
////            }
////
////            @Override
////            public void onShow() {
////
////            }
////        });
//    }
//
//    private void validButtonConfirm() {
//        String phone = editPhone.getText().toString().trim();
//        if (phone.length() < 10) {
//            buttonConfirm.setEnabled(false);
//            return;
//        }
//        String password = editPassword.getText().toString().trim();
//        if (password.length() < 6) {
//            buttonConfirm.setEnabled(false);
//            return;
//        }
//        buttonConfirm.setEnabled(true);
//    }
//
//    private void initControl() {
//        editPhone.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                validButtonConfirm();
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//        editPassword.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                validButtonConfirm();
//            }
//
//            @Override
//            public void afterTextChanged(Editable editable) {
//
//            }
//        });
//        buttonBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                finish();
//            }
//        });
//        buttonConfirm.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                validForm();
//            }
//        });
//        buttonRegisterAccount.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                goRegisterScreen();
//            }
//        });
//        buttonLoginFacebook.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                loginFacebook();
//            }
//        });
//        findViewById(R.id.layoutLoginFacebook).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                loginFacebook();
//            }
//        });
//        imageFacebook.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                loginFacebook();
//            }
//        });
//        buttonForgotPassword.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                hideKeyboard(editPhone);
//                Bundle bundle = new Bundle();
//                bundle.putString(Constant.TYPE, Constant.TYPE_FORGOT_PASSWORD);
//                bundle.putString(Constant.PHONE, editPhone.getText().toString().trim());
//                gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
//            }
//        });
//    }
//
//    private void goRegisterScreen() {
//        hideKeyboard(editPhone);
//        Bundle bundle = new Bundle();
//        bundle.putString(Constant.PHONE, editPhone.getText().toString().trim());
//        gotoActivityForResult(UserRegisterActivity.class, bundle, Constant.CODE_LOGIN);
//    }
//
//    private void loginFacebook() {
//        if (1 == 1) {
//            loginFacebook2();
//            return;
//        }
//        showLoadingDialog(false, null);
//        if (mAuth == null) {
//            mAuth = FirebaseAuth.getInstance();
//        }
//        if (callbackManager == null) {
//            callbackManager = CallbackManager.Factory.create();
//        }
//        loginType = Constant.TYPE_REGISTER_FACEBOOK;
//        userIdSocial = "";
//        emailSocial = "";
//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        handleFacebookAccessToken(loginResult.getAccessToken());
//                    }
//
//                    @Override
//                    public void onCancel() {
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        exception.printStackTrace();
//                        showToast(getString(R.string.msg_login_error) + " (100): " + exception.getMessage());
//                    }
//                });
//        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",
//                "email"));
//
//    }
//
//    private void loginFacebook2() {
//        showLoadingDialog(false, null);
//        /*if (mAuth == null) {
//            mAuth = FirebaseAuth.getInstance();
//        }*/
//        if (callbackManager == null) {
//            callbackManager = CallbackManager.Factory.create();
//        }
//        loginType = Constant.TYPE_REGISTER_FACEBOOK_2;
//        userIdSocial = "";
//        emailSocial = "";
//        LoginManager.getInstance().registerCallback(callbackManager,
//                new FacebookCallback<LoginResult>() {
//                    @Override
//                    public void onSuccess(LoginResult loginResult) {
//                        handleFacebookAccessToken(loginResult.getAccessToken());
//                    }
//
//                    @Override
//                    public void onCancel() {
//                    }
//
//                    @Override
//                    public void onError(FacebookException exception) {
//                        exception.printStackTrace();
//                        showToast(getString(R.string.msg_login_error) + " (100): " + exception.getMessage());
//                    }
//                });
//        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",
//                "email"));
//    }
//
//    private void handleFacebookAccessToken(AccessToken accessToken) {
//        if (accessToken == null) {
//            showToast(getString(R.string.msg_login_error) + " (101)");
//            return;
//        }
//        accessTokenSocial = accessToken.getToken();
//        userIdSocial = accessToken.getUserId();
//        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
//            showToast(getString(R.string.msg_login_error) + " (102)");
//            return;
//        }
//        Loggers.e("MY_CHECK_LOGIN Facebook", accessTokenSocial);
////        START: GET USER EMAIL
//        GraphRequest request = GraphRequest.newMeRequest(
//                accessToken,
//                new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(
//                            JSONObject object,
//                            GraphResponse response) {
//                        Loggers.e("MY_CHECK_LOGIN Facebook_object", object.toString());
//                        userIdSocial = JsonParser.Companion.getString(object, "id");
//                        emailSocial = JsonParser.Companion.getString(object, "email");
//                        String fullNameSocial = JsonParser.Companion.getString(object, "name");
//                        String avatar = "https://graph.facebook.com/" + userIdSocial + "/picture" +
//                                "?type=large";
//                        if (MyApplication.getInstance().isEmpty(emailSocial)) {
//                            emailSocial = userIdSocial + "@mediatech.vn";
//                        }
//                        itemUserTemp.setSocialId(userIdSocial);
//                        itemUserTemp.setAvatar(avatar);
//                        itemUserTemp.setFullname(fullNameSocial);
//                        itemUserTemp.setEmail(emailSocial);
//                        MyApplication.getInstance().getDataManager().setItemUserTemp(itemUserTemp);
//                        login(null, null);
//                    }
//                });
//        Bundle parameters = new Bundle();
//        parameters.putString("fields", "id,email,name,gender,birthday");
//        request.setParameters(parameters);
//        request.executeAsync();
////        END: GET USER EMAIL
//    }
//
//    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
//        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle id:" + acct.getId());
//        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle token:" + acct.getIdToken());
//        accessTokenSocial = acct.getIdToken();
//        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
//            showToast(getString(R.string.msg_login_error) + " (102)");
//            return;
//        }
//        Loggers.e("MY_CHECK_LOGIN accessTokenGoogle", accessTokenSocial);
//        login(null, null);
//    }
//
//    @Override
//    public void onDestroy() {
//        if (myHttpRequest != null) {
//            myHttpRequest.cancel();
//        }
//        super.onDestroy();
//    }
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == Constant.REQUEST_CODE_LOGIN_GOOGLE) {
//            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
//            try {
//                GoogleSignInAccount account = task.getResult(ApiException.class);
//                firebaseAuthWithGoogle(account);
//            } catch (ApiException e) {
//                e.printStackTrace();
//            }
//            return;
//        }
//        if (callbackManager != null) {
//            callbackManager.onActivityResult(requestCode, resultCode, data);
//        }
//        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
//            MyApplication.getInstance().getDataManager().setItemUserTemp(null);
//            if (data != null) {
//                Bundle bundle = data.getExtras();
//                if (bundle != null) {
//                    int type = bundle.getInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SCREEN);
//                    if (type == Constant.RESULT_CODE_LOGIN_SUCCESS) {
//                        resultOk = true;
//                        finish();
//                    }
//                }
//            }
////            finish();
//        }
//    }
//
//    @Override
//    public void finish() {
//        if (resultOk) {
//            Intent intent = new Intent();
////        Bundle bundle = new Bundle();
////        intent.putExtras(bundle);
//            setResult(RESULT_OK, intent);
//        }
//        super.finish();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        hideLoadingDialog();
//    }
//
//    MyDialog currentDialog;
//    private String mPresenterType, mPresenterValue;
//    private boolean isConfirm = false;
//
//    private MyDialog showMoreInfoUserDialog() {
//        if (isDestroyed() || isFinishing()) {
//            return null;
//        }
//        MyDialog dialog = new MyDialog(this);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
//        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
////        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        lp.gravity = Gravity.CENTER;
//        lp.windowAnimations = R.style.DialogAnimationLeftRight;
//        dialog.getWindow().setAttributes(lp);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setCancelable(true);
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.setContentView(R.layout.layout_dialog_user_info_more);
//        ScreenSize screenSize = new ScreenSize(this);
//        dialog.getWindow().setLayout(screenSize.getWidth() * 9 / 10, WindowManager.LayoutParams.WRAP_CONTENT);
//        View layoutRoot = dialog.findViewById(R.id.layoutRoot);
//        EditText editPresenterID = dialog.findViewById(R.id.editPresenterID);
//        EditText editOther = dialog.findViewById(R.id.editOther);
//        TextView buttonCheck = dialog.findViewById(R.id.buttonCheck);
//        TextView textName = dialog.findViewById(R.id.textName);
//        TextView buttonSkip = dialog.findViewById(R.id.buttonCancel);
//        TextView buttonNext = dialog.findViewById(R.id.buttonConfirm);
//        CheckBox checkboxPresenter = dialog.findViewById(R.id.checkboxPresenter);
//        CheckBox checkboxSocial = dialog.findViewById(R.id.checkboxSocial);
//        CheckBox checkboxTV = dialog.findViewById(R.id.checkboxTV);
//        CheckBox checkboxOther = dialog.findViewById(R.id.checkboxOther);
//        Spinner spinnerTv = dialog.findViewById(R.id.spinnerTV);
//        Spinner spinnerSocial = dialog.findViewById(R.id.spinnerSocial);
//        layoutRoot.setClipToOutline(true);
//        layoutRoot.setOutlineProvider(new ViewOutlineProvider() {
//            @Override
//            public void getOutline(View view, Outline outline) {
//                float cornerRadiusDP = 15f;
//                outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), cornerRadiusDP);
//            }
//        });
//        ArrayList<CheckBox> listCheckBox = new ArrayList(Arrays.asList(checkboxTV, checkboxPresenter, checkboxSocial, checkboxOther));
//        initCotrolCheckbox(listCheckBox);
//        initSpinerTV(spinnerTv, checkboxTV);
//        initSpinerSocial(spinnerSocial, checkboxSocial);
//        buttonCheck.setOnClickListener(v -> {
//            String userId = editPresenterID.getText().toString().trim();
//            if (userId.equals("")) {
//                Toast.makeText(this, "Mã giới thiệu không đúng", Toast.LENGTH_SHORT).show();
//                editPresenterID.requestFocus();
//                return;
//            }
//            getName(textName, userId);
//        });
//        editPresenterID.setOnEditorActionListener((v, actionId, event) -> {
//            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
//                getName(textName, editPresenterID.getText().toString().trim());
//            }
//            GeneralUtils.Companion.hideKeyboard(UserLoginActivityold.this);
//            return true;
//        });
//
//        editPresenterID.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (!checkboxPresenter.isChecked()) {
//                    checkboxPresenter.setChecked(true);
//                }
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                textName.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//        editPresenterID.setOnFocusChangeListener((v, hasFocus) ->
//        {
//            if (hasFocus) {
//                editPresenterID.setText("");
//                if (!checkboxPresenter.isChecked()) {
//                    checkboxPresenter.setChecked(true);
//                }
//            }
//        });
//        editOther.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                if (!checkboxOther.isChecked()) {
//                    checkboxOther.setChecked(true);
//                }
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//        editOther.setOnFocusChangeListener((v, hasFocus) ->
//        {
//            if (hasFocus) {
//                if (!checkboxOther.isChecked()) {
//                    checkboxOther.setChecked(true);
//                }
//            }
//        });
//        buttonSkip.setOnClickListener(v ->
//                dialog.dismiss());
//        buttonNext.setOnClickListener(v -> {
//            if (checkboxPresenter.isChecked()) {
//                mPresenterType = "user";
//                String id = editPresenterID.getText().toString().trim();
//                if (GeneralUtils.Companion.isEmpty(id)) {
//                    showKeyboardFocus(editPresenterID);
//                    GeneralUtils.Companion.showToast(UserLoginActivityold.this, "Mã giới thiệu không đúng. Vui lòng nhập và kiểm tra lại.");
//                    return;
//                } else {
//                    ServiceUtil.getNameUser(UserLoginActivityold.this, id, new OnResultRequestListener() {
//                        @Override
//                        public void onResult(boolean isSuccess, @org.jetbrains.annotations.Nullable String msg, @org.jetbrains.annotations.Nullable String avatar) {
//                            runOnUiThread(() -> {
//                                if (isSuccess) {
//                                    dialog.dismiss();
//                                    mPresenterValue = id;
//                                    isConfirm = true;
//                                } else {
//                                    GeneralUtils.Companion.showToast(UserLoginActivityold.this, "Mã giới thiệu không đúng. Vui lòng nhập lại!");
//                                    editPresenterID.requestFocus();
//                                }
//                            });
//                        }
//                    });
//                    return;
//                }
//            } else if (checkboxSocial.isChecked()) {
//                mPresenterType = "social";
//                if (GeneralUtils.Companion.isEmpty(social)) {
//                    GeneralUtils.Companion.showToast(UserLoginActivityold.this, "Vui lòng chọn mạng xã hội.");
//                    spinnerSocial.requestFocus();
//                    return;
//                }
//                mPresenterValue = social;
//            } else if (checkboxTV.isChecked()) {
//                mPresenterType = "tv_station";
//                if (GeneralUtils.Companion.isEmpty(tv)) {
//                    GeneralUtils.Companion.showToast(UserLoginActivityold.this, "Vui lòng chọn đài truyền hình.");
//                    spinnerTv.requestFocus();
//                    return;
//                }
//                mPresenterValue = tv;
//            } else if (checkboxOther.isChecked()) {
//                mPresenterType = "other";
//                if (GeneralUtils.Companion.isEmpty(editOther.getText().toString().trim())) {
//                    GeneralUtils.Companion.showToast(UserLoginActivityold.this, "Bạn vui lòng nhập nội dung khác.");
//                    return;
//                }
//                mPresenterValue = editOther.getText().toString().trim();
//            }
//
//            dialog.dismiss();
//            isConfirm = true;
//        });
//        try {
//            dialog.show();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        checkboxPresenter.setChecked(true);
//        return dialog;
//    }
//
//    private void getName(TextView textName, String userId) {
//        ServiceUtil.getNameUser(this, userId, (isSuccess, msg,avatar) -> {
//            runOnUiThread(() -> {
//                if (!isSuccess) {
//                    GeneralUtils.Companion.showToast(this, msg);
//                    textName.setText("");
//                    textName.setVisibility(View.GONE);
//                } else {
//                    textName.setText(msg);
//                    textName.setVisibility(View.VISIBLE);
//                }
//            });
//        });
//    }
//
//    private String DEFAULT_STRING = "Chọn";
//    private String other, social;
//
//    private void selectCheckbox(CheckBox checkBox, ArrayList<CheckBox> checkBoxArrayList) {
//        if (checkBoxArrayList == null || checkBoxArrayList.size() == 0 || checkBox == null) {
//            return;
//        }
//        for (CheckBox item : checkBoxArrayList) {
//            if (!item.equals(checkBox)) {
//                item.setChecked(false);
//            }
//        }
//    }
//
//    private void initCotrolCheckbox(ArrayList<CheckBox> checkBoxArrayList) {
//        if (checkBoxArrayList == null || checkBoxArrayList.size() == 0) {
//            return;
//        }
//
//        for (CheckBox item : checkBoxArrayList) {
//            item.setOnCheckedChangeListener((buttonView, isChecked) -> {
//                if (isChecked) {
//                    if (item.getId() != R.id.checkboxPresenter && item.getId() != R.id.checkboxOther) {
//                        GeneralUtils.Companion.hideKeyboard(UserLoginActivityold.this);
//                    }
//                    selectCheckbox(item, checkBoxArrayList);
//                }
//            });
//        }
//
//    }
//
//    private void initSpinerTV(Spinner spinner, CheckBox checkBox) {
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                GeneralUtils.Companion.hideKeyboard(UserLoginActivityold.this);
//                if (parent != null) {
//                    if (position == 0 && tv == null) {
//                        return;
//                    }
//                    tv = parent.getItemAtPosition(position).toString();
//                    if (!checkBox.isChecked()) {
//                        checkBox.setChecked(true);
//                    }
//                    if (tv.equals(DEFAULT_STRING)) {
//                        tv = null;
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        ArrayList<String> places = new ArrayList(
//                Arrays.asList(
//                        DEFAULT_STRING,
//                        "Hà Nội",
//                        "Bắc Giang",
//                        "Bắc Kạn",
//                        " Bắc Ninh",
//                        " Bình Phước",
//                        "Hà Nam",
//                        "Hưng Yên",
//                        "Khánh Hòa",
//                        "Lạng Sơn",
//                        "Lào Cai",
//                        "Nghệ An",
//                        "Ninh Bình",
//                        "Quảng Ninh",
//                        "Quảng Trị",
//                        "Thái Bình",
//                        "Thái Nguyên",
//                        "Thanh Hóa",
//                        "Thừa Thiên Huế",
//                        "Tiền Giang",
//                        "Trà Vinh",
//                        "Tuyên Quang",
//                        "Vĩnh Long",
//                        "Vĩnh Phúc", "Khác"
//                )
//        );
//        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivityold.this, android.R.layout.simple_spinner_item, places);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(dataAdapter);
//    }
//
//    private void initSpinerSocial(Spinner spinner, CheckBox checkBox) {
//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                if (parent != null) {
//                    if (position == 0 && social == null) {
//                        return;
//                    }
//                    GeneralUtils.Companion.hideKeyboard(UserLoginActivityold.this);
//                    if (!checkBox.isChecked()) {
//                        checkBox.setChecked(true);
//                    }
//                    social = parent.getItemAtPosition(position).toString();
//                    if (social.equals(DEFAULT_STRING)) {
//                        social = null;
//                    }
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//        ArrayList<String> categories = new ArrayList();
//        categories.add(DEFAULT_STRING);
//        categories.add("Facebook");
//        categories.add("Instagram");
//        categories.add("YouTube");
//        categories.add("TikTok");
//        categories.add("Twitter");
//        categories.add("Khác");
//        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivityold.this, android.R.layout.simple_spinner_item, categories);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(dataAdapter);
//    }
//}