package vn.mediatech.istudiocafe.activity.user

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_user_register_form.*
import kotlinx.android.synthetic.main.layout_actionbar_login_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import vn.mediatech.istudiocafe.listener.OnResultRequestListener
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.util.*
import java.util.*


class UserRegisterFormFragment() : DialogFragment() {
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var userName: String? = null
    var password: String? = null
    var gender: String? = null
    var address: String? = null
    var job: String? = null
    val DEFAULT_STRING = "Chọn"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_user_register_form, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setCancelable(true)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = dialog?.window?.attributes
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp?.gravity = Gravity.CENTER
        lp?.windowAnimations = R.style.DialogAnimationLeftRight
        dialog?.window?.setAttributes(lp!!)
        val screenSize = ScreenSize(requireActivity())
        dialog?.window?.setLayout(
            screenSize.width * 10 / 10,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
//        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
    }

    fun initData() {
        if (arguments == null) {
            return
        }
        val useName = requireArguments().getString(Constant.USERNAME)
        val password = requireArguments().getString(Constant.PASSWORD)
        gender = requireArguments().getString(Constant.GENDER)
        address = requireArguments().getString(Constant.ADDRESS)
        job = requireArguments().getString(Constant.JOB)
        if (!useName.isNullOrEmpty()) {
            editUserName.setText(useName)
        }
        if (!password.isNullOrEmpty()) {
            editPassword.setText(password)
        }
        editConfirmPassword.setText(password)
        editPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
//        editConfirmPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
        initSpinGender()
        initSpinProvice()
        initSpinJob()
        layoutPresenter.visibility = View.GONE
        buttonCancel.visibility = View.GONE
//        val notice = TextUtil.getStringHtmlStype(
//            "Chú ý:",
//            "#FB8622",
//            true,
//            false
//        ) + " Hãy Chatbox ngay với " + TextUtil.getStringHtmlStype(
//            "facebook/ <br/> Đấu Giá Truyền Hình - TuongTac.tv",
//            "#FB8622",
//            true,
//            false
//        ) + " để xác thực tài khoản!"
        val notice = TextUtil.getStringHtmlStype(
            "Chú ý:",
            "#FB8622",
            true,
            false
        ) + " Hãy Chatbox ngay với " + TextUtil.getStringHtmlStype(
            "facebook.com/tuongtac.tv<br/>",
            "#FB8622",
            true,
            false
        ) + " để xác thực tài khoản!"
        TextUtil.setHtmlTextView(notice, textNotice)
        if (!InteractiveConstant.CHANEL_ID.equals("tuongtac.tv")) {
            textTitlePresenter.setText("Biết đến Đấu Giá Truyền Hình qua")
        }
    }

    private fun initSpinProvice() {
        spinnerProvince.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (parent != null) {
                    address = parent.getItemAtPosition(position).toString().trim()
                    if (address.equals(DEFAULT_STRING.trim())) {
                        address = null
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        })
        val places = ArrayList(
            Arrays.asList(
                DEFAULT_STRING,
                "Hà Nội",
                "Tp Hồ Chí Minh",
                "An Giang",
                "Bà Rịa - Vũng Tàu",
                "Bắc Giang",
                "Bắc Kạn",
                "Bạc Liêu",
                "Bắc Ninh",
                "Bến Tre",
                "Bình Định",
                "Bình Dương",
                "Bình Phước",
                "Bình Thuận",
                "Cà Mau",
                "Cần Thơ",
                "Cao Bằng",
                "Đà Nẵng",
                "Đắk Lắk",
                "Đắk Nông",
                "Điện Biên",
                "Đồng Nai",
                "Đồng Tháp",
                "Gia Lai",
                "Hà Giang",
                "Hà Nam",
                "Hà Tĩnh",
                "Hải Dương",
                "Hải Phòng",
                "Hậu Giang",
                "Hòa Bình",
                "Hưng Yên",
                "Khánh Hòa",
                "Kiên Giang",
                "Kon Tum",
                "Lai Châu",
                "Lâm Đồng",
                "Lạng Sơn",
                "Lào Cai",
                "Long An",
                "Nam Định",
                "Nghệ An",
                "Ninh Bình",
                "Ninh Thuận",
                "Phú Thọ",
                "Phú Yên",
                "Quảng Bình",
                "Quảng Nam",
                "Quảng Ngãi",
                "Quảng Ninh",
                "Quảng Trị",
                "Sóc Trăng",
                "Sơn La",
                "Tây Ninh",
                "Thái Bình",
                "Thái Nguyên",
                "Thanh Hóa",
                "Thừa Thiên Huế",
                "Tiền Giang",
                "Trà Vinh",
                "Tuyên Quang",
                "Vĩnh Long",
                "Vĩnh Phúc",
                "Yên Bái"
            )
        )
        var index = 0
        if (!address.isNullOrEmpty()) {
            var isSet = false
            for (i in 0 until places.size) {
                val item = places.get(i)
                if (item.trim().lowercase().equals(address!!.trim().lowercase())) {
                    index = i
                    isSet = true
                }
            }
            if (!isSet) {
                places.add(0, address)
            }
        }
        val dataAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(requireContext(), R.layout.item_spinner_user, places)
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop)
        spinnerProvince.adapter = dataAdapter
        if (index > 0) {
            spinnerProvince.setSelection(index)
        }
    }

    private fun initSpinJob() {
        spinnerJob.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (parent != null) {
                    job = parent.getItemAtPosition(position).toString().trim()
                    if (job.equals(DEFAULT_STRING.trim())) {
                        job = null
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        })
        val categories: MutableList<String> = ArrayList()
        categories.add(DEFAULT_STRING)
        categories.add("Sinh Viên")
        categories.add("Học Sinh")
        categories.add("Đi làm")
        categories.add("Khác")
        var index = 0
        if (!job.isNullOrEmpty()) {
            var isSet = false
            for (i in 0 until categories.size) {
                val item = categories.get(i)
                if (item.trim().lowercase().equals(job!!.trim().lowercase())) {
                    index = i
                    isSet = true
                }
            }
            if (!isSet) {
                categories.add(0, job!!)
            }
        }
        val dataAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(requireContext(), R.layout.item_spinner_user, categories)
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop)
        spinnerJob.adapter = dataAdapter
        if (index > 0) {
            spinnerJob.setSelection(index)
        }
    }

    private fun initSpinGender() {
        spinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (parent != null) {
                    gender = parent.getItemAtPosition(position).toString().trim()
                    if (gender.equals(DEFAULT_STRING.trim())) {
                        gender = null
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        })
        val categories: MutableList<String> = ArrayList()
        categories.add(DEFAULT_STRING)
        categories.add("Nam")
        categories.add("Nữ")
        categories.add("Khác")
        var index = 0
        if (!gender.isNullOrEmpty()) {
            var isSet = false
            for (i in 0 until categories.size) {
                val item = categories.get(i)
                if (item.trim().lowercase().equals(gender!!.trim().lowercase())) {
                    index = i
                    isSet = true
                }
            }
            if (!isSet) {
                categories.add(0, gender!!)
            }
        }
        val dataAdapter: ArrayAdapter<String> =
            ArrayAdapter<String>(requireContext(), R.layout.item_spinner_user, categories)
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop)
        spinner.adapter = dataAdapter
        if (index > 0) {
            spinner.setSelection(index)
        }
    }

    fun initControl() {
        editUserName?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                validButtonConfirm()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        editPassword?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                validButtonConfirm()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        editConfirmPassword?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                validButtonConfirm()
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        buttonBack?.setOnClickListener(View.OnClickListener {
            dismiss()
        })
        buttonCancel?.setOnClickListener {
            isShowMessenger = false
            val notice = TextUtil.getStringHtmlStype(
                "Chú ý:",
                "#FB8622",
                true,
                false
            ) + " Hãy Chatbox ngay với " + TextUtil.getStringHtmlStype(
                "facebook.com/tuongtac.tv<br/>",
                "#FB8622",
                true,
                false
            ) + " để xác thực tài khoản!"
            getData()
//            showDialogAutoClose(notice)
//            showDialog()
        }
        layoutClickEditPrensenter.setOnClickListener {
            showDialog()
        }

        buttonEdit?.setOnClickListener {
            (activity as? BaseActivity)?.showKeyboardFocus(editPassword)
        }
        buttonShowPassword.setOnClickListener {
            val text = buttonShowPassword.text.toString()
            if ("Hiện".equals(text)) {
                buttonShowPassword.text = "Ẩn"
                editPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                buttonShowPassword.text = "Hiện"
                editPassword.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }
        buttonConfirm.setOnClickListener {
            isShowMessenger = true
            getData()
        }
    }

    private fun getData() {
        userName = editUserName.text.toString().trim()
        password = editPassword.text.toString().trim()
        val passwordConfirm = editConfirmPassword.text.toString().trim()
//        if (userName!!.isNullOrEmpty() || userName!!.length < 6 || userName!!.contains(" ")) {
//            Toast.makeText(
//                activity,
//                "Vui lòng nhập tên đăng nhập viết liền và lớn hơn 6 ký tự!",
//                Toast.LENGTH_SHORT
//            ).show()
//            editUserName.requestFocus()
//            return
//        }
        if (password!!.isNullOrEmpty() || password!!.length < 6 || password!!.contains(" ")) {
            Toast.makeText(
                activity,
                "Vui lòng nhập mật khẩu viết liền và lớn hơn 6 ký tự!",
                Toast.LENGTH_SHORT
            ).show()
            editPassword.requestFocus()
            return
        }
//        if (passwordConfirm.isEmpty() || passwordConfirm.length < 6 || passwordConfirm!!.contains(" ")) {
//            Toast.makeText(
//                activity,
//                "Vui lòng nhập mật khẩu viết liền và lớn hơn 6 ký tự!",
//                Toast.LENGTH_SHORT
//            ).show()
//            editConfirmPassword.requestFocus()
//            return
//        }
//
//        if (!passwordConfirm.equals(password)) {
//            Toast.makeText(
//                activity,
//                "Mật khẩu không trùng khớp. Vui lòng nhập lại mật khẩu",
//                Toast.LENGTH_SHORT
//            ).show()
//            editConfirmPassword.requestFocus()
//            return
//        }
        if (gender == null || gender.equals(DEFAULT_STRING)) {
            Toast.makeText(activity, "Vui lòng lựa chọn giới tính", Toast.LENGTH_SHORT).show()
            spinner.requestFocus()
            return
        }
        if (job == null || job.equals(DEFAULT_STRING)) {
            Toast.makeText(activity, "Vui lòng lựa chọn nghề nghiệp", Toast.LENGTH_SHORT).show()
            spinnerJob.requestFocus()
            return
        }
        if (address == null || address.equals(DEFAULT_STRING)) {
            Toast.makeText(activity, "Vui lòng lựa chọn Tỉnh/Thành Phố", Toast.LENGTH_SHORT).show()
            spinnerProvince.requestFocus()
            return
        }
        if (mPresenterType.isNullOrEmpty() || mPresenterValue.isNullOrEmpty()) {
            showDialog()
        } else {
            isConfirm = true
            dismiss()
        }
    }

    private fun showDialog() {
        val dialog = showMoreInfoUserDialog()
        dialog?.setOnDismissListener {
            if (!mPresenterType.isNullOrEmpty() && !mPresenterValue.isNullOrEmpty()) {
                layoutPresenter.visibility = View.VISIBLE
                if (isCommitKnow) {
                    var value = " " + mPresenterValue
                    if (type.equals(TYPE_PRESENTER)) {
                        value = " " + presenterId + "-" + presenterName
                        if (!presenterAvatar.isNullOrEmpty()) {
                            imageAvatar.visibility = View.VISIBLE
                            textPresenter.visibility = View.VISIBLE
                            textPresenter.setText(value)
                            textPresenterType.setText(type)
                            GeneralUtils.loadImageCircle(
                                context,
                                imageAvatar,
                                presenterAvatar,
                                R.drawable.avatar_placeholder
                            )
                        } else {
                            imageAvatar.visibility = View.GONE
                        }
                    } else {
                        imageAvatar.visibility = View.GONE
                        textPresenter.visibility = View.GONE
                        if (type.equals(TYPE_OTHER)) {
                            textPresenterType.setText(value)
                        } else {
                            textPresenterType.setText(type + value)
                        }
                    }
                    buttonCancel.visibility = View.VISIBLE
                    buttonConfirm.setText("Xác thực ngay")
                } else {
                    when (mPresenterType) {
                        TYPE_PRESENTER_VALUE -> {
                            type = TYPE_PRESENTER
                        }
                        TYPE_TV_VALUE -> {
                            type = TYPE_TV
                        }
                        TYPE_SOCIAL_VALUE -> {
                            type = TYPE_SOCIAL
                        }
                        TYPE_OTHER_VALUE -> {
                            type = TYPE_OTHER
                        }
                    }
                }
            }
        }
    }

    private fun validButtonConfirm() {
//        val userName: String = editUserName.getText().toString().trim { it <= ' ' }
//        if (userName.length < 6) {
//            buttonConfirm.isEnabled = false
//            return
//        }
//        val password = editPassword.text.toString().trim { it <= ' ' }
//        if (password.length < 6) {
//            buttonConfirm.isEnabled = false
//            return
//        }
//        val passwordConfirm = editConfirmPassword.text.toString().trim { it <= ' ' }
//        if (passwordConfirm.length < 6) {
//            buttonConfirm.isEnabled = false
//            return
//        }
//        buttonConfirm.isEnabled = password == passwordConfirm
    }

    interface OnShowDismissListener {
        fun onShow()
        fun onDismiss(
            isSuccess: Boolean,
            isShowFacebookMessenger: Boolean,
            userName: String?,
            password: String?,
            gender: String?,
            job: String?,
            address: String?,
            presenterType: String?,
            presenterValue: String?,
        )
    }

    var isConfirm = false
    var isShowMessenger = false
    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onShowDismissListener?.onDismiss(
            isConfirm,
            isShowMessenger,
            userName,
            password,
            gender,
            job,
            address,
            mPresenterType,
            mPresenterValue
        )
    }

    private var mPresenterType: String? = null
    private var mPresenterValue: String? = null
    private var tv: String? = null
    private var other: String? = null
    private var social: String? = null
    private var type: String? = null
    private var isCommitKnow: Boolean = false
    private fun showMoreInfoUserDialog(): MyDialog? {
        if (isDetached) {
            return null
        }
        isCommitKnow = false
        val dialog = MyDialog(requireContext())
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window!!.attributes = lp
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.layout_dialog_user_know_app)
        val screenSize =
            ScreenSize(requireActivity())
        dialog.window!!.setLayout(
            screenSize.width * 10 / 10,
            WindowManager.LayoutParams.MATCH_PARENT
        )
        val layoutRoot = dialog.findViewById<View>(R.id.layoutRoot)
        val editPresenterID = dialog.findViewById<EditText>(R.id.editPresenterID)
        val editOther = dialog.findViewById<EditText>(R.id.editOther)
        val buttonCheck =
            dialog.findViewById<ImageView>(R.id.buttonCheck)
        val textName = dialog.findViewById<TextView>(R.id.textName)
        val textScreenUserDescription =
            dialog.findViewById<TextView>(R.id.textScreenUserDescription)
        if (!InteractiveConstant.CHANEL_ID.equals("tuongtac.tv")) {
            val title = "Bạn biết đến Đấu Giá Truyền Hình<br/>qua hình thức nào"
            TextUtil.setHtmlTextView(title, textScreenUserDescription)
//            textScreenUserDescription.setText("Biết đến Đấu Giá Truyền Hình qua")
        }
        val imageAvatar =
            dialog.findViewById<ImageView>(R.id.imageAvatar)
        val buttonSkip = dialog.findViewById<TextView>(R.id.buttonCancel)
        val buttonNext = dialog.findViewById<TextView>(R.id.buttonConfirm)
        val spinnerTv = dialog.findViewById<Spinner>(R.id.spinnerTV)
        val spinnerSocial = dialog.findViewById<Spinner>(R.id.spinnerSocial)
        val spinnerType = dialog.findViewById<Spinner>(R.id.spinnerType)
        val layoutPresenter =
            dialog.findViewById<View>(R.id.layoutPresenter)
        val layoutTV = dialog.findViewById<View>(R.id.layoutTV)
        val layoutSocial =
            dialog.findViewById<View>(R.id.layoutSocial)
        val layoutOther =
            dialog.findViewById<View>(R.id.layoutOther)
        val progressBar =
            dialog.findViewById<View>(R.id.progressBar)
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        initSpinerTV(spinnerTv, buttonNext)
        initSpinerSocial(spinnerSocial, buttonNext)
        buttonCheck.setOnClickListener { v: View? ->
            val userId = editPresenterID.text.toString().trim { it <= ' ' }
            if (userId == "") {
                Toast.makeText(requireContext(), "Mã giới thiệu không đúng", Toast.LENGTH_SHORT)
                    .show()
                editPresenterID.requestFocus()
                return@setOnClickListener
            }
            getName(textName, imageAvatar, userId, progressBar, buttonNext)
        }
        editPresenterID.setOnEditorActionListener { v: TextView?, actionId: Int, event: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                getName(
                    textName,
                    imageAvatar,
                    editPresenterID.text.toString().trim { it <= ' ' },
                    progressBar, buttonNext
                )
            }
            GeneralUtils.hideKeyboard(activity)
            true
        }
        editPresenterID.setText(presenterId)
        editOther.setText(other)
        editPresenterID.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                textName.visibility = View.GONE
                imageAvatar.visibility = View.GONE
                if (editPresenterID.text.toString().trim().length >= 1) {
//                    getName(textName, imageAvatar, editPresenterID.getText().toString().trim(), progressBar);
                    buttonNext.setEnabled(true);
                } else {
                    buttonNext.isEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        editOther.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (editOther.text.toString().trim().length >= 1) {
                    buttonNext.isEnabled = true
                } else {
                    buttonNext.isEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        val listLayout: ArrayList<View?> = ArrayList<View?>(
            Arrays.asList(
                layoutSocial,
                layoutTV,
                layoutOther,
                layoutPresenter
            )
        )
        initSpinerType(spinnerType)
        spinnerType.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                if (parent != null) {
                    GeneralUtils.hideKeyboard(context)
                    type = parent.getItemAtPosition(position).toString()
                    if (type == DEFAULT_STRING) {
                        buttonNext.isEnabled = false
                        showLayout(layoutRoot, listLayout)
                    } else if (type == TYPE_PRESENTER) {
                        showLayout(layoutPresenter, listLayout)
                    } else if (type == TYPE_SOCIAL) {
                        showLayout(layoutSocial, listLayout)
                        if (social != null) {
                            buttonNext.isEnabled = true
                        } else {
                            spinnerSocial.performClick()
                            buttonNext.isEnabled = false
                        }
                    } else if (type == TYPE_TV) {
                        showLayout(layoutTV, listLayout)
                        if (tv != null) {
                            buttonNext.isEnabled = true
                        } else {
                            spinnerTv.performClick()
                            buttonNext.isEnabled = false
                        }
                    } else if (type == TYPE_OTHER) {
                        showLayout(layoutOther, listLayout)
                        if (other != null) {
                            buttonNext.isEnabled = true
                        } else {
                            editOther.requestFocus()
                            buttonNext.isEnabled = false
                        }
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        buttonSkip.setOnClickListener { v: View? -> dialog.dismiss() }
        buttonNext.setOnClickListener { v: View? ->
            GeneralUtils.hideKeyboard(context)
            if (type == TYPE_PRESENTER) {
                mPresenterType = TYPE_PRESENTER_VALUE
                val id = editPresenterID.text.toString().trim { it <= ' ' }
                if (GeneralUtils.isEmpty(id)) {
                    GeneralUtils.showKeyboardFocus(activity, editPresenterID)
                    showDialogAutoClose(
                        "Mã giới thiệu không đúng. Vui lòng nhập và kiểm tra lại."
                    )
                    return@setOnClickListener
                } else {
                    if (imageAvatar.visibility != View.VISIBLE) {
                        if (textName.visibility == View.VISIBLE) {
                            val msg = "Mã giới thiệu không đúng. Vui lòng nhập và kiểm tra lại."
                            showDialogAutoClose(msg)
                        } else {
                            getName(textName, imageAvatar, id, progressBar, buttonNext, true)
                        }
                        return@setOnClickListener
                    }
                    mPresenterValue = id
/*//                    GeneralUtils.getNameUser(
//                        context,
//                        id,
//                        object : OnResultRequestListener {
//                            override fun onResult(
//                                isSuccess: Boolean,
//                                msg: String?,
//                                avatar: String?
//                            ) {
//                                activity?.runOnUiThread(Runnable {
//                                    if (isSuccess) {
//                                        dialog.dismiss()
//                                        mPresenterValue = id
//                                    } else {
//                                        GeneralUtils.showToast(
//                                            activity,
//                                            "Mã giới thiệu không đúng. Vui lòng nhập lại!"
//                                        )
//                                        editPresenterID.requestFocus()
//                                    }
//                                })
//                            }
//                        })
//                    return@setOnClickListener*/
                }
            } else if (type == TYPE_SOCIAL) {
                mPresenterType = TYPE_SOCIAL_VALUE
                if (GeneralUtils.isEmpty(social)) {
                    showDialogAutoClose(
                        "Vui lòng chọn mạng xã hội."
                    )
                    spinnerSocial.requestFocus()
                    return@setOnClickListener
                }
                mPresenterValue = social
            } else if (type == TYPE_TV) {
                mPresenterType = TYPE_TV_VALUE
                if (GeneralUtils.isEmpty(tv)) {
                    showDialogAutoClose(
                        "Vui lòng chọn đài truyền hình."
                    )
                    spinnerTv.requestFocus()
                    return@setOnClickListener
                }
                mPresenterValue = tv
            } else if (type == DEFAULT_STRING) {
                mPresenterType = TYPE_TV_VALUE
                if (GeneralUtils.isEmpty(tv)) {
                    showDialogAutoClose(
                        "Vui lòng chọn hình thức bạn biết đến app TuongTac.tv"
                    )
                    return@setOnClickListener
                }
                mPresenterValue = tv
            } else if (type == TYPE_OTHER) {
                mPresenterType = TYPE_OTHER_VALUE
                if (GeneralUtils.isEmpty(
                        editOther.text.toString().trim { it <= ' ' })
                ) {
                    showDialogAutoClose(
                        "Bạn vui lòng nhập nội dung khác."
                    )
                    return@setOnClickListener
                }
                mPresenterValue = editOther.text.toString().trim()
                other = mPresenterValue
            }
            dialog.dismiss()
            isCommitKnow = true
        }
        try {
            dialog.show()
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                if (!mPresenterType.isNullOrEmpty() && !mPresenterValue.isNullOrEmpty()) {
                    buttonNext.isEnabled = true
                } else {
                    buttonNext.isEnabled = false
                }
            }, 1000)

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    var myHttpRequestGetName: MyHttpRequest? = null
    private var presenterName: String? = null
    private var presenterAvatar: String? = null
    private var presenterId: String? = null
    private fun getName(
        textName: TextView?,
        imageAvatar: ImageView?,
        userId: String,
        progress: View?, buttonNext: View? = null, isShowDialogConfirm: Boolean = false
    ) {
        if (myHttpRequestGetName != null) {
            myHttpRequestGetName!!.cancel()
        }
        progress?.visibility = View.VISIBLE
        myHttpRequestGetName =
            ServiceUtilTT.getNameUser(context, userId, object : OnResultRequestListener {
                override fun onResult(isSuccess: Boolean, msg: String?, msg1: String?) {
                    activity?.runOnUiThread(
                        Runnable
                        {
                            progress?.visibility = View.GONE
                            presenterId = userId
                            if (!isSuccess) {
//                    GeneralUtils.Companion.showToast(this, msg);
                                textName?.text = msg
                                textName?.setTextColor(Color.RED)
                                showDialogAutoClose(msg)
                                imageAvatar?.visibility = View.GONE
                                textName?.visibility = View.VISIBLE
                                presenterName = ""
                                buttonNext?.isEnabled = false
                            } else {
                                presenterName = msg
                                presenterAvatar = msg1
                                textName?.setTextColor(Color.parseColor("#4f8cff"))
                                textName?.text = msg
                                textName?.visibility = View.VISIBLE
                                imageAvatar?.visibility = View.VISIBLE
                                if (!GeneralUtils.isEmpty(msg1)) {
                                    GeneralUtils.loadImageCircle(
                                        context,
                                        imageAvatar,
                                        msg1,
                                        R.drawable.avatar_placeholder
                                    )
                                }
                                if (isShowDialogConfirm) {
                                    showDialogAutoClose("Vui lòng kiểm tra thông tin người giới thiệu đã đúng chưa?")
                                }
                            }
                        })
                }
            })
    }


    private fun showLayout(
        layout: View?,
        layoutArrayList: ArrayList<View?>?
    ) {
        if (layoutArrayList == null || layoutArrayList.size == 0 || layout == null) {
            return
        }
        for (item in layoutArrayList) {
            if (item != layout) {
                item!!.visibility = View.INVISIBLE
            } else {
                item.visibility = View.VISIBLE
            }
        }
    }

    private fun initSpinerTV(spinner: Spinner, buttonNext: View) {
        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                GeneralUtils.hideKeyboard(context)
                if (parent != null) {
                    tv = parent.getItemAtPosition(position).toString().trim { it <= ' ' }
                    if (tv == DEFAULT_STRING.trim { it <= ' ' }) {
                        tv = null
                        buttonNext.isEnabled = false
                    } else {
                        buttonNext.isEnabled = true
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        val places: ArrayList<String> = ArrayList(
            Arrays.asList(
                DEFAULT_STRING,
                "Hà Nội",
                "Bắc Giang",
                "Bắc Kạn",
                " Bắc Ninh",
                " Bình Phước",
                "Hà Nam",
                "Hưng Yên",
                "Khánh Hòa",
                "Lạng Sơn",
                "Lào Cai",
                "Nghệ An",
                "Ninh Bình",
                "Quảng Ninh",
                "Quảng Trị",
                "Thái Bình",
                "Thái Nguyên",
                "Thanh Hóa",
                "Thừa Thiên Huế",
                "Tiền Giang",
                "Trà Vinh",
                "Tuyên Quang",
                "Vĩnh Long",
                "Vĩnh Phúc",
                "Khác"
            )
        )
        var index = 0
        if (!tv.isNullOrEmpty()) {
            var isSet = false
            for (i in 0 until places.size) {
                val item = places.get(i)
                if (item.trim().lowercase().equals(tv!!.trim().lowercase())) {
                    index = i
                    isSet = true
                }
            }
            if (!isSet) {
                places.add(0, tv!!)
            }
        }
        val dataAdapter: ArrayAdapter<String> =
            ArrayAdapter(requireContext(), R.layout.item_spinner_user, places)
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop)
        spinner.adapter = dataAdapter

        if (index > 0) {
            spinner.setSelection(index)
        }
    }

    private val TYPE_PRESENTER = "Người giới thiệu"
    private val TYPE_TV = "Đài truyền hình"
    private val TYPE_SOCIAL = "Mạng xã hội"
    private val TYPE_OTHER = "Hình thức khác"
    private val TYPE_PRESENTER_VALUE = "user"
    private val TYPE_TV_VALUE = "tv_station"
    private val TYPE_SOCIAL_VALUE = "social"
    private val TYPE_OTHER_VALUE = "other"

    private fun initSpinerSocial(spinner: Spinner, buttonNext: View) {
        spinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View,
                position: Int,
                id: Long
            ) {
                if (parent != null) {
                    GeneralUtils.hideKeyboard(context)
                    social = parent.getItemAtPosition(position).toString().trim { it <= ' ' }
                    if (social == DEFAULT_STRING.trim { it <= ' ' }) {
                        social = null
                        buttonNext.isEnabled = false
                    } else {
                        buttonNext.isEnabled = true
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {}
        }
        val categories: ArrayList<String> = ArrayList()
        categories.add(DEFAULT_STRING)
        categories.add("Facebook")
        categories.add("Instagram")
        categories.add("YouTube")
        categories.add("TikTok")
        categories.add("Twitter")
        categories.add("Khác")
        var index = 0
        if (!social.isNullOrEmpty()) {
            var isSet = false
            for (i in 0 until categories.size) {
                val item = categories.get(i)
                if (item.trim().lowercase().equals(social!!.trim().lowercase())) {
                    index = i
                    isSet = true
                }
            }
            if (!isSet) {
                categories.add(0, social!!)
            }
        }
        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            requireContext(),
            R.layout.item_spinner_user,
            categories
        )
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop)
        spinner.adapter = dataAdapter
        if (index > 0) {
            spinner.setSelection(index)
        }
    }

    private fun initSpinerType(spinner: Spinner) {
        val categories: ArrayList<String> = ArrayList()
        categories.add(DEFAULT_STRING)
        categories.add(TYPE_TV)
        categories.add(TYPE_SOCIAL)
        categories.add(TYPE_PRESENTER)
        categories.add(TYPE_OTHER)
        var index = 0
        if (!type.isNullOrEmpty()) {
            var isSet = false
            for (i in 0 until categories.size) {
                val item = categories.get(i)
                if (item.trim().lowercase().equals(type!!.trim().lowercase())) {
                    index = i
                    isSet = true
                }
            }
            if (!isSet) {
                categories.add(0, type!!)
            }
        }
        //        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, android.R.layout.simple_spinner_item, categories);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        val dataAdapter: ArrayAdapter<String> = ArrayAdapter<String>(
            requireContext(),
            R.layout.item_spinner_user,
            categories
        )
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop)
        spinner.adapter = dataAdapter
        if (index > 0) {
            spinner.setSelection(index)
        }
    }

    private fun showDialogAutoClose(msg: String?) {
        if (msg.isNullOrEmpty()) {
            return
        }
        val dialog = GeneralUtils.showDialog(activity, msg)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            dialog?.dismiss()
        }, 2000)
    }
}




