package vn.mediatech.istudiocafe.activity

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.webkit.*
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.activity_detail_news_tt.*
import kotlinx.android.synthetic.main.layout_actionbar_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.adapter.NewsAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.swipebacklayout.SwipeBackLayout
import vn.mediatech.istudiocafe.swipebacklayout.app.SwipeBackActivity
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.voicecontrol.voiceControl.VoiceControllerManager
import java.util.*

@Suppress("DEPRECATION")
class DetailNewsActivity : SwipeBackActivity() {
    var itemObj: ItemNews? = null
    var isLoading: Boolean = false
    val limit = 10
    var itemList: ArrayList<ItemNews> = ArrayList()
    var adapter: NewsAdapter? = null
    var myHttpRequest: MyHttpRequest? = null

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(Constant.DATA, itemObj)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_news_tt)
        initSwipeBack()
        initUI()
        initData(savedInstanceState)
        initControl()
    }

    fun initSwipeBack() {
        val edgeFlag: Int = SwipeBackLayout.EDGE_LEFT
        swipeBackLayout.setEdgeTrackingEnabled(edgeFlag)
        swipeBackLayout.setEdgeSize(ScreenSize(this).width / 9)
//        mSwipeBackLayout.setTouchFullScreen(true);
    }

    fun initUI() {
        setStatusbarColor("#ffffff", true)
    }

    fun initData(savedInstanceState: Bundle?) {
        val bundle = savedInstanceState ?: intent.extras
        itemObj = bundle?.getParcelable(Constant.DATA)
        if (itemObj == null) {
            finish()
            return
        }
        textActionbarTitle.text = getString(R.string.back)
        /*textTitle.text = itemObj!!.name
        if (itemObj!!.categoryName.isNullOrEmpty()) {
            textCategoryName.visibility = View.GONE
        } else {
            textCategoryName.text = itemObj!!.categoryName
        }
        textTime.text = itemObj!!.timeAgo*/
        initWebSetting()
        getData()
    }

    fun showErrorNetwork(message: String) {
        runOnUiThread {
            showDialog(
                false,
                R.string.notification,
                message,
                R.string.close,
                R.string.try_again,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        finish()
                    }

                    override fun onRightButtonClick() {
                        getData()
                    }

                })
        }
    }

    fun getData() {
        if (isLoading) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(this)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("id", "${itemObj!!.id}")
        val itemUser = MyApplication.getInstance().dataManager.itemUser
        if (itemUser != null) {
            requestParams.put("access_token", "${itemUser.accessToken}")
        }
        var api = ServiceUtilTT.validAPI(Constant.API_NOTIFICATION_DETAIL)
        api = ServiceUtilTT.validAPIDGTH(this, Constant.API_NEWS_DETAIL_NEW)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isFinishing || isDestroyed) {
                    return
                }
                isLoading = false
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isFinishing || isDestroyed) {
                    return
                }
                handleData(responseString)
                runOnUiThread {
                    progressBar.visibility = View.GONE
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            runOnUiThread {
                showDialog(message)
            }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
//        val dataObj = JsonParser.getJsonObject(resultObj, Constant.DATA)
        itemObj = JsonParser.parseItemNews(this, resultObj, null)
        if (itemObj == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        /* val othersObj = JsonParser.getJsonObject(resultObj, "others")
         val dataOtherArr = JsonParser.getJsonArray(othersObj, Constant.DATA)
         val list = JsonParser.parseItemNewsList(this, dataOtherArr)
         itemList.clear()
         if (list != null) {
             itemList.addAll(list)
         }*/
        setData()
//        setRelatesData()
    }

    fun setData() {
        runOnUiThread {
            textTitle.text = itemObj!!.name
            if (itemObj!!.categoryName.isNullOrEmpty()) {
                textCategoryName.visibility = View.GONE
            } else {
                textCategoryName.text = itemObj!!.categoryName
                textCategoryName.visibility = View.VISIBLE
            }
            textTime.text = itemObj!!.timeAgo
            textView.text = itemObj!!.view
            textComment.text = itemObj!!.comment
            layoutContent.visibility = View.VISIBLE
//            TextUtil.setWebviewContent(itemObj!!.info, webView);
//            val url = if (itemObj!!.url.isNullOrEmpty()) "about:blank" else itemObj!!.webview
            val url = itemObj!!.webview ?: "about:blank"
            if (itemObj!!.webview == null) {
                if (itemObj!!.info != null) {
                    val header =
                        "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'><style>img{max-width:100%} body{text-align:justify; padding: 0px; margin: 0px;font-family: Arial; font-size: 100%;}</style></header>"
                    webView.loadData(header + itemObj!!.info!!, "text/html", "UTF-8")
                }
            } else {
                webView.loadUrl(url!!)
            }
//            VoiceControllerManager.instance?.speak(this,GeneralUtils.getHtmlFormat(itemObj!!.info).toString())
            webView.requestFocus(View.FOCUS_UP)
        }
    }

    fun setRelatesData() {
        if (adapter != null) {
            runOnUiThread {
                adapter?.notifyDataSetChanged()
            }
            return
        }
        val numberColumn = 1
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        val layoutManager =
            StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
        val style = Constant.STYLE_LISTVIEW
        val itemDecoration: RecyclerView.ItemDecoration =
            SpacesItemDecoration(spacePx, numberColumn)
        recyclerView.setHasFixedSize(true)
        recyclerView.itemAnimator = DefaultItemAnimator()
        runOnUiThread {
            adapter = NewsAdapter(this, itemList, style, numberColumn, true)
            recyclerView.isNestedScrollingEnabled = false
            recyclerView.addItemDecoration(itemDecoration)
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
//            recyclerView.scheduleLayoutAnimation()
            adapter!!.onItemClickListener = object : NewsAdapter.OnItemClickListener {
                override fun onClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                    when (itemObj?.contentType) {
                        Constant.TYPE_NEWS -> {
                            itemObj = itemObject
                            reloadDataClicked()
                        }
                        else -> {
                            goDetailNewsActivity(itemObject)
                        }
                    }
                }

                override fun onLongClick(itemObject: ItemNews, position: Int, view: View) {
                }

                override fun onCommentClick(itemObject: ItemNews, position: Int) {
                }

                override fun onShareClick(itemObject: ItemNews, position: Int) {
                    shareApp(itemObject)
                }

                override fun onOrderClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                }
            }
        }
    }

    fun reloadDataClicked() {
        layoutContent.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
        stopLoadWebview()
        getData()
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initWebSetting() {
        val webSettings = webView.settings
        val defaultFontSizePercent = SharedPreferencesManager.getDefaultWebviewFontSize(this)
        webSettings.textZoom = defaultFontSizePercent
        webSettings.setSupportZoom(false)
        webSettings.builtInZoomControls = false
//        webSettings.setLoadWithOverviewMode(true);
//        webSettings.setUseWideViewPort(true);
        //        webSettings.setLoadWithOverviewMode(true);
//        webSettings.setUseWideViewPort(true);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.R) {
            webSettings.setAppCacheEnabled(true)
        }
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportMultipleWindows(true)
//        webSettings.setPluginState(WebSettings.PluginState.ON);
        //        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.allowFileAccess = true
        webSettings.loadsImagesAutomatically = true
        webSettings.mediaPlaybackRequiresUserGesture = false
        CookieManager.getInstance().setAcceptCookie(true)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webView.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
            CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
        }
        //        String info = "<style>video, img{width: 100%; height: auto;} body{text-align:justify; padding: 0px; margin: 0px;}</style>" + itemObj.getInfo();
        webView.webChromeClient = MyChromeClient(this)
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressBarWebview.visibility = View.VISIBLE
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBarWebview.visibility = View.GONE
                super.onPageFinished(view, url)
            }

            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                progressBarWebview.visibility = View.GONE
                super.onReceivedError(view, request, error)
            }
        }
        webView.setBackgroundColor(Color.TRANSPARENT)
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            finish()
        }
        textShare.setOnClickListener {
            shareApp(itemObj!!)
        }
    }

    private fun stopLoadWebview() {
        if (webView != null) {
            webView.webViewClient = WebViewClient()
            webView.loadUrl("javascript:(function() { document.getElementsByTagName('video')[0].pause(); })()")
            webView.loadUrl("about:blank")
        }
    }

    override fun finish() {
        myHttpRequest?.cancel()
        stopLoadWebview()
        if (webView != null) {
            val webSettings = webView.settings
            SharedPreferencesManager.saveDefaultWebviewFontSize(this, webSettings.textZoom)
        }
        super.finish()
    }
}