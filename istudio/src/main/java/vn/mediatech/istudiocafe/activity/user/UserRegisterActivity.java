package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.listener.OnKeyboardVisibilityListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;
import vn.mediatech.istudiocafe.util.SharedPreferencesManager;

public class UserRegisterActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonConfirm;
    private EditText editPhone, editPassword, editConfirmPassword;
    private MyHttpRequest myHttpRequest;
    private boolean resultOk = false;
    private String loginType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register_tt);
        initUI();
        initData();
        initControl();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editPhone = findViewById(R.id.editPhone);
        editPassword = findViewById(R.id.editPassword);
        editConfirmPassword = findViewById(R.id.editConfirmPassword);
        buttonConfirm = findViewById(R.id.buttonConfirm);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
        String phoneEntered = bundle.getString(Constant.PHONE, "");
        loginType = bundle.getString(Constant.TYPE, Constant.TYPE_REGISTER_FORM);
        editPhone.setText(phoneEntered);
        editPhone.setSelection(phoneEntered.length());
    }

    private void validForm() {
        getData();
    }

    private void getData() {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        String phone = editPhone.getText().toString().trim();
        String password = editPassword.getText().toString().trim();
        String passwordConfirm = editConfirmPassword.getText().toString().trim();
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        String socialId = "", email = "", fullName = "";
        if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK)) {
            ItemUser itemUserTemp = MyApplication.getInstance().getDataManager().getItemUserTemp();
            if (itemUserTemp != null) {
                socialId = itemUserTemp.getSocialId();
                email = itemUserTemp.getEmail();
                fullName = itemUserTemp.getFullname();
            }
        }
        requestParams.put("type", loginType);
        requestParams.put("phone", phone);
        requestParams.put("password", password);
        requestParams.put("confirm_password", passwordConfirm);
        requestParams.put("social_id", socialId);
        requestParams.put("email", email);
        requestParams.put("fullname", fullName);
        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(this,Constant.API_USER_REGISTER),
                requestParams, new MyHttpRequest.ResponseListener() {
            @Override
            public void onFailure(int statusCode) {
                if (isFinishing() || isDestroyed()) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog();
                        showDialogUser(getString(R.string.msg_network_error));
                    }
                });
            }

            @Override
            public void onSuccess(int statusCode, String responseString) {
                if (isFinishing() || isDestroyed()) {
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        hideLoadingDialog();
                        handleRegisterData(responseString);
                    }
                });
            }
        });
    }

    private void handleRegisterData(String responseString) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        String phone = editPhone.getText().toString().trim();
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode == Constant.CODE_VERIFY_OTP) {
            int result = JsonParser.Companion.getInt(jsonObject, Constant.RESULT);
            Bundle bundle = new Bundle();
            bundle.putInt(Constant.DATA, result);
            bundle.putString(Constant.TYPE, Constant.TYPE_REGISTER_FORM);
            bundle.putString(Constant.PHONE, phone);
            gotoActivityForResult(UserOTPConfirmActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        if (errorCode != Constant.SUCCESS) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
        if (itemUser == null) {
            showDialogUser(getString(R.string.msg_empty_data));
            return;
        }
        resultOk = true;
        SharedPreferencesManager.saveUser(this, itemUser.getPhone(), itemUser.getAccessToken());
        if (itemUser.isPassword()) {
            gotoActivityForResult(UserEnterNewPasswordActivity.class, Constant.CODE_LOGIN);
            return;
        }
        if (bundleResult == null) {
            bundleResult = new Bundle();
        }
        bundleResult.putInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SUCCESS);
        finish();
    }

    private void validButtonConfirm() {
        String phone = editPhone.getText().toString().trim();
        if (phone.length() < 10) {
            buttonConfirm.setEnabled(false);
            return;
        }
        String password = editPassword.getText().toString().trim();
        if (password.length() < 6) {
            buttonConfirm.setEnabled(false);
            return;
        }
        String passwordConfirm = editConfirmPassword.getText().toString().trim();
        if (passwordConfirm.length() < 6) {
            buttonConfirm.setEnabled(false);
            return;
        }
        buttonConfirm.setEnabled(password.equals(passwordConfirm));
    }

    private void initControl() {
        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editConfirmPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPassword);
                finish();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPassword);
                validForm();
            }
        });
        setOnKeyboardVisibilityListener(new OnKeyboardVisibilityListener() {
            @Override
            public void onVisibilityChanged(boolean visible) {
                TextView textScreenUserDescription = findViewById(R.id.textScreenUserDescription);
                textScreenUserDescription.setVisibility(visible ? View.GONE : View.VISIBLE);
            }
        });
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    private Bundle bundleResult;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            resultOk = true;
           /* if (data != null) {
                Bundle bundle = data.getExtras();
                if (bundle == null) {
                    int a = 6 % 2;
                } else {
                    int typeM = bundle.getInt(Constant.TYPE, -1001);
                }
            }*/
            bundleResult = data != null ? data.getExtras() : null;
            finish();
        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
            if (bundleResult != null) {
                intent.putExtras(bundleResult);
            }
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }
}