package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnKeyboardVisibilityListener;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;

public class UserRegisterUpdateInfoActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonConfirm, textDistrict;
    private EditText editFullName;
    private MyHttpRequest myHttpRequest;
    private boolean resultOk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register_update_info_tt);
        initUI();
        initData();
        initControl();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editFullName = findViewById(R.id.editText);
        textDistrict = findViewById(R.id.textDistrict);
        buttonConfirm = findViewById(R.id.buttonConfirm);
    }

    private void initData() {
    }

    private void validForm() {
        getData();
    }

    private void getData() {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        ItemUser itemUser = MyApplication.getInstance().getDataManager().getItemUser();
        if (itemUser == null) {
            showDialogUser(getString(R.string.msg_empty_data));
            return;
        }
        String accessToken = itemUser.getAccessToken();
        if (MyApplication.getInstance().isEmpty(accessToken)) {
            showDialogUser(getString(R.string.msg_empty_data));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        String password = editFullName.getText().toString().trim();
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        requestParams.put("fullname", password);
        requestParams.put("access_token", accessToken);
        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(this,Constant.API_USER_CHANGE_PASSSWORD),
                requestParams, new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                showDialogUser(getString(R.string.msg_network_error));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                handleData(responseString);
                            }
                        });
                    }
                });
    }

    private void handleData(String responseString) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode != Constant.SUCCESS) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        showToast(message);
        resultOk = true;
        finish();
    }

    private void validButtonConfirm() {
        String password = editFullName.getText().toString().trim();
        if (password.length() < 2) {
            buttonConfirm.setEnabled(false);
            return;
        }
        buttonConfirm.setEnabled(true);
    }

    private void initControl() {
        editFullName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editFullName);
                resultOk = true;
                finish();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editFullName);
                validForm();
            }
        });
        setOnKeyboardVisibilityListener(new OnKeyboardVisibilityListener() {
            @Override
            public void onVisibilityChanged(boolean visible) {
                TextView textScreenUserDescription = findViewById(R.id.textScreenUserDescription);
                textScreenUserDescription.setVisibility(visible ? View.GONE : View.VISIBLE);
            }
        });
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    private Bundle bundleResult;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            resultOk = true;
            bundleResult = data != null ? data.getExtras() : null;
            finish();
        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
            if (bundleResult == null) {
                bundleResult = new Bundle();
            }
            bundleResult.putInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SUCCESS);
            intent.putExtras(bundleResult);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            hideKeyboard(editFullName);
            resultOk = true;
            finish();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}
