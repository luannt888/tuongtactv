package vn.mediatech.istudiocafe.activity

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.FragmentActivity
import kotlinx.android.synthetic.main.activity_interactive.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.fragment.MainInteractiveFragment
import vn.mediatech.istudiocafe.listener.OnCreateViewListener

class InteractiveActivity : FragmentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_interactive)
        init()
    }

    var mainInteractiveFragment: MainInteractiveFragment? = null
    private fun init() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        mainInteractiveFragment = MainInteractiveFragment()
        val bundle = Bundle()
        bundle.putString(Constant.APP_ID, "tuongtac.tv")
        mainInteractiveFragment!!.arguments = bundle
        fragmentTransaction.add(frameLayout.id, mainInteractiveFragment!!)
        fragmentTransaction.commitNowAllowingStateLoss()
        mainInteractiveFragment?.onCreateViewListener = object : OnCreateViewListener {
            override fun onCreated() {
                mainInteractiveFragment?.checkRefresh()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val list = supportFragmentManager.fragments
        if (list.size > 0) {
            for (item in list) {
                item.onActivityResult(requestCode, resultCode, data)
            }
        }
    }

    override fun onBackPressed() {
        if (mainInteractiveFragment == null || !mainInteractiveFragment!!.canBack()) {
            super.onBackPressed();
        }
    }
}