package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.Loggers;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.listener.OnLoginFormListener;
import vn.mediatech.istudiocafe.listener.OnResultRequestListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.GeneralUtils;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.MyDialog;
import vn.mediatech.istudiocafe.util.ScreenSize;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;
import vn.mediatech.istudiocafe.util.SharedPreferencesManager;
import vn.mediatech.istudiocafe.util.TextUtil;
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant;

public class UserLoginActivity extends BaseActivity {
    private ImageView buttonBack, imageFacebook;
    private TextView buttonRegisterAccount, buttonLoginFacebook, buttonConfirm,
            buttonForgotPassword;
    private EditText editPhone, editPassword;
    private MyHttpRequest myHttpRequest;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private String loginType = Constant.TYPE_REGISTER_FORM;
    private String accessTokenSocial = "";
    private String userIdSocial = "";
    private String emailSocial = "";
    private ItemUser itemUserTemp = new ItemUser();
    private boolean resultOk = false;
    private String tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login_tt);
        MyApplication.getInstance().getDataManager().setItemUserTemp(null);
        initUI();
        initData();
        initControl();
//        showFormDialog("ThanhNguyen872", "abecd3", null, null, null);
//        showMoreInfoUserDialog();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editPhone = findViewById(R.id.editPhone);
        editPassword = findViewById(R.id.editPassword);
        buttonConfirm = findViewById(R.id.buttonConfirm);
        buttonRegisterAccount = findViewById(R.id.buttonRegisterAccount);
        buttonForgotPassword = findViewById(R.id.buttonForgotPassword);
        buttonLoginFacebook = findViewById(R.id.buttonLoginFacebook);
        imageFacebook = findViewById(R.id.imageFacebook);
//        if (!Constant.IS_TEST) {
        findViewById(R.id.layoutFacebook).setVisibility(View.VISIBLE);
        findViewById(R.id.layoutLoginNomal).setVisibility(View.GONE);
//        } else {
//            findViewById(R.id.layoutFacebook).setVisibility(View.GONE);
//            findViewById(R.id.layoutLoginNomal).setVisibility(View.VISIBLE);
//        }
        TextUtil.setHtmlTextView(getString(R.string.user_form_register), buttonRegisterAccount);
//        setBackgroudActionbarFragment(rootView);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            return;
        }
        boolean isRegisterScreen = bundle.getBoolean(Constant.IS_REGISTER, false);
        if (isRegisterScreen) {
            goRegisterScreen();
        }
    }

    private void validForm() {
        String username = editPhone.getText().toString().trim();
        if (username.isEmpty()) {
            editPhone.requestFocus();
            showToast(R.string.msg_username_empty);
            return;
        }
        String password = editPassword.getText().toString().trim();
        if (password.isEmpty()) {
            editPassword.requestFocus();
            showToast(R.string.msg_password_empty);
            return;
        }
        loginType = Constant.TYPE_REGISTER_FORM;
        login(username, password);
    }

    private void login(String username, String password) {
        login(username, password, null, null, null, null, null);
    }

    String id;

    private void login(String username, String password, String gender, String job, String address, String presenterType, String presentervalue) {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        String api = Constant.API_USER_LOGIN;
        boolean isPostMethod = true;
        if (loginType.equals(Constant.TYPE_REGISTER_FORM)) {
            api = Constant.API_USER_LOGIN;
            requestParams.put("phone", username);
            requestParams.put("password", password);
        } else if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK)) {
           /* if (getApplication().getPackageName().equals("vn.mediatech.istudiocafe")) {
                api = Constant.API_USER_LOGIN_SOCIAL;
            } else {
                api = Constant.API_USER_LOGIN_SOCIAL_NONE_PHONE;
            }*/
            api = Constant.API_USER_LOGIN_SOCIAL_NONE_PHONE;
            requestParams.put("social_id", userIdSocial);
            requestParams.put("email", emailSocial);
            requestParams.put("access_token", accessTokenSocial);
            if (itemUserTemp != null) {
                requestParams.put("fullname", itemUserTemp.getFullname());
            }
        } else if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK_2)) {
            api = Constant.API_USER_LOGIN_FACEBOOK_FORUM;
            requestParams.put("access_token", accessTokenSocial);
            requestParams.put("userID", userIdSocial);
            requestParams.put("email", emailSocial);
            requestParams.put("type", "mb");
            requestParams.put("username", username);
            requestParams.put("password", password);
            requestParams.put("gender", gender);
            requestParams.put("address", address);
            requestParams.put("job", job);
            requestParams.put("id", id);
            requestParams.put("channel", InteractiveConstant.CHANEL_ID);
            requestParams.put("presenter_type", presenterType);
            requestParams.put("presenter_value", presentervalue);
            if (itemUserTemp != null) {
                requestParams.put("first_name", itemUserTemp.getFullname());
                requestParams.put("last_name", itemUserTemp.getFullname());
            }
        } else if (loginType.equals(Constant.TYPE_REGISTER_GOOGLE)) {
            api = Constant.API_USER_LOGIN_SOCIAL;
            requestParams.put("social_id", userIdSocial);
            requestParams.put("email", emailSocial);
            requestParams.put("access_token", accessTokenSocial);
        }
        requestParams.put("type", loginType);
        myHttpRequest.request(isPostMethod, ServiceUtilTT.validAPIDGTH(this, api), requestParams,
                new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                showDialogUser(getString(R.string.msg_network_error));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                handleDataLogin(responseString, username, password);
                            }
                        });
                    }
                });
    }

    Boolean isTestShowDialog = false;

    private void handleDataLogin(String responseString, String username, String password) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode == Constant.CODE_ENTER_PHONE) {
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
            gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        if (errorCode == Constant.CODE_VERIFY_OTP) {
            JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
            int expire = JsonParser.Companion.getInt(resultObj, "expire");
            String phone = JsonParser.Companion.getString(resultObj, "phone");
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
            bundle.putInt(Constant.DATA, expire);
            bundle.putString(Constant.PHONE, phone);
            gotoActivityForResult(UserOTPConfirmActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        if (errorCode == Constant.CODE_REGISTER_FORM) {
            Bundle bundle = new Bundle();
            bundle.putString(Constant.TYPE, loginType);
            gotoActivityForResult(UserRegisterActivity.class, bundle, Constant.CODE_LOGIN);
            return;
        }
        String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
        if (errorCode != Constant.SUCCESS) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
        if (itemUser == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        if (Constant.IS_SHOW_FORM_LOGIN) {
            Constant.IS_SHOW_FORM_LOGIN = false;
            itemUser.setLoginStatus("1");
        }
        if (loginType.equals(Constant.TYPE_REGISTER_FACEBOOK_2) && "1".equals(itemUser.getLoginStatus())) {
//            if (itemUser.getUserNameForum() == null || itemUser.getPasswordForum() == null) {
//                return;
//            }
            id = itemUser.getId().toString();
            MyApplication.getInstance().getDataManager().setItemUser(null);
            showFormDialog(itemUser.getUserNameForum(), itemUser.getPasswordForum(), itemUser.getGender(), itemUser.getAddress(), itemUser.getJob());
            return;
        }
        SharedPreferencesManager.saveUser(this, itemUser.getPhone(), itemUser.getAccessToken());
        resultOk = true;
        runOnUiThread(() -> {
            if (isShowDialogForm) {
                isShowDialogForm = false;
                if (isShowMessenger) {
                    isShowMessenger = false;
                    String str =
                            "Tài khoản của tôi là " + itemUser.getFullname() + ", mã số " + itemUser.getId() + ". Chương trình vui lòng xác thực tài khoản giúp tôi!";
                    GeneralUtils.Companion.coppyToClipboard(UserLoginActivity.this, str);
                    //                    progressBar.setVisibility(View.VISIBLE);
                    GeneralUtils.Companion.openMessenger(UserLoginActivity.this);
                    GeneralUtils.Companion.openMessenger(this);
                    GeneralUtils.Companion.setShowLogin(true);
                } else {
                    GeneralUtils.Companion.setShowLogin(true);
                }
            }
            finish();
        });
    }

    boolean isShowMessenger = false;
    boolean isShowDialogForm = false;

    private void showFormDialog(String mUserName, String mPassword, String mGender, String mAddress, String mJob) {
        UserRegisterFormFragment userRegisterFormFragment = new UserRegisterFormFragment();
        isShowDialogForm = true;
        Bundle bundle = new Bundle();
        bundle.putString(Constant.USERNAME, mUserName);
        bundle.putString(Constant.PASSWORD, mPassword);
//        bundle.putString(Constant.GENDER, mGender);
//        bundle.putString(Constant.ADDRESS, mAddress);
//        bundle.putString(Constant.JOB, mJob);
        userRegisterFormFragment.setArguments(bundle);
//        GeneralUtils.Companion.showFragment(getSupportFragmentManager(),userRegisterFormFragment,R.id.frameLayoutDialog,false,false);
        userRegisterFormFragment.show(getSupportFragmentManager(), "UserRegisterFormFragment");
        userRegisterFormFragment.setOnShowDismissListener(new UserRegisterFormFragment.OnShowDismissListener() {

            @Override
            public void onDismiss(boolean isSuccess, boolean isShowMessengerM, @org.jetbrains.annotations.Nullable String userName, @org.jetbrains.annotations.Nullable String password, @org.jetbrains.annotations.Nullable String gender, @org.jetbrains.annotations.Nullable String job, @org.jetbrains.annotations.Nullable String address, @org.jetbrains.annotations.Nullable String presenterType, @org.jetbrains.annotations.Nullable String presenterValue) {
                if (isSuccess) {
                    isShowMessenger = isShowMessengerM;
                    login(userName, password, gender, job, address, presenterType, presenterValue);
//                    Dialog dialogMoreInfo = showMoreInfoUserDialog();
//                    dialogMoreInfo.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                        @Override
//                        public void onDismiss(DialogInterface dialog) {
//                            Loggers.e("INFOLOGIN", userName + "/" + password + "/" + gender + "/" + job + "/" + address + "/" + mPresenterType + "/" + mPresenterValue);
//                            login(userName, password, gender, job, address, mPresenterType, mPresenterValue);
//
//                        }
//                    });
                } else {
                    isShowDialogForm = false;
                }
//                else {
//                    login(mUserName, mPassword, mGender, mJob, mAddress);
//                }
            }

            @Override
            public void onShow() {

            }
        });
    }

    private void validButtonConfirm() {
        String phone = editPhone.getText().toString().trim();
        if (phone.length() < 10) {
            buttonConfirm.setEnabled(false);
            return;
        }
        String password = editPassword.getText().toString().trim();
        if (password.length() < 6) {
            buttonConfirm.setEnabled(false);
            return;
        }
        buttonConfirm.setEnabled(true);
    }

    private void initControl() {
        editPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        editPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validButtonConfirm();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                finish();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                validForm();
            }
        });
        buttonRegisterAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goRegisterScreen();
            }
        });
        buttonLoginFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                loginFacebook();
            }
        });
        findViewById(R.id.layoutLoginFacebook).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                loginFacebook();
            }
        });
        imageFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                loginFacebook();
            }
        });
        buttonForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                Bundle bundle = new Bundle();
                bundle.putString(Constant.TYPE, Constant.TYPE_FORGOT_PASSWORD);
                bundle.putString(Constant.PHONE, editPhone.getText().toString().trim());
                gotoActivityForResult(UserEnterPhoneActivity.class, bundle, Constant.CODE_LOGIN);
            }
        });
        findViewById(R.id.layoutLoginDemo).setVisibility(Constant.IS_DEMO ? View.VISIBLE : View.GONE);
        findViewById(R.id.textDemo1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                //Tiến Minh
                String act = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE2MTQiLCJwaG9uZSI6IiIsImZ1bGxuYW1lIjoiVGlcdTFlYmZuIE1pbmgiLCJhdmF0YXIiOiJodHRwczpcL1wvZGF1Z2lhdHJ1eWVuaGluaC5jb21cL3VwbG9hZFwvaXN0dWRpb3NcL2F2YXRhclwvMTYxNF9UaWVuX01pbmguanBnIiwidXNlcnNfc3RhdHVzIjoiMiIsImVtYWlsIjoidmJ2amdybmNkel8xNjI3NTQ2MzI0QHRmYm53Lm5ldCIsImFkZHJlc3MiOiJIXHUwMGUwIE5cdTFlZDlpIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiJOYW0iLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJjaGVja19wYXNzd29yZCI6IjEiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5SXlZemRsTTJVaVhRLjlsN1Jvd093RWgxeUtzQkpnS21oYm9zX1NEdzU5S2xQRTJ3UjZwV2x3RnciLCJpc191c2VyIjoiMCIsInVzZXJGb3J1bSI6IlRpZW5NaW5oMTYxNCIsImpvYiI6Ilx1MDExMGkgbFx1MDBlMG0iLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJtZW1iZXJfY2xhc3NpZmljYXRpb24iOm51bGwsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj1JY2NTQVZiOEZ3TEtrdHBXTEhpS0pwV0thMDl4LW5nMCZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiIyYzdlM2UiLCJhY2Nlc3NfdG9rZW4iOnt9fQ.bElFa65awoTOQ9MmB_Z3hSq4AArytxL8r0uYRYO1Ycs";
                SharedPreferencesManager.saveAccessToken(UserLoginActivity.this, act);
                autoLogin();
            }
        });
        findViewById(R.id.textDemo2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                //Hùng Dũng
                String act = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxNSwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IkhcdTAwZjluZyBEXHUwMTY5bmciLCJhdmF0YXIiOiJodHRwczpcL1wvZGF1Z2lhdHJ1eWVuaGluaC5jb21cL3VwbG9hZFwvaXN0dWRpb3NcL2F2YXRhclwvMTYxNV9IdW5nX0R1bmcuanBnIiwidXNlcnNfc3RhdHVzIjoiMiIsImVtYWlsIjoieWFoZWRkb21lY18xNjI3NTQ2NzA3QHRmYm53Lm5ldCIsImFkZHJlc3MiOiJCXHUxZWFmYyBHaWFuZyIsImJpcnRoZGF5IjoiIiwiZ2VuZGVyIjoiTmFtIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUl3TURCbFpXSWlYUS5LNXk3NHdZU0x3SXZUR3BhUkhxNWJzQWdsY3VMa0VGNXl2Tlo3WnUzOTdjIiwidXNlckZvcnVtIjoiSHVuZ0R1bmcxNjE1Iiwiam9iIjoiU2luaCBWaVx1MDBlYW4iLCJjaGVja19wYXNzd29yZCI6IjEiLCJjaGFubmVsIjoidHVvbmd0YWMudHYiLCJtZW1iZXJfY2xhc3NpZmljYXRpb24iOm51bGwsInVybF9mb3J1bSI6Imh0dHA6XC9cL3R1b25ndGFjLnR2XC9pbmRleC5waHA_bG9naW5cL2FwaS10b2tlbiZ0b2tlbj1PZEFQNXlJcThaeVdwbnJlckRLcjIzbUZyc21SNGhVZCZmb3JjZT0wJnJlbWVtYmVyPTEiLCJwYXNzRm9ydW0iOiIwMDBlZWIifQ.gtNaEVVtC1xWbf842rbB6GS85v9EanSFH719OHEY_0Q";
                SharedPreferencesManager.saveAccessToken(UserLoginActivity.this, act);
                autoLogin();
            }
        });
        findViewById(R.id.textDemo3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                //Huy Hoàng
                String act = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxNiwicGhvbmUiOiIiLCJmdWxsbmFtZSI6Ikh1eSBIb1x1MDBlMG5nIiwiYXZhdGFyIjoiaHR0cHM6XC9cL2RhdWdpYXRydXllbmhpbmguY29tXC91cGxvYWRcL2lzdHVkaW9zXC9hdmF0YXJcLzE2MTZfSHV5X0hvYW5nLmpwZyIsInVzZXJzX3N0YXR1cyI6IjIiLCJlbWFpbCI6ImV1d3V1aGRiYmZfMTYyNzU0NjQzMUB0ZmJudy5uZXQiLCJhZGRyZXNzIjoiVHAgSFx1MWVkMyBDaFx1MDBlZCBNaW5oIiwiYmlydGhkYXkiOiIiLCJnZW5kZXIiOiJOYW0iLCJyb2xlIjoidXNlciIsImNoaXAiOiIiLCJwYXNzX2ZvcnVtIjoiZXlKMGVYQWlPaUpLVjFRaUxDSmhiR2NpT2lKSVV6STFOaUo5Lld5STFZV05pTlRNaVhRLmJwYzlPNkJ1VkVMOWhWSEUzWEQ1RzI5YkNiUUdKVlVDalhjdl9WdEduNDQiLCJ1c2VyRm9ydW0iOiJIdXlIb2FuZzE2MTYiLCJqb2IiOiJcdTAxMTBpIGxcdTAwZTBtIiwiY2hlY2tfcGFzc3dvcmQiOiIxIiwiY2hhbm5lbCI6InR1b25ndGFjLnR2IiwibWVtYmVyX2NsYXNzaWZpY2F0aW9uIjpudWxsLCJ1cmxfZm9ydW0iOiJodHRwOlwvXC90dW9uZ3RhYy50dlwvaW5kZXgucGhwP2xvZ2luXC9hcGktdG9rZW4mdG9rZW49YnoxSENCQ3AzbVRacXlKUnRpV0MwSDdsbmpvZ28wekwmZm9yY2U9MCZyZW1lbWJlcj0xIiwicGFzc0ZvcnVtIjoiNWFjYjUzIn0.-SrZdQmDWLmiUZM2lebhp1JfT_btX4Ao5uUzSBmf4-M";
                SharedPreferencesManager.saveAccessToken(UserLoginActivity.this, act);
                autoLogin();
            }
        });
        findViewById(R.id.textDemo4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
//                "Thành Công"
                String act = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxNywicGhvbmUiOiIiLCJmdWxsbmFtZSI6IlRoXHUwMGUwbmggQ1x1MDBmNG5nIiwiYXZhdGFyIjoiaHR0cHM6XC9cL2RhdWdpYXRydXllbmhpbmguY29tXC91cGxvYWRcL2lzdHVkaW9zXC9hdmF0YXJcLzE2MTdfVGhhbmhfQ29uZy5qcGciLCJ1c2Vyc19zdGF0dXMiOiIyIiwiZW1haWwiOiJmZ2Zha2VmZXh4XzE2Mjc1NDY0NDdAdGZibncubmV0IiwiYWRkcmVzcyI6IkhcdTAwZTAgTlx1MWVkOWkiLCJiaXJ0aGRheSI6IiIsImdlbmRlciI6Ik5hbSIsInJvbGUiOiJ1c2VyIiwiY2hpcCI6IiIsInBhc3NfZm9ydW0iOiJleUowZVhBaU9pSktWMVFpTENKaGJHY2lPaUpJVXpJMU5pSjkuV3lJd1pEUXlObUlpWFEuZWZVTmRjVjhjUjJ5MUNkQ2F1LXJjcmpKUm1wNzdYcWh3SHZyYUlaZ2luVSIsInVzZXJGb3J1bSI6IlRoYW5oQ29uZzE2MTciLCJqb2IiOiJTaW5oIFZpXHUwMGVhbiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsImNoYW5uZWwiOiJ0dW9uZ3RhYy50diIsIm1lbWJlcl9jbGFzc2lmaWNhdGlvbiI6bnVsbCwidXJsX2ZvcnVtIjoiaHR0cDpcL1wvdHVvbmd0YWMudHZcL2luZGV4LnBocD9sb2dpblwvYXBpLXRva2VuJnRva2VuPWdrVDVSTXFtVGFjYXlsSEpXcXVOZHFKVkVNTEV2LTVnJmZvcmNlPTAmcmVtZW1iZXI9MSIsInBhc3NGb3J1bSI6IjBkNDI2YiJ9.SUqR7b10JaAqUi3LhUiz299O6zQfmgzV_NWT4wsWex4";
                SharedPreferencesManager.saveAccessToken(UserLoginActivity.this, act);
                autoLogin();
            }
        });
        findViewById(R.id.textDemo5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editPhone);
                //Tấn Phát
                String act = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6MTYxOCwicGhvbmUiOiIiLCJmdWxsbmFtZSI6IlRcdTFlYTVuIFBoXHUwMGUxdCIsImF2YXRhciI6Imh0dHBzOlwvXC9kYXVnaWF0cnV5ZW5oaW5oLmNvbVwvdXBsb2FkXC9pc3R1ZGlvc1wvYXZhdGFyXC8xNjE4X1Rhbl9QaGF0LmpwZyIsInVzZXJzX3N0YXR1cyI6IjIiLCJlbWFpbCI6InV2anRyZWN5emlfMTYyNzU0NjY5M0B0ZmJudy5uZXQiLCJhZGRyZXNzIjoiSFx1MDBlMCBOXHUxZWQ5aSIsImJpcnRoZGF5IjoiIiwiZ2VuZGVyIjoiTmFtIiwicm9sZSI6InVzZXIiLCJjaGlwIjoiIiwicGFzc19mb3J1bSI6ImV5SjBlWEFpT2lKS1YxUWlMQ0poYkdjaU9pSklVekkxTmlKOS5XeUkxWmpjeE9XRWlYUS5wWmJCVnJoSjJxOVBVNFpWYXotYlV5azE4Vl9HU05hNkdfRnJSc085aVBrIiwidXNlckZvcnVtIjoiVGFuUGhhdDE2MTgiLCJqb2IiOiJTaW5oIFZpXHUwMGVhbiIsImNoZWNrX3Bhc3N3b3JkIjoiMSIsImNoYW5uZWwiOiJ0dW9uZ3RhYy50diIsIm1lbWJlcl9jbGFzc2lmaWNhdGlvbiI6bnVsbCwidXJsX2ZvcnVtIjoiaHR0cDpcL1wvdHVvbmd0YWMudHZcL2luZGV4LnBocD9sb2dpblwvYXBpLXRva2VuJnRva2VuPWQ5RExyR0R1QlRIbUV2aUI2eWhTRHdzdlhGVktNNmROJmZvcmNlPTAmcmVtZW1iZXI9MSIsInBhc3NGb3J1bSI6IjVmNzE5YSJ9.IKQfleBakh5EIWt04yT2IfI4YUXyGx0vroHpDPp3xWs";
                SharedPreferencesManager.saveAccessToken(UserLoginActivity.this, act);
                autoLogin();
            }
        });
    }

    public void autoLogin() {
        showLoadingDialog(false, null);
        ServiceUtilTT.autoLogin(UserLoginActivity.this, new OnLoginFormListener() {
            @Override
            public void onLogin(boolean hasLogin) {
                hideLoadingDialog();
                if (hasLogin) {
                    resultOk = true;
                    finish();
                }
            }
        });
    }

    private void goRegisterScreen() {
        hideKeyboard(editPhone);
        Bundle bundle = new Bundle();
        bundle.putString(Constant.PHONE, editPhone.getText().toString().trim());
        gotoActivityForResult(UserRegisterActivity.class, bundle, Constant.CODE_LOGIN);
    }

    private void loginFacebook() {
        if (1 == 1) {
            loginFacebook2();
            return;
        }
        showLoadingDialog(false, null);
        if (mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
        }
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
        loginType = Constant.TYPE_REGISTER_FACEBOOK;
        userIdSocial = "";
        emailSocial = "";
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                        showToast(getString(R.string.msg_login_error) + " (100): " + exception.getMessage());
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",
                "email"));

    }

    private void loginFacebook2() {
        showLoadingDialog(false, null);
        /*if (mAuth == null) {
            mAuth = FirebaseAuth.getInstance();
        }*/
        if (callbackManager == null) {
            callbackManager = CallbackManager.Factory.create();
        }
        loginType = Constant.TYPE_REGISTER_FACEBOOK_2;
        userIdSocial = "";
        emailSocial = "";
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        handleFacebookAccessToken(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                        showToast(getString(R.string.msg_login_error) + " (100): " + exception.getMessage());
                    }
                });
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile",
                "email"));
    }

    private void handleFacebookAccessToken(AccessToken accessToken) {
        if (accessToken == null) {
            showToast(getString(R.string.msg_login_error) + " (101)");
            return;
        }
        accessTokenSocial = accessToken.getToken();
        userIdSocial = accessToken.getUserId();
        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
            showToast(getString(R.string.msg_login_error) + " (102)");
            return;
        }
        Loggers.e("MY_CHECK_LOGIN Facebook", accessTokenSocial);
//        START: GET USER EMAIL
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject object,
                            GraphResponse response) {
                        Loggers.e("MY_CHECK_LOGIN Facebook_object", object.toString());
                        userIdSocial = JsonParser.Companion.getString(object, "id");
                        emailSocial = JsonParser.Companion.getString(object, "email");
                        String fullNameSocial = JsonParser.Companion.getString(object, "name");
                        String avatar = "https://graph.facebook.com/" + userIdSocial + "/picture" +
                                "?type=large";
                        if (MyApplication.getInstance().isEmpty(emailSocial)) {
                            emailSocial = userIdSocial + "@mediatech.vn";
                        }
                        itemUserTemp.setSocialId(userIdSocial);
                        itemUserTemp.setAvatar(avatar);
                        itemUserTemp.setFullname(fullNameSocial);
                        itemUserTemp.setEmail(emailSocial);
                        MyApplication.getInstance().getDataManager().setItemUserTemp(itemUserTemp);
                        login(null, null);
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,name,gender,birthday");
        request.setParameters(parameters);
        request.executeAsync();
//        END: GET USER EMAIL
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle id:" + acct.getId());
        Loggers.e("MY_CHECK_LOGIN", "firebaseAuthWithGoogle token:" + acct.getIdToken());
        accessTokenSocial = acct.getIdToken();
        if (MyApplication.getInstance().isEmpty(accessTokenSocial)) {
            showToast(getString(R.string.msg_login_error) + " (102)");
            return;
        }
        Loggers.e("MY_CHECK_LOGIN accessTokenGoogle", accessTokenSocial);
        login(null, null);
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE_LOGIN_GOOGLE) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
            return;
        }
        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            MyApplication.getInstance().getDataManager().setItemUserTemp(null);
            if (data != null) {
                Bundle bundle = data.getExtras();
                if (bundle != null) {
                    int type = bundle.getInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SCREEN);
                    if (type == Constant.RESULT_CODE_LOGIN_SUCCESS) {
                        resultOk = true;
                        finish();
                    }
                }
            }
//            finish();
        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
//        Bundle bundle = new Bundle();
//        intent.putExtras(bundle);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideLoadingDialog();
    }

    private String mPresenterType, mPresenterValue;

    private MyDialog showMoreInfoUserDialog() {
        if (isDestroyed() || isFinishing()) {
            return null;
        }
        MyDialog dialog = new MyDialog(this);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER;
        lp.windowAnimations = R.style.DialogAnimationLeftRight;
        dialog.getWindow().setAttributes(lp);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.layout_dialog_user_know_app);
        ScreenSize screenSize = new ScreenSize(this);
        dialog.getWindow().setLayout(screenSize.getWidth() * 10 / 10, WindowManager.LayoutParams.MATCH_PARENT);
        View layoutRoot = dialog.findViewById(R.id.layoutRoot);
        EditText editPresenterID = dialog.findViewById(R.id.editPresenterID);
        EditText editOther = dialog.findViewById(R.id.editOther);
        ImageView buttonCheck = dialog.findViewById(R.id.buttonCheck);
        TextView textName = dialog.findViewById(R.id.textName);
        ImageView imageAvatar = dialog.findViewById(R.id.imageAvatar);
        TextView buttonSkip = dialog.findViewById(R.id.buttonCancel);
        TextView buttonNext = dialog.findViewById(R.id.buttonConfirm);
        Spinner spinnerTv = dialog.findViewById(R.id.spinnerTV);
        Spinner spinnerSocial = dialog.findViewById(R.id.spinnerSocial);
        Spinner spinnerType = dialog.findViewById(R.id.spinnerType);
        View layoutPresenter = dialog.findViewById(R.id.layoutPresenter);
        View layoutTV = dialog.findViewById(R.id.layoutTV);
        View layoutSocial = dialog.findViewById(R.id.layoutSocial);
        View layoutOther = dialog.findViewById(R.id.layoutOther);
        View progressBar = dialog.findViewById(R.id.progressBar);
        layoutRoot.setClipToOutline(true);
        layoutRoot.setOutlineProvider(new ViewOutlineProvider() {
            @Override
            public void getOutline(View view, Outline outline) {
                float cornerRadiusDP = 15f;
                outline.setRoundRect(0, 0, view.getWidth(), view.getHeight(), cornerRadiusDP);
            }
        });
        initSpinerTV(spinnerTv, buttonNext);
        initSpinerSocial(spinnerSocial, buttonNext);
        buttonCheck.setOnClickListener(v -> {
            String userId = editPresenterID.getText().toString().trim();
            if (userId.equals("")) {
                Toast.makeText(this, "Mã giới thiệu không đúng", Toast.LENGTH_SHORT).show();
                editPresenterID.requestFocus();
                return;
            }
            getName(textName, imageAvatar, userId, progressBar);
        });
        editPresenterID.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                getName(textName, imageAvatar, editPresenterID.getText().toString().trim(), progressBar);
            }
            GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
            return true;
        });

        editPresenterID.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textName.setVisibility(View.GONE);
                imageAvatar.setVisibility(View.GONE);
                if (editPresenterID.getText().toString().trim().length() >= 1) {
//                    getName(textName, imageAvatar, editPresenterID.getText().toString().trim(), progressBar);
//                    buttonNext.setEnabled(true);
                } else {
                    buttonNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editOther.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editOther.getText().toString().trim().length() >= 1) {
                    buttonNext.setEnabled(true);
                } else {
                    buttonNext.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        ArrayList<View> listLayout = new ArrayList(Arrays.asList(layoutSocial, layoutTV, layoutOther, layoutPresenter));
        initSpinerType(spinnerType);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent != null) {
                    GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
                    type = parent.getItemAtPosition(position).toString();
                    if (type.equals(DEFAULT_STRING)) {
                        type = null;
                        showLayout(layoutRoot, listLayout);
                    } else if (type.equals(TYPE_PRESENTER)) {
                        showLayout(layoutPresenter, listLayout);
                    } else if (type.equals(TYPE_SOCIAL)) {
                        showLayout(layoutSocial, listLayout);
                        if (social != null) {
                            buttonNext.setEnabled(true);
                        } else {
                            spinnerSocial.performClick();
                            buttonNext.setEnabled(false);
                        }
                    } else if (type.equals(TYPE_TV)) {
                        showLayout(layoutTV, listLayout);
                        if (social != null) {
                            buttonNext.setEnabled(true);
                        } else {
                            spinnerTv.performClick();
                            buttonNext.setEnabled(false);
                        }
                    } else if (type.equals(TYPE_OTHER)) {
                        showLayout(layoutOther, listLayout);
                        if (other != null) {
                            buttonNext.setEnabled(true);
                        } else {
                            editOther.requestFocus();
                            buttonNext.setEnabled(false);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        buttonSkip.setOnClickListener(v ->
                dialog.dismiss());
        buttonNext.setOnClickListener(v -> {
            if (type.equals(TYPE_PRESENTER)) {
                mPresenterType = TYPE_PRESENTER_VALUE;
                String id = editPresenterID.getText().toString().trim();
                if (GeneralUtils.Companion.isEmpty(id)) {
                    showKeyboardFocus(editPresenterID);
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Mã giới thiệu không đúng. Vui lòng nhập và kiểm tra lại.");
                    return;
                } else {
                    ServiceUtilTT.getNameUser(UserLoginActivity.this, id, new OnResultRequestListener() {
                        @Override
                        public void onResult(boolean isSuccess, @org.jetbrains.annotations.Nullable String msg, @org.jetbrains.annotations.Nullable String avatar) {
                            runOnUiThread(() -> {
                                if (isSuccess) {
                                    dialog.dismiss();
                                    mPresenterValue = id;
                                } else {
                                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Mã giới thiệu không đúng. Vui lòng nhập lại!");
                                    editPresenterID.requestFocus();
                                }
                            });
                        }
                    });
                    return;
                }
            } else if (type.equals(TYPE_SOCIAL)) {
                mPresenterType = TYPE_SOCIAL_VALUE;
                if (GeneralUtils.Companion.isEmpty(social)) {
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Vui lòng chọn mạng xã hội.");
                    spinnerSocial.requestFocus();
                    return;
                }
                mPresenterValue = social;
            } else if (type.equals(TYPE_TV)) {
                mPresenterType = TYPE_TV_VALUE;
                if (GeneralUtils.Companion.isEmpty(tv)) {
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Vui lòng chọn đài truyền hình.");
                    spinnerTv.requestFocus();
                    return;
                }
                mPresenterValue = tv;
            } else if (type.equals(TYPE_OTHER)) {
                mPresenterType = TYPE_OTHER_VALUE;
                if (GeneralUtils.Companion.isEmpty(editOther.getText().toString().trim())) {
                    GeneralUtils.Companion.showToast(UserLoginActivity.this, "Bạn vui lòng nhập nội dung khác.");
                    return;
                }
                mPresenterValue = editOther.getText().toString().trim();
            }

            dialog.dismiss();
        });
        try {
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dialog;
    }

    MyHttpRequest myHttpRequestGetName;

    private void getName(TextView textName, ImageView imageAvatar, String userId, View progress) {
        if (myHttpRequestGetName != null) {
            myHttpRequestGetName.cancel();
        }
        progress.setVisibility(View.VISIBLE);
        myHttpRequestGetName = ServiceUtilTT.getNameUser(this, userId, (isSuccess, msg, avatar) -> {
            runOnUiThread(() -> {
                progress.setVisibility(View.GONE);
                if (!isSuccess) {
//                    GeneralUtils.Companion.showToast(this, msg);
                    textName.setText(msg);
                    textName.setTextColor(Color.RED);
                    imageAvatar.setVisibility(View.GONE);
                    textName.setVisibility(View.VISIBLE);
                } else {
                    textName.setTextColor(Color.parseColor("#4f8cff"));
                    textName.setText(msg);
                    textName.setVisibility(View.VISIBLE);
                    if (!GeneralUtils.Companion.isEmpty(avatar)) {
                        imageAvatar.setVisibility(View.VISIBLE);
                        GeneralUtils.Companion.loadImageCircle(this, imageAvatar, avatar, R.drawable.avatar_placeholder);
                    } else {
                        imageAvatar.setVisibility(View.GONE);
                    }

                }
            });
        });
    }

    private String DEFAULT_STRING = "Chọn";
    private String other, social, type;

    private void showLayout(View layout, ArrayList<View> layoutArrayList) {
        if (layoutArrayList == null || layoutArrayList.size() == 0 || layout == null) {
            return;
        }
        for (View item : layoutArrayList) {
            if (!item.equals(layout)) {
                item.setVisibility(View.INVISIBLE);
            } else {
                item.setVisibility(View.VISIBLE);
            }
        }
    }

    private void initSpinerTV(Spinner spinner, View buttonNext) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
                if (parent != null) {
                    tv = parent.getItemAtPosition(position).toString().trim();
                    if (tv.equals(DEFAULT_STRING.trim())) {
                        tv = null;
                        buttonNext.setEnabled(false);
                    } else {
                        buttonNext.setEnabled(true);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayList<String> places = new ArrayList(
                Arrays.asList(
                        DEFAULT_STRING,
                        "Hà Nội",
                        "Bắc Giang",
                        "Bắc Kạn",
                        " Bắc Ninh",
                        " Bình Phước",
                        "Hà Nam",
                        "Hưng Yên",
                        "Khánh Hòa",
                        "Lạng Sơn",
                        "Lào Cai",
                        "Nghệ An",
                        "Ninh Bình",
                        "Quảng Ninh",
                        "Quảng Trị",
                        "Thái Bình",
                        "Thái Nguyên",
                        "Thanh Hóa",
                        "Thừa Thiên Huế",
                        "Tiền Giang",
                        "Trà Vinh",
                        "Tuyên Quang",
                        "Vĩnh Long",
                        "Vĩnh Phúc",
                        "Khác"
                )
        );
        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, R.layout.item_spinner_user, places);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop);
        spinner.setAdapter(dataAdapter);
    }

    private final String TYPE_PRESENTER = "Người giới thiệu";
    private final String TYPE_TV = "Đài truyền hình";
    private final String TYPE_SOCIAL = "Mạng xã hội";
    private final String TYPE_OTHER = "Khác";
    private final String TYPE_PRESENTER_VALUE = "user";
    private final String TYPE_TV_VALUE = "tv_station";
    private final String TYPE_SOCIAL_VALUE = "social";
    private final String TYPE_OTHER_VALUE = "other";

    private void initSpinerSocial(Spinner spinner, View buttonNext) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent != null) {
                    GeneralUtils.Companion.hideKeyboard(UserLoginActivity.this);
                    social = parent.getItemAtPosition(position).toString().trim();
                    if (social.equals(DEFAULT_STRING.trim())) {
                        social = null;
                        buttonNext.setEnabled(false);
                    } else {
                        buttonNext.setEnabled(true);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayList<String> categories = new ArrayList();
        categories.add(DEFAULT_STRING);
        categories.add("Facebook");
        categories.add("Instagram");
        categories.add("YouTube");
        categories.add("TikTok");
        categories.add("Twitter");
        categories.add("Khác");
        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, R.layout.item_spinner_user, categories);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop);
        spinner.setAdapter(dataAdapter);
    }

    private void initSpinerType(Spinner spinner) {
        ArrayList<String> categories = new ArrayList();
        categories.add(DEFAULT_STRING);
        categories.add(TYPE_TV);
        categories.add(TYPE_SOCIAL);
        categories.add(TYPE_PRESENTER);
        categories.add(TYPE_OTHER);
//        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, android.R.layout.simple_spinner_item, categories);
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter dataAdapter = new ArrayAdapter(UserLoginActivity.this, R.layout.item_spinner_user, categories);
        dataAdapter.setDropDownViewResource(R.layout.item_spinner_user_drop);
        spinner.setAdapter(dataAdapter);
    }
}