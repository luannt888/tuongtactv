package vn.mediatech.istudiocafe.activity

import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_handle_notification_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemChat

class HandleNotificationActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_handle_notification_tt)
        initUI()
        initData()
        finish()
    }

    fun initUI() {
        MyApplication.getInstance().loadDrawableImage(this, R.drawable.bg_splash, imageBackground)
        val sharedPreferences = getSharedPreferences(Constant.CONFIG, MODE_PRIVATE)
        val currentBgLaunch = sharedPreferences.getString(Constant.IMG_LAUNCH, null)
        val currentLogo = sharedPreferences.getString(Constant.LOGO, null)
        if (!currentBgLaunch.isNullOrEmpty()) {
            MyApplication.getInstance()
                .loadImageNoPlaceHolder(this, imageBackgroundServer, currentBgLaunch)
            imageBackgroundServer.visibility = View.VISIBLE
        }
        val screenSize = ScreenSize(this)
        val imageCenterWidth = screenSize.width / 2
        val lpLogoActionbarCenter: LinearLayout.LayoutParams =
            imageLogoActionbarCenter.layoutParams as LinearLayout.LayoutParams
        lpLogoActionbarCenter.width = imageCenterWidth
        lpLogoActionbarCenter.height = imageCenterWidth
        if (!currentLogo.isNullOrEmpty()) {
            MyApplication.getInstance().loadImage(this, imageLogoActionbarCenter, currentLogo)
        } else {
            MyApplication.getInstance().loadImageNoPlaceHolder(
                this,
                imageLogoActionbarCenter,
                R.drawable.ic_logo_actionbar
            )
        }
    }

    fun initData() {
        val bundle: Bundle = intent.extras ?: return
//        bundle.putBoolean(Constant.NOTIFY, true)
//        bundle.putBoolean(Constant.IS_VOUCHER, true)
        if (!MainActivity.instance) {
            val intent = Intent(this, SplashActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
            return
        }

        MainActivity.typeShowNotify = bundle.getString(Constant.TYPE_SHOW_NOTIFY)
//        Loggers.e("TYPE_SHOW_NOTIFY",bundle.getString(Constant.TYPE_SHOW_NOTIFY))
        val itemObj: Parcelable? = bundle.getParcelable(Constant.DATA)
        if (MyApplication.getInstance().dataManager.isRunBackground) {
            val typeNotifyVoucherKey: Int =
                bundle.getInt(Constant.TYPE_NOTIFY_VOUCHER_KEY, Constant.TYPE_NOTIFY_VOUCHER_DONATE)
            MyApplication.getInstance().dataManager.itemParcelable = itemObj
            MyApplication.getInstance().dataManager.typeNotifyVoucherKey = typeNotifyVoucherKey
            return
        }
        val action = intent.action
        val intent = Intent(action)
        intent.putExtras(bundle)
        sendBroadcast(intent)
    }
}