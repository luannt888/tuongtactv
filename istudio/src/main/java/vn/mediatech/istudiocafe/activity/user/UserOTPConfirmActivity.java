package vn.mediatech.istudiocafe.activity.user;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;

import org.json.JSONObject;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.app.MyApplication;
import vn.mediatech.istudiocafe.listener.OnCancelListener;
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener;
import vn.mediatech.istudiocafe.model.ItemUser;
import vn.mediatech.istudiocafe.service.MyHttpRequest;
import vn.mediatech.istudiocafe.service.RequestParams;
import vn.mediatech.istudiocafe.util.JsonParser;
import vn.mediatech.istudiocafe.util.ServiceUtilTT;
import vn.mediatech.istudiocafe.util.SharedPreferencesManager;

public class UserOTPConfirmActivity extends BaseActivity {
    private ImageView buttonBack;
    private TextView buttonConfirm, textOTPNote, buttonResend;
    private EditText editText;
    private MyHttpRequest myHttpRequest, myHttpRequestResend;
    private int otpTime;
    private boolean resultOk = false;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_otp_confirm_tt);
        initUI();
        initData();
        initControl();
    }

    private void initUI() {
        setFullStatusTransparent();
        buttonBack = findViewById(R.id.buttonBack);
        editText = findViewById(R.id.editText);
        buttonConfirm = findViewById(R.id.buttonConfirm);
        textOTPNote = findViewById(R.id.textOTPNote);
        buttonResend = findViewById(R.id.buttonResend);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            finish();
            return;
        }
        otpTime = bundle.getInt(Constant.DATA, 0);
        phone = bundle.getString(Constant.PHONE, "");
        initTimerOTP();
    }

    private Timer timerOTP;

    private void initTimerOTP() {
        if (otpTime <= 0) {
            textOTPNote.setVisibility(View.GONE);
//            buttonResend.setVisibility(View.VISIBLE);
            return;
        }
        buttonResend.setVisibility(View.GONE);
        textOTPNote.setVisibility(View.VISIBLE);
        timerOTP = new Timer();
        timerOTP.schedule(new TimerTask() {
            @Override
            public void run() {
                if (isFinishing() || isDestroyed()) {
                    stopTimerOTP();
                    return;
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (otpTime <= 0) {
                            stopTimerOTP();
                            textOTPNote.setVisibility(View.GONE);
                            buttonResend.setVisibility(View.VISIBLE);
                            buttonResend.setEnabled(true);
                            return;
                        }
                        String time = String.format(Locale.ENGLISH,
                                getString(R.string.user_form_note), otpTime);
                        textOTPNote.setText(time);
                        otpTime -= 1;
                    }
                });
            }
        }, 0, 1000);
    }

    private void stopTimerOTP() {
        if (timerOTP != null) {
            try {
                timerOTP.cancel();
                timerOTP.purge();
            } catch (Exception e) {
                e.printStackTrace();
            }
            timerOTP = null;
        }
    }

    private void validForm() {
        String otp = editText.getText().toString().trim();
        if (otp.isEmpty()) {
//            showToast(R.string.msg_phone_empty);
            editText.requestFocus();
            return;
        }
        if (otp.length() < 4) {
//            showToast(R.string.msg_phone_invalid_format);
            editText.requestFocus();
            return;
        }
        verifyOTP(otp);
    }

    private void verifyOTP(String otp) {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, null, new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequest != null) {
                    myHttpRequest.cancel();
                }
            }
        });
        if (myHttpRequest == null) {
            myHttpRequest = new MyHttpRequest(this);
        } else {
            myHttpRequest.cancel();
        }
        RequestParams requestParams = new RequestParams();
        requestParams.put("phone", phone);
        requestParams.put("otp", otp);
        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(this,Constant.API_USER_VERIFY_OTP),
                requestParams, new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                showDialogUser(getString(R.string.msg_network_error));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                handleData(responseString);
                            }
                        });
                    }
                });
    }

    private void handleData(String responseString) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS && errorCode != Constant.CODE_UPDATE_PROFILE) {
            String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        JSONObject resultObj = JsonParser.Companion.getJsonObject(jsonObject, Constant.RESULT);
        ItemUser itemUser = JsonParser.Companion.parseItemUserLogin(resultObj);
        if (itemUser == null) {
            showDialogUser(getString(R.string.msg_empty_data));
            return;
        }
        resultOk = true;
        SharedPreferencesManager.saveUser(this, itemUser.getPhone(), itemUser.getAccessToken());
        stopTimerOTP();
        if (itemUser.isPassword()) {
            gotoActivityForResult(UserEnterNewPasswordActivity.class, Constant.CODE_LOGIN);
            return;
        }
        /*if (errorCode == Constant.CODE_UPDATE_PROFILE) {
            gotoActivityForResult(UserRegisterUpdateInfoActivity.class, Constant.CODE_LOGIN);
            return;
        }*/
        if (bundleResult == null) {
            bundleResult = new Bundle();
        }
        bundleResult.putInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SUCCESS);
        finish();
    }

    private void resendOTP() {
        if (!MyApplication.getInstance().isNetworkConnect()) {
            showDialogUser(getString(R.string.msg_network_error));
            return;
        }
        showLoadingDialog(true, getString(R.string.please_waiting), new OnCancelListener() {
            @Override
            public void onCancel(boolean isCancel) {
                if (myHttpRequestResend != null) {
                    myHttpRequestResend.cancel();
                }
            }
        });
        if (myHttpRequestResend == null) {
            myHttpRequestResend = new MyHttpRequest(this);
        } else {
            myHttpRequestResend.cancel();
        }
        RequestParams requestParams = new RequestParams();
        requestParams.put("phone", phone);
        myHttpRequestResend.request(true, ServiceUtilTT.validAPIDGTH(this,Constant.API_USER_SEND_OTP),
                requestParams, new MyHttpRequest.ResponseListener() {
                    @Override
                    public void onFailure(int statusCode) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                showDialogUser(getString(R.string.msg_network_error));
                            }
                        });
                    }

                    @Override
                    public void onSuccess(int statusCode, String responseString) {
                        if (isFinishing() || isDestroyed()) {
                            return;
                        }
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                hideLoadingDialog();
                                buttonResend.setEnabled(true);
                                handleResendData(responseString);
                            }
                        });
                    }
                });
    }

    private void handleResendData(String responseString) {
        if (MyApplication.getInstance().isEmpty(responseString)) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        responseString = responseString.trim();
        JSONObject jsonObject = JsonParser.Companion.getJsonObject(responseString);
        if (jsonObject == null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(getString(R.string.msg_empty_data));
                }
            });
            return;
        }
        int errorCode = JsonParser.Companion.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            String message = JsonParser.Companion.getString(jsonObject, Constant.MESSAGE);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showDialogUser(message);
                }
            });
            return;
        }
        otpTime = JsonParser.Companion.getInt(jsonObject, Constant.RESULT);
        initTimerOTP();
    }

    private void initControl() {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String otp = editText.getText().toString().trim();
                buttonConfirm.setEnabled(otp.length() >= 4);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editText);
                showCancelDialog();
            }
        });
        buttonResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonResend.setEnabled(false);
                resendOTP();
            }
        });
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(editText);
                validForm();
            }
        });
    }

    private void showCancelDialog() {
        showDialogUser(true, getString(R.string.notification),
                getString(R.string.msg_cancel_register), getString(R.string.cancel),
                getString(R.string.close), new OnDialogButtonListener() {
                    @Override
                    public void onLeftButtonClick() {
                        resultOk = true;
                        if (bundleResult == null) {
                            bundleResult = new Bundle();
                        }
                        bundleResult.putInt(Constant.TYPE, Constant.RESULT_CODE_LOGIN_SCREEN);
                        finish();
                    }

                    @Override
                    public void onRightButtonClick() {

                    }
                });
    }

    @Override
    public void onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest.cancel();
        }
        if (myHttpRequestResend != null) {
            myHttpRequestResend.cancel();
        }
        super.onDestroy();
    }

    private Bundle bundleResult;

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == Constant.CODE_LOGIN) {
            resultOk = true;
            bundleResult = data != null ? data.getExtras() : null;
            finish();
        }
    }

    @Override
    public void finish() {
        if (resultOk) {
            Intent intent = new Intent();
            if (bundleResult != null) {
                intent.putExtras(bundleResult);
            }
            setResult(RESULT_OK, intent);
        }
        stopTimerOTP();
        super.finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showCancelDialog();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }
}