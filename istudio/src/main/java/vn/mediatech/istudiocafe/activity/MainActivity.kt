package vn.mediatech.istudiocafe.activity

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Bitmap
import android.net.Uri
import android.os.*
import android.view.*
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main_tt.*
import kotlinx.android.synthetic.main.layout_home_actionbar_tt.*
import kotlinx.android.synthetic.main.layout_main_menu_left_tt.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.user.UserLoginActivity
import vn.mediatech.istudiocafe.adapter.ViewPagerAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.fragment.*
import vn.mediatech.istudiocafe.listener.*
import vn.mediatech.istudiocafe.model.ItemAppConfig
import vn.mediatech.istudiocafe.model.ItemInteractiveConfig
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemNotify
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.fcm.MyFirebaseMessagingManager
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.chatsupport.ChatSupportActivity
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemChat
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemConversation
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import vn.mediatech.istudiocafe.zinteractive.webGame.WebGameFragment
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : BaseActivity() {
    var drawerToggle: ActionBarDrawerToggle? = null
    var fragmentList: ArrayList<Fragment> = ArrayList()
    var isStopAutoRotate = false
    var currentTabTag = ""
    var tabPersonFragment: TabPersonFragment? = null
    var isTabPersonFragmentShowing = false
    var cartFragment: CartFragment? = null
    var isCartFragmentShowing = false
    var callStaffFragment: CallStaffFragment? = null
    var isCallStaffFragmentShowing = false
    var itemAppConfig: ItemAppConfig? = null
    var imageLogoActionbarCenterLocation: IntArray? = null

    companion object {
        var instance = false
        var typeShowNotify: String? = null
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(Constant.APP_CONFIG, itemAppConfig)
        outState.putIntArray(Constant.LOCATION, imageLogoActionbarCenterLocation)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_tt)
        instance = true
        initUI(savedInstanceState)
    }

    fun refreshTabPersonFragment() {
        if (isTabPersonFragmentShowing && tabPersonFragment != null) {
            runOnUiThread { tabPersonFragment!!.initLogin() }
        }
        /*for (fragment in fragmentList) {
            if (fragment is TabPersonFragment) {
                val childFragment = fragment as TabPersonFragment
                runOnUiThread { childFragment.initLogin() }
                break
            }
        }*/
    }

    fun initUI(savedInstanceState: Bundle?) {
        setFullStatusTransparent()
//        setStatusbarColor("#ffffff", true)
        setDefaultBackgroundImage()
//        MyApplication.getInstance().loadImageBitmapListener(
//            this,
//            "http://mdtv.mediatech.vn/upload/1/group/mediatech.1621830664.60ab2c08a87dc.jpg",
//            object : MyApplication.OnLoadImageBitmapListener {
//                override fun onResourceReady(resource: Bitmap?) {
//                }
//            })

        drawerToggle = object :
            ActionBarDrawerToggle(this, drawerLayout, R.string.app_name, R.string.app_name) {
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
            }

            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
            }
        }
        drawerLayout?.addDrawerListener(drawerToggle as ActionBarDrawerToggle)
        drawerToggle?.syncState()
        setLayoutWidthLeftMenu()
//        loadImageLogoActionbar()

        val bundle = savedInstanceState ?: intent.extras
        imageLogoActionbarCenterLocation = bundle?.getIntArray(Constant.LOCATION)
        itemAppConfig = bundle?.getParcelable(Constant.APP_CONFIG)
        if (itemAppConfig == null) {
            return
        }
        MyApplication.getInstance().dataManager.itemAppConfig = itemAppConfig
        MyApplication.getInstance().baseApiUrl = itemAppConfig!!.baseApiUrl
        initLogoAnimation()
        getPopup()
    }

    fun init() {
        if (isFinishing || isDestroyed) {
            return
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            setLogoPos()
            initData()
            initControl()
            autoLogin()
        }, 200)
        val myFirebaseMessagingManager = MyFirebaseMessagingManager(this)
        myFirebaseMessagingManager.registerFCM(null)
    }

    fun autoLogin() {
        ServiceUtilTT.autoLogin(this, object : OnLoginFormListener {
            override fun onLogin(hasLogin: Boolean) {
                if (hasLogin) {
                    refreshTabPersonFragment()
                    if (isDestroyed || isFinishing) {
                        return
                    }
                    ServiceManager.setNeedUpdateFcmTokenForUser(this@MainActivity, true)
                    ServiceManager.updateFcmTokenUser(this@MainActivity)
                    runOnUiThread {
                        updateUIChangeAcount()
                        if (Constant.TYPE_VOUCHER.equals(typeShowNotify)) {
                            GeneralUtils.openMyVoucher(this@MainActivity)
                            typeShowNotify = null
                        }
                    }
                }
                mainInteractiveFragment?.isCheckAutoLogin = true
            }
        })
    }

    fun initLogoAnimation() {
        val itemAppConfig = MyApplication.getInstance().dataManager.itemAppConfig
        if (itemAppConfig == null || itemAppConfig.logo.isNullOrEmpty()) {
            imageLogoActionbarCenter.visibility = View.GONE
            init()
            return
        }
        if (imageLogoActionbarCenterLocation == null) {
            loadImageLogoActionbar()
            return
        }
        imageLogoActionbarCenter.visibility = View.VISIBLE
        imageLogoActionbarCenter.x = imageLogoActionbarCenterLocation!![0].toFloat()
        imageLogoActionbarCenter.y = imageLogoActionbarCenterLocation!![1].toFloat()

//        MyApplication.getInstance().loadImageNoPlaceHolder(this, imageLogoActionbarCenter, itemAppConfig.logo)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDestroyed || isFinishing) {
                return@Runnable
            }

            val screenSize = ScreenSize(this)
//            Loggers.e("screenSize","${screenSize.width} x ${screenSize.height}")

            val logoHeightOrigin = resources.getDimensionPixelSize(R.dimen.logo_actionbar_height)
            val logoMaxWidth = screenSize.width / 3
            MyApplication.getInstance().loadImageBitmapListener(
                this,
                itemAppConfig.logo,
                object : MyApplication.OnLoadImageBitmapListener {
                    override fun onResourceReady(resource: Bitmap?) {
                        if (resource == null || isDestroyed || isFinishing) {
                            init()
                            return
                        }
                        var imageWidth = resource.width
                        var imageHeight = resource.height
                        val widthRatio: Float = imageWidth * 1f / imageHeight

                        if (imageWidth > logoMaxWidth) {
                            imageWidth = logoMaxWidth
                        }
                        imageHeight = (imageWidth / widthRatio).toInt()

                        if (imageHeight > logoHeightOrigin) {
                            val heighRatio: Float = imageHeight * 1f / logoHeightOrigin
                            imageWidth = (imageWidth / heighRatio).toInt()
                            imageHeight = logoHeightOrigin
                        }
                        val lpLogoActionbar: RelativeLayout.LayoutParams =
                            imageLogoActionbar.layoutParams as RelativeLayout.LayoutParams
                        lpLogoActionbar.width = imageWidth
                        lpLogoActionbar.height = imageHeight
                        imageLogoActionbar.setImageBitmap(resource)
//                    imageLogoActionbar.setColorFilter(ContextCompat.getColor(this@MainActivity, R.color.transparent))

                        val imageCenterWidth = screenSize.width / 2
                        val imageCenterHeight = (imageCenterWidth / widthRatio).toInt()
                        val lpLogoActionbarCenter: RelativeLayout.LayoutParams =
                            imageLogoActionbarCenter.layoutParams as RelativeLayout.LayoutParams
                        lpLogoActionbarCenter.width = imageCenterWidth
                        lpLogoActionbarCenter.height = imageCenterHeight

//                    imageLogoActionbarCenter.setImageBitmap(resource)
                        imageLogoActionbarCenter.setImageBitmap(
                            Bitmap.createScaledBitmap(
                                resource,
                                imageCenterWidth,
                                imageCenterHeight,
                                false
                            )
                        )

                        imageLogoActionbarCenter.viewTreeObserver.addOnGlobalLayoutListener(object :
                            ViewTreeObserver.OnGlobalLayoutListener {
                            override fun onGlobalLayout() {
                                imageLogoActionbarCenter.viewTreeObserver.removeOnGlobalLayoutListener(
                                    this
                                )
                                imageLogoActionbarCenterReady = true
                                startAnimation()
                            }
                        })
                        imageLogoActionbar.viewTreeObserver.addOnGlobalLayoutListener(object :
                            ViewTreeObserver.OnGlobalLayoutListener {
                            override fun onGlobalLayout() {
                                imageLogoActionbar.viewTreeObserver.removeOnGlobalLayoutListener(
                                    this
                                )
                                imageLogoActionbarReady = true
                                startAnimation()
                            }
                        })
                    }
                })
        }, 0L)
    }

    var imageLogoActionbarReady = false
    var imageLogoActionbarCenterReady = false
    fun startAnimation() {
        if (!imageLogoActionbarReady) {
            return
        }
        if (!imageLogoActionbarCenterReady) {
            return
        }
        val imageWidth = imageLogoActionbar.width
        val imageHeight = imageLogoActionbar.height
        val imageCenterWidth = imageLogoActionbarCenter.width
        val imageCenterHeight = imageLogoActionbarCenter.height

        val scaleWidthRatio = imageWidth * 1f / imageCenterWidth
        val scaleHeightRatio = imageHeight * 1f / imageCenterHeight
        // when use setFullStatusTransparent
        val pointImageLogoActionbar = imageLogoActionbar.getLocationOnScreen()
        val posX = pointImageLogoActionbar.x * 1f
        val posY = pointImageLogoActionbar.y * 1f

//        //when not use setFullStatusTransparent
//        val posX = imageLogoActionbar.x * 1f
//        val posY = imageLogoActionbar.y * 1f
//        //when not use setFullStatusTransparent

        imageLogoActionbarCenter.pivotX = 0f
        imageLogoActionbarCenter.pivotY = 0f
        imageLogoActionbarCenter.animate()
            .x(posX)
            .y(posY)
            .scaleX(scaleWidthRatio)
            .scaleY(scaleHeightRatio)
//                .rotation(720f)
            .setInterpolator(DecelerateInterpolator())
//                .setInterpolator(LinearInterpolator())
//                .setInterpolator(LinearOutSlowInInterpolator())
//                .setInterpolator(BounceInterpolator())
            .setDuration(500)
            .withEndAction {
                init()
            }
            .setStartDelay(300L)
            .start()
    }

    var topSpace = 0
    fun initData() {
        ServiceUtilTT.itemInteractiveConfig = ItemInteractiveConfig(
            1,
            "istudio",
            "1",
            "1",
            "0",
            "",
            "",
            "",
            "wss://api.daugiatruyenhinh.com:8088/interactive?", "",
            "", "", null, null
        )
        startGetConfigInteractive()
        layoutActionbar.viewTreeObserver.addOnGlobalLayoutListener(object :
            ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                layoutActionbar.viewTreeObserver.removeOnGlobalLayoutListener(this)
                val actionBarHeight = layoutActionbar.measuredHeight
//                topSpace = actionBarHeight + getStatusBarHeight()
            }
        })
        try {
            initPager()
        } catch (e: Exception) {
        }
        val pInfo = packageManager.getPackageInfo(packageName, 0)
        textVersion.text = pInfo.versionName ?: ""
        val bundle = intent.extras
        if (bundle != null) {
            val item: Parcelable? = bundle.getParcelable(Constant.DATA)
            if (item is ItemVoucher) {
                showDonatedVoucherDialog(item)
            } else if (item is ItemChat) {
                val itemChat = item
                val itemConversation = ItemConversation(
                    "",
                    itemChat.voucherId,
                    itemChat.userId,
                    itemChat.uniqueId,
                    itemChat.userName,
                    itemChat.userAvatar,
                    itemChat.content,
                    itemChat.image,
                    itemChat.content,
                    "",
                    0,
                    null
                )
                val bundleChat = Bundle()
                bundleChat.putParcelable(Constant.DATA, itemConversation)
                gotoActivity(ChatSupportActivity::class.java, bundleChat)
            } else if (item is ItemNews) {
                goDetailNewsActivity(item)
            }
            typeShowNotify = bundle.getString(Constant.TYPE_SHOW_NOTIFY)
//            Loggers.e("TYPE_SHOW_NOTIFY_MAIN",bundle.getString(Constant.TYPE_SHOW_NOTIFY))
            showNotify()
        }
    }

    var mainInteractiveFragment: MainInteractiveFragment? = null
    var webviewFragment: WebviewFragment? = null
    fun initPager() {
        val tabHomeFragment = TabHomeFragment()
        val tabTitleList = java.util.ArrayList<String>()
        fragmentList.add(tabHomeFragment)
        tabTitleList.add(getString(R.string.home))
        var bundle = Bundle()
        if (Constant.IS_SHOW_FORUM) {
            appBarLayout.visibility = View.VISIBLE
            webviewFragment = WebviewFragment()
            bundle.putBoolean(Constant.IS_SHOW_TOP_SPACE, true)
//        bundle.putInt(Constant.TYPE, Constant.TAB_TYPE_PRODUCT)
            webviewFragment!!.arguments = bundle
            fragmentList.add(webviewFragment!!)
//            fragmentList.add(WebGameFragment())
            tabTitleList.add(getString(R.string.forum))
//            tabTitleList.add("Diễn đàn")
        }

        mainInteractiveFragment = MainInteractiveFragment()
        bundle = Bundle()
//            bundle.putString(Constant.APP_ID,"tuongtac.tv")
        mainInteractiveFragment!!.arguments = bundle
        mainInteractiveFragment?.onShowListener = object : OnShowListener {
            override fun onViewCreated() {
            }

            override fun onResume() {
                startGetConfigInteractive()
            }

            override fun onPause() {
                stopTimerGetLive()
            }

            override fun onStop() {
            }
        }
        fragmentList.add(mainInteractiveFragment!!)
        tabTitleList.add(getString(R.string.live_en))
        val tabVideoFragment = NewsFragment()
        bundle = Bundle()
        bundle.putBoolean(Constant.IS_SHOW_TOP_SPACE, true)
        bundle.putInt(Constant.TYPE, Constant.TAB_TYPE_VIDEO)
        tabVideoFragment.arguments = bundle
        fragmentList.add(tabVideoFragment)
        tabTitleList.add(getString(R.string.videos))

        val tabGalleryFragment = NewsFragment()
        bundle = Bundle()
        bundle.putBoolean(Constant.IS_SHOW_TOP_SPACE, true)
        bundle.putInt(Constant.TYPE, Constant.TAB_TYPE_NEWS)
        tabGalleryFragment.arguments = bundle
        fragmentList.add(tabGalleryFragment)
        tabTitleList.add(getString(R.string.news))


//        val tabPersonFragment = TabPersonFragment()
//        fragmentList.add(tabPersonFragment)

        val viewPagerAdapter = ViewPagerAdapter(supportFragmentManager, fragmentList, tabTitleList)
        viewPager.adapter = viewPagerAdapter
        viewPager.offscreenPageLimit = fragmentList.size
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                handleOnPageSelected(position)
            }

            override fun onPageScrollStateChanged(state: Int) {
            }
        })
        tabMain.setupWithViewPager(viewPager)

        val navIcons = if (Constant.IS_SHOW_FORUM) intArrayOf(
            R.mipmap.ic_menu_home,
            R.mipmap.ic_menu_forum,
//            R.mipmap.ic_menu_interactive,
            R.drawable.ic_tab_interactive3,
            R.mipmap.ic_menu_video_2,
            R.mipmap.ic_menu_news
        ) else intArrayOf(
            R.mipmap.ic_menu_home,
//            R.mipmap.ic_menu_forum,
//            R.mipmap.ic_menu_interactive,
            R.drawable.ic_tab_interactive3,
            R.mipmap.ic_menu_video_2,
            R.mipmap.ic_menu_news
        )
        val navLabels = if (Constant.IS_SHOW_FORUM) intArrayOf(
            R.string.home,
            R.string.forum,
            R.string.interactive_tab,
            R.string.videos,
            R.string.news
        ) else intArrayOf(
            R.string.home,
//            R.string.forum,
            R.string.interactive_tab,
            R.string.videos,
            R.string.news
        )

        for (i in 0 until tabMain.tabCount) {
            val tab =
                LayoutInflater.from(this).inflate(R.layout.item_tab_home_tt, null) as LinearLayout
            val title = tab.findViewById<TextView>(R.id.title)
            val icon = tab.findViewById<ImageView>(R.id.icon)
            title.text = resources.getString(navLabels[i])
            icon.setImageResource(navIcons[i])
//            if (i == 2) {
//                icon.visibility = View.INVISIBLE
//                title.setTypeface(title.getTypeface(), Typeface.BOLD)
//                title.setTextSize(14F)
//            }
            tabMain.getTabAt(i)!!.customView = tab
        }
        Handler(Looper.getMainLooper()).postDelayed({
            handleOnPageSelected(0)
        },200)
    }

    var selectedFragment: Fragment? = null
    fun handleOnPageSelected(position: Int) {
        val fragment = fragmentList.get(position)
        selectedFragment = fragment
        if (Constant.IS_SHOW_FORUM) {
            if (position == 1) {
                layoutActionbar.visibility = View.GONE
                imageLogoActionbar.visibility = View.GONE
            } else {
                layoutActionbar.visibility = View.VISIBLE
                imageLogoActionbar.visibility = View.VISIBLE
            }
            if (position == 2) {
                voiceControl?.hideIconVoiceController()
            } else {
                voiceControl?.showIconVoiceController()
            }
        } else {
            if (position == 1) {
                voiceControl?.hideIconVoiceController()
            } else {
                voiceControl?.showIconVoiceController()
            }
        }
        if (fragment is TabHomeFragment) {
            fragment.checkRefresh()
        } else if (fragment is NewsFragment) {
            fragment.checkRefresh()
        } else if (fragment is TabPersonFragment) {
            fragment.checkRefresh()
        } else if (fragment is TabNewsFragment) {
            fragment.checkRefresh()
        } else if (fragment is WebviewFragment) {
            fragment.checkRefresh()
        } else if (fragment is InteractiveLiveStreamFragment) {
            fragment.checkRefresh()
        } else if (fragment is NewsFragment) {
            fragment.checkRefresh()
        } else if (fragment is MainInteractiveFragment) {
            voiceControl?.hideIconVoiceController()
            fragment.checkRefresh()
        }
    }


    fun moveToTab(position: Int) {
        drawerLayout?.closeDrawers()
        removePersonFragment()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            try {
                viewPager?.currentItem = position
            } catch (e: Exception) {
            }
        }, 200)
    }

    fun showWebGame(urlGame: String? = null) {
        val fragment = WebGameFragment()
        val bundle = Bundle()
        val height =
            vn.mediatech.interactive.app.ScreenSize(this).height - AppUtils.convertDpToPixel(
                Constant.HEIGHT_TAB_BAR_TOP_DP.toFloat(),
                this
            ).toInt()
        bundle.putInt(InteractiveConstant.HEIGHT, height)
        bundle.putString(InteractiveConstant.DATA, urlGame)
        fragment.arguments = bundle
        fragment.show(supportFragmentManager, "Web")
    }

    var isLoginShowCHatSupport: Boolean = false
    fun initControl() {
//        getConfig()
        buttonMenuHome?.setOnClickListener {
//            showWebGame()
            moveToTab(0)
//            gotoActivity(InteractiveActivity::class.java)
        }
        buttonMenuForum?.setOnClickListener {
            moveToTab(1)
        }
        buttonMenuInteractive?.setOnClickListener {
            showInteractive()
        }

        buttonMenuVideo?.setOnClickListener {
            if (Constant.IS_SHOW_FORUM) {
                moveToTab(3)
            } else {
                moveToTab(2)
            }
        }
        buttonMenuNews?.setOnClickListener {
            if (Constant.IS_SHOW_FORUM) {
                moveToTab(4)
            } else {
                moveToTab(3)
            }
        }
        buttonMenuShare?.setOnClickListener {
            drawerLayout.closeDrawers()
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                shareLinkApp()
            }, 200)
        }
        buttonMenuAbout?.setOnClickListener {
        }

        buttonMenuToggle?.setOnClickListener {
            toogleLeftMenu()
        }
        buttonPerson?.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showPersonFragment()
            }, 100)
        }
        buttonNotification?.setOnClickListener {
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser == null) {
                showDialog(
                    true,
                    R.string.notification,
                    getString(R.string.msg_require_login_admin),
                    R.string.login,
                    R.string.close,
                    object : OnDialogButtonListener {
                        override fun onLeftButtonClick() {
                            isLoginShowCHatSupport = true
                            gotoActivityForResult(
                                UserLoginActivity::class.java,
                                Constant.CODE_LOGIN
                            )
                        }

                        override fun onRightButtonClick() {
                        }
                    })
                return@setOnClickListener
            }
            if (!itemUser.role.equals(Constant.ADMIN)) {
                showDialog(getString(R.string.msg_require_login_admin))
                return@setOnClickListener
            }
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
//                val fragment = NotificationFragment()
//                showBottomSheetDialog(fragment)
                gotoActivity(ChatSupportActivity::class.java)
            }, 200)
        }
        buttonSearch?.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                val fragment = SearchFragment()
                showBottomSheetDialog(fragment)
            }, 200)
        }
        layoutVersion.setOnClickListener {
//            openLink("http://api.daugiatruyenhinh.com/Media/istudio.apk")
//            openLink("http://apptv.bacgiangtv.vn/private/istudio.apk")
        }
        buttonVoucher?.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                val itemUser = MyApplication.getInstance().dataManager.itemUser
                if (itemUser == null) {
                    showDialog(
                        true,
                        R.string.notification,
                        R.string.msg_require_login,
                        R.string.close,
                        R.string.login,
                        object : OnDialogButtonListener {
                            override fun onLeftButtonClick() {
                            }

                            override fun onRightButtonClick() {
                                showPersonFragment()
                            }
                        })
                    return@Runnable
                }
                gotoActivity(VoucherActivity::class.java)
            }, 200)
        }
        buttonMessenger.setOnClickListener {
//            openFacebookPage("3144317609002970")
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser != null) {
                val str =
                    "Tài khoản của tôi là " + itemUser.fullname + ", mã số " + itemUser.id + ". Chương trình vui lòng xác thực tài khoản giúp tôi!"
                GeneralUtils.coppyToClipboard(this, str)
            }
//                    progressBar.setVisibility(View.VISIBLE);
            //                    progressBar.setVisibility(View.VISIBLE);
            try {
                val i = Intent(Intent.ACTION_VIEW, Uri.parse("fb://messaging/1356792611058221"))
                startActivity(i)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
                val i =
                    Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.facebook.orca"))
                startActivity(i)
            }
        }
        layoutPerson.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showPersonFragment()
            }, 100)
        }
    }


    fun showBottomSheetDialog(fragment: BottomSheetDialogFragment) {
        fragment.show(supportFragmentManager, fragment.tag)
    }

    fun setLayoutWidthLeftMenu() {
        val screenSize = ScreenSize(this)
        val param = layoutMainLeft.layoutParams
        val leftWidth = screenSize.width * 9 / 10
        param.width = leftWidth
//        layoutMainLeft.layoutParams = param
        MyApplication.getInstance()
            .loadDrawableImageNow(this, R.drawable.bg_main_tt, imageBgLeftMenu)

        val paramImageLogo: LinearLayout.LayoutParams =
            imageLogoLeftMenu.layoutParams as LinearLayout.LayoutParams
        val imageWidth = leftWidth * 3 / 5
        paramImageLogo.width = imageWidth
        paramImageLogo.height = imageWidth / 3
//        imageBgLeftMenu.layoutParams = paramImageLogo
    }

    fun toogleLeftMenu() {
        if (drawerLayout!!.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers()
        } else {
            drawerLayout.openDrawer(GravityCompat.START)
        }
    }

    fun showPersonFragment() {
        if (isTabPersonFragmentShowing) {
            return
        }
        isTabPersonFragmentShowing = true
        setCanShowInteractive(false)
        tabPersonFragment = TabPersonFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.fragment_drop_down, R.anim.fragment_drop_up)
        fragmentTransaction.add(R.id.frameLayoutDialog, tabPersonFragment!!)
        fragmentTransaction.commit()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            tabPersonFragment!!.checkRefresh()
        }, 200)
    }

    fun removePersonFragment(): Boolean {
        if (isTabPersonFragmentShowing && tabPersonFragment != null) {
            if (tabPersonFragment!!.isPopupMenuChangeAvatarShowing()) {
                return true
            }
            if (tabPersonFragment!!.isPopupMenuChangeGenderShowing()) {
                return true
            }
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                R.anim.fragment_drop_down,
                R.anim.fragment_drop_up
            )
            fragmentTransaction.remove(tabPersonFragment!!)
            fragmentTransaction.commit()
            isTabPersonFragmentShowing = false
            setCanShowInteractive(true)
            tabPersonFragment = null
            return true
        }
        return false
    }

    fun showCartFragment(onCancelListener: OnCancelListener?) {
        if (isCartFragmentShowing) {
            return
        }
        isCartFragmentShowing = true
        cartFragment = CartFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
        fragmentTransaction.add(android.R.id.content, cartFragment!!)
        fragmentTransaction.commit()
        cartFragment!!.onCancelListener = onCancelListener
    }

    fun removeCartFragment(): Boolean {
        if (isCartFragmentShowing && cartFragment != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.slide_out_down, R.anim.slide_in_down)
            fragmentTransaction.remove(cartFragment!!)
            fragmentTransaction.commit()
            isCartFragmentShowing = false
            cartFragment = null
            return true
        }
        return false
    }

    fun showCallStaffFragment() {
        if (isCallStaffFragmentShowing) {
            return
        }
        isCallStaffFragmentShowing = true
        callStaffFragment = CallStaffFragment()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up)
        fragmentTransaction.add(android.R.id.content, callStaffFragment!!)
        fragmentTransaction.commit()
    }

    fun removeCallStaffFragment(): Boolean {
        if (isCallStaffFragmentShowing && callStaffFragment != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(R.anim.slide_out_down, R.anim.slide_in_down)
            fragmentTransaction.remove(callStaffFragment!!)
            fragmentTransaction.commit()
            isCallStaffFragmentShowing = false
            callStaffFragment = null
            return true
        }
        return false
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle?.onConfigurationChanged(newConfig)
        setLayoutWidthLeftMenu()
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToggle?.syncState()
    }

    override fun onKeyUp(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                if (selectedFragment != null && selectedFragment is WebviewFragment) {
                    val isBack: Boolean = (selectedFragment as WebviewFragment).backUrl()
                    if (isBack) {
                        return true
                    }
                }
                if (selectedFragment != null && selectedFragment is MainInteractiveFragment) {
                    val isBack: Boolean = (selectedFragment as MainInteractiveFragment).canBack()
                    if (isBack) {
                        return true
                    }
                }
                if (drawerLayout?.isDrawerOpen(GravityCompat.START)!!) {
                    drawerLayout.closeDrawers()
                    return true
                }
                if (removePersonFragment()) {
                    return true
                }
                if (removeCartFragment()) {
                    return true
                }
                if (isCallStaffFragmentShowing && callStaffFragment != null) {
                    if (callStaffFragment!!.isPopupMenuTableShowing()) {
                        return true
                    }
                }
                if (removeCallStaffFragment()) {
                    return true
                }
                showDialog(
                    true,
                    true,
                    getString(R.string.notification),
                    getString(R.string.exit_msg),
                    getString(R.string.exit),
                    getString(R.string.close),
                    object : OnDialogButtonListener {
                        override fun onLeftButtonClick() {
                            finish()
                        }

                        override fun onRightButtonClick() {
                        }
                    })
                return true
            }
        }
        return super.onKeyUp(keyCode, event)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in fragmentList) {
            fragment.onActivityResult(requestCode, resultCode, intent)
        }
        if (isTabPersonFragmentShowing && tabPersonFragment != null) {
            tabPersonFragment!!.onActivityResult(requestCode, resultCode, intent)
        }
        if (isCartFragmentShowing && cartFragment != null) {
            cartFragment!!.onActivityResult(requestCode, resultCode, intent)
        }
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == Constant.CODE_LOGIN && !isTabPersonFragmentShowing && !isCartFragmentShowing && isLoginShowCHatSupport) {
            isLoginShowCHatSupport = false
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser == null || !itemUser.role.equals("admin")) {
                showToast(getString(R.string.msg_require_login_admin))
                return
            }
            gotoActivity(ChatSupportActivity::class.java)
        }
    }

    override fun onDestroy() {
        instance = false
        super.onDestroy()
    }


    fun setLogoPos() {
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            imageLogoActionbarCenter.x = imageLogoActionbar.x
//            imageLogoActionbarCenter.y = imageLogoActionbar.y
            imageLogoActionbarCenter.visibility = View.GONE
            imageLogoActionbar.visibility = View.VISIBLE
        }, 0)
    }

    fun updateUIChangeAcount() {
        runOnUiThread {
            if (selectedFragment != null && selectedFragment is WebviewFragment) {
                (selectedFragment as WebviewFragment).checkUrlForum()
            }
            mainInteractiveFragment?.checkShowInterActive()
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            buttonNotification?.visibility =
                if (itemUser != null && !itemUser.role.isNullOrEmpty() && itemUser.role.equals(
                        Constant.ADMIN
                    )
                ) View.VISIBLE else View.GONE
            if (itemUser == null) {
                buttonPerson.setImageResource(R.mipmap.ic_avatar)
                textUserId.visibility = View.GONE
            } else {
                if (itemUser.id != null) {
                    val id = "ID:" + "${itemUser.id}"
//                    val id = "${itemUser.id}"
                    TextUtil.setHtmlTextView(id, textUserId)
                    textUserId.visibility = View.VISIBLE
                }
                MyApplication.getInstance()
                    .loadImageCircleClearCache(
                        this,
                        buttonPerson,
                        itemUser.avatar,
                        R.mipmap.ic_avatar
                    )
            }
        }
    }

    fun setCanShowInteractive(isShow: Boolean) {
        runOnUiThread {
            if (selectedFragment != null && selectedFragment is InteractiveLiveStreamFragment) {
                (selectedFragment as InteractiveLiveStreamFragment).setOnScreenInteractive(isShow)
            }
            if (selectedFragment != null && selectedFragment is MainInteractiveFragment) {
                (selectedFragment as MainInteractiveFragment).interactiveLiveStreamFragment?.setOnScreenInteractive(
                    isShow
                )
            }
        }
    }

    var timerGetLive: Timer? = null
    fun startGetConfigInteractive() {
        ServiceUtilTT.startTimerRequestConfig(
            this,
            "tuongtac.tv",
            object : OnGetConfigInteractiveListener {
                override fun onResponse(
                    isSuccess: Boolean,
                    message: String?,
                    item: ItemInteractiveConfig?
                ) {
                    if (isSuccess) {
                        runOnUiThread {
                            mainInteractiveFragment?.initInteractiveLiveStreamFragment()
                        }
                    }
                }
            },
            10000
        )
    }

    private fun stopTimerGetLive() {
        ServiceUtilTT.stopRequestConfig()
        /*   if (timerGetLive != null) {
               try {
                   timerGetLive?.cancel()
                   timerGetLive?.purge()
               } catch (e: Exception) {
                   e.printStackTrace()
               }
               timerGetLive = null
           }*/
    }

    override fun onResume() {
        super.onResume()
        hideLoadingDialog()
        showNotify()
//        startGetLive()
    }


    override fun onPause() {
        super.onPause()
        stopTimerGetLive()
    }

    override fun onStop() {
        super.onStop()
        mainInteractiveFragment?.closeSocket()
    }

    private fun getPopup() {
        if (isDestroyed || isFinishing) {
            return
        }
        //    GeneralUtils.showDialogIncludeImage(this,true,true,"GIỚI THIỆU NGAY - NHẬN QUÀ LIỀN TAY!",null,"https://daugiatruyenhinh.com/upload/banner/file/56d2ad3fe2b85f90500ac82e33e3ca6e.jpg","https://daugiatruyenhinh.com/upload/video/file/dbe881067cd522ca7c51a9f5a818d904.mp4","Huỷ bỏ", "Tìm hiểu ngay",null)
//        GeneralUtils.showDialogIncludeImage(this, true, true, "GIỚI THIỆU NGAY - NHẬN QUÀ LIỀN TAY!", null, null, "https://daugiatruyenhinh.com/upload/video/file/dbe881067cd522ca7c51a9f5a818d904.mp4", "Huỷ bỏ", "Tìm hiểu ngay", 10, null)
        val myHttpRequest = MyHttpRequest(this)
        val api = ServiceUtilTT.validAPIDGTH(this, Constant.API_GET_POPUP)
        myHttpRequest.request(false, api, null, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                val json = JsonParser.getJsonObject(responseString)
                val statusCode = JsonParser.getInt(json, Constant.CODE)
                if (statusCode != Constant.SUCCESS) {
                    return
                }
                val data = JsonParser.getString(json, Constant.RESULT)
                val gson = Gson()
                val itemNotify: ItemNotify? = try {
                    gson.fromJson(data, ItemNotify::class.java)
                } catch (e: Exception) {
                    null
                }
                if (isFinishing || isDestroyed) {
                    return
                }
//                itemNotify?.fullImage = null
                runOnUiThread {
                    showPopup(itemNotify)
                }
            }
        })
    }

    private fun showPopup(itemNotify: ItemNotify?) {
        if (itemNotify == null || (itemNotify.message.isNullOrEmpty() && itemNotify.image.isNullOrEmpty() && itemNotify.video.isNullOrEmpty() && itemNotify.fullImage.isNullOrEmpty())) {
            return
        }
        val popupId = SharedPreferencesManager.getPopupID(this@MainActivity)
        var countPopup = SharedPreferencesManager.getPopupCount(this@MainActivity)
        if (itemNotify.id != null && !itemNotify.id.equals(popupId)) {
            SharedPreferencesManager.savePopupID(this@MainActivity, itemNotify.id)
            SharedPreferencesManager.savePopupCount(this@MainActivity, 0)
            countPopup = 0
        }
        if (itemNotify.count != null && itemNotify.count <= countPopup) {
            return
        }
        countPopup = countPopup + 1
        SharedPreferencesManager.savePopupCount(this@MainActivity, countPopup)
        if (!itemNotify.fullImage.isNullOrEmpty()) {
            GeneralUtils.showDialogImage(
                this@MainActivity,
                true,
                true,
                itemNotify.fullImage,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        clickPoup(itemNotify)
                    }

                    override fun onRightButtonClick() {
                    }
                }
            )
        } else {
            var leftButton: String? = null
            var rightButton: String? = null
            val contentType = itemNotify.contentType
            if (!contentType.isNullOrEmpty() && (contentType.equals(Constant.TYPE_VIDEO) || contentType.equals(
                    Constant.TYPE_WEB
                ) || contentType.equals(Constant.TYPE_NEWS) || contentType.equals(
                    Constant.TYPE_LIVE
                ))
            ) {
                rightButton = "Xem ngay"
            }
//            if (itemNotify.time == null || itemNotify.time <= 0) {
            leftButton = "Bỏ qua"
//            }
            GeneralUtils.showDialogIncludeImage(
                this@MainActivity,
                true,
                true,
                itemNotify.title,
                itemNotify.message,
                itemNotify.image,
                itemNotify.video, leftButton, rightButton,
                itemNotify.time, object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                    }

                    override fun onRightButtonClick() {
                        clickPoup(itemNotify)
                    }
                }
            )
        }
    }

    fun clickPoup(itemNotify: ItemNotify) {
        if (itemNotify.contentType.isNullOrEmpty()) {
            return
        }
        when (itemNotify.contentType) {
            Constant.TYPE_VIDEO, Constant.TYPE_NEWS -> {
                if (itemNotify.contentId.isNullOrEmpty()) {
                    return
                }
                val itemNews = ItemNews()
                itemNews.id = itemNotify.contentId
                itemNews.contentType = itemNotify.contentType
                goDetailNewsActivity(itemNews)
            }
            Constant.TYPE_LIVE -> {
                moveToTab(2)
            }
            Constant.TYPE_WEB -> {
                openLink(itemNotify.url)
            }
        }
    }

    fun showNotify() {
        if (Constant.TYPE_INTERACTIVE.equals(typeShowNotify)) {
            showInteractive()
        } else if (Constant.TYPE_FORUM.equals(typeShowNotify)) {
            showForum()
        } else if (Constant.TYPE_VIDEO.equals(typeShowNotify)) {
            runOnUiThread {
                if (Constant.IS_SHOW_FORUM) {
                    moveToTab(3)
                } else {
                    moveToTab(2)
                }
            }
        } else if (Constant.TYPE_NEWS.equals(typeShowNotify)) {
            runOnUiThread {
                if (Constant.IS_SHOW_FORUM) {
                    moveToTab(4)
                } else {
                    moveToTab(3)
                }
            }
        } else if (Constant.TYPE_HOME.equals(typeShowNotify)) {
            runOnUiThread {
                moveToTab(0)
            }
        } else if (Constant.TYPE_PERSONAL.equals(typeShowNotify)) {
            runOnUiThread {
                Handler(Looper.getMainLooper()).postDelayed(Runnable {
                    showPersonFragment()
                }, 100)
            }
        }
        typeShowNotify = null
    }

    override fun onBackPressed() {
        if (mainInteractiveFragment == null || !mainInteractiveFragment!!.canBack()) {
            super.onBackPressed();
        }
    }

    fun showInteractive() {
        runOnUiThread {
            if (Constant.IS_SHOW_FORUM) {
                moveToTab(2)
            } else {
                moveToTab(1)
            }
        }
    }

    fun showForum() {
        runOnUiThread {
            moveToTab(1)
        }
    }

    fun initITV() {
        Handler(Looper.getMainLooper()).postDelayed({
            initITV(true)
        }, 200)
    }

//    private fun handleSwitchAppHandle(type: Int, keyword: String?) {
////        when (type) {
////            VoiceControl.TYPE_TAB_HOME -> viewPager.currentItem = 0
////            VoiceControl.TYPE_TAB_NEWS -> viewPager.currentItem = 1
////            VoiceControl.TYPE_TAB_TV -> viewPager.currentItem = 2
////            VoiceControl.TYPE_TAB_RADIO -> viewPager.currentItem = 3
////            VoiceControl.TYPE_TAB_VIDEO -> viewPager.currentItem = 4
////        }
//    }

    fun playPlayer(isPlay: Boolean) {
        if (selectedFragment != null) {
            if (selectedFragment is MainInteractiveFragment) {
                val childFragment = selectedFragment as MainInteractiveFragment
                childFragment.playPlayer(isPlay)
            }
        }
    }

}