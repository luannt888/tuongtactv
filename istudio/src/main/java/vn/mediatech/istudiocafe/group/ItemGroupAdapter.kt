package vn.mediatech.istudiocafe.group

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_group.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication

class ItemGroupAdapter(val context: Context, val list: ArrayList<ItemGroup>) : Adapter<ItemGroupAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_group,
                parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val itemObj: ItemGroup = list.get(position)
        MyApplication.getInstance().loadImageCircle(context, holder.imgAvatar, itemObj.avatar,R.drawable.ic_avatar)
        holder.textName.setText(itemObj.name)
        holder.textTotalMenber.setText("Có ${itemObj.totalMenber} thành viên")
        holder.textOnlineMenber.setText("${itemObj.onlineMenber} thành viên online")
        if (!itemObj.joined.isNullOrEmpty()) {
            holder.buttonJoin.text = itemObj.joined
            holder.buttonJoin.isSelected = Constant.GROUP_JOINED.equals(itemObj.joined)
        }
        holder.buttonJoin.setOnClickListener {
            onItemClickListener?.onClickJoin(itemObj, position)
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
        }
        holder.layoutRoot.setOnLongClickListener {
            onItemClickListener?.onLongClick(itemObj, position)
            true
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textTotalMenber = itemView.textTotalMenber
        val textOnlineMenber = itemView.textOnlineMenber
        val textName = itemView.textName
        val buttonJoin = itemView.buttonJoin
    }

    interface OnItemClickListener {
        fun onClick(item: ItemGroup, position: Int)
        fun onClickJoin(item: ItemGroup, position: Int)
        fun onLongClick(item: ItemGroup, position: Int)
    }

}