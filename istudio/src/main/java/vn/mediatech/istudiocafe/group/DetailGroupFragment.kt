package vn.mediatech.istudiocafe.group

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Outline
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.Settings
import android.view.*
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_group_detail.*
import kotlinx.android.synthetic.main.fragment_group_detail.buttonBack
import kotlinx.android.synthetic.main.fragment_group_detail.editSearch
import kotlinx.android.synthetic.main.fragment_group_detail.imageBgMain
import kotlinx.android.synthetic.main.fragment_group_detail.layoutRefresh
import kotlinx.android.synthetic.main.fragment_group_detail.progressBar
import kotlinx.android.synthetic.main.fragment_group_detail.recyclerView
import kotlinx.android.synthetic.main.fragment_group_detail.textNotify
import kotlinx.android.synthetic.main.fragment_list_group.*
import kotlinx.android.synthetic.main.item_checkbox.*
import vn.mediatech.interactive.app.*
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.interactive.service.SocketMangager
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.util.ScreenSize
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DetailGroupFragment : BottomSheetDialogFragment() {
    private var isLoadMore: Boolean = false
    private var page: Int = 1
    private var limit: Int = 200
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var myHttpRequest: MyHttpRequest? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var itemUser: ItemUser? = null
    var itemList: ArrayList<ItemMenber> = ArrayList()
    var itemListNotConfirm: ArrayList<ItemMenber> = ArrayList()
    var itemGroup: ItemGroup? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_group_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.USERNAME, itemUser)
            putParcelable(Constant.DATA, itemGroup)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
//        (activity as BaseActivity).setFullStatusTransparentFragment(layoutActionbar)
        MyApplication.getInstance().loadDrawableImageNow(activity, R.drawable.bg_main_tt, imageBgMain)

    }


    fun initData() {
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
        val bundle = arguments
        if (bundle == null) {
            dismiss()
        }
        itemGroup = bundle!!.getParcelable(Constant.DATA)
        if (itemGroup == null) {
            dismiss()
        }
        textTotalMenber.text = "${itemGroup!!.totalMenber}"
        getData(TYPE_GET_DATA_CONFIRMED)
        if (!itemUser!!.id!!.toString().equals(itemGroup!!.adminId!!)) {
            buttonSetUp.visibility = View.GONE
        } else {
            buttonSetUp.visibility = View.VISIBLE
            getData(TYPE_GET_DATA_NOT_CONFIRM)
        }
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    val TYPE_GET_DATA_CONFIRMED = 1
    val TYPE_GET_DATA_NOT_CONFIRM = 2
    fun getData(type: Int) {
        if (isLoading || isDetached) {
            return
        }
        isLoading = false
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        }
//        else {
//            myHttpRequest!!.cancel()
//        }
        val requestParams = RequestParams()
        requestParams.put("page", "${page}")
        requestParams.put("limit", "${limit}")
        requestParams.put("groupid", "${itemGroup!!.id}")
        requestParams.put("userid", "${itemUser!!.id}")
//        val api = String.format(Locale.ENGLISH, Constant.API_USE_VOUCHER_HISTORY, itemUser!!.id, itemObj!!.id)
        var api: String? = null
        if (type == TYPE_GET_DATA_CONFIRMED) {
            api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_DETAIL)
        } else
            if (type == TYPE_GET_DATA_NOT_CONFIRM) {
                api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_DETAIL_NOT_CORFIRM)
            }
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString, type)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?, type: Int) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString)
        if (jsonObject == null) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getString(jsonObject, Constant.RESULT)
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemMenber>>() {}.type
        var newList: ArrayList<ItemMenber> = ArrayList()
        try {
            val list: ArrayList<ItemMenber>? = gson.fromJson(resultObj, gsonType)
            newList.addAll(list!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        activity?.runOnUiThread {
            if (type == TYPE_GET_DATA_CONFIRMED) {
                try {
                    itemList.clear()
                    if (adapter != null) {
                        itemList.addAll(newList)
                        adapter?.notifyDataSetChanged()
                    } else {
                        itemList.addAll(newList)
                        initAdapter()
                    }
                    setData()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else if (type == TYPE_GET_DATA_NOT_CONFIRM) {
                itemListNotConfirm.clear()
                try {
                    if (adapterNotCorfirm != null) {
                        itemListNotConfirm.addAll(newList)
                        adapter?.notifyDataSetChanged()
                    } else {
                        itemListNotConfirm.addAll(newList)
                        initAdapterNotConfirm()
                    }
                    setData()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun setData() {
        textTotalMenber.text = itemList.size.toString()
        textConfirmMenber.text = itemListNotConfirm.size.toString()
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
        }
        if (itemListNotConfirm.size == 0) {
            layoutTitleWaitingConfirm.visibility = View.GONE
            recyclerViewConfirm.visibility = View.GONE
        } else {
            layoutTitleWaitingConfirm.visibility = View.VISIBLE
            recyclerViewConfirm.visibility = View.VISIBLE
        }
    }

    var adapter: ItemMenberAdapter? = null
    private fun initAdapter() {
        adapter = ItemMenberAdapter(requireContext(), itemList, false)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
//        recyclerView.scheduleLayoutAnimation()
        adapter!!.onItemClickListener = object : ItemMenberAdapter.OnItemClickListener {
            override fun onClick(item: ItemMenber, position: Int) {
            }

            override fun onClickChat(item: ItemMenber, position: Int) {
            }

            override fun onClickConfirm(item: ItemMenber, position: Int) {
            }

            override fun onLongClick(item: ItemMenber, position: Int) {
                showDialogDeleteMenber(item, position)
            }
        }
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    private fun showDialogDeleteMenber(
        item: ItemMenber,
        position: Int
    ) {
        if (itemUser == null || itemUser!!.id == null || itemGroup == null || !itemUser!!.id!!.toString()
                .equals(itemGroup!!.adminId!!) || itemUser!!.id!!.toString().equals(item.userId)
        ) {
            return
        }
        if (activity is BaseActivity) {
            (activity as? BaseActivity)?.showDialog(
                true,
                R.string.notification,
                "Bạn có muốn xoá thành viên ${
                    TextUtil.getStringHtmlStype(
                        item.name,
                        "#0e5d30",
                        true,
                        false
                    )
                } ra khỏi nhóm không? ",
                R.string.cancel,
                R.string.ok,
                object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {

                    }

                    override fun onRightButtonClick() {
                        requestDeleteMenber(item, position)
                    }
                })
        } else {
            requestDeleteMenber(item, position)
        }
    }

    fun requestDeleteMenber(item: ItemMenber?, pos: Int) {
        if (!MyApplication.getInstance().isNetworkConnect || item == null) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
//            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
//        requestParams.put("userid", "${itemUser!!.id}")
        requestParams.put("user_id", "${item.userId}")
        requestParams.put("admin_id", "${itemUser!!.id}")
        requestParams.put("group_id", "${itemGroup!!.id}")
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_REMOVE_USER)
        myHttpRequest!!.request(true, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        activity?.getString(R.string.msg_empty_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString.trim());
                if (jsonObject == null) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    message?.let {
                        activity?.runOnUiThread {
                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                        }
                    }
                    return
                }
                val result = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
                val mess = JsonParser.getString(result, "mess")
                activity?.runOnUiThread {
                    refreshData()
//                    onGroupListener?.onRequireConnectSocket()
                    mess?.let {
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    }
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    var adapterNotCorfirm: ItemMenberAdapter? = null
    private fun initAdapterNotConfirm() {
        adapterNotCorfirm = ItemMenberAdapter(requireContext(), itemListNotConfirm, true)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
//        recyclerView.scheduleLayoutAnimation()
        adapterNotCorfirm!!.onItemClickListener = object : ItemMenberAdapter.OnItemClickListener {
            override fun onClick(item: ItemMenber, position: Int) {
            }

            override fun onClickChat(item: ItemMenber, position: Int) {
            }

            override fun onClickConfirm(item: ItemMenber, position: Int) {
                requestConfirmMenber(item, position)
            }

            override fun onLongClick(item: ItemMenber, position: Int) {
                showDialogDeleteMenber(item, position)
            }
        }
        recyclerViewConfirm.layoutManager = layoutManager
        recyclerViewConfirm.adapter = adapterNotCorfirm
    }

    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData(TYPE_GET_DATA_CONFIRMED)
        }
        buttonBack.setOnClickListener {
            dismiss()
        }
        editSearch.setOnClickListener {
//            var fragment = SearchSupportFragment()
//            (activity as? ChatSupportActivity)?.showFragment(fragment)
        }
        layoutRefresh.setOnRefreshListener {
            refreshData()
        }
        buttonSetUp.setOnClickListener {
            showEditGroup()
        }
    }

    private fun refreshData() {
        page = 1
        adapter?.notifyDataSetChanged()
        progressBar.visibility = View.VISIBLE
        layoutRefresh.isRefreshing = false
        getData(TYPE_GET_DATA_CONFIRMED)
        if (!itemUser!!.id!!.toString().equals(itemGroup!!.adminId!!)) {
            buttonSetUp.visibility = View.GONE
        } else {
            buttonSetUp.visibility = View.VISIBLE
            getData(TYPE_GET_DATA_NOT_CONFIRM)
        }
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        mSocketMangager?.close()
        super.onDestroyView()
    }

    private var port: String? = ""
    private val PORT: String = "wss://api.daugiatruyenhinh.com:8088/interactive?" // debug
    private var mSocketMangager: SocketMangager? = null
    private fun initSocket() {
//        mSocketMangager = SocketMangager.getInstance()
//        var listenner = (object : SocketMangager.MySocketListenner {
//            override fun onConnected(response: Response) {
//                Loggers.e("Socket", "onConnected")
//            }
//
//            override fun onMessage(string: String) {
//                autoConnectCount = 0
//                val data = EncryptUtil.base64Decode(string)
////                data = resume
//                Loggers.e("Socket", "onMessage :" + data)
//                if (data.isNullOrEmpty()) {
//                    return
//                }
//                activity?.runOnUiThread {
//                    try {
//                        handleMessage(data)
//                    } catch (e: Exception) {
//                    }
//                }
//            }
//
//            override fun onClosed(code: Int, reason: String) {
//                Loggers.e("Socket", "onClosed" + reason)
//                if (code == InteractiveConstant.CLOSE_CONNECT) {
//                    return
//                }
//            }
//
//            override fun onClosing(code: Int, reason: String) {
//                Loggers.e("Socket", "onClosing" + reason)
//                if (code == InteractiveConstant.CLOSE_CONNECT) {
//                    return
//                }
//                tryConnect()
//            }
//
//            override fun onFailure(t: Throwable, response: Response?) {
//                Loggers.e("Socket", "onFailure:" + t.message + " --- " + (if (response != null) (response.body.toString()) else ""))
//                tryConnect()
//            }
//        })
////        mSocketMangager?.addSocketlistenner(ChatSupportFragment::class.java.canonicalName, listenner)
//        mSocketMangager?.listenner = listenner
//        val item = ServiceUtil.itemInteractiveConfig
//        if (SharedPreferencesManager.isTest(context) && !MyApplication.getInstance().isEmpty(SharedPreferencesManager.getPortChat(context))) {
//            port = SharedPreferencesManager.getPortChat(context) + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
//        } else if (item != null && !item.portSupport.isNullOrEmpty()) {
//            port = item.portSupport + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
//        } else {
//            port = mSocketMangager!!.url
//            if (port == null) {
//                port = PORT + "uid=" + itemUser!!.id + "&fullname=" + itemUser!!.fullname + "&access_token=" + itemUser!!.accessToken
//            }
//        }
//        mSocketMangager?.run(port)
    }

    var autoConnectCount: Int = 0
    private fun tryConnect() {
        if (autoConnectCount >= 0) {
            activity?.runOnUiThread {
                AppUtils.showDialog(
                    activity,
                    false,
                    false,
                    null,
                    "Không thể kết nối đến máy chủ. Vui lòng kiểm tra và kết nối lại!",
                    "Huỷ",
                    "Kết nối",
                    object : AppUtils.OnDialogButtonListener {
                        override fun onLeftButtonClick() {
//                        activity?.finish()
                        }

                        override fun onRightButtonClick() {
                            mSocketMangager?.reconnect()
                        }
                    })
            }
        } else {
            autoConnectCount++
            mSocketMangager?.reconnect()
        }
    }


    fun playVivration() {
        if (isDetached) {
            return
        }
        val v = requireContext().getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?
// Vibrate for 500 milliseconds
// Vibrate for 500 milliseconds
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            v?.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE))
        } else {
            //deprecated in API 26
            v?.vibrate(500)
        }
    }

    override fun onResume() {
        super.onResume()
      onShowDismissListener?.onShow()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        onShowDismissListener?.onDismiss()
    }
    var currentDialog: MyDialog? = null
    fun showEditGroup(): MyDialog? {
        if (isDetached || context == null || itemGroup == null) {
            return null
        }
        val dialog = MyDialog(requireContext())
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = WindowManager.LayoutParams()
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.CENTER
        lp.windowAnimations = R.style.DialogAnimationLeftRight
        dialog.window?.attributes = lp
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setCanceledOnTouchOutside(false)
        dialog.setContentView(R.layout.dialog_create_group_tt)
        val screenSize = ScreenSize(requireActivity())
        dialog.window?.setLayout(screenSize.width * 8 / 10, WindowManager.LayoutParams.WRAP_CONTENT)
        val layoutRoot: View = dialog.findViewById(R.id.layoutRoot)
        val editTextName: EditText = dialog.findViewById(R.id.editTextGroupName)
        val editTextAvatar: EditText = dialog.findViewById(R.id.editTextAvatar)
        val buttonSelectImage: ImageView = dialog.findViewById(R.id.buttonSelectImage)
        val buttonCreateGroup: TextView = dialog.findViewById(R.id.buttonCreateGroup)
        val buttonCancel: TextView = dialog.findViewById(R.id.buttonCancel)
        val textTitle: TextView = dialog.findViewById(R.id.textTitle)
        val imageAvatar: ImageView = dialog.findViewById(R.id.imageAvatar)
        val checkBoxConfirm: CheckBox = dialog.findViewById(R.id.checkBoxConfirm)
        //        View viewDivider = dialog.findViewById(R.id.viewDivider);
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
        textTitle.text = "Cài đặt nhóm"
        buttonCreateGroup.text = requireContext().resources.getString(R.string.update)
        editTextName.setText(itemGroup!!.name!!)
        MyApplication.getInstance()
            .loadImageNoPlaceHolder(requireContext(), imageAvatar, itemGroup!!.avatar)
        checkBoxConfirm.isChecked = "1".equals(itemGroup!!.autoConfirm)
        buttonSelectImage.setOnClickListener {
            showChooseImagePopup()
        }
        buttonCancel.setOnClickListener {
            dialog.dismiss()
        }
        buttonCreateGroup.setOnClickListener {
            val name = editTextName.text.toString().trim()
            if (name.length < 3) {
                Toast.makeText(
                    requireContext(),
                    "Tên nhóm phải lớn hơn 3 ký tự",
                    Toast.LENGTH_SHORT
                ).show()
                editTextName.requestFocus()
                return@setOnClickListener
            }
            val isAutoConfirm = checkBoxConfirm.isChecked
            requestUpdateGroup(name, isAutoConfirm)
        }

        currentDialog?.dismiss()
        currentDialog = dialog
        try {
            dialog.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    fun requestUpdateGroup(nameGroup: String?, isAutoConfirm: Boolean? = false) {
        if (!MyApplication.getInstance().isNetworkConnect || nameGroup.isNullOrEmpty()) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        requestParams.put("name", nameGroup)
        requestParams.put("userid", "${itemUser!!.id}")
        requestParams.put("auto_confirm", if (isAutoConfirm!!) "1" else "0")
        requestParams.put("groupid", "${itemGroup!!.id}")
        if (photoFile != null) {
            if (photoFile!!.exists()) {
                requestParams.put("avatar_file", photoFile.toString())
                requestParams.put("typedata", myHttpRequest!!.getMimeType(photoFile))
                requestParams.put("typeext", myHttpRequest!!.getExtension(photoFile!!.name))
            }
        }
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_UPDATE)
        myHttpRequest!!.request(true, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        activity?.getString(R.string.msg_empty_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString.trim())
                if (jsonObject == null) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    message?.let {
                        activity?.runOnUiThread {
                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                            currentDialog?.dismiss()
                        }
                    }
                    return
                }

                val result = JsonParser.getString(jsonObject, Constant.RESULT)
                result?.let {
                    activity?.runOnUiThread {
                        currentDialog?.dismiss()
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    }
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    var popupMenuChooseImage: PowerMenu? = null
    fun showChooseImagePopup() {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(getString(R.string.select_from_gallery)))
        itemList.add(PowerMenuItem(getString(R.string.select_from_camera)))

        val screenSize = ScreenSize(requireActivity())
        val popupWidth = screenSize.width * 2 / 3

        val textView = TextView(activity)
        textView.textSize = 17f
        textView.gravity = Gravity.CENTER
        textView.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.color_primary_dark_tt
            )
        )
        textView.text = activity?.getString(R.string.send_image_title) ?: ""
        textView.setPadding(25, 15, 15, 15)
        textView.setBackgroundColor(Color.parseColor("#f8f8f8"))
        textView.typeface = Typeface.create("sans-serif-medium", Typeface.BOLD)

        popupMenuChooseImage = PowerMenu.Builder(requireContext())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOW_UP_CENTER)
            .setShowBackground(true)
            .setWidth(popupWidth)
//                .setHeight(popupHeight)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireContext(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireContext(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireContext(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    onMenuItemChooseImageClickListener(position, item?.title.toString())
                    popupMenuChooseImage!!.dismiss()
                }
            })
            .setHeaderView(textView)
            .build()
        popupMenuChooseImage!!.showAtCenter(currentDialog!!.findViewById(R.id.buttonSelectImage))
    }

    fun onMenuItemChooseImageClickListener(position: Int, title: String) {
        if (title.equals(getString(R.string.select_from_gallery))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    Constant.REQUEST_CODE_GALLERY
                )
                return
            }
            openGallery()
            return
        }
        if (title.equals(getString(R.string.select_from_camera))) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(
                    arrayOf(Manifest.permission.CAMERA),
                    Constant.REQUEST_CODE_CAMERA
                )
                return
            }
            openCamera()
        }
    }

    fun openGallery() {
//        val intent = Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
//        intent.putExtra(Intent.EXTRA_MIME_TYPES, arrayOf("image/*"))
        startActivityForResult(intent, Constant.REQUEST_CODE_GALLERY)
    }

    private fun createImageFile(): File? {
        val fileName = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        if (storageDir == null) {
            return null
        }
        return File.createTempFile(fileName, ".jpg", storageDir)
    }

    var photoFile: File? = null
    fun openCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (intent.resolveActivity(requireActivity().packageManager) != null) {
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(
                    requireActivity(), requireActivity().packageName + ".provider",
                    photoFile
                )
                this.photoFile = photoFile
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(intent, Constant.REQUEST_CODE_CAMERA)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY, Constant.REQUEST_CODE_CAMERA -> {
                for (permission in permissions) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            requireActivity(),
                            permission
                        )
                    ) {
                        (activity as BaseActivity).showDialog(getString(R.string.msg_accept_permission))
                        break
                    } else {
                        if (ActivityCompat.checkSelfPermission(
                                requireActivity(),
                                permission
                            ) == PackageManager.PERMISSION_GRANTED
                        ) {
                            if (requestCode == Constant.REQUEST_CODE_GALLERY) {
                                openGallery()
                            } else if (requestCode == Constant.REQUEST_CODE_CAMERA) {
                                openCamera()
                            }
                        } else {
                            callPermissionSettings(requestCode)
                        }
                    }
                }
            }
        }
    }

    fun callPermissionSettings(requestCode: Int) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        val uri = Uri.fromParts("package", requireActivity().packageName, null)
        intent.data = uri
        startActivityForResult(intent, requestCode)
    }

    var fileCompressor: FileCompressor? = null
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            Constant.REQUEST_CODE_GALLERY -> {
                if (data == null) return
                data.let {
                    val selectedImage = data.data
                    if (selectedImage == null) {
                        return
                    }
                    if (fileCompressor == null) {
                        fileCompressor = FileCompressor(requireActivity())
                    }
                    val realPath =
                        fileCompressor!!.getRealPathFromUri(requireActivity(), selectedImage)
                    if (realPath.isNullOrEmpty()) {
                        return
                    }
                    photoFile = fileCompressor!!.compressToFile(File(realPath))
                    if (currentDialog != null && currentDialog!!.isShowing) {
                        MyApplication.getInstance()?.loadImage(
                            requireActivity(),
                            currentDialog!!.findViewById(R.id.imageAvatar),
                            photoFile,
                            false
                        )
                    }
                }
            }
            Constant.REQUEST_CODE_CAMERA -> {
                if (photoFile == null) {
                    return
                }
                if (fileCompressor == null) {
                    fileCompressor = FileCompressor(requireActivity())
                }
                photoFile = fileCompressor!!.compressToFile(photoFile)
                if (currentDialog != null && currentDialog!!.isShowing) {
                    MyApplication.getInstance()?.loadImage(
                        requireActivity(),
                        currentDialog!!.findViewById(R.id.imageAvatar),
                        photoFile,
                        false
                    )
                }
            }
        }
    }

    fun requestConfirmMenber(item: ItemMenber?, pos: Int) {
        if (!MyApplication.getInstance().isNetworkConnect || item == null) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        }
        val requestParams = RequestParams()
        requestParams.put("userid", "${item.userId}")
        requestParams.put("groupid", "${item.groupId}")
        requestParams.put("id", "${item.id}")
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_CONFIRM_MENBER)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread {
                    Toast.makeText(
                        requireContext(),
                        activity?.getString(R.string.msg_empty_data),
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                if (responseString.isNullOrEmpty()) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val jsonObject = JsonParser.getJsonObject(responseString.trim())
                if (jsonObject == null) {
                    activity?.runOnUiThread {
                        Toast.makeText(
                            requireContext(),
                            activity?.getString(R.string.msg_empty_data),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    message?.let {
                        activity?.runOnUiThread {
                            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                        }
                    }
                    return
                }
                val result = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
                val mess = JsonParser.getString(result, "mess")
                activity?.runOnUiThread {
                    refreshData()
                    mess?.let {
                        Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
                    }
                }
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }
}