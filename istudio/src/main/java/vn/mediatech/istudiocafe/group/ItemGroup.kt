package vn.mediatech.istudiocafe.group

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//<<<<<<< HEAD
class ItemGroup(@SerializedName("id") val id: String?,
                @SerializedName("name") val name: String?,
                @SerializedName("avatar") val avatar: String?,
                @SerializedName("userid") val adminId: String?,
                @SerializedName("joined") var joined: String?,
                @SerializedName("status") val status: Int?,
                @SerializedName("total") val totalMenber: Int = 0,
                @SerializedName("number_online") val onlineMenber: Int = 0,
//                @SerializedName("type") val type: String?,
//                @SerializedName("mess") var message: String?,
//                @SerializedName("image") val image: String?,
//                @SerializedName("date") val date: String?,
//                @SerializedName("voucher_name") var voucherName: String?,
                @SerializedName("count") var count: Int?,
                @SerializedName("auto_confirm") var autoConfirm: String?

) : Parcelable
//=======
/*
"id": "29",
                "name": "Hội đồng hương Nghệ An & Hà Tịnh",
                "status": "0",
                "avatar": "https:\/\/daugiatruyenhinh.com\/upload\/avatar\/group\/taptran_etux_thumb_thumb.jpg",
                "userid": "84",
                "created_at": "2021-04-28 15:54:12",
                "updated_at": "2021-04-28 15:54:12",
                "total": 0,
                "number_online": 0,
                "joined": "Tham gia"*/
