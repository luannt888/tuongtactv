package vn.mediatech.istudiocafe.group

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_menber_tt.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication

class ItemMenberAdapter(val context: Context, val list: ArrayList<ItemMenber>, val isAdmin: Boolean) : Adapter<ItemMenberAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_menber_tt,
                parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val itemObj: ItemMenber = list.get(position)
        MyApplication.getInstance().loadImageCircle(context, holder.imgAvatar, itemObj.avatar, R.drawable.ic_avatar)
        holder.textName.setText(itemObj.name)
        holder.textTime.setText(itemObj.timeOnline)
        if (Constant.GROUP_ONLINE.equals(itemObj.state)) {
            holder.textStatus.text = itemObj.state
            holder.textStatus.setTextColor(Color.parseColor("#FFFFFF"))
            holder.iconStatus.isSelected = false
        } else {
            holder.iconStatus.isSelected = true
            holder.textStatus.text = Constant.GROUP_OFFLINE
            if (!itemObj.state.isNullOrEmpty()) {
                holder.textStatus.text = itemObj.state
            }
        }
        if (Constant.GROUP_LEVEL_ADMIN.equals(itemObj.level)) {
            holder.textAdmin.visibility = View.VISIBLE
            if (!itemObj.levelName.isNullOrEmpty()) {
                holder.textAdmin.text = itemObj.levelName
            }
        } else {
            holder.textAdmin.visibility = View.GONE
        }
        holder.buttonChat.setOnClickListener {
            onItemClickListener?.onClickChat(itemObj, position)
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
        }
        holder.layoutRoot.setOnLongClickListener {
            onItemClickListener?.onLongClick(itemObj, position)
            true
        }
//        if (Constant.GROUP_CONFIRMED.equals(itemObj.status)) {
//            holder.buttonConfirm.visibility = View.GONE
//            holder.layoutOnlineMenber.visibility = View.VISIBLE
//            holder.buttonChat.visibility = View.VISIBLE
//
//        } else {
//            holder.buttonConfirm.visibility = View.VISIBLE
//            holder.layoutOnlineMenber.visibility = View.GONE
//            holder.buttonChat.visibility = View.GONE
        if (isAdmin) {
            holder.textAdmin.visibility = View.GONE
            holder.buttonConfirm.visibility = View.VISIBLE
            holder.layoutOnlineMenber.visibility = View.GONE
            holder.buttonChat.visibility = View.GONE
            holder.buttonConfirm.text = "Duyệt"
            holder.buttonConfirm.setOnClickListener {
                onItemClickListener?.onClickConfirm(itemObj, position)
            }
        } else {
            holder.buttonConfirm.visibility = View.GONE
            holder.layoutOnlineMenber.visibility = View.VISIBLE
            holder.buttonChat.visibility = View.VISIBLE
        }
//            else {
//                holder.buttonConfirm.text = "Chờ Duyệt"
//            }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textTime = itemView.textTime
        val iconStatus = itemView.iconStatus
        val textName = itemView.textName
        val buttonChat = itemView.buttonChat
        val buttonConfirm = itemView.buttonConfirm
        val textStatus = itemView.textStatus
        val textAdmin = itemView.textAdmin
        val layoutOnlineMenber = itemView.layoutOnlineMenber
    }

    interface OnItemClickListener {
        fun onClick(item: ItemMenber, position: Int)
        fun onClickChat(item: ItemMenber, position: Int)
        fun onClickConfirm(item: ItemMenber, position: Int)
        fun onLongClick(item: ItemMenber, position: Int)
    }

}