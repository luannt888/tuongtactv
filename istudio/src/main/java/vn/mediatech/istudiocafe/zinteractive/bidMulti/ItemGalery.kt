package vn.mediatech.interactive.bidMulti

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemGalery(
        @SerializedName("id") val id: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("price") val price: String?,
        @SerializedName("src") val src: String?,
) : Parcelable {
}
/*
{
      "countdowntext": "00:00",
      "id": 1648,
      "name": "Loa Bluetooth Xiaomi Square Box Gen 2",
      "price_bid": 0,
      "image": "95b9cfb7d0fd52580c0f209c23aff278.jpg",
      "namerewrite": null,
      "parent_id": "0",
      "creator": "mediatechshop",
      "created_at": "2021-01-27 09:32:57",
      "status": 1,
      "hot": "1",
      "sort": 2,
      "price_start": 0,
      "price_end": 250000,
      "description": "",
      "price_step": 25000,
      "discount_quantity": 1,
      "price_sale": 449000,
      "price_sale_discount": 250000,
      "brand_name": "ami",
      "brand_image": "1648_ami.png",
      "brand_about": "mi",
      "src": "https://daugiatruyenhinh.com/upload/product/95b9cfb7d0fd52580c0f209c23aff278.jpg",
      "brand_logo": "https://daugiatruyenhinh.com/upload/istudios/brands/1648_ami.png",
      "gallery": [
        "\\\\demo-pc\\MPI.SHARE\\DOWNLOAD\\IMG_BID\\d4916a494b48860bf9db58a677934cd8.jpg",
        "\\\\demo-pc\\MPI.SHARE\\DOWNLOAD\\IMG_BID\\22850bb7858e113913f2ba20093a8a69.jpg",
        "\\\\demo-pc\\MPI.SHARE\\DOWNLOAD\\IMG_BID\\ea0ef949858467419c00af5d63a392c9.jpg"
      ],
      "gallery_image": [
        "{\r\n  \"id\": 4730,\r\n  \"name\": \"Loa Bluetooth Xiaomi Square Box Gen 2\",\r\n  \"image\": \"d4916a494b48860bf9db58a677934cd8.jpg\",\r\n  \"src\": \"https://daugiatruyenhinh.com/upload/gallery/thumb/d4916a494b48860bf9db58a677934cd8.jpg\"\r\n}",
        "{\r\n  \"id\": 4731,\r\n  \"name\": \"Loa Bluetooth Xiaomi Square Box Gen 2\",\r\n  \"image\": \"22850bb7858e113913f2ba20093a8a69.jpg\",\r\n  \"src\": \"https://daugiatruyenhinh.com/upload/gallery/thumb/22850bb7858e113913f2ba20093a8a69.jpg\"\r\n}",
        "{\r\n  \"id\": 4732,\r\n  \"name\": \"Loa Bluetooth Xiaomi Square Box Gen 2\",\r\n  \"image\": \"ea0ef949858467419c00af5d63a392c9.jpg\",\r\n  \"src\": \"https://daugiatruyenhinh.com/upload/gallery/thumb/ea0ef949858467419c00af5d63a392c9.jpg\"\r\n}"
      ],
      "starttime": "2021-01-29T09:32:34.4440085+07:00",
      "endtime": "2021-01-29T09:33:05.4440085+07:00",
      "curenttime": "0001-01-01T00:00:00",
      "note": null,
      "countdown": 0,
      "local": 1,
      "DependencyObjectType": {
        "Id": 218,
        "SystemType": "CONTROLS.Model.Bid_Product, CONTROLS.MPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
        "BaseType": {
          "Id": 70,
          "SystemType": "CONTROLS.Class.NotifyPropertyChangedBase, CONTROLS.MPI, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null",
          "BaseType": {
            "Id": 0,
            "SystemType": "System.Windows.DependencyObject, WindowsBase, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35",
            "BaseType": null,
            "Name": "DependencyObject"
          },
          "Name": "NotifyPropertyChangedBase"
        },
        "Name": "Bid_Product"
      }*/
