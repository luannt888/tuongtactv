package vn.mediatech.interactive.iQ

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemIQAnswer(
        val title: String?,
        var answer: String?,
        var image: String?,
        var countSelected: String?,
        var percent: String?
) : Parcelable