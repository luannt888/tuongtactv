package vn.mediatech.interactive.listener

interface OnShowDismissListener {
    fun onShow()
    fun onDismiss()
}