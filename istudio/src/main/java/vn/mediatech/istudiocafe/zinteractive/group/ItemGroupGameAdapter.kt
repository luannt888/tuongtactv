package vn.mediatech.interactive.group

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_group_game.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils

class ItemGroupGameAdapter(val context: Context, val list: ArrayList<ItemGroupGame>, val isShowOnline: Boolean) : Adapter<ItemGroupGameAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_group_game,
                parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val itemObjGame: ItemGroupGame = list.get(position)
        AppUtils.loadImageCircle(context, holder.imgAvatar, itemObjGame.avatar, R.drawable.ic_avatar_interactive)
        holder.textName.setText(itemObjGame.name)
//        holder.textTotalMenber.setText("Có ${itemObjGame.menberCount} thành viên")

        holder.textOnlineMenber.setText("${itemObjGame.menberCount} thành viên " + (if (isShowOnline) "online" else ""))
        holder.textIndex.setText("${itemObjGame.index}")
        holder.buttonJoin.setOnClickListener {
            onItemClickListener?.onClickJoin(itemObjGame, position)
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObjGame, position)
        }
        holder.layoutRoot.setOnLongClickListener {
            onItemClickListener?.onLongClick(itemObjGame, position)
            true
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textTotalMenber = itemView.textTotalMenber
        val textOnlineMenber = itemView.textOnlineMenber
        val textName = itemView.textName
        val buttonJoin = itemView.buttonJoin
        val imgStatus = itemView.imgStatus
        val textIndex = itemView.textIndex

        init {
            imgStatus.visibility = if (isShowOnline!!) View.VISIBLE else View.GONE
        }
    }

    interface OnItemClickListener {
        fun onClick(itemGame: ItemGroupGame, position: Int)
        fun onClickJoin(itemGame: ItemGroupGame, position: Int)
        fun onLongClick(itemGame: ItemGroupGame, position: Int)
    }

}