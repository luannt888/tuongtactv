package vn.mediatech.interactive.bidFree

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemBidProduct(
        @SerializedName("id") val id: String?,
        @SerializedName("name") val name: String?,
//        @SerializedName("desc") val desc: String?,
        @SerializedName("price") val price: String?,
        @SerializedName("price_sale") val priceSale: String?,
//        @SerializedName("price_discount") val priceDiscount: String?,
        @SerializedName("price_bid_startup") val priceBidStartup: String?,
        @SerializedName("price_bid_step") val priceBidStep: String?,
        @SerializedName("price_bid_percent") val priceBidPercent: String?,
//        @SerializedName("start_discount") val startDiscount: String?,
//        @SerializedName("end_discount") val endDiscount: String?,
        @SerializedName("image") val image: String?,
        @SerializedName("image_url") val imageUrl: String?,
        @SerializedName("status") val status: String?,
        @SerializedName("note") val note: String?,
//        @SerializedName("template_style") val templateStyle: String?,
        @SerializedName("price_bid") var priceBid: String?,
        @SerializedName("StartTime") val startTime: String?,
        @SerializedName("CurentTime") val curentTime: String?,
        @SerializedName("EndTime") var endTime: String?,
//        @SerializedName("EndGame_Status") val endGameStatus: String?,
//        @SerializedName("Template_Local") val templateLocal: String?,
        @SerializedName("CountDown") var countDown: Long?,
        @SerializedName("Player_Message") var playerMessage: String?,
        var msg: String?,
        var dataSend: String?,
        var endTimeMs: Long?,
        var startTimeMs: Long?,
        var isBid: Boolean = false,
        var isWin: Boolean = false,
        @SerializedName("gallery_url") val galleryUrl: Array<String>?
) : Parcelable {
}
/*
"id":-1,
"name":"Xịt khoáng Sukin",
"desc":"Sukin là dòng mỹ phẩm từ thiên nhiên được bán chạy nhất tại Úc năm 2019.",
"price":168000,
"price_sale":240000,
"price_discount":168000,
"price_bid_startup":0,
"price_bid_step":5000,
"price_bid_percent":50,
"start_discount":"2020-12-14",
"end_discount":"2020-12-19",
"image":"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\PRODUCT_DATA\\1611.Png",
"image_url":"http:\/\/daugiatruyenhinh.com\/upload\/product\/1611_kem_duong_da.jpg",
"status":0,
"note":"free",
"template_style":"",
"price_bid":0,
"StartTime":"0001-01-01T00:00:00",
"EndTime":"0001-01-01T00:00:00",
"EndGame_Status":0,
"Template_Local":0*/

/*
"id":-1,
"name":"Kem tẩy da chết Sukin",
"desc":"Sukin là dòng mỹ phẩm từ thiên nhiên được bán chạy nhất tại Úc năm 2019.",
"price":124000,
"price_sale":180000,
"price_discount":124000,
"price_bid_startup":0,
"price_bid_step":5000,
"price_bid_percent":50,
"start_discount":"2020-12-14",
"end_discount":"2020-12-19",
"image":"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\PRODUCT_DATA\\1608.Png",
"image_url":"http://daugiatruyenhinh.com/upload/product/1608_kem_duong_da.jpg",
"status":0,
"note":"free",
"CountDown":0,
"template_style":null,
"price_bid":124000,
"StartTime":"2021-01-20T14:19:56.8574223+07:00",
"EndTime":"0001-01-01T00:00:00",
"CurentTime":"2021-01-20T14:19:56.8574223+07:00",
"EndGame_Status":0,
"Template_Local":0,
"gallery":[
"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\PRODUCT_DATA\\1608-1.Png",
"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\PRODUCT_DATA\\1608-2.Png",
"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\PRODUCT_DATA\\1608-3.Png",
"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\PRODUCT_DATA\\1608-4.Png"
],
"gallery_url":[
"http://daugiatruyenhinh.com/upload/gallery/large/a6ddd356ba1c7a7228cd87e3d927583c.jpg",
"http://daugiatruyenhinh.com/upload/gallery/large/f93c2e7fd30b7307982ebdb5d6b1d3e7.jpg",
"http://daugiatruyenhinh.com/upload/gallery/large/448c443ea4c5fe8364fe9f5b04813eec.jpg",
"http://daugiatruyenhinh.com/upload/gallery/large/4936d842ba736b6290a2cb061760a8e5.jpg"
],
"store_avatar":"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\PRODUCT_DATA\\1608-store.Png",
"store_avatar_url":"http://daugiatruyenhinh.com/upload/store/avatar/c8a703fcc8a6c92b526b81c0f4234842.jpg",
"condition":{
    "data":[
    {
        "Index":0,
        "Quantity":0,
        "Count":4,
        "Title":"Từ 0 -> 20",
        "Content":"1/20"
    },
    {
        "Index":1,
        "Quantity":1,
        "Count":5,
        "Title":"Từ 0 -> 20",
        "Content":"1/20"
    },
    {
        "Index":2,
        "Quantity":2,
        "Count":10,
        "Title":"Từ 0 -> 20",
        "Content":"1/20"
    },
    {
        "Index":3,
        "Quantity":3,
        "Count":15,
        "Title":"Từ 0 -> 20",
        "Content":"1/20"
    }
    ],
    "text":[
    "SỐ LƯỢT ĐẤU: < 05 SỐ LƯỢNG: 0 GIẢI",
    "SỐ LƯỢT ĐẤU: 05 → 09 SỐ LƯỢNG: 1 GIẢI",
    "SỐ LƯỢT ĐẤU: 10 → 14 SỐ LƯỢNG: 2 GIẢI",
    "SỐ LƯỢT ĐẤU: => 15 SỐ LƯỢNG: 3 GIẢI"
    ]*/
