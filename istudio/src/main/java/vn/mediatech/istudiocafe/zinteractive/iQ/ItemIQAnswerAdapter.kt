package vn.mediatech.interactive.iQ

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_iq_answer.view.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.R
import java.util.*
import kotlin.concurrent.timerTask

class ItemIQAnswerAdapter(
        val context: Context, val itemList: ArrayList<ItemIQAnswer>, val type: Int, val style: Int, val
        numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var indexSelect: Int = -1
    var selectable: Boolean = true
    var indexTrue: Int = -1
    var holderTrue: FrameViewHolder? = null

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_iq_answer,
                parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemIQAnswer = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemIQAnswer, position: Int) {
        AppUtils.loadImage(context, holder.imgAnswer, itemObj.image)
        holder.textTitleAnswer.text = itemObj.title
        holder.textAnswer.text = itemObj.answer
        if (!itemObj.countSelected.isNullOrEmpty()) {
            holder.textCountSelected.text = itemObj.countSelected
            holder.textCountSelected.visibility = View.VISIBLE
        } else {
            holder.textCountSelected.visibility = View.GONE
        }
        if (!itemObj.image.isNullOrEmpty()) {
            AppUtils.loadImage(context, holder.imgAnswer, itemObj.image)
            holder.imgAnswer.visibility = View.VISIBLE
        } else {
            holder.imgAnswer.visibility = View.GONE
        }
        if (!itemObj.percent.isNullOrEmpty()) {
            val percent = try {
                itemObj.percent!!.trim().toFloat()
            } catch (e: Exception) {
                0F
            }
            holder.progress.progress = percent
        }
        if (indexSelect >= 0) {
//            holder.layoutRoot.isClickable = false
//            holder.layoutRoot.isEnabled = false
        }
        if (indexSelect == position) {
            holder.layoutRoot.isSelected = true
        }
        if (indexTrue >= 0) {
            holder.layoutRoot.isClickable = false
            holder.layoutRoot.isEnabled = false
            if (indexTrue == position) {
                holderTrue = holder
            }
        }

        holder.layoutRoot.setOnClickListener {
//            if (indexSelect == -1) {
//                indexSelect = position
//                notifyDataSetChanged()
//            }
            onItemClickListener?.onClick(itemObj, position,false)
        }
        holder.viewTrue.isActivated = false
    }

    fun setCancelSelectable(selectable: Boolean) {
        this.selectable = selectable
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAnswer = itemView.imgAnswer
        val textAnswer = itemView.textAnswer
        val textTitleAnswer = itemView.textTitleAnswer
        val textCountSelected = itemView.textCountSelected
        val progress = itemView.progress
        val viewTrue = itemView.viewTrue

        init {
        }
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemIQAnswer?, position: Int, isTimeOut:Boolean)
    }

    fun setSelected(index: Int) {
        indexSelect = index
        notifyDataSetChanged()
    }

    fun startEffectTrue() {
        var count: Int = 8
        if (indexTrue >= 0) {
            var timer = Timer()
            timer.schedule(timerTask {
                if (count > 0) {
//                    holderTrue?.layoutRoot?.isActivated = !holderTrue!!.layoutRoot!!.isActivated
                    (context as Activity).runOnUiThread {
                        holderTrue?.viewTrue?.isActivated = !holderTrue!!.viewTrue!!.isActivated
                    }
                    count = count - 1
                } else {
                    (context as Activity).runOnUiThread {
                        holderTrue?.viewTrue?.isActivated = true
                    }
                    timer.cancel()
                }
            }, 0, 100L)
        }
    }
}