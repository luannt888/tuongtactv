package vn.mediatech.istudiocafe.zinteractive.emotion

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//<<<<<<< HEAD
class ItemEmotion(
        @SerializedName("id") var id: String?,
        @SerializedName("user_id") val userId: Int?,
        @SerializedName("name") val name: String?,
        @SerializedName("png") val png: String?,
        @SerializedName("image") val image: String?,
        @SerializedName("img_png") val imageFrame: String?,
        @SerializedName("type") val type: String?,
        @SerializedName("sprite") val sprite: ItemSprite?,
        @SerializedName("seq") val seq: ItemSeq?,
) : Parcelable
//=======
/*
{
"id": "49",
"name": "Dev",
"image": "https://daugiatruyenhinh.com/upload/istudios/emoji/49_ca0260fd2536d2688b27.jpg.png",
"type": "heart",
"seq": {
"seq_start": "1",
"seq_stop": "2",
"link": "https://daugiatruyenhinh.com/upload/istudios/emoji/49_ca0260fd2536d2688b27.jpg.png"
},
"sprite": {
"sprite_long": "12",
"sprite_wide": "25",
"link": "https://daugiatruyenhinh.com/upload/istudios/emoji/49_ca0260fd2536d2688b27.jpg.png"
}
},*/
