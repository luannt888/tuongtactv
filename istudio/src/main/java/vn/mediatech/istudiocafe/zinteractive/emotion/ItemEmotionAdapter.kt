package vn.mediatech.istudiocafe.zinteractive.emotion

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.content.Context
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_emotion.view.*
import vn.mediatech.istudiocafe.R
import java.util.logging.Handler

class ItemEmotionAdapter(
    val context: Context,
    val list: ArrayList<ItemEmotion>, val spacePx: Int, val layoutHeight: Int, val layoutWidth: Int
) : Adapter<ItemEmotionAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var indexSelect = -1
    var currentHolder: MyViewHolder? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_emotion,
            parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val itemObjGame = list.get(position)
        holder.layoutRoot.isSelected = position == indexSelect
        try {
            holder.spriteView.columns =
                itemObjGame.sprite!!.imageWidth!! / itemObjGame.sprite.framewidth!!
            holder.spriteView.fps = itemObjGame.sprite.fps!!
            holder.textName.setText(itemObjGame.name)
//            holder.textName.visibility = View.GONE
            holder.spriteView.isFitXY = false
            holder.spriteView.start(itemObjGame.sprite.link)
//            holder.spriteView.start(itemObjGame.png)
        } catch (e: Exception) {
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObjGame, position)
            currentHolder?.layoutRoot?.isSelected = false
            indexSelect = position
            currentHolder = holder
            holder.layoutRoot.isSelected = true
            startEffect(holder.spriteView)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val spriteView = itemView.spriteView
        val textName = itemView.textName

        init {
            var param = layoutRoot.layoutParams as RecyclerView.LayoutParams
            if (layoutHeight > 0 && layoutWidth > 0) {
                param.height = layoutHeight
                param.width = layoutWidth
            }
//            param.leftMargin = spacePx
//            param.topMargin = 0
//            param.bottomMargin = 0
            layoutRoot?.layoutParams = param
        }
    }

    interface OnItemClickListener {
        fun onClick(itemGame: ItemEmotion, position: Int)
    }
    var scaleDown: ObjectAnimator? = null
    fun startEffect(view: View){
//        scaleDown?.cancel()
//         scaleDown = ObjectAnimator.ofPropertyValuesHolder(
//            view,
//            PropertyValuesHolder.ofFloat("scaleX", 1.2f),
//            PropertyValuesHolder.ofFloat("scaleY", 1.2f)
//        )
//        scaleDown?.duration = 300
//        scaleDown?.repeatCount = ObjectAnimator.INFINITE
//        scaleDown?.repeatMode = ObjectAnimator.REVERSE
//        scaleDown?.start()
    }
}