package vn.mediatech.interactive.git

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_gift.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.ScreenSize
import java.util.*

class ItemGitAdapter(
        val context: Context, val itemList: ArrayList<ItemGit>, val spacePx: Int, val style: Int, val
        numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0


    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        when (viewType) {
//            InteractiveConstant.STYLE_HORIZONTAL -> {
//                imageWidth = height * 2 / 3
//                imageHeight = height
//            }
        }
        val screenSize = ScreenSize(context as Activity)
        imageWidth = (screenSize.width - (spacePx * (numberColumn + 1))) / numberColumn
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_gift,
                parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemGit = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemGit, position: Int) {
        AppUtils.loadImage(context, holder.imgThumbnail, itemObj.src)
        holder.textChip.setText(itemObj.priceChip + " Chip")
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgThumbnail = itemView.imgAvata
        val textChip = itemView.textChip

        init {
            var param = ViewGroup.LayoutParams(imageWidth, ViewGroup.LayoutParams.WRAP_CONTENT)
            layoutRoot?.layoutParams = param
        }
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemGit, position: Int)
    }


}