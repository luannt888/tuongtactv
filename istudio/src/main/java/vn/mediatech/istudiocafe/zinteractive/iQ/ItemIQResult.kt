package vn.mediatech.interactive.iQ

import com.google.gson.annotations.SerializedName

class ItemIQResult(
        @SerializedName("chooseReply") val chooseReply: String?,
        @SerializedName("A") val A: String?,
        @SerializedName("B") val B: String?,
        @SerializedName("C") val C: String?,
        @SerializedName("D") val D: String?,
        @SerializedName("PerA") val perA: String?,
        @SerializedName("PerB") val perB: String?,
        @SerializedName("PerC") val perC: String?,
        @SerializedName("PerD") val perD: String?
//        @SerializedName("gallery_url") val galleryUrl: String?
)
/*
"chooseReply":"B",
"A":"00",
"B":"00",
"C":"00",
"D":"00",
"PerA":0,
"PerB":0,
"PerC":0,
"PerD":0,*/
