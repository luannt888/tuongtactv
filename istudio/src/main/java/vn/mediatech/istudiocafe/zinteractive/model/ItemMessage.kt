package vn.mediatech.interactive.model

import com.google.gson.annotations.SerializedName

class ItemMessage(
        @SerializedName("ID") val id: Long?,
        @SerializedName("user_id") val userId: String?,
        @SerializedName("user_fullname") val userFullname: String?,
        @SerializedName("user_avt") val avatar: String?,
        @SerializedName("service") val service: String?,
        @SerializedName("key") val key: String?,
        @SerializedName("data") val data: String?,
        @SerializedName("message") val message: String?,
        @SerializedName("status") val status: String?,
        @SerializedName("desc") val desc: String?,
        @SerializedName("last_updated") val lastUpdate: String?,
        @SerializedName("note") val note: String?
) {
}