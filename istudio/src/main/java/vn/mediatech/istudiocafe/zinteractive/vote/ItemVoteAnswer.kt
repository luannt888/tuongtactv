package vn.mediatech.interactive.vote

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemVoteAnswer(
        @SerializedName("ID") val id: String?,
        @SerializedName("Vote_ID") val voteID: String?,
        @SerializedName("Title") val title: String?,
        @SerializedName("Value") val value: String?,
        @SerializedName("Img") val img: String?,
        @SerializedName("Percent") val percent: Float?,
        @SerializedName("Change") val change: String?,
        @SerializedName("Ls_Player") val lsPlayer: String?,
        @SerializedName("Img_link") val imgLink: String?
) : Parcelable

   /* "ID":1,
    "Vote_ID":"134fc796b4f844d1a53641ba90cccc2a",
    "Title":"1 thùng bia Hà Nội",
    "Value":25,
    "Img":"\\\\DEMO-PC\\MPI.SHARE\\DOWNLOAD\\IMG_MESS\\vote2_1_cf24157223dd4c938cbd2aa7454b44ec.png",
    "Percent":43.86,
    "Change":1,
    "Ls_Player":"",
    "Img_link":"https://api.daugiatruyenhinh.com/Media/Vote/vote2_1_534f7d70c70b4aedb5b5ad3e1495e6b0.png"*/