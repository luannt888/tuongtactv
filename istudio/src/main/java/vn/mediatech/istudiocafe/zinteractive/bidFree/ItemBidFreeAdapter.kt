package vn.mediatech.interactive.bidFree

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_bid_zero.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.AppUtils.Companion.moneyFormat
import java.util.*

class ItemBidFreeAdapter(
        val context: Context, val itemList: ArrayList<ItemBidProduct>, val type: Int, val style: Int, val
        numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0


    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_bid_zero,
                parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemBidProduct = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemBidProduct, position: Int) {
            AppUtils.loadImage(context,holder.imgThumbnail,itemObj.imageUrl!!)
            holder.textTitle.text = itemObj.name
            holder.textPrice.text = "GIÁ THỊ TRƯỜNG " + moneyFormat(itemObj.price)
//        holder.layoutRoot.setOnClickListener {
//            onItemClickListener?.onClick(itemObj,position)
//        }
        holder.buttonBidZero.setOnClickListener {
            onItemClickListener?.onClick(itemObj,position)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgThumbnail = itemView.imgAvata
        val textTitle = itemView.textTitle
        val textPrice = itemView.textPriceInfo
        val buttonBidZero = itemView.buttonBid

        init {
        }
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemBidProduct, position: Int)
    }



}