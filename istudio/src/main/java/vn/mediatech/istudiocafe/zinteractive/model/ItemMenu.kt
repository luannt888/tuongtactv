package vn.mediatech.interactive.model

import com.google.gson.annotations.SerializedName

class ItemMenu(
        @SerializedName("ID") val id: String?,
        @SerializedName("Name") val name: String?,
        @SerializedName("Icon") val icon: String?,
        @SerializedName("Playing") var playing: Boolean?,
        @SerializedName("Msg") val msg: String?,
        @SerializedName("StartTime") val startTime: String?,
        @SerializedName("Duration") val duration: String?,
        @SerializedName("Status") val status: String?,
        @SerializedName("Change") val change: Boolean,
        @SerializedName("Desc") val desc: String?
)
/*
"ID":null,
"Name":"Voice",
"Icon":"https://api.daugiatruyenhinh.com/media/icon.png",
"Playing":false,
"Msg":"Tính năng Tương tác âm thanh vừa được kích hoạt.",
"StartTime":-62135596800,
"Duration":0,
"Status":1,
"Change":false,
"Desc":"VOICE"*/
