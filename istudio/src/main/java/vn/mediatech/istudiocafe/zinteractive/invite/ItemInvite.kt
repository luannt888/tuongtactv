package vn.mediatech.interactive.invite

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
//<<<<<<< HEAD
class ItemInvite(
        @SerializedName("Index") var index: Int?,
        @SerializedName("user_id") val userId: Int?,
        @SerializedName("full_name") val name: String?,
        @SerializedName("avatar") val avatar: String?,
        @SerializedName("count") val count: String?

) : Parcelable
//=======
/*
Index: 1,
Name: "Hội cựu chiến binh",
Avt: "https://daugiatruyenhinh.com/upload/avatar/group/ttnga_vmor.jpg",
MemberCount: 4,
MemberDetail: null,
Note: null,
Description: null*/
