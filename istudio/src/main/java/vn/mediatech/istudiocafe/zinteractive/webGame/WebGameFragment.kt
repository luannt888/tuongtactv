package vn.mediatech.istudiocafe.zinteractive.webGame

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.media.MediaFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnScrollChangedListener
import android.view.inputmethod.EditorInfo
import android.webkit.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_list_group.*
import kotlinx.android.synthetic.main.fragment_webgame_tt.*
import kotlinx.android.synthetic.main.fragment_webgame_tt.buttonBack
import kotlinx.android.synthetic.main.fragment_webgame_tt.editSearch
import kotlinx.android.synthetic.main.fragment_webgame_tt.layoutRefresh
import kotlinx.android.synthetic.main.fragment_webgame_tt.progressBar
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnCreateViewListener
import vn.mediatech.istudiocafe.listener.OnShowListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.SharedPreferencesManager
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import vn.mediatech.istudiocafe.zinteractive.bitmap2video.Muxer
import vn.mediatech.istudiocafe.zinteractive.bitmap2video.MuxerConfig
import vn.mediatech.istudiocafe.zinteractive.bitmap2video.MuxingCompletionListener
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.voicecontrol.voiceControl.VcUtils
import vn.mediatech.voicecontrol.voiceControl.VoiceControllerManager
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.concurrent.timerTask
import kotlin.random.Random


class WebGameFragment : BottomSheetDialogFragment() {
    //    private val REQUEST_SELECT_FILE = 1002
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isInit: Boolean = false
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var itemUser: ItemUser? = null
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var url: String? = null
    var game_name: String? = null

    //    val URL_DEFAUL = "http://tuongtac.tv/index.php"
//    var URL_DEFAUL = "http://210.211.126.222:3000/"
//    var URL_DEFAUL = "https://api.daugiatruyenhinh.com/html/flappy%20bird/index.html?"
    var URL_DEFAUL = "https://api.daugiatruyenhinh.com/html/tower_game/index.html?"
//    var URL_DEFAUL = "https://api.daugiatruyenhinh.com/html/html5-fruit-ninja/index.html?"

    //    var URL_DEFAUL = "https://api.daugiatruyenhinh.com/html/clumsy-bird-master/index.html"
    var isLogin = false
    var isOnscreen = false
    var onShowListener: OnShowListener? = null
    var onCreateViewListener: OnCreateViewListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WebView.enableSlowWholeDocumentDraw()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_webgame_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = false
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
            val params = requireView().layoutParams;
            (params as? CoordinatorLayout.LayoutParams)?.gravity = Gravity.CENTER_HORIZONTAL
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
        onShowListener?.onResume()
        isOnscreen = true
//        Handler(Looper.getMainLooper()).postDelayed({
//            startExportBitmap()
//        }, 500)
//        checkUrlGame()
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        layoutSearch?.visibility = View.GONE
    }

    var height: Int? = 0

    @SuppressLint("SetJavaScriptEnabled")
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        height = bundle?.getInt(InteractiveConstant.HEIGHT)
        try {
            if (height != null && height!! > 0) {
                val param: ViewGroup.LayoutParams? = layoutFragmentRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
            }
        } catch (e: Exception) {
        }
        try {
            url = bundle?.getString(InteractiveConstant.DATA)
            game_name = bundle?.getString(InteractiveConstant.NAME)

        } catch (e: Exception) {
//            dismiss()
        }
        if (url.isNullOrEmpty()) {
            url = URL_DEFAUL
        }
        initWebView()
    }


    private fun initControl() {
        buttonGo.setOnClickListener {
            loadUrl()
        }
        buttonBack.setOnClickListener {
            backUrl()
        }
        editSearch.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                (activity as? BaseActivity)?.hideKeyboard(editSearch)
                loadUrl()
                true
            } else false
        }
        layoutRefresh.setOnRefreshListener {
            layoutRefresh.isRefreshing = false
            webView?.reload()
        }
        btExport.setOnClickListener {
        }

        btStop.setOnClickListener {
            stopExport()
        }
        layoutRefresh?.getViewTreeObserver()?.addOnScrollChangedListener(OnScrollChangedListener {
            if (webView == null) {
                return@OnScrollChangedListener
            }
            if (webView.getScrollY() == 0) layoutRefresh.setEnabled(true) else layoutRefresh.setEnabled(
                false
            )
        })
    }

    private fun loadUrl() {
        loadUrl(editSearch.text.toString().trim())
    }

    private fun loadUrl(link: String) {
        var url = link
        val googleSearch = "https://www.google.com/search?q="
        if (!url.isEmpty()) {
            if (!url.startsWith("http")) {
                url = googleSearch + url
            }
            Loggers.e("WEBVIEW_URL", url)
            editSearch?.setText(url)
            webView?.loadUrl(url)
            webView?.requestFocus(View.FOCUS_UP)
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun initWebView() {
        webView.setInitialScale((height!! * 100 / 568).toInt());
        webView?.clearCache(true)
        webView?.clearHistory()
        val webSettings: WebSettings = webView.getSettings()
        webSettings.setSupportZoom(false)
        webSettings.builtInZoomControls = false
//        webSettings.setLoadWithOverviewMode(true);
//        webSettings.setUseWideViewPort(true);

        webSettings.setAppCacheEnabled(true)
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.javaScriptCanOpenWindowsAutomatically = true
        webSettings.setSupportMultipleWindows(true)
        webSettings.allowFileAccess = true
        webSettings.loadsImagesAutomatically = true
        webSettings.mediaPlaybackRequiresUserGesture = false
        webSettings.allowFileAccess = true
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
//        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
//        webView?.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        webSettings.setDomStorageEnabled(true);
//        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
//        webSettings.setSavePassword(true);
//        webSettings.setSaveFormData(true);
//        webSettings.setEnableSmoothTransition(true);

        CookieManager.getInstance().setAcceptCookie(true)
        CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true)
        //        String info = "<style>video, img{width: 100%; height: auto;} body{text-align:justify; padding: 0px; margin: 0px;}</style>" + itemObj.getInfo();
        //        String info = "<style>video, img{width: 100%; height: auto;} body{text-align:justify; padding: 0px; margin: 0px;}</style>" + itemObj.getInfo();
        webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                progressBar?.visibility = View.VISIBLE
                super.onPageStarted(view, url, favicon)
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?,
                request: WebResourceRequest?
            ): Boolean {
                if (request != null) {
                    Loggers.e("WEBVIEW_URL_OverrideUrlLoading", request!!.url.toString())
                    url = request!!.url.toString()
                    activity?.runOnUiThread {
                        editSearch?.setText(request!!.url.toString())
                    }
                }
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                progressBar?.visibility = View.GONE
                editSearch?.setText(url)
                super.onPageFinished(view, url)
            }
        }

        webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        webView.webChromeClient = MyWebChromeClient()

        webView.addJavascriptInterface(object : Any() {
            @JavascriptInterface
            fun start() {
                Loggers.e("SCORE_START", "START")
            }

            @JavascriptInterface
            fun score(score: String?) {
                Loggers.e("SCORE", "" + score)
                handleScore(score)
            }

            @JavascriptInterface
            fun died(score: String?) {
                Loggers.e("SCORE_DIED", "" + score)
                handleDied(score)
            }

        }, "Mobile")
//        webView.webChromeClient = WebChromeClient()
        isInit = true
        checkUrlGame()
    }

    var imageFilePath: String? = null
    var scoreCount: Int = 0
    private fun handleScore(score: String?) {
        scoreCount += 1
//        if (indexScore == -1) {
//            indexScore = indexBm
//        }
        startExportBitmap()
        activity?.runOnUiThread {
            val scoreInt: Int = try {
                score!!.toInt()
            } catch (e: Exception) {
                0
            }
            mScore = scoreInt
            val randomNumber = Random.nextInt(0, 5)
            if (randomNumber != 2) return@runOnUiThread
            var message: String? = ""
            if (scoreCount < 10) {
                message = VcUtils.getRandomString(
                    arrayOf(
                        "Chúc mừng bạn đã giành được $scoreInt điểm. Phát huy tiếp nhé",
                        "$scoreInt điểm rồi ,Bạn giỏi quá",
                        "Bạn đang làm rất tốt, tiếp tục phát huy nhé",
                        "Tăng tốc nào đối thủ đang dẫn trước bạn đó"
                    )
                )
            } else {
                message = VcUtils.getRandomString(
                    arrayOf(
                        "Bạn đã được $scoreInt điểm rồi. Cố lên",
                        "Chúc bạn sẽ giành được số điểm cao hơn mong đợi",
                        "Cố lên bạn ơi, chỉ còn 1 chút nữa thôi",
                        "Hi vọng bạn sẽ là người đứng đầu bảng xếp hạng hôm nay",
                        "Yayy bạn đã vượt qua một mốc rất khó",
                        "Bạn thật giỏi khi vượt qua chướng ngại vừa rồi",
                        "Thật không thể tin được đích đến đang ở trước mắt rồi"
                    )
                )

            }
            VoiceControllerManager.instance?.speak(
                activity,
                message,
                null,
                null,
                1.2F,
                1.2F
            )
        }
    }

    private fun handleDied(score: String?) {
        activity?.runOnUiThread {
            val scoreInt: Int = try {
                score!!.toInt()
            } catch (e: Exception) {
                0
            }
            mScore = scoreInt
            Handler(Looper.getMainLooper()).postDelayed({
                stopExport(false)},200)
            val imgName: String =
                if (itemUser != null && !itemUser!!.fullname.isNullOrEmpty()) itemUser!!.fullname!!.replace(
                    " ",
                    "_"
                ) else "user"
            imageFilePath = AppUtils.getImageFileView(context, "Img_${imgName}", getBitmap(webView))
            Handler(Looper.getMainLooper()).postDelayed({

                uploadScreenUser(mScore)
            }, 200)

            var message: String? = "Nhanh tay chơi lại thôi bạn ơi"
            if (scoreCount < 10) {
                message = VcUtils.getRandomString(
                    arrayOf(
                        "Không sao bạn ơi, nhanh tay để chơi lượt mới thôi",
                        "Thua ván này ta chơi ván khác đừng nản nhé bạn ơi",
                        "Cùng thử lại nào, chắc chắn lần này bạn sẽ làm được"
                    )
                )
            } else {
                message = VcUtils.getRandomString(
                    arrayOf(
                        "Đừng bỏ cuộc nhé, bạn sắp chiến thắng rồi",
                        "Hi vọng bạn sẽ là người đứng đầu bảng xếp hạng hôm nay",
                        "Nhanh tay chơi lại để được điểm cao hơn nữa bạn nhé",
                        "Không sao bạn ơi, bạn được đã đạt được $scoreInt điểm,Một số điểm khá cao, chơi thêm ván nữa nhé ",
                        "Ôi tiếc quá, chỉ còn 1 chút nữa thôi"
                    )
                )
            }
            VoiceControllerManager.instance?.speak(
                activity,
                message,
                null,
                null,
                1.2F,
                1.2F
            )
        }
        scoreCount = 0
        mScore = 0
    }

    var myFilePathCallback: ValueCallback<Array<Uri>>? = null
    fun backUrl(): Boolean {
        if (webView != null && webView.canGoBack()) {
            webView?.goBack()
            return true
        }
        return false
    }

    private fun getCustomHeaders(): Map<String, String>? {
        val headers: MutableMap<String, String> = HashMap()
        headers["YOURHEADER"] = "VALUE"
        return headers
    }


    override fun onPause() {
        super.onPause()
//        webView?.onPause()
//        webView?.pauseTimers()
        onShowListener?.onPause()
        isOnscreen = false
    }

    override fun onStop() {
        super.onStop()
        webView?.destroy()
        onShowListener?.onStop()
        timer?.cancel()
        timerRecord?.cancel()
    }

    fun checkUrlGame() {
        if (!isInit) {
            return
        }
        clearCookies(requireContext())
        webView?.clearCache(true)
        itemUser = MyApplication.getInstance().dataManager.itemUser
        if (itemUser != null && !url.isNullOrEmpty()) {
            if (!url!!.endsWith("?")) {
                url = url + "?"
            }
            var urlGame =
                url + "name=${itemUser!!.fullname}&id=${itemUser!!.id}&avt=${itemUser!!.avatar}"
            loadUrl(urlGame)
        } else {
            loadUrl("$url")
        }
    }

    private fun loadReset() {
        webView.clearCache(true)
        webView.clearHistory()
        clearCookies(context)
        loadUrl(url!!)
    }

    //    START: UPLOAD_FILE
    private var uploadMessage: ValueCallback<Array<Uri>>? = null
    private val REQUEST_SELECT_FILE = 3001
    private val REQUEST_PERMISSION_CAMERA_CODE = 2001
    private val REQUEST_PERMISSION_AUDIO_CODE = 2002
    private val TYPE_IMAGE = 1
    private val TYPE_VIDEO = 2
    private val TYPE_AUDIO = 3
    private var takeMediaCameraUri: Uri? = null

    inner class MyWebChromeClient : WebChromeClient() {
        override fun onPermissionRequest(request: PermissionRequest?) {
            request?.grant(request.resources)
            super.onPermissionRequest(request)
        }

        override fun onPermissionRequestCanceled(request: PermissionRequest?) {
            super.onPermissionRequestCanceled(request)
        }

        override fun onShowFileChooser(
            webView: WebView?,
            filePathCallback: ValueCallback<Array<Uri>>?,
            fileChooserParams: FileChooserParams?
        ): Boolean {
            if (fileChooserParams == null) {
                return true
            }
            val acceptTypeList = fileChooserParams.acceptTypes
            var intent = fileChooserParams.createIntent()
            for (acceptType in acceptTypeList) {
//                Loggers.e("onShowFileChooser", "acceptType = $acceptType")
                intent = checkIntentView(acceptType)
                if (intent != null) {
                    break
                }
            }
            if (intent == null) {
                return false
            }
            if (uploadMessage != null) {
                uploadMessage!!.onReceiveValue(null)
                uploadMessage = null
            }
            uploadMessage = filePathCallback
            try {
                startActivityForResult(intent, REQUEST_SELECT_FILE)
            } catch (e: Exception) {
                e.printStackTrace()
                uploadMessage = null
                (requireActivity() as? BaseActivity)?.showToast(R.string.msg_not_found_task)
                return false
            }
            return true
        }
    }

    fun checkIntentView(acceptType: String): Intent? {
        var mediaType = 0
        if (acceptType.contains("image")) {
            mediaType = TYPE_IMAGE
        } else if (acceptType.contains("camera")) {
            mediaType = TYPE_VIDEO
        } else if (acceptType.contains("audio")) {
            mediaType = TYPE_AUDIO
        }
        if (!checkPermission(mediaType)) {
            return null
        }
        var intent: Intent
        if (acceptType.contains("image")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "image/*"
            val takeCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takeMediaCameraUri = createMediaCameraUri(TYPE_IMAGE)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent =
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else if (acceptType.contains("camera")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "video/*"
            val takeCameraIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            takeMediaCameraUri = createMediaCameraUri(TYPE_VIDEO)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent =
                Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else if (acceptType.contains("audio")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "audio/*"
            val takeCameraIntent = Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION)
            takeMediaCameraUri = createMediaCameraUri(TYPE_AUDIO)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent = Intent()
            galleryIntent.type = "audio/*"
            galleryIntent.action = Intent.ACTION_GET_CONTENT
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "*/*"
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
        }
        return intent
    }

    private fun createMediaCameraUri(mediaType: Int): Uri? {
        val sdf = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH)
        var fileName = "_${sdf.format(Date())}"
        fileName = if (mediaType == TYPE_IMAGE) {
            "Img_$fileName.jpg"
        } else if (mediaType == TYPE_VIDEO) {
            "Video_$fileName.mp4"
        } else if (mediaType == TYPE_AUDIO) {
            "Audio_$fileName.m4a"
        } else {
            "Noname_$fileName"
        }
        val file = File(fileName)
        return Uri.fromFile(file)
    }

    private fun checkPermission(mediaType: Int): Boolean {
        val permissionWriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val permissionCamera = Manifest.permission.CAMERA
        val permissionRecordAudio = Manifest.permission.RECORD_AUDIO
        val permissionCameraList =
            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
        when (mediaType) {
            TYPE_IMAGE, TYPE_VIDEO -> {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionWriteStorage
                    ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionCamera
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(permissionCameraList, REQUEST_PERMISSION_CAMERA_CODE)
                    return false
                }
            }
            TYPE_AUDIO -> {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionRecordAudio
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(permissionCameraList, REQUEST_PERMISSION_AUDIO_CODE)
                    return false
                }
            }
            else -> {
                if (ContextCompat.checkSelfPermission(
                        requireContext(),
                        permissionWriteStorage
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    requestPermissions(permissionCameraList, REQUEST_PERMISSION_CAMERA_CODE)
                    return false
                }
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CAMERA_CODE || requestCode == REQUEST_PERMISSION_AUDIO_CODE) {
            if (!checkGrantPermission(grantResults, permissions as Array<String>)) {
                (activity as? BaseActivity)?.showToast(R.string.msg_require_accept_permission)
            }
        }
    }

    private fun checkGrantPermission(grantResults: IntArray, permissions: Array<String>): Boolean {
        for (i in grantResults.indices) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

//    END: UPLOAD_FILE

    @SuppressWarnings("deprecation")
    fun clearCookies(context: Context?) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null)
            CookieManager.getInstance().flush()
        } else if (context != null) {
            val cookieSyncManager = CookieSyncManager.createInstance(context)
            cookieSyncManager.startSync()
            val cookieManager: CookieManager = CookieManager.getInstance()
            cookieManager.removeAllCookie()
            cookieManager.removeSessionCookie()
            cookieSyncManager.stopSync()
            cookieSyncManager.sync()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SELECT_FILE) {
            if (uploadMessage == null) {
                return
            }
            val intent = data ?: Intent()
            if (takeMediaCameraUri != null && intent.data == null) {
                intent.data = takeMediaCameraUri
            }
            uploadMessage!!.onReceiveValue(
                WebChromeClient.FileChooserParams.parseResult(
                    resultCode,
                    intent
                )
            )
            uploadMessage = null
            takeMediaCameraUri = null
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        VoiceControllerManager.instance?.stopSpeak()
        onShowDismissListener?.onDismiss()
    }

    var timer: Timer? = null
    var timeLeft: Int? = 0
    fun startCountDown(timeSeconds: Long) {
        if (true) return
        timeLeft = timeSeconds.toInt()
        if (timeSeconds < 0) {
            return
        }
        val playTimes: Int = try {
            SharedPreferencesManager.getInt(context, "WEBGAME")
        } catch (e: Exception) {
            0
        }
        var message: String? = null
        if (playTimes == 0) {
            message =
                "Chào mừng bạn đến với minigame Tương tác chấm TV, Hãy nhanh tay chơi game để giành được nhiều phần thưởng hấp dẫn nhé. Chúc bạn may mắn"
        } else if (playTimes == 5 || playTimes == 10 || playTimes == 10) {
            message =
                "Chào mừng bạn đến với minigame Tương tác chấm TV, Chúc bạn sẽ là người chiến thắng trong phần thi này"
        }
        val saveTimes = playTimes + 1
        SharedPreferencesManager.saveInt(context, saveTimes, "WEBGAME")
        if (!message.isNullOrEmpty()) {
            Handler(Looper.getMainLooper()).postDelayed({
                VoiceControllerManager.instance?.speak(
                    activity, message,
                    null,
                    null,
                    1.2f,
                    1.2f
                )
            }, 500)
        }
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            if (timeCount >= 0) {
                val time = timeCount
                activity?.runOnUiThread {
                    layoutTime?.visibility = View.VISIBLE
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(time)
                    if (timeCount == 20L && scoreCount > 10) {
                        Handler(Looper.getMainLooper()).post {
                            VoiceControllerManager.instance?.speak(
                                requireActivity(),
                                "Còn rất ít thời gian chơi bạn hãy tập trung để trở thành nhà vô địch nhé",
                                null,
                                null, 1.2f,
                                1.2f
                            )
                        }
                    }
                    if (timeCount <= 10) {
                        if (timeCount == 10L) {
                            Handler(Looper.getMainLooper()).post {
                                VoiceControllerManager.instance?.speak(
                                    requireActivity(),
                                    "Còn 10 giây nữa thôi, Cố lên bạn nhé",
                                    null,
                                    null, 1.2f,
                                    1.2f
                                )
                            }
                        }
                        textTimeCountDown?.setTextColor(Color.YELLOW)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                    timeLeft = timeCount.toInt()
                }
            } else {
                timer?.cancel()
                activity?.runOnUiThread {
                    Handler(Looper.getMainLooper()).post {
                        VoiceControllerManager.instance?.speak(
                            requireActivity(),
                            "Hết giờ rồi. Vui lòng chờ xem kết quả nhé. Chúc bạn may mắn!",
                            null,
                            null,
                            1.2f,
                            1.2f
                        )
                    }
                    sendDataWhenTimeOut()
                    dismiss()
//                    showLayoutTimeOut()
                }
            }
        }, 0, 1000)
    }

    fun showLayoutTimeOut(isVisible: Boolean = true) {
        activity?.runOnUiThread {
            if (isVisible) {
                layoutTimeOut?.visibility = View.VISIBLE
            } else {
                layoutTimeOut?.visibility = View.GONE

            }
        }
    }

    var mScore = 0
    fun sendDataWhenTimeOut() {
        itemUser = MyApplication.getInstance().dataManager.itemUser
        if (itemUser == null) {
            return
        }
        val myHttpRequest = MyHttpRequest(requireContext())
        val api =
            "https://api.daugiatruyenhinh.com/api/minigame/" + itemUser!!.id + "/" + itemUser!!.fullname + "/" + mScore + "?avt=" + itemUser!!.avatar;
        myHttpRequest.request(false, api, null, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                Loggers.e("HTTPGAME", "" + statusCode)
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                Loggers.e("HTTPGAME", "" + responseString)
            }
        })
    }

    val API_UPLOAD_IMAGE = "http://api.daugiatruyenhinh.com/Upload_Score?"
    val API_UPLOAD_VIDEO = "http://api.daugiatruyenhinh.com/Upload_Video_Score?"
    val API_CHECK_TOP_SCORE = "http://api.daugiatruyenhinh.com/api/minigame/rank/"
    fun uploadScreenUser(diedScore: Int) {
        val sc = diedScore
        if (!MyApplication.getInstance().isNetworkConnect || imageFilePath.isNullOrEmpty()) {
            return
        }
        val myHttpRequest = MyHttpRequest(activity)
        var api = API_CHECK_TOP_SCORE + game_name
        myHttpRequest.request(false, api, null, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                responseString?.let {
                    try {
                        val scoreServer = it.trim().toInt()
//                        val scoreServer = 3
                        if (sc >= scoreServer) {
                            stopExport()
                        } else {
                            uploadImage(true)
                        }
                    } catch (e: Exception) {
                    }
                }
            }
        })
    }

    fun uploadImage(isImage: Boolean = true) {
        if (!MyApplication.getInstance().isNetworkConnect || imageFilePath.isNullOrEmpty()) {
            return
        }
        val myHttpRequest = MyHttpRequest(activity)
        val requestParams = RequestParams()
        requestParams.put("id", "${itemUser!!.id}")
        requestParams.put("nickname", "${itemUser!!.fullname}")
        requestParams.put("score", "$mScore")
        requestParams.put("game", "$game_name")
        requestParams.put("avt", "${itemUser!!.avatar}")
        var photoFile = if (isImage) File(imageFilePath!!) else File(videoFilePath)
        if (photoFile.exists()) {
            requestParams.put("screenshot_file", photoFile.toString())
            requestParams.put("typedata", myHttpRequest.getMimeType(photoFile))
            requestParams.put("typeext", myHttpRequest.getExtension(photoFile.name))
        } else return

//        val api = ServiceUtilTT.validAPIDGTH(context, API_UPLOAD_IMAGE)
        val api = if (isImage) API_UPLOAD_IMAGE else API_UPLOAD_VIDEO
        myHttpRequest.request(
            true,
            api,
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    Loggers.e("WEB_GAME_UPLOAD_IMAGE", "$statusCode")
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    Loggers.e("WEB_GAME_UPLOAD_IMAGE", "$responseString")
                }
            })
    }

    var timerRecord: Timer? = null

    //    var mp4EnCoder: MP4Encoder? = null
//    private lateinit var exportDisposable: Disposable
    var isEnableRecode: Boolean = true

    private val bitmapArr: ArrayList<Bitmap> = ArrayList()
    private var videoFilePath: String? = ""
    private var heightBm: Int = 480
    private var widthBm: Int = 480
    private var indexBm = -1
    private var indexScore = -1
    var timePeriod = 150
    private fun startExportBitmap() {
//        if(true){
//        startExport(webView)
//        return}
        if (!isEnableRecode) {
            return
        }
//        heightBm = webView.width
//        widthBm = webView.width
        isEnableRecode = false
        Loggers.e("WEBGAME", "start")
        indexBm = 0
        indexScore = 0
        bitmapArr.clear()
        timerRecord?.cancel()
        timerRecord = Timer()
        var timeRecord = 0L
        var timeDuration = 3000L
        var bm: Bitmap? = null
        bitmapArr.clear()
        timerRecord?.schedule(timerTask {
//            timeRecord = timeRecord + timePeriod
//            Thread {
            Handler(Looper.getMainLooper()).post{
                bm = getBitmap(webView)
                if (bm != null) {
                    bitmapArr.add(bm!!)
                    if (bitmapArr.size > 70) {
                        bitmapArr.removeAt(0)
                    }
                    indexBm += 1
                }
            }
//            }.start()
        }, 0, timePeriod.toLong())/*//        if(true){
//        startExport(webView)
//        return}
        if (!isEnableRecode) {
            return
        }
//        heightBm = webView.width
//        widthBm = webView.width
        isEnableRecode = false
        Loggers.e("WEBGAME", "start")
        indexBm = 0
        indexScore = 0
        bitmapArr.clear()
        timerRecord?.cancel()
        timerRecord = Timer()
        var timeRecord = 0L
        var timeDuration = 3000L
        var bm: Bitmap? = null
        bitmapArr.clear()
        timerRecord?.schedule(timerTask {
//            timeRecord = timeRecord + timePeriod
//            Thread {
            Handler(Looper.getMainLooper()).post{
                bm = getBitmap(webView)
                if (bm != null) {
                    bitmapArr.add(bm!!)
                    if (bitmapArr.size > 70) {
                        bitmapArr.removeAt(0)
                    }
                    indexBm += 1
                }
            }
//            }.start()
        }, 0, timePeriod.toLong())*/
    }

    private fun stopExport(isExportMp4: Boolean = true) {
        timerRecord?.cancel()
        isEnableRecode = true
        if (indexScore < 0 || ((bitmapArr.size - indexScore) <= 5)) {
            return
        }
        if (!isExportMp4) {
            return
        }
        val videoName: String =
            if (itemUser != null && !itemUser!!.fullname.isNullOrEmpty()) itemUser!!.fullname!!.replace(
                " ",
                "_"
            ) else "user"
        val downloarDir = requireActivity().getExternalFilesDir(null)
        val exportedFile = File(downloarDir, "video_$videoName.mp4")
        if (exportedFile.exists()) {
            exportedFile.delete()
        }
        videoFilePath = exportedFile.path
        Handler(Looper.getMainLooper()).postDelayed({
            val resultBitmapList: ArrayList<Bitmap> = ArrayList()
            for (i in 0 until bitmapArr.size) {
                if (i >= indexScore) {
                    val bmAdd = scaleToFitWidth(bitmapArr.get(i), widthBm)
                    if (bmAdd == null) {
                        continue
                    }
                    heightBm = bmAdd.height
                    Loggers.e("WEBGAME", "" + bmAdd.width + "/" + bmAdd.height)
                    resultBitmapList.add(bmAdd)
                    resultBitmapList.add(bmAdd)
                    resultBitmapList.add(bmAdd)
                }
                bitmapArr.get(i).recycle()
            }
            val bitmap = Bitmap.createBitmap(widthBm, heightBm, Bitmap.Config.ARGB_8888)
            bitmap.eraseColor(Color.WHITE);
            val bitmapEnd = resultBitmapList.get(resultBitmapList.lastIndex)
            resultBitmapList.add(bitmapEnd)
            resultBitmapList.add(bitmapEnd)
            resultBitmapList.add(bitmap)
            resultBitmapList.add(bitmap)
            resultBitmapList.add(bitmapEnd)
            resultBitmapList.add(bitmapEnd)
            if (resultBitmapList.size < 75) {
                resultBitmapList.addAll(resultBitmapList)
            }
            if (widthBm % 2 != 0) {
                widthBm = widthBm - 1
            }
            if (heightBm % 2 != 0) {
                heightBm = heightBm - 1
            }
//            startExportBitmap()
            val muxerConfig = MuxerConfig(
                exportedFile,
                widthBm,
                heightBm,
                MediaFormat.MIMETYPE_VIDEO_AVC,
                1,
                25F,
                2000000
            )
            val muxer = Muxer(requireActivity(), muxerConfig)
            muxer.setOnMuxingCompletedListener(object : MuxingCompletionListener {
                override fun onVideoSuccessful(file: File) {
                    Log.e("WEBGAME", "Video muxed - file path: ${file.absolutePath}")
                    uploadImage(false)
                }

                override fun onVideoError(error: Throwable) {
                    Log.e("WEBGAME", "There was an error muxing the video")
                }
            })

            Thread(Runnable {
//                muxer.mux(resultBitmapList, R.raw.bensound_happyrock)
                muxer.mux(resultBitmapList)
            }).start()
            Loggers.e("WEBGAME", "finish")
        }, 100)

    }

    private fun getBitmap(v: View?): Bitmap? {
//        return screenshot2(v!!)
        if (v == null) {
            return null
        }
        if (v.width <= 0 || v.height <= 0) {
            return null
        }
//        val bitmap = try {
//            v.drawToBitmap()
//        } catch (e: Exception) {
//            null
//        }
        val bitmap = Bitmap.createBitmap(v.width, v.height, Bitmap.Config.ARGB_8888)
        val c = Canvas(bitmap)
        v.draw(c)
        return bitmap
    }

    fun scaleToFitWidth(b: Bitmap, width: Int): Bitmap? {
        val factor = width / b.width.toFloat()
        try {
            return Bitmap.createScaledBitmap(b, width, (b.height * factor).toInt(), true)
        } catch (e: Exception) {
            return null
        }
    }

    fun scaleToFitHeight(b: Bitmap, height: Int): Bitmap {
        val factor = height / b.height.toFloat()
        return Bitmap.createScaledBitmap(b, (b.width * factor).toInt(), height, true)
    }
}