package vn.mediatech.interactive.chat

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_chat_interactive.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import java.util.*


class ChatFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: OnClickButtonSendListenner? = null
    var onShowDismissListener: OnShowDismissListener? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_chat_interactive, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }
    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    fun initData() {
    }

    fun initControl() {
        editChat.setOnEditorActionListener { textView, actionId, keyEvent ->
            if (actionId == EditorInfo.IME_ACTION_SEND) {
                submitSend()
                true
            } else false
        }
        buttonSend.setOnClickListener {
            submitSend()
        }
    }

    private fun submitSend() {
        val msg = editChat.text.toString().trim()
        if (msg.length == 0) {
//            Toast.makeText(context, "Giá đoán chưa hợp lệ. Vui lòng nhập lại", Toast.LENGTH_SHORT).show()
            editChat.requestFocus()
            return
        }
        onClickListener?.onClick(msg)
        editChat.setText("")
    }

    override fun onDismiss(dialog: DialogInterface) {
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
        hideKeyboard()
//        hideSoftKeyboard(editChat)
    }

    override fun onDestroyView() {
        super.onDestroyView()
//        hideKeyboard()
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                true
            } else
                false
        }
    }


    interface OnClickButtonSendListenner {
        fun onClick(message: String?)
    }

    private val endFix = "VND"


    fun hideKeyboard() {
        if (AppUtils.isShowKeyboard(requireContext())) {
            val imm = requireActivity().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
        editChat?.post(Runnable {
            editChat?.requestFocus()
            val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm?.showSoftInput(editChat, InputMethodManager.SHOW_IMPLICIT)
        })

    }
}