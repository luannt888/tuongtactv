package vn.mediatech.interactive.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_account.*
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import java.util.*

class AccountFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var username: String? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var onButtonClickListenner: OnButtonClickListenner? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(InteractiveConstant.DATA, username!!)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        username = bundle?.getString(InteractiveConstant.DATA)!!
        val isAdmin = bundle?.getBoolean(InteractiveConstant.ROLE, false)
        val userStatus = bundle?.getString(InteractiveConstant.USER_STATUS, "2")
        buttonSetUp.visibility = if (isAdmin) View.VISIBLE else View.GONE
        buttonConfirm.visibility =
            if ("2".equals(userStatus) || "0".equals(userStatus)) View.VISIBLE else View.GONE
        textName.text = username
    }


    fun initControl() {
        buttonBuyChip.setOnClickListener {
            dismiss()
            onButtonClickListenner?.onClickBuy()
        }
        buttonHistory.setOnClickListener {
            dismiss()
            onButtonClickListenner?.onClickHistory()
        }
        buttonGit.setOnClickListener {
            dismiss()
            onButtonClickListenner?.onClickGit()
        }
        buttonSupport.setOnClickListener {
            dismiss()
            onButtonClickListenner?.onClickSupport()
        }
        buttonSignOut.setOnClickListener {
            dismiss()
            onButtonClickListenner?.onClickSignOut()
        }
        buttonSetUp.setOnClickListener {
            dismiss()
            onButtonClickListenner?.onClickSetUp()
        }
        buttonConfirm.setOnClickListener {
            dismiss()
            onButtonClickListenner?.onClickConfirm()
        }
    }


    override fun onDismiss(dialog: DialogInterface) {
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    interface OnButtonClickListenner {
        fun onClickBuy()
        fun onClickHistory()
        fun onClickSignOut()
        fun onClickSupport()
        fun onClickGit()
        fun onClickSetUp()
        fun onClickConfirm()
    }
}