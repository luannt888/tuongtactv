package vn.mediatech.istudiocafe.zinteractive.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_message_interactive.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.app.AppUtils.Companion.getDateString
import vn.mediatech.interactive.app.AppUtils.Companion.getTimeString
import vn.mediatech.interactive.app.JsonParserUtil
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import vn.mediatech.interactive.model.ItemMessage
import java.util.*

class ItemMessageAdapter(
    val context: Context, val itemList: ArrayList<ItemMessage>, val type: Int, val style: Int, val
    numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0


    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_message_interactive,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position >= itemList.size) {
            return
        }
        val itemObj: ItemMessage = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemMessage, position: Int) {
        if (itemObj.message != null && itemObj.message.contains("\uD83C\uDF81")) {
            holder.layoutMessage.isSelected = true
        } else {
            holder.layoutMessage.isSelected = false
        }
        if (itemObj.service.equals(InteractiveConstant.TIME)) {
            holder.layoutDate.visibility = View.VISIBLE
            holder.layoutMessage.visibility = View.GONE
        } else {
            holder.layoutDate.visibility = View.GONE
            holder.layoutMessage.visibility = View.VISIBLE
        }

        when (itemObj.service) {
            InteractiveConstant.SERVICE_BID -> {
                holder.layoutMessage.visibility = View.VISIBLE
                AppUtils.loadImageCircle(
                    context,
                    holder.imgAvatar,
                    itemObj.avatar,
                    R.drawable.ic_avatar_interactive
                )
                holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                val string =
                    if (!itemObj.note!!.trim().equals("")) itemObj.note else itemObj.userFullname
                val sp1 =
                    AppUtils.getStringHtmlColor("(" + string + "): ", "#33c9dc") + itemObj.message
                AppUtils.setHtmlTextView(sp1, holder.textMessage)
            }
            InteractiveConstant.TIME -> {
                holder.layoutDate.visibility = View.VISIBLE
                holder.textDate.setText(getDateString(context, itemObj.lastUpdate))
            }
            InteractiveConstant.SERVICE_IQ -> {
                if (itemObj.key.equals("IQ-MESSAGE")) {
                    holder.layoutMessage.visibility = View.VISIBLE
                    AppUtils.loadImageCircle(
                        context,
                        holder.imgAvatar,
                        itemObj.avatar,
                        R.drawable.ic_avatar_interactive
                    )
                    holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                    val sp1 = AppUtils.getStringHtmlColor(
                        "(" + itemObj.note + "): ",
                        "#33c9dc"
                    ) + itemObj.message
                    AppUtils.setHtmlTextView(sp1, holder.textMessage)
                } else if (itemObj.key.equals("PLAYER-ANSWER")) {
                    holder.layoutMessage.visibility = View.VISIBLE
                    AppUtils.loadImageCircle(
                        context,
                        holder.imgAvatar,
                        itemObj.avatar,
                        R.drawable.ic_avatar_interactive
                    )
                    holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                    val sp1 =
                        AppUtils.getStringHtmlColor("(Thử thách IQ): ", "#33c9dc") + itemObj.note
                    AppUtils.setHtmlTextView(sp1, holder.textMessage)
                }
            }

            InteractiveConstant.SERVICE_SPIN -> {
                holder.layoutMessage.visibility = View.VISIBLE
                AppUtils.loadImageCircle(
                    context,
                    holder.imgAvatar,
                    itemObj.avatar,
                    R.drawable.ic_avatar_interactive
                )
                var title = "(Vòng quay may mắn): "
                if(itemObj.data!= null && itemObj.data.equals("DIEMDANH")){
                    title = "(Điểm danh): "
                }
                holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                val sp1 = AppUtils.getStringHtmlColor(
                    title,
                    "#33c9dc"
                ) + itemObj.message
                AppUtils.setHtmlTextView(sp1, holder.textMessage)

            }
            InteractiveConstant.SERVICE_DIEMDANH -> {
                holder.layoutMessage.visibility = View.VISIBLE
                AppUtils.loadImageCircle(
                    context,
                    holder.imgAvatar,
                    itemObj.avatar,
                    R.drawable.ic_avatar_interactive
                )
                holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                val sp1 = AppUtils.getStringHtmlColor(
                    "(Điểm danh): ",
                    "#33c9dc"
                ) + itemObj.message
                AppUtils.setHtmlTextView(sp1, holder.textMessage)

            }
            InteractiveConstant.SERVICE_GUESS_PRICE -> {
                holder.layoutMessage.visibility = View.VISIBLE
                AppUtils.loadImageCircle(
                    context,
                    holder.imgAvatar,
                    itemObj.avatar,
                    R.drawable.ic_avatar_interactive
                )
                holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                val sp1 =
                    AppUtils.getStringHtmlColor("(Game đoán giá): ", "#33c9dc") + itemObj.message
                AppUtils.setHtmlTextView(sp1, holder.textMessage)
            }
            InteractiveConstant.SERVICE_VOTE2 -> {
                holder.layoutMessage.visibility = View.VISIBLE
                AppUtils.loadImageCircle(
                    context,
                    holder.imgAvatar,
                    itemObj.avatar,
                    R.drawable.ic_avatar_interactive
                )
                holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                val sp1 = AppUtils.getStringHtmlColor("(Bình chọn): ", "#33c9dc") + itemObj.message
                AppUtils.setHtmlTextView(sp1, holder.textMessage)
            }
            InteractiveConstant.SERVICE_CHAT -> {
                holder.layoutMessage.visibility = View.VISIBLE
                AppUtils.loadImageCircle(
                    context,
                    holder.imgAvatar,
                    itemObj.avatar,
                    R.drawable.ic_avatar_interactive
                )
                holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                val string = itemObj.userFullname
                val sp1 =
                    AppUtils.getStringHtmlColor("(" + string + "): ", "#33c9dc") + itemObj.message
                AppUtils.setHtmlTextView(sp1, holder.textMessage)
            }
            InteractiveConstant.SERVICE_GIT -> {
                holder.layoutMessage.visibility = View.VISIBLE
                AppUtils.loadImageCircle(
                    context,
                    holder.imgAvatar,
                    itemObj.avatar,
                    R.drawable.ic_avatar_interactive
                )
                holder.textTime.setText(getTimeString(itemObj.lastUpdate))
                val string = itemObj.userFullname
                val sp1 =
                    AppUtils.getStringHtmlColor("(" + string + "): ", "#33c9dc") + itemObj.message
                AppUtils.setHtmlTextView(sp1, holder.textMessage)
                val jsonArray = JsonParserUtil.getJsonArray(itemObj.data)
                if (jsonArray != null) {
                    val json = JsonParserUtil.getJsonObject(jsonArray.get(0).toString())
                    val imgGit = JsonParserUtil.getString(json, "filename")
                    if (!imgGit.isNullOrEmpty()) {
                        AppUtils.loadImage(context, holder.imageGit, imgGit)
                        holder.imageGit.visibility = View.VISIBLE
                    }
                }

            }
            else -> {
                holder.layoutRoot.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textTime = itemView.textTime
        val textDate = itemView.textDate
        val textMessage = itemView.textMessage
        val layoutMessage = itemView.layoutMessage
        val layoutDate = itemView.layoutDate
        val imageGit = itemView.imageGit

    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemMessage, position: Int)
    }

}
