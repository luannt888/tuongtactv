package vn.mediatech.istudiocafe.zinteractive.webGame

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemUserWebgame(
        @SerializedName("rank_index") var index: Int?,
        @SerializedName("user_id") val userId: Int?,
        @SerializedName("user_fullname") val name: String?,
        @SerializedName("user_avt") val avatar: String?,
        @SerializedName("data") val score: String?
) : Parcelable

/*
"ID": 5347,
"last_updated": "2021-09-30 11:12:37",
"user_id": "1614",
"user_fullname": "Tiến Minh",
"user_avt": "https://daugiatruyenhinh.com/upload/istudios/avatar/1614_Tien_Minh.jpg",
"service": "MINIGAME",
"key": "SCORE",
"data": "14",
"message": "",
"ip": "",
"status": 0,
"desc": "",
"note": ""*/
