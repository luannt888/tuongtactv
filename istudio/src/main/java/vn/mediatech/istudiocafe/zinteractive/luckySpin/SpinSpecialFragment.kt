package vn.mediatech.interactive.luckySpin

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_spin_new.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import java.util.*
import kotlin.concurrent.timerTask

class SpinSpecialFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var itemObj: ItemSpin? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: OnClickButtonRightListenner? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_spin_new, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
//            bottomSheet.setBackgroundColor(Color.parseColor("#7A000000"))
//            bottomSheet.setBackgroundColor(Color.parseColor("#000000"))
//            bottomSheet.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }
    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }
    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(InteractiveConstant.DATA, itemObj)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    var data: String? = null
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        val height = requireArguments().getInt(InteractiveConstant.HEIGHT)
        try {
            if (height > 0) {
                val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                param?.height = height
            }
        } catch (e: Exception) {
        }
        itemObj = bundle?.getParcelable(InteractiveConstant.DATA)!!
        if (itemObj == null) {
            dismiss()
            return
        }
        if (!itemObj!!.msg.isNullOrEmpty()) {
            textMessage.text = itemObj!!.msg
        }
        if (itemObj!!.timeCount != null) {
            startCountDown(itemObj!!.timeCount!!)
        }
        if (!itemObj!!.status.isNullOrEmpty() && itemObj!!.status!!.trim().equals("1")) {
            textMessage.text = "Bạn đã tham gia game. Vui lòng chờ kết quả"
            buttonLeft.visibility = View.GONE
            buttonRight.visibility = View.GONE
        }
    }

    fun initControl() {
        buttonLeft.setOnClickListener {
            onClickListener?.onClick(itemObj!!)
            dismiss()
        }
        buttonRight.setOnClickListener {
            dismiss()
        }

        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    override fun onDismiss(dialog: DialogInterface) {
//        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        timer?.cancel()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    var timer: Timer? = null
    fun startCountUp(timeStartSecond: Long) {
        var timeCount = timeStartSecond
        timer?.cancel()
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(timeCount)
                timeCount = timeCount + 1
            }
        }, 0, 1000)
    }

    fun startCountDown(timeSeconds: Long) {
        if (timeSeconds <= 0) {
            return
        }
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            activity?.runOnUiThread {
                if (timeCount >= 0) {
                    val time = timeCount
                    if (textTimeCountDown?.visibility == View.GONE) {
                        textTimeCountDown?.visibility = View.VISIBLE
                    }
                    textTimeCountDown?.text = time.toString() + "s"
                    if (time <= 10) {
                        textTimeCountDown?.setTextColor(Color.YELLOW)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                } else {
                    dismiss()
                }
            }
        }, 0, 1000)
    }

    private fun setupBackPressListener() {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { _, keyCode, _ ->
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                activity?.finish()
                true
            } else
                false
        }
    }


    interface OnClickButtonRightListenner {
        fun onClick(item: ItemSpin)
    }
}