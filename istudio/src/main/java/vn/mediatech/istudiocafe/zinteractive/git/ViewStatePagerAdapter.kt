package vn.mediatech.interactive.git

import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter

class ViewStatePagerAdapter(fm: FragmentManager?, private var fragmentList: ArrayList<Fragment>, private var tabTitleList: ArrayList<String>?) : FragmentStatePagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return fragmentList.get(position)

    }

    override fun getCount(): Int {
        return fragmentList.size
    }

    val tabTitleCount: Int
        get() = if (tabTitleList != null) {
            tabTitleList!!.size
        } else 0

    fun setTabTitleList(tabTitleList: ArrayList<String>?) {
        this.tabTitleList = tabTitleList
    }

    fun addTabPage(fragment: Fragment?, tabTitle: String?) {
        if (fragment == null || tabTitle == null) {
            return
        }
        if (fragmentList == null) {
            fragmentList = ArrayList()
        }
        if (tabTitleList == null) {
            tabTitleList = ArrayList()
        }
        fragmentList!!.add(fragment)
        tabTitleList!!.add(tabTitle)
        //        notifyDataSetChanged();
    }

    fun updateTabTitle(position: Int, tabTitle: String?) {
        if (fragmentList == null || tabTitle == null || tabTitleList!!.size <= position) {
            return
        }
        tabTitleList!!.removeAt(position)
        tabTitleList!!.add(position, tabTitle)
        notifyDataSetChanged()
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (tabTitleList == null || tabTitleList!!.size <= position) {
            null
        } else tabTitleList!![position]
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {}
}