package vn.mediatech.interactive.guide

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemGuide(
        @SerializedName("id") val id: String?,
        @SerializedName("title") var title: String?,
        @SerializedName("img") val img: String?,
        @SerializedName("content") var content: String?,
        @SerializedName("galery") var galery: Array<String>?,
        @SerializedName("ztype") var ztype: String?,
        @SerializedName("typename") val typeName: String?,
        @SerializedName("status") val status: String?
): Parcelable
/*id: "1",
appid: "bacgiang",
title: "Hướng dẫn tham gia đấu giá truyền hình",
img: "https://daugiatruyenhinh.com/upload/product/1673_voucher_giam_gia_50_000_dong_tai_highlands_bac_gia.jpg",
content: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Why do we use it? It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). ",
galery: [
"https://daugiatruyenhinh.com/upload/endows/warehouse/1464742842166_6052.jpg",
"https://daugiatruyenhinh.com/upload/endows/warehouse/cb939851443167061cc1f1f30d7a1639.jpg",
"https://daugiatruyenhinh.com/upload/endows/warehouse/noi-that-chung-cu.jpg",
"https://daugiatruyenhinh.com/upload/endows/warehouse/thi-cong-noi-that-tai-chung-cu-hoang-van-thai-1.jpg",
"https://daugiatruyenhinh.com/upload/endows/warehouse/thi-cong-phong-ngu-nha-pho-tai-bac-tu-liem-3.jpg",
"https://daugiatruyenhinh.com/upload/endows/warehouse/unnamed (1).jpg",
"https://daugiatruyenhinh.com/upload/endows/warehouse/unnamed.jpg"
],
ztype: "guess_price",
status: "1"*/
