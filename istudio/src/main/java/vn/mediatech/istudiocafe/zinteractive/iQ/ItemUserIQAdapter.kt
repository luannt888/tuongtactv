package vn.mediatech.istudiocafe.zinteractive.iQ

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_iq_user.view.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.R

class ItemUserIQAdapter(
    val context: Context,
    val list: ArrayList<ItemUserIQ>
) : Adapter<ItemUserIQAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_iq_user,
            parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val itemObjGame: ItemUserIQ = list.get(position)
        AppUtils.loadImageCircle(
            context,
            holder.imgAvatar,
            itemObjGame.avatar,
            R.drawable.ic_avatar_interactive
        )
        holder.textName.setText("${itemObjGame.name}")
        holder.textPoint.setText("${itemObjGame.score}")
        holder.textTime.setText("${itemObjGame.time}")
//        holder.textIndex.setText("${itemObjGame.index}")
        var indexStr = (position +1).toString()
        if(position < 9){
            indexStr = "0" + indexStr
        }
//        holder.textIndex.setText("${itemObjGame.userId}")
        holder.textIndex.setText("$indexStr")
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObjGame, position)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textPoint = itemView.textPoint
        val textTime = itemView.textTime
        val textName = itemView.textName
        val textIndex = itemView.textIndex
    }

    interface OnItemClickListener {
        fun onClick(itemGame: ItemUserIQ, position: Int)
        fun onClickJoin(itemGame: ItemUserIQ, position: Int)
        fun onLongClick(itemGame: ItemUserIQ, position: Int)
    }

}