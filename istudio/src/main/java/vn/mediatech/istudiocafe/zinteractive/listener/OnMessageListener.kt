package vn.mediatech.interactive.listener

interface OnMessageListener {
    fun onShow(msg: String?, type: String?, timeCloseMs: Long = 0L)
}