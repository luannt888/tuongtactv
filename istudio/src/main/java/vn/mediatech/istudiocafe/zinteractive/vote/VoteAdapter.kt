package vn.mediatech.interactive.vote

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_vote.view.*
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.istudiocafe.R
import java.util.*

class VoteAdapter(
        val context: Context, val itemList: ArrayList<ItemVoteAnswer>, val type: Int, val style: Int, val
        numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var indexSelect: Int = -1
    var selectable: Boolean = true
    var indexTrue: Int = -1

    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
//        val screenSize = ScreenSize(activity)
//        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
//            screenSize.width
//        imageWidth = screenWidth / 4
//        imageHeight = imageWidth * 7 / 10
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_vote,
                parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemVoteAnswer = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemVoteAnswer, position: Int) {
        holder.textNumber.text = String.format(Locale.ENGLISH, "%02d", (position + 1))
        holder.textAnswer.text = itemObj.title
        holder.progress.setProgress(itemObj.percent!!.toInt())
        if (itemObj.percent != null) {
            holder.textCountSelected.text = "" + itemObj.percent + " %"
            holder.textCountSelected.visibility = View.VISIBLE
//            holder.textChip.text = "(${itemObj.chip}chip)"
//            holder.textChip.visibility = View.VISIBLE
        } else {
            holder.textCountSelected.visibility = View.GONE
            holder.textChip.visibility = View.GONE
        }
        if (!itemObj.imgLink.isNullOrEmpty()) {
            AppUtils.loadImage(context, holder.imgThumbnail, itemObj.imgLink)
            holder.imgThumbnail.visibility = View.VISIBLE
        } else {
            holder.imgThumbnail.visibility = View.GONE
        }
        if (indexSelect >= 0) {
//            holder.layoutRoot.isClickable = false
//            holder.layoutRoot.isEnabled = false
        }
        if (indexTrue >= 0) {
            holder.layoutRoot.isClickable = false
            holder.layoutRoot.isEnabled = false
            if (indexTrue == position) {
                holder.layoutRoot.isActivated = true
            }
        } else if (indexSelect == position) {
            holder.layoutRoot.isSelected = true
        }

        holder.layoutRoot.setOnClickListener {
            if (indexSelect == -1) {
                indexSelect = position
                notifyDataSetChanged()
            }
            onItemClickListener?.onClick(itemObj, position)
        }
    }

    fun setCancelSelectable(selectable: Boolean) {
        this.selectable = selectable
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val textNumber = itemView.textNumber
        val textAnswer = itemView.textAnswer
        val textCountSelected = itemView.textCountSelected
        val textChip = itemView.textChip
        val progress = itemView.progress
        val imgThumbnail = itemView.imgAvata
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemVoteAnswer, position: Int)
    }

    fun setSelected(index: Int) {
        indexSelect = index
        notifyDataSetChanged()
    }

}