package vn.mediatech.interactive.guide

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_guide.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import vn.mediatech.interactive.git.ViewStatePagerAdapter
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import java.util.*
import kotlin.collections.ArrayList

class GuideFragment : BottomSheetDialogFragment() {
    private var isViewCreated: Boolean = false
    var data: String? = null
    var isSuccess = false
    var bottomSheetDialog: BottomSheetDialog? = null
    var onClickListener: OnClickListenner? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var savedInstanceState: Bundle? = null

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_guide, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED

            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                    com.google.android.material.R.id.design_bottom_sheet)
                    ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
//            bottomSheet.setBackgroundColor(Color.parseColor("#7A000000"))
//            bottomSheet.setBackgroundColor(Color.parseColor("#000000"))
//            bottomSheet.setBackgroundColor(Color.parseColor("#00FFFFFF"))
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putString(InteractiveConstant.DATA, data)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    private fun initUI() {
//        setupBackPressListener()
    }

    var itemList: ArrayList<ItemGuide>? = null
    var currentPosGuide = 0
    fun initData() {
        val bundle = savedInstanceState ?: arguments
        itemList = bundle?.getParcelableArrayList(InteractiveConstant.DATA)
        val height = bundle?.getInt(InteractiveConstant.HEIGHT)
        if (height != null && height > 0) {
            Loggers.e("BID", "HeightBidMulti" + height)
            val param = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height)
//            param?.height = height
            layoutRoot?.layoutParams = param
        }
        currentPosGuide =   bundle!!.getInt(InteractiveConstant.POSITION, 0)
        if (itemList == null || itemList!!.size == 0) {
            return
        }
        fragmentList.clear()
        tabTitleList.clear()
        for (i in 0 until itemList!!.size) {
            val fragment = GuideDetailFragment()
            val bundle = Bundle()
            bundle.putParcelable(InteractiveConstant.DATA, itemList!!.get(i))
            bundle.putInt(InteractiveConstant.POSITION, i)
            fragment.arguments = bundle
            fragmentList.add(fragment)
            tabTitleList.add(itemList!!.get(i).title!!)
        }
//        (fragmentList.get(0) as? GitCategoryFragment)?.checkRefresh()
        initPager()
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            dismiss()
        }
    }

    var fragmentList: ArrayList<Fragment> = ArrayList()
    var tabTitleList: ArrayList<String> = ArrayList()
    fun initPager() {
        val viewPagerAdapter = ViewStatePagerAdapter(childFragmentManager, fragmentList, tabTitleList)
        viewPager.adapter = viewPagerAdapter
        viewPager.offscreenPageLimit = fragmentList.size
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                onClickListener?.onSelectPage(itemList!!.get(position), position)
                ( fragmentList.get(position) as? GuideDetailFragment)?.checkRefresh()
            }

            override fun onPageScrollStateChanged(state: Int) {
            }

        })
        if(currentPosGuide > 0){
            viewPager.setCurrentItem(currentPosGuide)
        }
        tabLayout.setupWithViewPager(viewPager)
//        setupTabIcons()
    }

//    private fun setupTabIcons() {
//        for (i in 0 until tabLayout.tabCount) {
//            var view = LayoutInflater.from(context).inflate(R.layout.item_tab_imageview, null)
//            tabLayout.getTabAt(i)!!.setCustomView(view)
//            AppUtils.loadImage(context, view.imgAvatar, itemList!!.get(i).img)
////            view.textTitle.text = itemList!!.get(i).title
////            view.textTitle.visibility = View.VISIBLE
//
//        }
//    }

    override fun onDismiss(dialog: DialogInterface) {
//        onDismissListener?.onDismiss(isSuccess, videoPath, null)
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

    interface OnClickListenner {
        fun onSelectPage(item: ItemGuide?, position: Int)
    }
}