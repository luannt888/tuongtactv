package vn.mediatech.istudiocafe.zinteractive.emotion

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_emotion.*
import vn.mediatech.interactive.app.*
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.GeneralUtils
import vn.mediatech.istudiocafe.util.SpacesItemDecorationGrid
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask


class EmotionFragment : BottomSheetDialogFragment() {
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var myHttpRequest: MyHttpRequest? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var itemList: ArrayList<ItemEmotion> = ArrayList()
    var onItemClickListener: ItemEmotionAdapter.OnItemClickListener? = null
    var rate = 5
    var currentItem: ItemEmotion? = null
    private val API_GROUP: String =
        "https://daugiatruyenhinh.com/api/v1.0/tuongtactv/emoji"
    private val API_POST: String =
        "https://daugiatruyenhinh.com/api/v1.0/tuongtactv/postEmoji"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_emotion, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
//    spriteView.start("https://i.imgur.com/loYRir6.png")
        btnStar1.isSelected = true
        btnStar2.isSelected = true
        btnStar3.isSelected = true
        btnStar4.isSelected = true
        btnStar5.isSelected = true
    }

    var height = 0
    fun initData() {
        var dataGroup: String? = null
        if (arguments != null) {
            height = requireArguments().getInt(InteractiveConstant.HEIGHT)
            userId = requireArguments().getString(InteractiveConstant.USER_ID)
            userToken = requireArguments().getString(InteractiveConstant.USER_TOKEN)
            dataGame = requireArguments().getString(InteractiveConstant.DATA_GAME)
            try {
                if (height > 0) {
                    val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
                    param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    param?.height = height
                }
            } catch (e: Exception) {
            }
            var timeEndMs = requireArguments().getLong(InteractiveConstant.TIME, 0)
            val timeStart = (timeEndMs - System.currentTimeMillis()) / 1000
            startCountDown(timeStart)
            dataGroup = requireArguments().getString(InteractiveConstant.DATA, null)
        }
        if (dataGroup.isNullOrEmpty()) {
            getData()
        } else {
            handleDataEmotion(dataGroup)
        }
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!AppUtils.isNetworkConnect(context)) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
//        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        val api = API_GROUP
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                startTrack()
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                startTrack()
                if (isDetached) {
                    return
                }
                handleData(responseString)

                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_not_data))
            return
        }
        val responseString = responseStr.trim()
        handleDataEmotion(responseString)
    }

    private fun handleDataEmotion(dataGroup: String) {
        val json = JsonParserUtil.getJsonObject(dataGroup)
        val data = JsonParserUtil.getString(json, "result")
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemEmotion>>() {}.getType()
        var newList: ArrayList<ItemEmotion> = ArrayList()
        try {
            val list: ArrayList<ItemEmotion>? = gson.fromJson(data, gsonType)
            if (list != null && list.size > 0)
//                for (item in list) {
//                    item.sprite?.fps = 25
//                    item.sprite?.link = "https://i.imgur.com/loYRir6.png"
//                    item.sprite?.imageWidth = 8
//                    item.sprite?.framewidth = 1
////                    newList.add(item)
////                    newList.add(item)
////                    newList.add(item)
//                }
                newList.addAll(list!!)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        activity?.runOnUiThread {
            try {
                if (adapter != null) {
                    recyclerView?.stopScroll()
                    itemList.clear()
                    itemList.addAll(newList)
                    adapter?.notifyDataSetChanged()
                } else {
                    itemList.clear()
                    itemList.addAll(newList)
                    initAdapter()
                }
                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun setData() {
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
        }
    }


    var adapter: ItemEmotionAdapter? = null
    private fun initAdapter() {
        val numberColumn = 3
        val numberRow = 2
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.dimen_5)
        val spaceText: Int = resources.getDimensionPixelSize(R.dimen.dimen_15)
        var mHeight = 0
        val width = (recyclerView.width - spacePx * (numberColumn + 1)) / numberColumn
        if (height <= 0) {
            mHeight = (recyclerView.height - spacePx * (numberRow + 1)) / numberRow
        } else {
            mHeight = ((height - AppUtils.convertDpToPixel(120F, requireContext())
                .toInt()) - (spacePx * (numberRow + 1))) / numberRow
        }
        if (mHeight > (width + spaceText)) {
            mHeight = width+ spaceText
        }

        val layoutManager =
            GridLayoutManager(context, numberRow, GridLayoutManager.HORIZONTAL, false)
        val itemDecoration: RecyclerView.ItemDecoration =
            SpacesItemDecorationGrid(spacePx, numberColumn, true)
        adapter = ItemEmotionAdapter(requireContext(), itemList, spacePx, mHeight, width)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
//        recyclerView.addItemDecoration(itemDecoration)
        adapter?.onItemClickListener = object : ItemEmotionAdapter.OnItemClickListener {
            override fun onClick(itemGame: ItemEmotion, position: Int) {
                currentItem = itemGame
                if (layoutRate.visibility != View.VISIBLE) {
                    layoutRate.visibility = View.VISIBLE
                }
            }
        }

    }

    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)true
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonClose.setOnClickListener {
            true
            dismiss()
        }
        btnStar1.setOnClickListener {
            btnStar1.isSelected = true
            btnStar2.isSelected = false
            btnStar3.isSelected = false
            btnStar4.isSelected = false
            btnStar5.isSelected = false
            rate = 1
        }
        btnStar2.setOnClickListener {
            btnStar1.isSelected = true
            btnStar2.isSelected = true
            btnStar3.isSelected = false
            btnStar4.isSelected = false
            btnStar5.isSelected = false
            rate = 2
        }
        btnStar3.setOnClickListener {
            btnStar1.isSelected = true
            btnStar2.isSelected = true
            btnStar3.isSelected = true
            btnStar4.isSelected = false
            btnStar5.isSelected = false
            rate = 3
        }
        btnStar4.setOnClickListener {
            btnStar1.isSelected = true
            btnStar2.isSelected = true
            btnStar3.isSelected = true
            btnStar4.isSelected = true
            btnStar5.isSelected = false
            rate = 4

        }
        btnStar5.setOnClickListener {
            btnStar1.isSelected = true
            btnStar2.isSelected = true
            btnStar3.isSelected = true
            btnStar4.isSelected = true
            btnStar5.isSelected = true
            rate = 5
        }
        btnSend.setOnClickListener {
            if (currentItem == null) {
                GeneralUtils.showToast(requireActivity(), "Vui lòng chọn biểu tượng cảm xúc!")
                return@setOnClickListener
            }
//            btnSend.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({
//                btnSend?.isEnabled = true
//            }, 1000)
//            onItemClickListener?.onClick(currentItem!!, 0)
            postData()
        }
    }

    var userId: String? = null
    var userToken: String? = null
    var dataGame: String? = null
    fun postData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!AppUtils.isNetworkConnect(context)) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
//        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
//        Parmas : channel, emoji_id, emoji_type, count_start, user_id, access_token, game_data
        val requestParams = RequestParams()
        requestParams.put("channel", InteractiveConstant.CHANEL_ID)
        requestParams.put("emoji_id", currentItem!!.id)
        requestParams.put("emoji_type", currentItem!!.type)
        requestParams.put("user_id", userId)
        requestParams.put("access_token", userToken)
        requestParams.put("game_data", "${dataGame}")
        requestParams.put("count_start", "5")
        val api = API_POST
        myHttpRequest!!.request(true, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                isLoading = false
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                        val status = JsonParserUtil.getInt(
                            JsonParserUtil.getJsonObject(responseString),
                            "status"
                        )
                        val msg = JsonParserUtil.getString(
                            JsonParserUtil.getJsonObject(responseString),
                            "msg"
                        )
                        if (status == 200) {
//                            Toast.makeText(requireContext(), msg, Toast.LENGTH_SHORT).show()
                            dismiss()
                        } else{

                            Toast.makeText(requireContext(), msg +". Vui lòng thử lại!", Toast.LENGTH_SHORT).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        })
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }

    var timerGetData: Timer? = null
    private fun startTrack() {
//        val timer = Timer()
//        timer.schedule(timerTask {
//            activity?.runOnUiThread {
//                getData()
//            }
//        }, 0, 10000)
    }

    override fun onPause() {
        super.onPause()
        timerGetData?.cancel()
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
        if (adapter != null) {
            startTrack()
        }
    }

    override fun onStop() {
        super.onStop()
        timer?.cancel()
    }

    var timer: Timer? = null
    fun startCountDown(timeSeconds: Long) {
        if (timeSeconds < 0) {
            return
        }
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            if (timeCount >= 0) {
                val time = timeCount
                activity?.runOnUiThread {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(time)
                    if (timeCount <= 10) {
                        textTimeCountDown?.setTextColor(Color.YELLOW)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                }
            } else {
                timer?.cancel()
                activity?.runOnUiThread {
                    dismiss()
                }
            }
        }, 0, 1000)
    }

    override fun onDismiss(dialog: DialogInterface) {
        timer?.cancel()
        timerGetData?.cancel()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }

}