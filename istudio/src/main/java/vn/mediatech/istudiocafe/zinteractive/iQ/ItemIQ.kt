package vn.mediatech.interactive.iQ

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemIQ(
        @SerializedName("id") val id: String?,
        @SerializedName("nameQuestion") val nameQuestion: String?,
        @SerializedName("replyA") val replyA: String?,
        @SerializedName("replyB") val replyB: String?,
        @SerializedName("replyC") val replyC: String?,
        @SerializedName("replyD") val replyD: String?,
        @SerializedName("status") val status: String?,
//        @SerializedName("created_at") val createdAt: String?,
//        @SerializedName("updated_at") val updatedAt: String?,
//        @SerializedName("imgQuestion") val imgQuestion: String?,
        @SerializedName("imgreplyA") val imgreplyA: String?,
        @SerializedName("imgreplyB") val imgreplyB: String?,
        @SerializedName("imgreplyC") val imgreplyC: String?,
        @SerializedName("imgreplyD") val imgreplyD: String?,
//        @SerializedName("soundQuestion") val soundQuestion: String?,
        @SerializedName("index") val index: String?,
        @SerializedName("endtime") val endTime: String?,
        @SerializedName("timeleft") val timeLeft: String?,
        var data: String?,
        var timeEndMs: Long?,
        var dataResult:String?,
        var indexSelected:Int = -1
//        @SerializedName("gallery_url") val galleryUrl: String?
) : Parcelable
/*
"id":"811",
"nameQuestion":"Big City Boy là ca khúc do ca sỹ nào thể hiện?",
"replyA":"Sơn Tùng MTP",
"replyB":"zBinzy",
"replyC":"Soobin Hoàng Sơn",
"replyD":"Đen Vâu",
"chooseReply":"",
"status":"SET",
"created_at":"2020-12-04 11:54:21",
"updated_at":"2020-12-04 11:58:53",
"user":"quynhchi",
"imgQuestion":"",
"imgreplyA":"",
"imgreplyB":"",
"imgreplyC":"",
"imgreplyD":"",
"soundQuestion":"\\\\DEMO-PC\\MPI.SHARE\\GameShow\\Audio\\160-1.mp3",
"index":1,
"endtime":"12/31/2020 1:51:59 PM",
"timeleft":15*/
