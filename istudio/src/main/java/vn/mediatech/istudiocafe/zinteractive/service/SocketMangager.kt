package vn.mediatech.interactive.service

import okhttp3.*
import okio.ByteString
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant.CLOSE_CONNECT
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.TimeUnit

class SocketMangager {
    companion object {
        private var instance: SocketMangager? = null
        fun getInstance(): SocketMangager {
            if (instance == null) {
                instance = SocketMangager()
            }
            return instance!!
        }
    }

    var mWebSocket: WebSocket? = null
    var url: String? = null
    var listenner: MySocketListenner? = null
    var isConnecting: Boolean = false
    var isRequest: Boolean = false
    var client: OkHttpClient? = null
    var isClosetoOpenPort: Boolean = false
    var mapListener: ConcurrentHashMap<String, MySocketListenner> = ConcurrentHashMap()
    fun run(port: String?) {
        Loggers.e("Socket", "run:" + port)
        if (port.isNullOrEmpty()) {
            return
        }
        if ((this.url.equals(port) && (isConnecting || isRequest)) || isClosetoOpenPort) {
            if (isClosetoOpenPort) {
                this.url = port
            }
            return
        }
        this.url = port
        if (isConnecting||isRequest) {
            isRequest = false
            isClosetoOpenPort = true
            close()
            return
        }
//        if (isConnecting && client != null) {
//            for (call in client!!.dispatcher.runningCalls()) {
//                if (call.request().tag() != null && call.request().tag()!!.equals(this.url)) {
//                    call.cancel()
//                    break
//                }
//            }
//        }
        Loggers.e("Socket", "request:" + port)
        client = OkHttpClient.Builder()
            .readTimeout(5000, TimeUnit.SECONDS)
            .build()
        val request: Request = Request.Builder()
            .url(port).tag(url)
//                .url("wss://api.daugiatruyenhinh.com:8089/interactive?")
            .build()
        isRequest = true
        mWebSocket = client!!.newWebSocket(request, object : WebSocketListener() {

            override fun onOpen(webSocket: WebSocket, response: Response) {
                super.onOpen(webSocket, response)
                isConnecting = true
                isRequest = false
                isClosetoOpenPort = false
                listenner?.onConnected(response)
                for ((key, item) in mapListener) {
                    item.onConnected(response)
                }
            }

            override fun onMessage(webSocket: WebSocket, bytes: ByteString) {
                super.onMessage(webSocket, bytes)
                listenner?.onMessage(bytes.toString())
                for ((key, item) in mapListener) {
                    item.onMessage(bytes.toString())
                }
            }

            override fun onMessage(webSocket: WebSocket, text: String) {
                super.onMessage(webSocket, text)
                listenner?.onMessage(text)
                for ((key, item) in mapListener) {
                    item.onMessage(text)
                }
            }

            override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
                super.onFailure(webSocket, t, response)
//                run(url)
                isConnecting = false
                isRequest = false
                isClosetoOpenPort = false
//                if (!isClosetoOpenPort) {
                listenner?.onFailure(t, response)
                for ((key, item) in mapListener) {
                    item.onFailure(t, response)
                }
//                }
            }

            override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
                super.onClosed(webSocket, code, reason)
                isConnecting = false
                isRequest = false
                listenner?.onClosed(code, reason)
                if (isClosetoOpenPort && code == CLOSE_CONNECT) {
                    isClosetoOpenPort = false
                    run(url)
                }
                for ((key, item) in mapListener) {
                    item.onClosed(code, reason)
                }
            }

            override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {
                super.onClosing(webSocket, code, reason)
                isConnecting = false
                isRequest = false
                listenner?.onClosing(code, reason)
                for ((key, item) in mapListener) {
                    item.onClosing(code, reason)
                }
            }

        })

        client?.dispatcher?.executorService?.shutdown()
    }

    fun addSocketlistenner(key: String?, listenner: MySocketListenner?) {
        if (key.isNullOrEmpty() || listenner == null) {
            return
        }
        mapListener.put(key, listenner)
    }

    fun clearSocketListenner(key: String?) {
        mapListener?.remove(key)
    }

    fun clearAllSocketListenner() {
        mapListener.clear()
    }

    fun send(string: String) {
        mWebSocket?.send(string)
    }


    fun close() {
        mWebSocket?.close(CLOSE_CONNECT, "User close Connect")
        clearAllSocketListenner()
//        mWebSocket?.cancel()
//        client?.dispatcher?.cancelAll();
    }

    fun reconnect() {
        if (url.isNullOrEmpty()) {
            return
        }
        if (isConnecting) {
            isClosetoOpenPort = true
            close()
        } else {
            run(url)
        }

    }

    fun request() {
        mWebSocket?.request()
    }

    //    fun send (byteArray:ByteArray){
//        webSocket?.send(ByteString.of(byteArray))
//    }
    interface MySocketListenner {
        fun onConnected(response: Response)
        fun onMessage(string: String)
        fun onClosed(code: Int, reason: String)
        fun onClosing(code: Int, reason: String)
        fun onFailure(t: Throwable, response: Response?)
    }
}