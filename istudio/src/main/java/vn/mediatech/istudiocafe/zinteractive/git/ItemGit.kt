package vn.mediatech.interactive.git

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ItemGit(
        @SerializedName("id") val id: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("foldername") val folderame: String?,
        @SerializedName("price_chip") var priceChip: String?,
        @SerializedName("madv") var madv: String?,
        @SerializedName("typeid") var typeid: String?,
        @SerializedName("typename") val typeName: String?,
        @SerializedName("src") val src: String?,
        @SerializedName("sort") val sort: String?
): Parcelable
/*
"id": 77,
"name": "cooktail 10",
"foldername": "Cooktail",
"image": "77_cooktail_10.png",
"price_chip": 1,
"madv": "GIFT_10_77-1",
"ma_service_click_chip": "GIFT_10_CLICK_77-1",
"typeid": "10",
"nameicon": "cooktail 10",
"version": "1.0.0.2",
"versionint": "2",
"typename": "Cooktail",
"servicekey": "GIFT",
"sort": "9",
"status": "1",
"imagesrc": "https://daugiatruyenhinh.com/upload/Gifts/Cooktail/77_cooktail_10.png",
"src": "https://daugiatruyenhinh.com/upload/Gifts/Cooktail/77_cooktail_10.png",
"time": 1611652957*/
