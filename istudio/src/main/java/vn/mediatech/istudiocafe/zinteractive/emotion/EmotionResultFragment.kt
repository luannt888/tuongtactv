package vn.mediatech.istudiocafe.zinteractive.emotion

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_emotion_result.*
import vn.mediatech.interactive.app.*
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask


class EmotionResultFragment : BottomSheetDialogFragment() {
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var myHttpRequest: MyHttpRequest? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var itemList: ArrayList<ItemEmotionResult> = ArrayList()
    var onItemClickListener: ItemEmotionAdapter.OnItemClickListener? = null
    private val API: String =
        "https://api.daugiatruyenhinh.com/api/GetTotalRate/mediatech"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_emotion_result, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setDimAmount(0f)
        }
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        ratingbar.stepSize = 0.1f
        layoutRoot?.setPadding(layoutRoot.paddingLeft,layoutRoot.paddingTop,layoutRoot.paddingRight, AppUtils.convertDpToPixel(
            Constant.HEIGHT_TAB_BAR_BOTTOM_DP + 10F,requireContext()).toInt())
//    spriteView.start("https://i.imgur.com/loYRir6.png")
    }

    fun initData() {
        var dataGroup: String? = null
        if (arguments != null) {
            val height = requireArguments().getInt(InteractiveConstant.HEIGHT)
            try {
                if (height > 0) {
                    val param: ViewGroup.LayoutParams? = layoutRoot.layoutParams;
//                    param?.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    param?.height = height
                }
            } catch (e: Exception) {
            }
//            var timeEndMs = requireArguments().getLong(InteractiveConstant.TIME, 0)
//            val timeStart = (timeEndMs - System.currentTimeMillis()) / 1000
//            startCountDown(timeStart)
            dataGroup = requireArguments().getString(InteractiveConstant.DATA, null)
        }
        if (dataGroup.isNullOrEmpty()) {
            getData()
        } else {
            handleDataGroup(dataGroup)
        }
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!AppUtils.isNetworkConnect(context)) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
//        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        val requestParams = RequestParams()
        val api = API
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_not_data))
            return
        }
        val responseString = responseStr.trim()
        handleDataGroup(responseString)
    }

    private fun handleDataGroup(responseString: String) {
        val json = JsonParserUtil.getJsonObject(responseString)
        val data = JsonParserUtil.getString(json, "data")
        val star = try {
            JsonParserUtil.getString(json, "Rate_avg")!!.toFloat()
        } catch (e: Exception) {
            0F
        }
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemEmotionResult>>() {}.getType()
        var newList: ArrayList<ItemEmotionResult> = ArrayList()
        try {
            val list: ArrayList<ItemEmotionResult>? = gson.fromJson(data, gsonType)
            if (list != null && list.size > 0)
//                for (item in list) {
//                    item.sprite = ItemSprite(25,1,10,"https://daugiatruyenhinh.com/upload/istudios/emoji/51_Like_(_)_00000.png.png")
//                }
                newList.addAll(list!!)

        } catch (e: Exception) {
            e.printStackTrace()
        }

        activity?.runOnUiThread {
            try {
                if (star > 0) {
                    ratingbar.rating = star
                    textRate?.setText(star.toString())
                }
                if (adapter != null) {
                    recyclerView?.stopScroll()
                    itemList.clear()
                    itemList.addAll(newList)
                    adapter?.notifyDataSetChanged()
                } else {
                    itemList.clear()
                    itemList.addAll(newList)
                    initAdapter()
                }
                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun setData() {
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
        }
    }


    var adapter: ItemEmotionResultAdapter? = null
    private fun initAdapter() {
        val numberColumn = 3
        val numberRow = 2
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.dimen_10)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        adapter = ItemEmotionResultAdapter(requireContext(), itemList, spacePx, numberColumn)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        adapter?.onItemClickListener = object : ItemEmotionResultAdapter.OnItemClickListener {
            override fun onClick(itemGame: ItemEmotionResult, position: Int) {
            }
        }

    }

    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)true
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonClose.setOnClickListener {
            dismiss()
        }
    }


    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }


    override fun onResume() {
        super.onResume()
        try {
            dialog?.getWindow()?.getDecorView()
                ?.setSystemUiVisibility(requireActivity().window!!.decorView!!.systemUiVisibility!!)
        } catch (e: Exception) {
        }
    }

    override fun onStop() {
        super.onStop()
        timer?.cancel()
    }

    var timer: Timer? = null
    fun startCountDown(timeSeconds: Long) {
        if (timeSeconds < 0) {
            return
        }
        timer?.cancel()
        var timeCount = timeSeconds
        timer = Timer()
        timer!!.schedule(timerTask {
            if (timeCount >= 0) {
                val time = timeCount
                activity?.runOnUiThread {
                    textTimeCountDown?.text = AppUtils.convertSecondsToMmSs(time)
                    if (timeCount <= 10) {
                        textTimeCountDown?.setTextColor(Color.YELLOW)
                    } else {
                        textTimeCountDown?.setTextColor(Color.WHITE)
                    }
                    timeCount = timeCount - 1
                }
            } else {
                timer?.cancel()
                activity?.runOnUiThread {
                    dismiss()
                }
            }
        }, 0, 1000)
    }

    override fun onDismiss(dialog: DialogInterface) {
        timer?.cancel()
        onShowDismissListener?.onDismiss()
        super.onDismiss(dialog)
    }
}