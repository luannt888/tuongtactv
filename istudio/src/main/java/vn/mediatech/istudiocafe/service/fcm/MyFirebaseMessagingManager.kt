package vn.mediatech.istudiocafe.service.fcm

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.os.Build
import com.google.firebase.messaging.FirebaseMessaging
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser.Companion.getInt
import vn.mediatech.istudiocafe.util.JsonParser.Companion.getJsonObject
import vn.mediatech.istudiocafe.util.JsonParser.Companion.getString
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import java.io.*

class MyFirebaseMessagingManager(private val mContext: Context) {
    private val fileName = "did.cache"
    private var mRegid: String? = null
    private var deviceId: String? = ""
    private var isUpdate = false

    companion object {
        const val FCM = "FCM"
        const val PROPERTY_ID = "ID"
        const val PROPERTY_FCM_TOKEN = "FCM_TOKEN"
        const val PROPERTY_APP_VERSION = "APP_VERSION"
        const val PROPERTY_NEED_UPDATE_TOKEN = "HAS_UPDATE_TOKEN"
    }

    fun registerFCM(token: String?) {
        if (!isPlayServicesAvailable) {
            return
        }
        Loggers.e("registerFCM_mRegid", "token null")
        if (!token.isNullOrEmpty()) {
            mRegid = getRegistrationId(mContext)
            if (!mRegid.isNullOrEmpty()) {
                if (mRegid == token) {
                    return
                }
                isUpdate = true
            }
            mRegid = token
            sendRegistrationIdToBackend()
            return
        }
        mRegid = getRegistrationId(mContext)
        Loggers.e("registerFCM_mRegid", mRegid + "")
        if (mRegid.isNullOrEmpty()) {
            registerInBackground()
        }
    }

    val isPlayServicesAvailable: Boolean = true

    private fun storeRegistrationId(context: Context, regId: String?, deviceId: String?) {
        val prefs = getFcmPreferences(context)
        val appVersion = MyApplication.getInstance().getVersionCode(context)
        val editor = prefs.edit()
        editor.putString(PROPERTY_ID, deviceId)
        editor.putString(PROPERTY_FCM_TOKEN, regId)
        editor.putInt(PROPERTY_APP_VERSION, appVersion)
        editor.putBoolean(PROPERTY_NEED_UPDATE_TOKEN, true)
        editor.apply()
    }

    private fun  getRegistrationId(context: Context): String? {
        val prefs = getFcmPreferences(context)
        deviceId = prefs.getString(PROPERTY_ID, null)
        return prefs.getString(PROPERTY_FCM_TOKEN, null)
    }

    @SuppressLint("NewApi")
    private fun registerInBackground() {
        if (mContext is Activity) {
            FirebaseMessaging.getInstance().token.addOnCompleteListener {
                if (!it.isSuccessful) {
                    Loggers.e("registerFCM_mRegid_bg", "Cannot get FCM token from Firebase")
                    return@addOnCompleteListener
                }
                mRegid = it.result
                Loggers.e("registerFCM_mRegid_bg", mRegid)
                sendRegistrationIdToBackend()
            }
        }
    }

    private fun getFcmPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(FCM, Context.MODE_PRIVATE)
    }

    private fun sendRegistrationIdToBackend() {
        Loggers.e("registerFCM_mRegid_Backend", mRegid + "")
        if (MyApplication.getInstance().isSendingFcm) {
            return
        }
        val appId = MyApplication.getInstance().appId
        if (appId.isNullOrEmpty()) {
            return
        }
        MyApplication.getInstance().isSendingFcm = true
        val requestParams = RequestParams()
        if (isUpdate) {
            requestParams.put("id", deviceId)
        }
        requestParams.put("device_id", mRegid)
        requestParams.put("os", "Android " + Build.VERSION.RELEASE)
        requestParams.put("name", Build.MODEL)
        requestParams.put("type", Constant.OS_TYPE.toString())
        val myHttpRequest = MyHttpRequest(mContext)
        myHttpRequest.request(true, ServiceUtilTT.validAPIDGTH(mContext, Constant.API_FCM), requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                Loggers.e("registerFCM_mRegid_API", "onFailure statusCode = $statusCode")
                MyApplication.getInstance().isSendingFcm = false
            }

            override fun onSuccess(statusCode: Int, responseString: String) {
                handleData(responseString)
            }
        })
    }

    private fun handleData(responseString: String?) {
        val jsonObject = getJsonObject(responseString)
        if (jsonObject == null) {
            MyApplication.getInstance().isSendingFcm = false
            return
        }
        val errorCode = getInt(jsonObject, Constant.CODE)!!
        if (errorCode != Constant.SUCCESS) {
            MyApplication.getInstance().isSendingFcm = false
            return
        }
        val resultObj = getJsonObject(jsonObject, "result")
        val deviceId = getString(resultObj, "id")
        storeRegistrationId(mContext, mRegid, deviceId)
    }

}