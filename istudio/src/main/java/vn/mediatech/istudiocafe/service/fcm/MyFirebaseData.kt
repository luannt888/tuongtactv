package vn.mediatech.istudiocafe.service.fcm

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import vn.mediatech.istudiocafe.app.Constant

@Parcelize
data class MyFirebaseData(
        @SerializedName("id") var id: String = "",
        @SerializedName("title") var title: String? = "",
        @SerializedName("body") var body: String? = "",
        @SerializedName("icon") var icon: String? = "",
        @SerializedName("cardType") var cardType: String? = "",
        @SerializedName("type") var type: String? = "",
        @SerializedName("img") var img: String? = "",
        @SerializedName("time") var time: Long = 0,
        @SerializedName("date") var date: String? = "",
        @SerializedName("data") var data: String? = "",
        @SerializedName("type_voucher") var voucherType: Int = Constant.TYPE_NOTIFY_VOUCHER_DONATE
) : Parcelable