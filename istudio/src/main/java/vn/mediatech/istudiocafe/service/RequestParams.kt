package vn.mediatech.istudiocafe.service

import vn.mediatech.istudiocafe.model.ItemSimpleParam
import java.util.concurrent.ConcurrentHashMap

class RequestParams {
    var urlParams = ConcurrentHashMap<String, String>()

    fun put(key: String?, value: String?) {
        if (key != null && value != null) {
            urlParams[key] = value
        }
    }

    fun clear() {
        urlParams.clear()
    }

    fun getParamList() : ArrayList<ItemSimpleParam> {
        val itemList: ArrayList<ItemSimpleParam> = ArrayList()
        for ((key, value) in urlParams) {
            itemList.add(ItemSimpleParam(key, value))
        }
        return itemList
    }

    fun size(): Int {
        return urlParams.size
    }

    fun addParams(requestParams: RequestParams?) {
        if (requestParams == null || requestParams.size() == 0) {
            return
        }
        urlParams.putAll(requestParams.urlParams)
    }
}