package vn.mediatech.istudiocafe.service.fcm

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.*
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.HandleNotificationActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.fcm.MyFirebaseMessagingService.DownloadImageBitmapFromUrl.DownloadImageBitmapFromUrlListener
import vn.mediatech.istudiocafe.util.EncryptUtil
import vn.mediatech.istudiocafe.util.SharedPreferencesManager
import vn.mediatech.istudiocafe.util.TextUtil
import vn.mediatech.istudiocafe.voucher.ItemVoucher
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemChat
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class MyFirebaseMessagingService : FirebaseMessagingService() {
    var mNotificationManager: NotificationManager? = null
    lateinit var myFirebaseData: MyFirebaseData
    val CHANNEL_ID = "MyAppNotification"
    var CHANNEL_NAME = "App"
    var NOTIFICATION_ID = 1
    var itemVoucher: ItemVoucher? = null
    var itemChat: ItemChat? = null


    override fun onNewToken(s: String) {
        super.onNewToken(s)
        Loggers.e("registerFCM_mRegid_onNewToken", s)
        try {
            val myFirebaseMessagingManager = MyFirebaseMessagingManager(this)
            myFirebaseMessagingManager.registerFCM(s)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        /*Loggers.e("myFirebaseData_onMessageReceived_A", remoteMessage.getFrom());
        if (remoteMessage.getData().size() > 0) {
            Loggers.e("myFirebaseData_onMessageReceived_B", "" + remoteMessage.getData());
        }
        if (remoteMessage.getNotification() != null) {
            Loggers.e("myFirebaseData_onMessageReceived_C", remoteMessage.getNotification().getBody());
        }*/
        /*if (!SharedPreferencesManager.isReceiveNotification(this)) {
            return;
        }*/
        if (remoteMessage.data.isEmpty()) {
            return
        }
        val map = remoteMessage.data
        Loggers.e("FCM_onMessageReceived", "$map")
        myFirebaseData = MyFirebaseData()
        for (key in map.keys) {
            if (key.isNullOrEmpty()) {
                continue
            }
            val value = map[key]
            if (key.equals("card_type")) {
                myFirebaseData.cardType = value
                continue
            }
            if (key.equals("type")) {
                myFirebaseData.type = value
                continue
            }
            if (key.equals("title")) {
                myFirebaseData.title = value
                continue
            }
            if (key.equals("body")) {
                myFirebaseData.body = value
                continue
            }
            if (key.equals("icon")) {
                myFirebaseData.icon = value
                continue
            }
            if (key.equals("news_id")) {
                myFirebaseData.id = value!!
                continue
            }
            if (key.equals("img")) {
                myFirebaseData.img = value
                continue
            }
            if (key.equals("data")) {
                myFirebaseData.data = value
                continue
            }
            if (key.equals("type_voucher")) {
                myFirebaseData.voucherType = if (value.isNullOrEmpty()) Constant.TYPE_NOTIFY_VOUCHER_DONATE else value.toInt()
                continue
            }
        }
        CHANNEL_NAME = getString(R.string.app_name)
        prepareShowNotify()
    }

    private var iconBitmap: Bitmap? = null
    private var imgBitmap: Bitmap? = null

    internal interface OnLoadImageListener {
        fun onResourceReady(resource: Bitmap?)
    }

    private fun prepareShowNotify() {
//        Loggers.e("myFirebaseData", myFirebaseData.toJSON());
        if (MyApplication.getInstance().isEmpty(myFirebaseData.title)) {
            return
        }
        if (!myFirebaseData.type.isNullOrEmpty() && (myFirebaseData.type.equals(Constant.TYPE_VOUCHER) || myFirebaseData.type.equals(
                Constant.TYPE_CHAT
            ))
        ) {
            if (SharedPreferencesManager.getAccessToken(this).isNullOrEmpty()) {
                return
            }
            if (!myFirebaseData.data.isNullOrEmpty()) {
                val gson = Gson()

                if (myFirebaseData.type.equals(Constant.TYPE_VOUCHER)) {
                    try {
                        val data: String = EncryptUtil.base64Decode(myFirebaseData.data!!)
                        Loggers.e("FCM_onMessageReceived_data", data)
                        itemVoucher = gson.fromJson(data, ItemVoucher::class.java)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (myFirebaseData.type.equals(Constant.TYPE_CHAT)) {
                    try {
//                    val data: String = EncryptUtil.base64Decode(myFirebaseData.data!!)
                        val data: String = myFirebaseData.data!!
                        Loggers.e("FCM_onMessageReceived_data", data)
                        itemChat = gson.fromJson(data, ItemChat::class.java)
                    } catch (e: Exception) {
                        itemChat = null
                        e.printStackTrace()
                    }
                }
            }
            if (itemChat != null && (itemChat!!.type == Constant.TYPE_ADMIN)) {
                return
            }
            if (itemVoucher != null) {
                myFirebaseData.img = itemVoucher!!.image
            }
            if (MainActivity.instance) {
                if (MyApplication.getInstance().dataManager.isRunBackground) {
                    var itemObj: Parcelable? = null
                    when (myFirebaseData.voucherType) {
                        Constant.TYPE_NOTIFY_VOUCHER_DONATE -> itemObj = itemVoucher
                        Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT -> itemObj = myFirebaseData
                    }
                    MyApplication.getInstance().dataManager.itemParcelable = itemObj
                    MyApplication.getInstance().dataManager.typeNotifyVoucherKey =
                        myFirebaseData.voucherType
                } else {
                    if (myFirebaseData.type.equals(Constant.TYPE_CHAT)) {
                        val item = MyApplication.getInstance().dataManager.itemConversations;
                        if (MyApplication.getInstance().dataManager.isChat || (itemChat != null && ((item != null && item.userId.equals(
                                itemChat!!.userId
                            ) && item.voucherId.equals(itemChat!!.voucherId)) || itemChat!!.type == Constant.TYPE_ADMIN))
                        ) {
                            return
                        }
                    }
                    if (myFirebaseData.type.equals(Constant.TYPE_VOUCHER)) {
                        val bundle = Bundle()
                        when (myFirebaseData.voucherType) {
                            Constant.TYPE_NOTIFY_VOUCHER_DONATE -> bundle.putParcelable(
                                Constant.DATA,
                                itemVoucher
                            )
                            Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT -> bundle.putParcelable(
                                Constant.DATA,
                                myFirebaseData
                            )
                        }
                        bundle.putInt(Constant.TYPE_NOTIFY_VOUCHER_KEY, myFirebaseData.voucherType)
                        val intent = Intent(Constant.VOUCHER_TYPE)
                        intent.putExtras(bundle)
                        sendBroadcast(intent)
                        return
                    }
                }
            }
        }

        if (!MyApplication.getInstance().isEmpty(myFirebaseData.icon)) {
            getImageFromUrl(myFirebaseData.icon, object : OnLoadImageListener {
                override fun onResourceReady(resource: Bitmap?) {
                    iconBitmap = resource
                    if (!MyApplication.getInstance().isEmpty(myFirebaseData.img)) {
                        getImageFromUrl(myFirebaseData.img, object : OnLoadImageListener {
                            override fun onResourceReady(resource: Bitmap?) {
                                imgBitmap = resource
                                checkShowNotify()
                            }
                        })
                    } else {
                        checkShowNotify()
                    }
                }
            })
            return
        }
        if (!MyApplication.getInstance().isEmpty(myFirebaseData.img)) {
            getImageFromUrl(myFirebaseData.img, object : OnLoadImageListener {
                override fun onResourceReady(resource: Bitmap?) {
                    imgBitmap = resource
                    checkShowNotify()
                }
            })
        } else {
            checkShowNotify()
        }
    }

    private fun checkShowNotify() {
        try {
            showNotify()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        iconBitmap = null
    }

    private fun showNotify() {
        val title = TextUtil.getHtmlFormat(myFirebaseData.title).toString().trim()
        var msg = myFirebaseData.body
        if (msg == null) {
            msg = ""
        }
        msg = TextUtil.getHtmlFormat(msg).toString()
        //        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        NotificationManager notificationManager = getSystemService(NotificationManager.class);
        mNotificationManager = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getSystemService(NotificationManager::class.java)
        } else {
            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        }
        val vibrate = longArrayOf(1000, 1000, 1000)
        val smallIcon =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) R.mipmap.ic_app_notification else R.mipmap.ic_app_notification_color
        val builder = NotificationCompat.Builder(
            this, CHANNEL_ID
        )
            .setSmallIcon(smallIcon)
            .setContentTitle(title)
            .setContentText(msg).setAutoCancel(true)
            .setLights(Color.BLUE, 500, 500).setVibrate(vibrate)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setColor(ContextCompat.getColor(this, R.color.color_primary_tt))
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setPriority(NotificationManager.IMPORTANCE_HIGH)
        } else {
            builder.setPriority(Notification.PRIORITY_MAX)
        }
        if (iconBitmap != null) {
            builder.setLargeIcon(iconBitmap);
        }
        /*  if (imgBitmap != null) {
              builder.setLargeIcon(imgBitmap)
          }*/
        if (!msg.isEmpty()) {
            builder.setStyle(
                NotificationCompat.BigTextStyle().bigText(msg).setBigContentTitle(title)
            )
        }
        //START: Set large image
        if (imgBitmap != null) {
            val notiStyle = NotificationCompat.BigPictureStyle()
            notiStyle.bigLargeIcon(iconBitmap)
            notiStyle.bigPicture(imgBitmap)
            //            notiStyle.setSummaryText(msg);
            builder.setStyle(notiStyle)
        }
        //END: Set large image
        /*if (imgBitmap != null) {
            NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
//            notiStyle.bigPicture(imgBitmap);
            notiStyle.bigLargeIcon(imgBitmap);
            notiStyle.setSummaryText(msg);
            builder.setStyle(notiStyle);
        } else {
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
        }*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(
                CHANNEL_ID, CHANNEL_NAME,
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationChannel.enableLights(true)
            notificationChannel.enableVibration(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
            mNotificationManager!!.createNotificationChannel(notificationChannel)
        }
        var intent: Intent? = null
        var pendingIntent: PendingIntent? = null
        val cardType = myFirebaseData.cardType
        if (cardType == Constant.TYPE_UPDATE_APP) {
            val url = "market://details?id=$packageName"
            intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            //            intent.setAction(Long.toString(System.currentTimeMillis()));
            pendingIntent = PendingIntent.getActivity(
                this, 0,
                intent, PendingIntent.FLAG_ONE_SHOT
            )
        } else if (cardType == Constant.TYPE_OPEN_APP || cardType == Constant.TYPE_VOUCHER || cardType == Constant.TYPE_VIDEO || cardType == Constant.TYPE_NEWS || cardType == Constant.TYPE_CHAT || cardType == Constant.TYPE_INTERACTIVE || cardType == Constant.TYPE_FORUM) {
            intent = Intent(this, HandleNotificationActivity::class.java)
            //            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.action = cardType
            val bundle = Bundle()
            when (cardType) {
                Constant.TYPE_VOUCHER -> {
                        when (myFirebaseData.voucherType) {
                            Constant.TYPE_NOTIFY_VOUCHER_DONATE -> bundle.putParcelable(
                                Constant.DATA,
                                itemVoucher
                            )
                            Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT -> bundle.putParcelable(
                                Constant.DATA,
                                myFirebaseData
                            )
                             else ->{
                                 bundle.putString(Constant.TYPE_SHOW_NOTIFY, cardType)
                             }
                        }
                        intent.action = Constant.VOUCHER_TYPE
                        bundle.putInt(Constant.TYPE_NOTIFY_VOUCHER_KEY, myFirebaseData.voucherType)
                }

                Constant.TYPE_VIDEO, Constant.TYPE_NEWS -> {
                    val itemObj = ItemNews()
                    itemObj.id = myFirebaseData.id
                    itemObj.type = cardType
                    itemObj.contentType = cardType
                    itemObj.cardType = myFirebaseData.type
                    bundle.putParcelable(Constant.DATA, itemObj)
                }

                Constant.TYPE_CHAT -> {
                    if (itemChat != null) {
                        bundle.putParcelable(Constant.DATA, itemChat)
                    }
                }
                Constant.TYPE_INTERACTIVE, Constant.TYPE_FORUM -> {
                    bundle.putString(Constant.TYPE_SHOW_NOTIFY, cardType)
                }

            }
            intent.putExtras(bundle)
//            intent.action = System.currentTimeMillis().toString()

            val stackBuilder = TaskStackBuilder.create(this)
            stackBuilder.addParentStack(HandleNotificationActivity::class.java)
            stackBuilder.addNextIntent(intent)

//            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
            pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        }
        if (intent == null || pendingIntent == null) {
            return
        }
        builder.setContentIntent(pendingIntent)
        mNotificationManager!!.notify(NOTIFICATION_ID, builder.build())
        NOTIFICATION_ID++
    }

    @SuppressLint("NewApi")
    private fun getImageFromUrl(url: String?, onLoadImageListener: OnLoadImageListener?) {
        try {
            val policy = StrictMode.ThreadPolicy.Builder()
                .permitAll().build()
            StrictMode.setThreadPolicy(policy)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val downloadImageBitmapFromUrl = DownloadImageBitmapFromUrl(url)
        downloadImageBitmapFromUrl.downloadImageBitmapFromUrlListener =
            object : DownloadImageBitmapFromUrlListener {
                override fun onPostExecute(bitmap: Bitmap?) {
                    onLoadImageListener?.onResourceReady(bitmap)
                }
            }
        downloadImageBitmapFromUrl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
    }

    class DownloadImageBitmapFromUrl(val imageUrl: String?) : AsyncTask<Void, Void, Bitmap>() {
        var downloadImageBitmapFromUrlListener: DownloadImageBitmapFromUrlListener? = null

        override fun doInBackground(vararg params: Void?): Bitmap? {
            val inputStream: InputStream
            try {
                val url = URL(imageUrl)
                val connection = url
                    .openConnection() as HttpURLConnection
                connection.doInput = true
                connection.connectTimeout = 20000
                connection.connect()
                inputStream = connection.inputStream
                return BitmapFactory.decodeStream(inputStream)
            } catch (e: MalformedURLException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

        override fun onPostExecute(bitmap: Bitmap?) {
            super.onPostExecute(bitmap)
            if (downloadImageBitmapFromUrlListener != null) {
                downloadImageBitmapFromUrlListener!!.onPostExecute(bitmap)
            }
        }

        interface DownloadImageBitmapFromUrlListener {
            fun onPostExecute(bitmap: Bitmap?)
        }
    }
}
//class MyFirebaseMessagingService : FirebaseMessagingService() {
//    var mNotificationManager: NotificationManager? = null
//    lateinit var myFirebaseData: MyFirebaseData
//    val CHANNEL_ID = "MyAppNotification"
//    var CHANNEL_NAME = "App"
//    var NOTIFICATION_ID = 1
//    var itemVoucher: ItemVoucher? = null
//
//    companion object {
//        const val VOUCHER = "voucher"
//        const val CHAT = "chat"
//    }
//
//    override fun onNewToken(s: String) {
//        super.onNewToken(s)
//        Loggers.e("registerFCM_mRegid_onNewToken", s)
//        try {
//            val myFirebaseMessagingManager = MyFirebaseMessagingManager(this)
//            myFirebaseMessagingManager.registerFCM(s)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    override fun onMessageReceived(remoteMessage: RemoteMessage) {
//        super.onMessageReceived(remoteMessage)
//        /*Loggers.e("myFirebaseData_onMessageReceived_A", remoteMessage.getFrom());
//        if (remoteMessage.getData().size() > 0) {
//            Loggers.e("myFirebaseData_onMessageReceived_B", "" + remoteMessage.getData());
//        }
//        if (remoteMessage.getNotification() != null) {
//            Loggers.e("myFirebaseData_onMessageReceived_C", remoteMessage.getNotification().getBody());
//        }*/
//        /*if (!SharedPreferencesManager.isReceiveNotification(this)) {
//            return;
//        }*/
//        if (remoteMessage.data.isEmpty()) {
//            return
//        }
//        val map = remoteMessage.data
//        Loggers.e("FCM_onMessageReceived", "$map")
//        myFirebaseData = MyFirebaseData()
//        for (key in map.keys) {
//            if (key.isNullOrEmpty()) {
//                continue
//            }
//            val value = map[key]
//            if (key.equals("card_type")) {
//                myFirebaseData.cardType = value
//                continue
//            }
//            if (key.equals("type")) {
//                myFirebaseData.type = value
//                continue
//            }
//            if (key.equals("title")) {
//                myFirebaseData.title = value
//                continue
//            }
//            if (key.equals("body")) {
//                myFirebaseData.body = value
//                continue
//            }
//            if (key.equals("icon")) {
//                myFirebaseData.icon = value
//                continue
//            }
//            if (key.equals("news_id")) {
//                myFirebaseData.id = value!!
//                continue
//            }
//            if (key.equals("img")) {
//                myFirebaseData.img = value
//                continue
//            }
//            if (key.equals("data")) {
//                myFirebaseData.data = value
//                continue
//            }
//            if (key.equals("type_voucher")) {
//                myFirebaseData.voucherType = if (value.isNullOrEmpty()) 1 else value.toInt()
//                continue
//            }
//        }
//        CHANNEL_NAME = getString(R.string.app_name)
//        prepareShowNotify()
//    }
//
//    private var iconBitmap: Bitmap? = null
//    private var imgBitmap: Bitmap? = null
//
//    internal interface OnLoadImageListener {
//        fun onResourceReady(resource: Bitmap?)
//    }
//
//    private fun prepareShowNotify() {
////        Loggers.e("myFirebaseData", myFirebaseData.toJSON());
//        if (MyApplication.getInstance().isEmpty(myFirebaseData.title)) {
//            return
//        }
//        if (!myFirebaseData.type.isNullOrEmpty() && (myFirebaseData.type.equals(VOUCHER)||myFirebaseData.type.equals(CHAT))) {
//            if(SharedPreferencesManager.getToken(this).isNullOrEmpty()){
//                return
//            }
//            if (!myFirebaseData.data.isNullOrEmpty()) {
//                val gson = Gson()
//                try {
//                    val data: String = EncryptUtil.base64Decode(myFirebaseData.data!!)
//                    Loggers.e("FCM_onMessageReceived_data", data)
//                    itemVoucher = gson.fromJson(data, ItemVoucher::class.java)
//                } catch (e: Exception) {
//                    e.printStackTrace()
//                }
//            }
//            if (itemVoucher != null) {
//                myFirebaseData.img = itemVoucher!!.image
//            }
//            if (MainActivity.instance) {
////<<<<<<< HEAD
////                //Send broadcast
////                val bundle = Bundle()
////                val typeNotify = myFirebaseData.typeVoucher
////                if (typeNotify != null && typeNotify!!.trim().toInt() == Constant.TYPE_NOTIFY_VOUCHER_DONATE) {
////                    bundle.putParcelable(Constant.DATA, itemVoucher)
////                }
////                if (typeNotify != null && typeNotify!!.trim().toInt() == Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT) {
////                    bundle.putParcelable(Constant.FIREBASE_DATA, myFirebaseData)
////                }
////                bundle.putBoolean(Constant.NOTIFY, true)
////                bundle.putBoolean(Constant.IS_VOUCHER, true)
////                bundle.putString(Constant.TYPE_NOTIFY_VOUCHER_KEY, myFirebaseData.typeVoucher)
////                val intent = Intent(Constant.VOUCHER_TYPE)
////                intent.putExtras(bundle)
////                sendBroadcast(intent)
////                return
////                /*if (!MainActivity.instance) {
////                    val intent = Intent(this, MainActivity::class.java)
////=======
//                if (MyApplication.getInstance().dataManager.isRunBackground) {
//                    var itemObj: Parcelable? = null
//                    when (myFirebaseData.voucherType) {
//                        Constant.TYPE_NOTIFY_VOUCHER_DONATE -> itemObj = itemVoucher
//                        Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT -> itemObj = myFirebaseData
//                    }
//                    MyApplication.getInstance().dataManager.itemVoucherDonated = itemObj
//                    MyApplication.getInstance().dataManager.typeNotifyVoucherKey = myFirebaseData.voucherType
//                } else {
//                    val bundle = Bundle()
//                    when (myFirebaseData.voucherType) {
//                        Constant.TYPE_NOTIFY_VOUCHER_DONATE -> bundle.putParcelable(Constant.DATA, itemVoucher)
//                        Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT -> bundle.putParcelable(Constant.DATA, myFirebaseData)
//                    }
//                    bundle.putBoolean(Constant.NOTIFY, true)
//                    bundle.putBoolean(Constant.IS_VOUCHER, true)
//                    bundle.putInt(Constant.TYPE_NOTIFY_VOUCHER_KEY, myFirebaseData.voucherType)
//                    val intent = Intent(Constant.VOUCHER_TYPE)
////>>>>>>> master_interactive
//                    intent.putExtras(bundle)
//                    sendBroadcast(intent)
//                }
//            }
//            //show notification
//        }
//
//        if (!MyApplication.getInstance().isEmpty(myFirebaseData.icon)) {
//            getImageFromUrl(myFirebaseData.icon, object : OnLoadImageListener {
//                override fun onResourceReady(resource: Bitmap?) {
//                    iconBitmap = resource
//                    if (!MyApplication.getInstance().isEmpty(myFirebaseData.img)) {
//                        getImageFromUrl(myFirebaseData.img, object : OnLoadImageListener {
//                            override fun onResourceReady(resource: Bitmap?) {
//                                imgBitmap = resource
//                                checkShowNotify()
//                            }
//                        })
//                    } else {
//                        checkShowNotify()
//                    }
//                }
//            })
//            return
//        }
//        if (!MyApplication.getInstance().isEmpty(myFirebaseData.img)) {
//            getImageFromUrl(myFirebaseData.img, object : OnLoadImageListener {
//                override fun onResourceReady(resource: Bitmap?) {
//                    imgBitmap = resource
//                    checkShowNotify()
//                }
//            })
//        } else {
//            checkShowNotify()
//        }
//    }
//
//    private fun checkShowNotify() {
//        try {
//            showNotify()
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        iconBitmap = null
//    }
//
//    private fun showNotify() {
//        val title = TextUtil.getHtmlFormat(myFirebaseData.title).toString().trim()
//        var msg = myFirebaseData.body
//        if (msg == null) {
//            msg = ""
//        }
//        msg = TextUtil.getHtmlFormat(msg).toString()
//        //        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
////        NotificationManager notificationManager = getSystemService(NotificationManager.class);
//        mNotificationManager = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            getSystemService(NotificationManager::class.java)
//        } else {
//            getSystemService(NOTIFICATION_SERVICE) as NotificationManager
//        }
//        val vibrate = longArrayOf(1000, 1000, 1000)
//        val smallIcon = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) R.mipmap.ic_app_notification else R.mipmap.ic_app_notification_color
//        val builder = NotificationCompat.Builder(
//                this, CHANNEL_ID)
//                .setSmallIcon(smallIcon)
//                .setContentTitle(title)
//                .setContentText(msg).setAutoCancel(true)
//                .setLights(Color.BLUE, 500, 500).setVibrate(vibrate)
//                .setDefaults(Notification.DEFAULT_SOUND)
//                .setColor(ContextCompat.getColor(this, R.color.color_primary))
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//            builder.setPriority(NotificationManager.IMPORTANCE_HIGH)
//        } else {
//            builder.setPriority(Notification.PRIORITY_MAX)
//        }
//        if (iconBitmap != null) {
//            builder.setLargeIcon(iconBitmap);
//        }
//      /*  if (imgBitmap != null) {
//            builder.setLargeIcon(imgBitmap)
//        }*/
//        if (!msg.isEmpty()) {
//            builder.setStyle(NotificationCompat.BigTextStyle().bigText(msg).setBigContentTitle(title))
//        }
//        //START: Set large image
//        if (imgBitmap != null) {
//            val notiStyle = NotificationCompat.BigPictureStyle()
//            notiStyle.bigLargeIcon(iconBitmap)
//            notiStyle.bigPicture(imgBitmap)
//            //            notiStyle.setSummaryText(msg);
//            builder.setStyle(notiStyle)
//        }
//        //END: Set large image
//        /*if (imgBitmap != null) {
//            NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
////            notiStyle.bigPicture(imgBitmap);
//            notiStyle.bigLargeIcon(imgBitmap);
//            notiStyle.setSummaryText(msg);
//            builder.setStyle(notiStyle);
//        } else {
//            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
//        }*/
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            val notificationChannel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME,
//                    NotificationManager.IMPORTANCE_DEFAULT)
//            notificationChannel.enableLights(true)
//            notificationChannel.enableVibration(true)
//            notificationChannel.lightColor = Color.BLUE
//            notificationChannel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
//            mNotificationManager!!.createNotificationChannel(notificationChannel)
//        }
//        var intent: Intent? = null
//        var pendingIntent: PendingIntent? = null
//        val cardType = myFirebaseData.cardType
//        if (cardType == Constant.TYPE_UPDATE_APP) {
//            val url = "market://details?id=$packageName"
//            intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
//            //            intent.setAction(Long.toString(System.currentTimeMillis()));
//            pendingIntent = PendingIntent.getActivity(this, 0,
//                    intent, PendingIntent.FLAG_ONE_SHOT)
//        } else if (cardType == Constant.TYPE_OPEN_APP || cardType == Constant.TYPE_VIDEO || cardType == Constant.TYPE_NEWS) {
//            intent = Intent(this, HandleNotificationActivity::class.java)
//            //            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
////            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
//
//            val bundle = Bundle()
//            if (itemVoucher != null) {
//                bundle.putParcelable(Constant.DATA, itemVoucher)
//                when (myFirebaseData.voucherType) {
//                    Constant.TYPE_NOTIFY_VOUCHER_DONATE -> bundle.putParcelable(Constant.DATA, itemVoucher)
//                    Constant.TYPE_NOTIFY_VOUCHER_DONATE_RESULT -> bundle.putParcelable(Constant.DATA, myFirebaseData)
//                }
//                bundle.putBoolean(Constant.NOTIFY, true)
//                bundle.putBoolean(Constant.IS_VOUCHER, true)
//                bundle.putInt(Constant.TYPE_NOTIFY_VOUCHER_KEY, myFirebaseData.voucherType)
//            } else {
//                if (cardType == Constant.TYPE_VIDEO || cardType == Constant.TYPE_NEWS) {
////                ItemNews itemObj = new ItemNews(myFirebaseData.getNewsId(), "", "", "", "", "", "", "", cardType, "", "", "", "");
//                    val itemObj = ItemNews()
//                    itemObj.id = myFirebaseData.id
//                    itemObj.type = cardType
//                    itemObj.contentType = cardType
//                    itemObj.cardType = myFirebaseData.type
//                    bundle.putParcelable(Constant.DATA, itemObj)
//                }
//            }
//            intent.putExtras(bundle)
//            intent.action = System.currentTimeMillis().toString()
//
//            val stackBuilder = TaskStackBuilder.create(this)
//            stackBuilder.addParentStack(HandleNotificationActivity::class.java)
//            stackBuilder.addNextIntent(intent)
//
////            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
////            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
//            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
//        }
//        if (intent == null || pendingIntent == null) {
//            return
//        }
//        builder.setContentIntent(pendingIntent)
//        mNotificationManager!!.notify(NOTIFICATION_ID, builder.build())
//        NOTIFICATION_ID++
//    }
//
//    @SuppressLint("NewApi")
//    private fun getImageFromUrl(url: String?, onLoadImageListener: OnLoadImageListener?) {
//        try {
//            val policy = StrictMode.ThreadPolicy.Builder()
//                    .permitAll().build()
//            StrictMode.setThreadPolicy(policy)
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        val downloadImageBitmapFromUrl = DownloadImageBitmapFromUrl(url)
//        downloadImageBitmapFromUrl.downloadImageBitmapFromUrlListener = object : DownloadImageBitmapFromUrlListener {
//            override fun onPostExecute(bitmap: Bitmap?) {
//                onLoadImageListener?.onResourceReady(bitmap)
//            }
//        }
//        downloadImageBitmapFromUrl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
//    }
//
//    class DownloadImageBitmapFromUrl(val imageUrl: String?) : AsyncTask<Void, Void, Bitmap>() {
//        var downloadImageBitmapFromUrlListener: DownloadImageBitmapFromUrlListener? = null
//
//        override fun doInBackground(vararg params: Void?): Bitmap? {
//            val inputStream: InputStream
//            try {
//                val url = URL(imageUrl)
//                val connection = url
//                        .openConnection() as HttpURLConnection
//                connection.doInput = true
//                connection.connectTimeout = 20000
//                connection.connect()
//                inputStream = connection.inputStream
//                return BitmapFactory.decodeStream(inputStream)
//            } catch (e: MalformedURLException) {
//                e.printStackTrace()
//            } catch (e: IOException) {
//                e.printStackTrace()
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//            return null
//        }
//
//        override fun onPostExecute(bitmap: Bitmap?) {
//            super.onPostExecute(bitmap)
//            if (downloadImageBitmapFromUrlListener != null) {
//                downloadImageBitmapFromUrlListener!!.onPostExecute(bitmap)
//            }
//        }
//
//        interface DownloadImageBitmapFromUrlListener {
//            fun onPostExecute(bitmap: Bitmap?)
//        }
//    }
//}