package vn.mediatech.istudiocafe.app

import android.os.Parcelable
import vn.mediatech.istudiocafe.model.ItemAppConfig
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.voucher.chatsupport.ItemConversation

class DataManager {
    var itemAppConfig: ItemAppConfig? = null
    var itemUserTemp: ItemUser? = null
    var itemUser: ItemUser? = null
    var isNeedRefreshLeftMenu: Boolean = false
    var itemCartList: ArrayList<ItemNews> = ArrayList()
    var isRunBackground = false
    var itemParcelable: Parcelable? = null
    var typeNotifyVoucherKey: Int = Constant.TYPE_NOTIFY_VOUCHER_DONATE
    var isChat = false
    var itemConversations: ItemConversation? = null
    var typeShowNotify: String? = null
}