package vn.mediatech.istudiocafe.fragment

import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Outline
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.method.LinkMovementMethod
import android.view.*
import android.widget.LinearLayout
import androidx.fragment.app.DialogFragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.layout_dialog_popup.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.listener.OnPlayerStateChangeListener
import vn.mediatech.istudiocafe.util.GeneralUtils
import vn.mediatech.istudiocafe.util.ScreenSize
import java.util.*
import kotlin.concurrent.timerTask


class MyDialogFragment() : DialogFragment() {
    private var playerFragment: PlayerFragment? = null
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var title: String? = null
    var image: String? = null
    var video: String? = null
    var message: String? = null
    var leftButtonTitle: String? = null
    var rightButtonTitle: String? = null
    var onDialogButtonListener: OnDialogButtonListener? = null
    var width: Int = 0
    var timeSkipSecond = 0
    var bitmap: Bitmap? = null
    var cancelAble: Boolean = false
    var cancelTouchOutsite = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
//        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.layout_dialog_popup, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setStyle(STYLE_NO_TITLE,R.style.MyDialogPopUp)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val lp = dialog?.window?.attributes
        //        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
//        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp?.gravity = Gravity.CENTER
        lp?.windowAnimations = R.style.DialogAnimationLeftRight
        val screenSize = ScreenSize(requireActivity())
        width = screenSize.width * 9 / 10
        dialog?.window?.setLayout(
            width,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        isViewCreated = true
        checkRefresh()
        return
    }

    override fun onStart() {
        super.onStart()
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        layoutRoot.clipToOutline = true
        layoutRoot.outlineProvider = object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadiusDP = 15f
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadiusDP)
            }
        }
    }

    fun initData() {
        title = requireArguments().getString(Constant.TITLTE)
        message = requireArguments().getString(Constant.MESSAGE)
        video = requireArguments().getString(Constant.VIDEO)
        image = requireArguments().getString(Constant.IMAGE)
        bitmap = requireArguments().getParcelable(Constant.BITMAP)
        leftButtonTitle = requireArguments().getString(Constant.LEFT_BUTTON)
        rightButtonTitle = requireArguments().getString(Constant.RIGHT_BUTTON)
        timeSkipSecond = requireArguments().getInt(Constant.TIME_SKIP, 0)
        cancelTouchOutsite = requireArguments().getBoolean(Constant.CANCEL_TOUCH_OUTSITE, false)
        cancelAble = requireArguments().getBoolean(Constant.CANCELABLE, false)
        dialog?.setCanceledOnTouchOutside(
            cancelTouchOutsite
        )
        dialog?.setCancelable(cancelAble)
        if (!title.isNullOrEmpty()) {
            textTitle.text = title
            textTitle.visibility = View.VISIBLE
        }
        if (!image.isNullOrEmpty()) {
            imageView.visibility = View.VISIBLE
            GeneralUtils.loadImagefitImageview(activity, image, imageView)
        }
        if (bitmap != null) {
            imageView.visibility = View.VISIBLE
            val lp: ViewGroup.LayoutParams = imageView.layoutParams
            val widthFr = width - layoutRoot.paddingStart - layoutRoot.paddingEnd
            lp.width = widthFr
            lp.height = lp.width * bitmap!!.height / bitmap!!.width
            imageView.layoutParams = lp
            imageView.setImageBitmap(bitmap)
        }
        if (!video.isNullOrEmpty()) {
            frameLayoutDialogtt.visibility = View.VISIBLE
            val widthFr = width - layoutRoot.paddingStart - layoutRoot.paddingEnd
            val height = widthFr * 9 / 16
            val params: LinearLayout.LayoutParams =
                frameLayoutDialogtt.layoutParams as LinearLayout.LayoutParams
            params.height = height
            params.width = width
            frameLayoutDialogtt.layoutParams = params
            val fragmentManager = childFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            playerFragment = PlayerFragment()
            val bundle = Bundle()
            bundle.putString(Constant.DATA, video)
//        bundle.putString(Constant.YOUTUBE_URL, itemObj!!.youtubeUrl)
            bundle.putString(Constant.TYPE, Constant.TYPE_VIDEO)
            bundle.putBoolean(Constant.IS_TAB_SELECTED, true)
            bundle.putBoolean(Constant.IS_RUN_PLAY, true)
            bundle.putBoolean(Constant.IS_LIVE_STREAM, false)
            bundle.putBoolean(Constant.PLAY_WHEN_READY, true)
//                bundle.putInt(Constant.FRAME_PLAYER_HEIGHT, height)
            bundle.putInt(Constant.TIME_START_POSITION, 0)
            playerFragment?.setArguments(bundle)
            fragmentTransaction.add(frameLayoutDialogtt.id, playerFragment!!)
            fragmentTransaction.commit()
            playerFragment?.setAllowRunBackground(false)
        }

        if (!message.isNullOrEmpty()) {
//            textMessage.setText(message);
            GeneralUtils.setHtmlTextView(message, textMessage)
            textMessage.movementMethod = LinkMovementMethod.getInstance()
        }
        if (leftButtonTitle.isNullOrEmpty()) {
            buttonLeft.visibility = View.GONE
            //            viewDivider.setVisibility(View.GONE);
        } else {
            buttonLeft.text = leftButtonTitle
            buttonLeft.visibility = View.VISIBLE
        }
        if (rightButtonTitle.isNullOrEmpty()) {
            buttonRight.visibility = View.GONE
            //            viewDivider.setVisibility(View.GONE);
        } else {
            buttonRight.text = rightButtonTitle
            buttonRight.visibility = View.VISIBLE
        }
        if (timeSkipSecond > 0) {
            dialog?.setCanceledOnTouchOutside(false)
            dialog?.setCancelable(false)
            buttonLeft.isEnabled = false
            textTimeCountDown?.visibility = View.VISIBLE
            if (playerFragment != null) {
                playerFragment?.setOnPlayerStateChangeListener(object :
                    OnPlayerStateChangeListener {
                    override fun onStateEnd() {
                    }

                    override fun onStateReady() {
                        coutdown()
                    }

                    override fun onPlayerError() {
                        TODO("Not yet implemented")
                    }
                })
            } else {
                coutdown()
            }
        }
    }


    fun initControl() {
        buttonLeft.setOnClickListener {
            dismiss()
            onDialogButtonListener?.onLeftButtonClick()
        }
        buttonRight.setOnClickListener {
            dismiss()
            onDialogButtonListener?.onRightButtonClick()
        }
    }


    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        playerFragment?.releasePlayer()
    }

    var timer: Timer? = null
    private fun coutdown() {
        timer?.cancel()
        if (timeSkipSecond <= 0) {
            return
        }
        dialog?.setCanceledOnTouchOutside(false)
        dialog?.setCancelable(false)
        buttonLeft.isEnabled = false
        textTimeCountDown?.visibility = View.VISIBLE
        buttonLeft.setTextColor(Color.parseColor("#FF9E9E9E"))
        timer = Timer()
        timer?.schedule(timerTask {
            activity?.runOnUiThread {
                if (timeSkipSecond >= 0) {
                    val time = timeSkipSecond
                    if (timeSkipSecond <= 5) {
                        textTimeCountDown?.setTextColor(Color.RED)
                    } else {
                        textTimeCountDown?.setTextColor(Color.BLACK)
                    }
                    textTimeCountDown?.text = "Chờ " + time + "s để bỏ qua"
                    timeSkipSecond = timeSkipSecond - 1

                } else {
                    try {
                        activity?.runOnUiThread {
                            textTimeCountDown?.visibility = View.INVISIBLE
                            dialog?.setCanceledOnTouchOutside(cancelTouchOutsite)
                            dialog?.setCancelable(cancelAble)
                            buttonLeft.setTextColor(Color.BLACK)
                            buttonLeft?.isEnabled = true
                        }
                    } catch (e: Exception) {
                    }
                }
            }

        }, 0, 1000)
    }
}




