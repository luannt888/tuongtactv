package vn.mediatech.istudiocafe.fragment

import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_call_staff.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceUtilTT

class CallStaffFragment : Fragment() {
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var preOrientation = 0
    var popupMenuTableList: PowerMenu? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        preOrientation = resources.configuration.orientation
        return inflater.inflate(R.layout.fragment_call_staff, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        val lp: RelativeLayout.LayoutParams = layoutContent.layoutParams as RelativeLayout.LayoutParams
        lp.topMargin = (activity as BaseActivity).getStatusBarHeight()
    }

    fun initData() {
        setData()
    }

    fun setData() {
    }

    fun send() {
//        getData()
    }

    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            (activity as BaseActivity).showDialog(message)
        }
    }

    fun getData() {
        if (!MyApplication.getInstance().isNetworkConnect) {
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        activity?.runOnUiThread { (activity as BaseActivity).showDialog(getString(R.string.msg_progress_waiting)) }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
//        val api = String.format(Locale.ENGLISH, Constant.API_GELLERY_LIST, itemObj!!.id, page, limit)
        val api = ""
        myHttpRequest!!.request(false, ServiceUtilTT.validAPI(api), null, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread { (activity as BaseActivity).hideLoadingDialog() }
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                activity?.runOnUiThread { (activity as BaseActivity).hideLoadingDialog() }
                handleData(responseString)
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { (activity as BaseActivity?)?.showDialog(message) }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
        /* val dataArray: JSONArray? = JsonParser.getJsonArray(resultObj, Constant.DATA)
         val list = JsonParser.parseItemNewsList(requireActivity(), dataArray)
         if (list == null || list.size == 0) {
             if (itemList.size > 0) {
                 return
             }
             showErrorNetwork(getString(R.string.msg_empty_data))
             return
         }
         itemList.addAll(list)*/
    }

    fun showSelectTablePopup() {
        if (popupMenuTableList != null) {
            popupMenuTableList!!.showAsDropDown(buttonSelectTable)
            popupMenuTableList!!.setSelection(popupMenuTableList!!.selectedPosition)
            return
        }
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        for (i in 0..19) {
            itemList.add(PowerMenuItem("Bàn số ${i + 1}"))
        }
        val screenSize = ScreenSize(requireActivity())
        val popupWidth = if (buttonSelectTable.measuredWidth > 0) buttonSelectTable.measuredWidth else (screenSize.width * 2 / 3)
        val popupHeight = screenSize.height / 2
        popupMenuTableList = PowerMenu.Builder(requireActivity())
                .addItemList(itemList)
                .setAnimation(MenuAnimation.SHOWUP_TOP_RIGHT)
                .setShowBackground(true)
                .setWidth(popupWidth)
                .setHeight(popupHeight)
                .setMenuRadius(10f)
                .setMenuShadow(10f)
                .setDividerHeight(1)
                .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
                .setCircularEffect(CircularEffect.BODY)
                .setTextSize(16)
                .setTextGravity(Gravity.START)
                .setTextColor(ContextCompat.getColor(requireActivity(), R.color.white_tt))
                .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
                .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.color_primary_tt))
                .setMenuColor(Color.parseColor("#0d0d0d"))
                .setSelectedMenuColor(Color.parseColor("#0d0d0d"))//#1a1a1a
                .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                    override fun onItemClick(position: Int, item: PowerMenuItem?) {
                        popupMenuTableList!!.dismiss()
                        popupMenuTableList!!.selectedPosition = position
                        textTableName.text = item?.title
                    }
                })
                .build()
        popupMenuTableList!!.showAsDropDown(buttonSelectTable)
    }

    fun isPopupMenuTableShowing(): Boolean {
        if (popupMenuTableList == null || !popupMenuTableList!!.isShowing()) {
            return false
        }
        popupMenuTableList!!.dismiss()
        return true
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            (activity as MainActivity).removeCallStaffFragment()
        }
        buttonSelectTable.setOnClickListener {
            showSelectTablePopup()
        }
        buttonOrder.setOnClickListener {
            send()
        }
    }

    override fun onDetach() {
        popupMenuTableList?.dismiss()
        (activity as BaseActivity).hideKeyboard(editDescription)
        super.onDetach()
    }

    override fun onDestroy() {
        myHttpRequest?.cancel()
        super.onDestroy()
    }
}