package vn.mediatech.istudiocafe.fragment

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.RecognizerIntent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.RadioButton
import android.widget.TextView.OnEditorActionListener
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_search_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.listener.OnScrolledRecyckerViewListener
import java.util.*

class SearchFragment : BottomSheetDialogFragment() {
    var fragment: NewsFragment? = null
    var bottomSheetDialog: BottomSheetDialog? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_search_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setOnShowListener {
            /*val bottomSheetInternal: View? = (it as BottomSheetDialog).findViewById(R.id.design_bottom_sheet)
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetInternal!!)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetBehavior.isDraggable = false
            bottomSheetBehavior.skipCollapsed = true*/
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        }
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
    }

    fun initData() {
        fragment = NewsFragment()
        val bundle = Bundle()
        bundle.putInt(Constant.TYPE, Constant.TYPE_SEARCH)
        fragment!!.arguments = bundle
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.frameLayout, fragment!!)
        fragmentTransaction.commit()
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            fragment!!.checkRefresh()
            (activity as BaseActivity).showKeyboardFocus(editSearch)
        }, 600)

        fragment!!.onScrolledListener = object : OnScrolledRecyckerViewListener {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager: StaggeredGridLayoutManager = recyclerView.layoutManager as StaggeredGridLayoutManager
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                bottomSheetDialog?.behavior?.isDraggable = if (firstPos < 0 || firstPos == 0) true else false
            }
        }
    }

    fun initControl() {
        buttonBack.setOnClickListener {
            dismiss()
        }
        buttonSpeak.setOnClickListener {
            promptSpeechInput()
        }

        editSearch.setOnEditorActionListener(OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                handleSearch()
                return@OnEditorActionListener true
            }
            false
        })

        try {
            checkVoiceRecognition()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        initRadioCheckListener(radioProduct)
        initRadioCheckListener(radioEvent)
        initRadioCheckListener(radioVideo)
        initRadioCheckListener(radioPhoto)
    }

    fun initRadioCheckListener(radio: RadioButton) {
        radio.setOnCheckedChangeListener { compoundButton, isChecked ->
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                if (isDetached) {
                    return@Runnable
                }
                if (isChecked) {
                    handleSearch()
                }
            }, 200)
        }
    }

    fun checkVoiceRecognition() {
        val pm: PackageManager = requireActivity().packageManager
        val activities = pm.queryIntentActivities(Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0)
        if (activities.size == 0) {
            buttonSpeak.visibility = View.GONE
        }
    }

    private fun promptSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE, (activity as Activity).packageName)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.msg_speech_hint))
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
        try {
            startActivityForResult(intent, Constant.REQUEST_CODE_SPEAK)
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(activity,
                    getString(R.string.speech_not_supported),
                    Toast.LENGTH_SHORT).show()
        }
    }

    private fun handleSearch() {
        if (fragment == null) {
            return
        }
        val keyword = editSearch.text.toString().trim { it <= ' ' }
        if (keyword.isEmpty()) {
//            (activity as BaseActivity).showToast(R.string.msg_keyword_empty)
            return
        }
        (activity as BaseActivity).hideKeyboard(editSearch)
        var typeSearch = ""
        var tabType = Constant.TAB_TYPE_PRODUCT
        val checkedId = radioGroup.checkedRadioButtonId
        if (checkedId == -1) {
            return
        }
        when (checkedId) {
            radioProduct.id -> {
                typeSearch = "product"
                tabType = Constant.TAB_TYPE_PRODUCT
            }
            radioVideo.id -> {
                typeSearch = "video"
                tabType = Constant.TAB_TYPE_VIDEO
            }
            radioEvent.id -> {
                typeSearch = "event"
                tabType = Constant.TAB_TYPE_EVENT
            }
            radioPhoto.id -> {
                typeSearch = "photo"
                tabType = Constant.TAB_TYPE_GALLERY
            }
        }
        fragment!!.tabType = tabType
        bottomSheetDialog?.behavior?.isDraggable = true
        fragment!!.handleSearch(keyword, typeSearch)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == Constant.REQUEST_CODE_SPEAK && data != null) {
            val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            if (result != null && result.size > 0) {
                val keyword = result[0]
                editSearch.setText(keyword)
                editSearch.setSelection(keyword.length)
                handleSearch()
            }
        }
    }
}