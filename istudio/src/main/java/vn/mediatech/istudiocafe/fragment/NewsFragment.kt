package vn.mediatech.istudiocafe.fragment

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_news_tt.*
import org.json.JSONArray
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.DetailNewsActivity
import vn.mediatech.istudiocafe.activity.DetailVideoActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.adapter.NewsAdapter
import vn.mediatech.istudiocafe.adapter.NewsAdapter.OnItemClickListener
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnBaseRequestListener
import vn.mediatech.istudiocafe.listener.OnScrolledRecyckerViewListener
import vn.mediatech.istudiocafe.listener.OnShowListener
import vn.mediatech.istudiocafe.model.ItemCategory
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.SpacesItemDecoration
import vn.mediatech.istudiocafe.util.SpacesItemDecorationGridMultipleViewType

class NewsFragment : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var itemList: ArrayList<ItemNews> = ArrayList()
    var itemCategory: ItemCategory? = null
    var adapter: NewsAdapter? = null
    var page: Int = 1
    var limit = 0
    var startInsert = 0
    var loadMoreAble: Boolean = false
    var tabType: Int = Constant.TAB_TYPE_VIDEO
    var apiType: Int = tabType
    var keyword = ""
    var publishDate: String? = null
    var typeSearch = ""
    var onScrolledListener: OnScrolledRecyckerViewListener? = null
    var savedInstanceState: Bundle? = null
    var topSpace = false
    var onShowListener:OnShowListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_news_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
        onShowListener?.onViewCreated()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(Constant.TYPE, tabType)
            putParcelable(Constant.DATA, itemCategory)
            putBoolean(Constant.IS_SHOW_TOP_SPACE, topSpace)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (isDetached) {
            return
        }
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        layoutRefresh?.setColorSchemeResources(R.color.color_primary_tt)
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        tabType = bundle.getInt(Constant.TYPE, Constant.TAB_TYPE_VIDEO)
        apiType = tabType
        itemCategory = bundle.getParcelable(Constant.DATA)
        topSpace = bundle.getBoolean(Constant.IS_SHOW_TOP_SPACE, false)
        if (topSpace) {
            if (activity is MainActivity) {
                val topSpacePadding = (activity as MainActivity).topSpace
                Loggers.e("TabNewsFrg", "topSpacePadding = $topSpacePadding")
                layoutFragmentRoot.setPadding(0, topSpacePadding, 0, 0)
            }
        }
        if (tabType != Constant.TAB_TYPE_GALLERY && tabType != Constant.TYPE_NOTIFICATION) {
//            if (itemCategory == null) {
//                progressBar.visibility = View.GONE
//                return
//            }
        }
        getData()
    }

    fun handleSearch(keyword: String, typeSearch: String) {
        this.keyword = keyword
        this.typeSearch = typeSearch
        myHttpRequest?.cancel()
        isLoading = false
        loadMoreAble = true
        page = 1
        publishDate = ""
        itemList.clear()
        adapter?.notifyDataSetChanged()
        adapter = null
        progressBar.visibility = View.VISIBLE
        for (i in 0 until recyclerView.itemDecorationCount) {
            recyclerView.removeItemDecorationAt(i)
        }
        getData()
    }

    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            if (itemList.size <= 0) {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
            }
            layoutLoadMore.visibility = View.GONE
            progressBar.visibility = View.GONE
        }
    }

    fun getData() {
        Loggers.e("getData", "isLoading = $isLoading")
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
//        progressBar.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        var api = ""
        val requestParams = RequestParams()
        when (apiType) {
            Constant.TAB_TYPE_EVENT -> {
                api = Constant.API_EVENT_LIST
            }
            Constant.TAB_TYPE_VIDEO -> {
//                api = Constant.API_ARTICLE_LIST
                api = Constant.API_VIDEO_LIST
                requestParams.put("type", "${itemCategory?.type}")
            }
            Constant.TAB_TYPE_NEWS -> {
//                api = Constant.API_ARTICLE_LIST
                api = Constant.API_NEWS_LIST
                requestParams.put("type", "${itemCategory?.type}")
            }
            Constant.TAB_TYPE_PRODUCT -> {
                api = Constant.API_PRODUCT_LIST
            }
            Constant.TAB_TYPE_GALLERY -> {
                api = Constant.API_ARTICLE_LIST
                requestParams.put("type", "${itemCategory?.type}")
            }
            Constant.TYPE_NOTIFICATION -> {
                api = Constant.API_NOTIFICATION_LIST
                val itemUser = MyApplication.getInstance().dataManager.itemUser
                if (itemUser != null) {
                    requestParams.put("access_token", "${itemUser.accessToken}")
                }
            }
            Constant.TYPE_SEARCH -> {
                api = Constant.API_SEARCH
                requestParams.put("name", keyword)
                requestParams.put("type", typeSearch)
            }
        }
        if (!publishDate.isNullOrEmpty()) {
            requestParams.put("update_at", publishDate)
        }
        if (apiType != Constant.TYPE_SEARCH && apiType != Constant.TYPE_NOTIFICATION) {
            requestParams.put("category_id", "${itemCategory?.id}")
        }
        if (api.isEmpty()) {
            progressBar.visibility = View.GONE
            layoutLoadMore.visibility = View.GONE
            isLoading = false
            return
        }
        requestParams.put("limit", "200")
//        myHttpRequest!!.request(false, ServiceUtil.validAPI(api), requestParams, object : MyHttpRequest.ResponseListener {
        myHttpRequest!!.request(
            false,
            ServiceUtilTT.validAPIDGTH(context, api),
            requestParams,
            object : MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    isLoading = false
                    showErrorNetwork(getString(R.string.msg_network_error))
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }
                    handleData(responseString)
                    activity?.runOnUiThread {
                        progressBar.visibility = View.GONE
                        layoutLoadMore.visibility = View.GONE
                    }
                    isLoading = false
                }
            })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { message?.let { showErrorNetwork(it) } }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
//        val dataArray: JSONArray? = JsonParser.getJsonArray(resultObj, Constant.DATA)
        val dataArray: JSONArray? = JsonParser.getJsonArray(jsonObject, Constant.RESULT)
        val list = JsonParser.parseItemNewsList(requireActivity(), dataArray)
        if (list == null || list.size == 0) {
            if (itemList.size > 0) {
                loadMoreAble = false
                activity?.runOnUiThread { layoutLoadMore.visibility = View.GONE }
                return
            }
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        for (item in list) {
            if (tabType == Constant.TAB_TYPE_VIDEO) {
                item.contentType = Constant.TYPE_VIDEO
            } else if (tabType == Constant.TAB_TYPE_NEWS) {
                item.contentType = Constant.TYPE_NEWS
            }
        }

        if (limit == 0) {
            limit = list.size
        }
        if (apiType == Constant.TYPE_SEARCH && tabType == Constant.TAB_TYPE_EVENT) {
            val updateAt = JsonParser.getString(resultObj, "update_at")
            publishDate = updateAt
            loadMoreAble = !publishDate.equals("0")
        } else {
            val lastItemObj = list.get(list.size - 1)
            publishDate = lastItemObj.publishDate
            loadMoreAble = !publishDate.isNullOrEmpty() && list.size >= limit
        }
        if (tabType == Constant.TAB_TYPE_VIDEO && itemList.size == 0) {
            list.get(0).type = Constant.TYPE_NEWS_FEATURE
        }
        startInsert = itemList.size
        itemList.addAll(list)
        setData()
    }

    fun setData() {
        if (adapter != null) {
            activity?.runOnUiThread {
                adapter?.notifyItemChanged(startInsert)
                if (limit == 0) {
                    recyclerView.scheduleLayoutAnimation()
//                recyclerView.notifyDataSetChanged()
                }
            }
            return
        }
        val spacePx: Int = resources.getDimensionPixelSize(R.dimen.space_item)
        var numberColumn = 1
        var style: Int = Constant.TAB_TYPE_NEWS
        var itemDecoration: ItemDecoration? =
            SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
        when (tabType) {
            Constant.TAB_TYPE_VIDEO -> {
                numberColumn = 2
                style = Constant.STYLE_GRIDVIEW
                itemDecoration = SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
            }

            Constant.TAB_TYPE_NEWS -> {
                numberColumn = 1
                style = Constant.STYLE_LISTVIEW_BOX
                itemDecoration = SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
            }
            Constant.TAB_TYPE_EVENT -> {
                numberColumn = 1
                style = Constant.STYLE_EVENT
                itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
            }
            Constant.TAB_TYPE_PRODUCT -> {
                numberColumn = 2
                style = Constant.STYLE_PRODUCT
                itemDecoration = SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
            }
            Constant.TAB_TYPE_GALLERY -> {
                numberColumn = 2
                style =
                    if (itemCategory != null) itemCategory!!.style!! else Constant.STYLE_GRIDVIEW
                itemDecoration = SpacesItemDecorationGridMultipleViewType(spacePx, numberColumn)
            }
            Constant.TYPE_NOTIFICATION -> {
                numberColumn = 1
                style = Constant.STYLE_LISTVIEW
                itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
            }
            Constant.TYPE_SEARCH -> {
                numberColumn = 1
                style = Constant.STYLE_LISTVIEW
                itemDecoration = SpacesItemDecoration(spacePx, numberColumn)
            }
        }
        val layoutManager =
            StaggeredGridLayoutManager(numberColumn, StaggeredGridLayoutManager.VERTICAL)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.itemAnimator = DefaultItemAnimator()
        activity?.runOnUiThread {
            adapter = NewsAdapter(requireActivity(), itemList, style, numberColumn)
            if (tabType == Constant.TAB_TYPE_GALLERY) {
                adapter!!.isShowButtonInfo = false
            }
            recyclerView?.addItemDecoration(itemDecoration!!)
            recyclerView?.layoutManager = layoutManager
            recyclerView?.adapter = adapter
            recyclerView?.scheduleLayoutAnimation()
            adapter!!.onItemClickListener = object : OnItemClickListener {
                override fun onClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        if (tabType == Constant.TAB_TYPE_PRODUCT) {
                            (activity as? BaseActivity)?.goDetailProductActivity(itemObject, holder)
                            return@Runnable
                        }
                        if (tabType == Constant.TAB_TYPE_EVENT) {
                            (activity as? BaseActivity)?.goDetailEventActivity(itemObject, holder)
                            return@Runnable
                        }
                        if (tabType == Constant.TAB_TYPE_GALLERY) {
                            (activity as? BaseActivity)?.showImageZoomPagerBottomSheetDialog(
                                itemObject
                            )
                            return@Runnable
                        }
//                        (activity as? BaseActivity)?.goDetailNewsActivity(itemObject)
                        val bundle = Bundle()
                        bundle.putParcelable(Constant.DATA, itemObject)
                        var cla: Class<*>? = DetailNewsActivity::class.java
                        when (itemObject.contentType) {
                            Constant.TYPE_NEWS -> cla = DetailNewsActivity::class.java
                            Constant.TYPE_VIDEO -> cla = DetailVideoActivity::class.java
                        }
                        val intent = Intent(context,cla)
                        intent.putExtras(bundle)
                        startActivity(intent)
                    }, 200)
                }

                override fun onLongClick(itemObject: ItemNews, position: Int, view: View) {
                    if (tabType == Constant.TYPE_NOTIFICATION) {
                        val itemUser = MyApplication.getInstance().dataManager.itemUser
                        if (itemUser == null) {
                            return
                        }
                        showOptionMenuLongClick(itemObject, position, view)
                    }
                }

                override fun onCommentClick(itemObject: ItemNews, position: Int) {
                    (activity as BaseActivity).showDialog(getString(R.string.msg_comming_soon))
                }

                override fun onShareClick(itemObject: ItemNews, position: Int) {
                    (activity as BaseActivity?)!!.shareApp(itemObject)
                }

                override fun onOrderClick(
                    itemObject: ItemNews,
                    position: Int,
                    holder: RecyclerView.ViewHolder
                ) {
                    Handler(Looper.getMainLooper()).postDelayed(Runnable {
                        if (tabType == Constant.TAB_TYPE_PRODUCT) {
                            (activity as? BaseActivity)?.goDetailProductActivity(itemObject, holder)
                            return@Runnable
                        }
                        if (tabType == Constant.TAB_TYPE_EVENT) {
                            (activity as? BaseActivity)?.goDetailEventActivity(itemObject, holder)
                            return@Runnable
                        }
                    }, 200)
                }
            }
        }
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                onScrolledListener?.onScrolled(recyclerView, dx, dy)
                if (isLoading || !loadMoreAble) {
                    return
                }
                val firstPos = layoutManager.findFirstCompletelyVisibleItemPositions(null)[0]
                layoutRefresh.isEnabled = firstPos == 0

                val visibleItemCount = layoutManager.childCount
                val firstVisible = layoutManager.findFirstVisibleItemPositions(null)[0]
                val totalItem = layoutManager.itemCount
                val spanCount = layoutManager.spanCount + 1
                if (visibleItemCount + firstVisible >= totalItem - spanCount) {
                    layoutLoadMore.visibility = View.VISIBLE
                    page++
                    getData()
                }
            }
        })
    }

    var popupMenuDeleteNotification: PowerMenu? = null
    fun showOptionMenuLongClick(itemObject: ItemNews, position: Int, view: View) {
        val itemList: ArrayList<PowerMenuItem> = ArrayList()
        itemList.add(PowerMenuItem(resources.getString(R.string.delete)))
        popupMenuDeleteNotification = PowerMenu.Builder(requireActivity())
            .addItemList(itemList)
            .setAnimation(MenuAnimation.SHOWUP_TOP_RIGHT)
            .setShowBackground(true)
            .setMenuRadius(10f)
            .setMenuShadow(10f)
            .setDividerHeight(1)
            .setDivider(ColorDrawable(ContextCompat.getColor(requireActivity(), R.color.dark_text)))
            .setCircularEffect(CircularEffect.BODY)
            .setTextSize(16)
            .setTextGravity(Gravity.START)
            .setTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setTextTypeface(Typeface.create("sans-serif-medium", Typeface.NORMAL))
            .setSelectedTextColor(ContextCompat.getColor(requireActivity(), R.color.dark_text))
            .setMenuColor(Color.parseColor("#f8f8f8"))
            .setSelectedMenuColor(Color.parseColor("#f8f8f8"))
            .setOnMenuItemClickListener(object : OnMenuItemClickListener<PowerMenuItem> {
                override fun onItemClick(position: Int, item: PowerMenuItem?) {
                    popupMenuDeleteNotification!!.dismiss()
                    (activity as? BaseActivity)?.showLoadingDialog(false, null)
                    ServiceUtilTT.deleteNotification(
                        requireActivity(),
                        itemObject.id,
                        object : OnBaseRequestListener {
                            override fun onResponse(
                                isSuccess: Boolean,
                                message: String?,
                                response: String?
                            ) {
                                requireActivity().runOnUiThread {
                                    (activity as? BaseActivity)?.hideLoadingDialog()
                                    if (!isSuccess) {
                                        message?.let { (activity as? BaseActivity)?.showToast(it) }
                                        return@runOnUiThread
                                    }
                                    itemList.removeAt(position)
                                    adapter!!.notifyDataSetChanged()
                                }
                            }
                        })
                }
            })
            .build()
        popupMenuDeleteNotification!!.showAsDropDown(view)
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        layoutRefresh.setOnRefreshListener {
            if (isLoading) {
                return@setOnRefreshListener
            }
            publishDate = ""
            itemList.clear()
            layoutLoadMore.visibility = View.GONE
            page = 1
            limit = 0
            loadMoreAble = true
            adapter?.notifyDataSetChanged()
            progressBar.visibility = View.VISIBLE
            layoutRefresh.isRefreshing = false
            getData()
        }
    }

    override fun onDetach() {
        myHttpRequest?.cancel()
        super.onDetach()
    }

    override fun onStop() {
        super.onStop()
        onShowListener?.onStop()
    }
    override fun onResume() {
        super.onResume()
        onShowListener?.onResume()
    }
    override fun onPause() {
        popupMenuDeleteNotification?.dismiss()
        super.onPause()
        onShowListener?.onPause()
    }
}