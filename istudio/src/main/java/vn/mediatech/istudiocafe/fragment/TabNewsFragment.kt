package vn.mediatech.istudiocafe.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.fragment_tab_news_tt.*
import kotlinx.android.synthetic.main.layout_bottom_cart.*
import org.json.JSONArray
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.adapter.ViewPagerAdapter
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.htextview.AnimationListener
import vn.mediatech.istudiocafe.htextview.HTextView
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemCategory
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.JsonParser
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.TextUtil
import java.text.DecimalFormat
import java.text.NumberFormat

class TabNewsFragment : Fragment() {
    var isTabSelected: Boolean = false
    var isViewCreated: Boolean = false
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var itemList: ArrayList<ItemCategory>? = null
    val fragmentList: ArrayList<Fragment> = ArrayList()
    var tabType: Int = 0
    var isTabProduct = false
    var savedInstanceState: Bundle? = null
    var topSpace = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_tab_news_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        isViewCreated = true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putInt(Constant.TYPE, tabType)
            putBoolean(Constant.IS_SHOW_TOP_SPACE, topSpace)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 1000)
            return
        }
        init()
    }

    fun init() {
        if (isTabSelected) {
            return
        }
        isTabSelected = true
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
    }

    fun initData() {
        val bundle = savedInstanceState ?: arguments ?: return
        tabType = bundle.getInt(Constant.TYPE, Constant.TAB_TYPE_VIDEO)
        isTabProduct = tabType == Constant.TAB_TYPE_PRODUCT
        topSpace = bundle.getBoolean(Constant.IS_SHOW_TOP_SPACE, false)
        if (topSpace) {
//            val lp: ViewGroup.LayoutParams = layoutFragmentRoot.layoutParams
//            layoutActionbar
            val topSpacePadding = (activity as MainActivity).topSpace
            Loggers.e("TabNewsFrg", "topSpacePadding = $topSpacePadding")
            layoutFragmentRoot.setPadding(0, topSpacePadding, 0, 0)
        }
        checkUpdateBottomCart()
        getData()
    }

    fun showErrorNetwork(message: String) {
        activity?.runOnUiThread {
            if (itemList == null || itemList!!.size <= 0) {
                textNotify.visibility = View.VISIBLE
                textNotify.text = message
            }
            progressBar.visibility = View.GONE
        }
    }

    fun getData() {
         if (isLoading|| isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        progressBar.visibility = View.GONE
        textNotify.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest!!.cancel()
        }
        var type = ""
        val requestParams = RequestParams()
        when (tabType) {
            Constant.TAB_TYPE_EVENT -> {
                type = "event"
            }
            Constant.TAB_TYPE_VIDEO -> {
                type = "video"
            }
            Constant.TAB_TYPE_GALLERY -> {
                type = "photo"
            }
            Constant.TAB_TYPE_PRODUCT -> {
                type = "product"
            }
            Constant.TAB_TYPE_NEWS -> {
                type = "news"
            }
        }
        if (!type.isEmpty()) {
            requestParams.put("type", type)
        }
        myHttpRequest!!.request(false, ServiceUtilTT.validAPI(Constant.API_CATEGORY), requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (activity?.isFinishing!! || activity?.isDestroyed!!) {
                    return
                }
                isLoading = false
                showErrorNetwork(getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (activity?.isFinishing!! || activity?.isDestroyed!!) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    progressBar.visibility = View.GONE
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString);
        if (jsonObject == null) {
            showErrorNetwork(getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
            activity?.runOnUiThread { (activity as BaseActivity?)?.showDialog(message) }
            return
        }
        val resultObj = JsonParser.getJsonObject(jsonObject, Constant.RESULT)
        val dataArray: JSONArray? = JsonParser.getJsonArray(resultObj, Constant.DATA)
        itemList = JsonParser.parseItemCategoryList(dataArray)
        if (itemList == null || itemList?.size == 0) {
            activity?.runOnUiThread {
                textNotify.text = getString(R.string.msg_empty_data)
                textNotify.visibility = View.VISIBLE
            }
            return
        }
        initPager()
    }

    fun initPager() {
        val tabTitleList: ArrayList<String> = ArrayList()
        for (i in 0 until itemList!!.size) {
            val itemCategory: ItemCategory = itemList!!.get(i)
            if (itemCategory.name.isNullOrEmpty()) {
                continue
            }
            val fragment = NewsFragment()
            val bundle = Bundle()
            bundle.putInt(Constant.TYPE, tabType)
            bundle.putParcelable(Constant.DATA, itemCategory)
            fragment.arguments = bundle
            fragmentList.add(fragment)
            tabTitleList.add(itemCategory.name)
            break
        }
        activity?.runOnUiThread {
            layoutTab.visibility = View.VISIBLE
            layoutTab.visibility = if (fragmentList.size > 1) View.VISIBLE else View.GONE
            val viewPagerAdapter = ViewPagerAdapter(childFragmentManager, fragmentList, tabTitleList)
            viewPager.adapter = viewPagerAdapter
            tabLayout.visibility = View.GONE
            tabLayout.setupWithViewPager(viewPager)
            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    handleOnPageSelected(position)
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            })
            handleOnPageSelected(0)
        }
    }

    fun handleOnPageSelected(position: Int) {
        val fragment = fragmentList.get(position)
        if (fragment is NewsFragment) {
            fragment.checkRefresh()
        }
    }

    fun initControl() {
        textNotify.setOnClickListener {
            getData()
        }
        buttonTrash?.setOnClickListener {
            (activity as BaseActivity).showDialog(true, R.string.notification, R.string.msg_delete_cart, R.string.delete, R.string.close, object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                    MyApplication.getInstance().dataManager.itemCartList.clear()
                    layoutBottomCart.visibility = View.GONE
                }

                override fun onRightButtonClick() {
                }
            })
        }
        layoutBottomCart?.setOnClickListener {
            (activity as MainActivity).showCartFragment(object : OnCancelListener {
                override fun onCancel(isCancel: Boolean) {
                    checkUpdateBottomCart()
                }

            })
        }
    }

    fun checkUpdateBottomCart() {
        if (!isTabProduct) {
            return
        }
        val itemCartList = MyApplication.getInstance().dataManager.itemCartList
        if (itemCartList.size == 0) {
            if (layoutBottomCart.visibility == View.GONE) {
                return
            }
            val anim = AnimationUtils.loadAnimation(activity, R.anim.slide_in_down)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    layoutBottomCart.visibility = View.GONE
                    textNumberProduct.animateText("")
                    textTotalPrice.animateText("")
                }

                override fun onAnimationRepeat(p0: Animation?) {
                }
            })
            layoutBottomCart.startAnimation(anim)
            return
        }
        var totalPrice = 0
        var quantity = 0
        for (item in itemCartList) {
            totalPrice += item.quantity * item.basePrice
            quantity += item.quantity
        }

        val numberFormat: NumberFormat = DecimalFormat("00")
        var quantityStr = numberFormat.format(quantity)
        quantityStr = if (quantity > 99) "99+" else quantityStr
//        textNumberProduct.text = quantityStr
//        textTotalPrice.text = TextUtil.formatMoney(totalPrice)
        textNumberProduct.setAnimationListener(object : AnimationListener {
            override fun onAnimationEnd(hTextView: HTextView?) {
                if (isDetached) {
                    return
                }
                textTotalPrice.animateText(TextUtil.formatMoney(totalPrice))
                textNumberProduct.clearAnimation()
            }
        })
        if (layoutBottomCart.visibility != View.VISIBLE) {
            val anim = AnimationUtils.loadAnimation(activity, R.anim.slide_in_up)
            anim.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(p0: Animation?) {
                }

                override fun onAnimationEnd(p0: Animation?) {
                    layoutBottomCart.visibility = View.VISIBLE
                    textNumberProduct.animateText(quantityStr)
//                    textTotalPrice.animateText(TextUtil.formatMoney(totalPrice))
                }

                override fun onAnimationRepeat(p0: Animation?) {
                }
            })
            layoutBottomCart.visibility = View.VISIBLE
            layoutBottomCart.startAnimation(anim)
            return
        }

        textNumberProduct.animateText(quantityStr)
//        textTotalPrice.animateText(TextUtil.formatMoney(totalPrice))
    }

    override fun onDestroy() {
        myHttpRequest?.cancel()
        super.onDestroy()
    }

    override fun onResume() {
        checkUpdateBottomCart()
        super.onResume()
    }
}