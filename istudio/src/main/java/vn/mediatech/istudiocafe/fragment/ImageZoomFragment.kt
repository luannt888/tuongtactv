package vn.mediatech.istudiocafe.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_image_zoom_tt.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemPhoto

class ImageZoomFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_zoom_tt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initData()
    }

    fun initData() {
        val itemObj: ItemPhoto = arguments?.getParcelable(Constant.DATA) ?: return
        textDescription.text = itemObj.title
        textTime.text = itemObj.time
        MyApplication.getInstance().loadImage(getActivity(), imageView, itemObj.image)
    }

    fun resetZoom() {
        imageView.resetZoom()
    }
}