package vn.mediatech.istudiocafe.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import vn.mediatech.istudiocafe.R;
import vn.mediatech.istudiocafe.activity.BaseActivity;
import vn.mediatech.istudiocafe.activity.DetailVideoActivity;
import vn.mediatech.istudiocafe.adapter.NewsAdapter;
import vn.mediatech.istudiocafe.app.Constant;
import vn.mediatech.istudiocafe.listener.OnCreateViewListener;
import vn.mediatech.istudiocafe.model.ItemNews;
import vn.mediatech.istudiocafe.util.SpacesItemDecorationGridMultipleViewType;
import vn.mediatech.istudiocafe.util.SpacesItemDecorationListBox;

public class RelateNewsFragment extends Fragment {
    private View rootView;
    private RecyclerView recyclerView;
    private NewsAdapter adapter;
    private ArrayList<ItemNews> itemList = new ArrayList<>();
    private RecyclerView.ItemDecoration spacesItemDecoration;
    private OnCreateViewListener onCreateViewListener;
    private boolean loadMoreAble = true, isViewCreated = false;

    public void setLoadMoreAble(boolean loadMoreAble) {
        this.loadMoreAble = loadMoreAble;
    }

    public void setOnCreateViewListener(OnCreateViewListener onCreateViewListener) {
        this.onCreateViewListener = onCreateViewListener;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }
        rootView = inflater.inflate(R.layout.fragment_relate_news, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI();
        isViewCreated = true;
    }

    public void checkRefresh() {
        if (isDetached()) {
            return;
        }
        if (!isViewCreated) {
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkRefresh();
                }
            }, 1000);
            return;
        }
        if (adapter != null) {
            adapter.notifyDataSetChanged();
            return;
        }
        initData();
    }

    private void initUI() {
//        ((BaseActivity) getActivity()).setDefaultBackgroudColorFromFragment(rootView);
        recyclerView = rootView.findViewById(R.id.recyclerView);
    }

    private void initData() {
        int numberColumn = 2;
//        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), numberColumn);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(numberColumn,
                StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        if (spacesItemDecoration == null) {
            int spacingPx = getResources().getDimensionPixelSize(R.dimen.space_item);
//            spacesItemDecoration = new SpacesItemDecorationListBox(spacingPx, numberColumn);
            spacesItemDecoration = new SpacesItemDecorationGridMultipleViewType(spacingPx, numberColumn);
            recyclerView.addItemDecoration(spacesItemDecoration);
        }
        recyclerView.setHasFixedSize(true);
//        adapter = new NewsAdapter(getActivity(), itemList, numberColumn, Constant
//        .STYLE_LISTVIEW_BOX);
//        adapter = new NewsAdapter(getActivity(), itemList, Constant.STYLE_LISTVIEW_BOX, 1, false);
        adapter = new NewsAdapter(getActivity(), itemList, Constant.STYLE_GRIDVIEW, numberColumn, false);
//        adapter.setShowButtonInfo(true);
//        adapter.setLightTheme(false);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(new NewsAdapter.OnItemClickListener() {
            @Override
            public void onClick(@NotNull ItemNews itemObject, int position,
                                @NotNull RecyclerView.ViewHolder holder) {
                if (getActivity() instanceof DetailVideoActivity) {
                    ((DetailVideoActivity) getActivity()).onItemRelateNewsClicked(itemObject);
                }
            }

            @Override
            public void onLongClick(@NotNull ItemNews itemObject, int position,
                                    @NotNull View view) {
            }

            @Override
            public void onShareClick(@NotNull ItemNews itemObject, int position) {
                ((BaseActivity) getActivity()).shareApp(itemObject);
            }

            @Override
            public void onCommentClick(@NotNull ItemNews itemObject, int position) {
                ((BaseActivity) getActivity()).showDialog(getString(R.string.msg_comming_soon));
            }

            @Override
            public void onOrderClick(@NotNull ItemNews itemObject, int position,
                                     @NotNull RecyclerView.ViewHolder holder) {

            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (!loadMoreAble) {
                    return;
                }
                int visibleItemCount = layoutManager.getChildCount();
                int firstVisible = layoutManager.findFirstVisibleItemPositions(null)[0];
                int totalItem = layoutManager.getItemCount();
                int spanCount = layoutManager.getSpanCount() + 1;
                if (visibleItemCount + firstVisible >= totalItem - spanCount) {
                    if (getActivity() instanceof DetailVideoActivity) {
                        ((DetailVideoActivity) getActivity()).setHandleLoadMore(true);
                        ((DetailVideoActivity) getActivity()).getData();
                    }
                }
                /*int lastPos = layoutManager.findLastCompletelyVisibleItemPositions(null)[0];
                int spanCount = layoutManager.getSpanCount();
                if (lastPos >= layoutManager.getItemCount() - spanCount) {
//                    loadMore.setVisibility(View.VISIBLE);
                    if (getActivity() instanceof DetailVideoActivity) {
                        ((DetailVideoActivity) getActivity()).setHandleLoadMore(true);
                        ((DetailVideoActivity) getActivity()).getData();
                    }
                }*/
            }
        });
    }

    public void setData(ArrayList<ItemNews> itemNewList) {
        if (itemList == null) {
            itemList = new ArrayList<>();
        }
        if (itemNewList != null) {
            itemList.addAll(itemNewList);
        }
        checkRefresh();
    }
}
