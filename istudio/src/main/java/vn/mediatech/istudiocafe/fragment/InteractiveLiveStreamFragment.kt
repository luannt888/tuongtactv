package vn.mediatech.istudiocafe.fragment

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.WindowManager
import android.webkit.ValueCallback
import android.webkit.WebChromeClient.FileChooserParams
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.facebook.GraphResponse
import com.facebook.HttpMethod
import com.facebook.login.LoginManager
import com.glide.slider.library.SliderLayout
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_livestream_interactive.*
import kotlinx.android.synthetic.main.layout_header_recycler_view_tt.view.*
import kotlinx.android.synthetic.main.my_slider_view.view.*
import org.json.JSONObject
import vn.mediatech.interactive.app.AppUtils
import vn.mediatech.interactive.fragment.AccountFragment
import vn.mediatech.interactive.fragment.InteractiveFragment
import vn.mediatech.interactive.listener.OnMessageListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.activity.user.UserLoginActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.group.ListGroupFragment
import vn.mediatech.istudiocafe.group.ListGroupFragment.OnGroupListener
import vn.mediatech.istudiocafe.listener.*
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.MyHttpRequest.ResponseListener
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import vn.mediatech.istudiocafe.util.GeneralUtils.Companion.coppyToClipboard
import vn.mediatech.istudiocafe.util.GeneralUtils.Companion.openMessenger
import vn.mediatech.istudiocafe.util.GeneralUtils.Companion.openMyVoucher
import vn.mediatech.istudiocafe.util.GeneralUtils.Companion.showDialogConfirm
import vn.mediatech.istudiocafe.util.LoadingDialogUtils.Companion.hideLoadingDialog
import vn.mediatech.istudiocafe.util.LoadingDialogUtils.Companion.showLoadingDialog
import vn.mediatech.istudiocafe.util.ServiceManager.Companion.setNeedUpdateFcmTokenForUser
import vn.mediatech.istudiocafe.util.ServiceManager.Companion.updateFcmTokenUser
import vn.mediatech.istudiocafe.zinteractive.app.InteractiveConstant
import vn.mediatech.istudiocafe.zinteractive.webGame.WebGameFragment
import java.io.File
import java.util.*

//import vn.mediatech.istudiocafe.activity.BaseActivity;
//import vn.mediatech.istudiocafe.activity.MainActivity;
class InteractiveLiveStreamFragment : Fragment() {
    private var rootView: View? = null
    private var myHttpRequest: MyHttpRequest? = null
    private var itemObj: ItemNews? = null
    private val channel = ""
    private val type = ""

    /*mIsVisibleToUser = true;
        if (!hasPlay) {
            resumePlay();
        }*/  var isTabSelected = false
    private var isInitInteractive = false
    private var linkPlay: String? = ""
    private var appID: String? = null

    //    private String linkPlay = "rtmp://rtmp.mediatech.vn/mdtvlive/group4toadam?vhost=__defaultVhost__";
    private var playerFragment: PlayerFragment? = null
    private var isAutoPlay = false
    private var isShowTopSpace = false
    private var topSpaceHeight = 0
    var currentTabTag: String? = null
    private var isViewCreated = false
    private var savedInstanceState: Bundle? = null
    private var mFragmentManager: FragmentManager? = null
    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(Constant.DATA, itemObj)
        outState.putString(Constant.TAB_TAG, currentTabTag)
        outState.putBoolean(Constant.IS_RUN_PLAY, isAutoPlay)
        outState.putBoolean(Constant.IS_SHOW_TOP_SPACE, isShowTopSpace)
        super.onSaveInstanceState(outState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (container == null) {
            return null
        }
        rootView = inflater.inflate(R.layout.fragment_livestream_interactive, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        isViewCreated = true
        this.savedInstanceState = savedInstanceState
        if (onShowListener != null) {
            onShowListener?.onViewCreated()
        }
    }

    fun checkRefresh() {
        if (isDetached) {
            return
        }
        if (!isTabSelected) {
            if (!isViewCreated) {
                Handler(Looper.getMainLooper()).postDelayed({
                    Loggers.e("checkRefresh", "not init. Trying...")
                    checkRefresh()
                }, 500) //Constant.TIME_DELAY_LOAD_TAB
                return
            }
            Loggers.e("checkRefresh", "inited. Starting...")
            isTabSelected = true
            initUI()
            initData()
            return
        }
        resumePlay()
    }

    private fun initUI() {
        initFrameLayoutHeight()
    }

    private fun initData() {
        if (MyApplication.getContext() == null) {
            MyApplication.setsContext(requireActivity().applicationContext)
        }
        val bundle = (if (savedInstanceState != null) savedInstanceState else arguments) ?: return
        itemObj = bundle.getParcelable(Constant.DATA)
        currentTabTag = bundle.getString(Constant.TAB_TAG)
        isAutoPlay = bundle.getBoolean(Constant.IS_RUN_PLAY)
        isShowTopSpace = bundle.getBoolean(Constant.IS_SHOW_TOP_SPACE, false)
        if (linkPlay == null || linkPlay!!.isEmpty()) {
            linkPlay = bundle.getString(Constant.LINK_PLAY)
        }
        if (portSocket == null || portSocket!!.isEmpty()) {
            portSocket = bundle.getString(InteractiveConstant.PORT)
        }
        Loggers.e("LINKPORT_INIT", linkPlay + "*****" + portSocket)
        urlShare = bundle.getString(InteractiveConstant.SHARE)
        appID = bundle.getString(InteractiveConstant.APP_ID, "bacgiang")
        if (playImagePlaceholder == null || playImagePlaceholder!!.isEmpty()) {
            playImagePlaceholder = bundle.getString(Constant.IMAGE)
        }
        Loggers.e("LiveStr", "isShowTopSpace = $isShowTopSpace")
        if (isShowTopSpace) {
            topSpaceHeight = statusBarHeight
            val lpViewTopSpaceStatusbar =
                viewTopSpaceStatusbar?.layoutParams as RelativeLayout.LayoutParams
            lpViewTopSpaceStatusbar?.height = topSpaceHeight
            viewTopSpaceStatusbar?.layoutParams = lpViewTopSpaceStatusbar
        }
        Loggers.e("LiveStr", "topSpaceHeight = $topSpaceHeight")
        initComponent()
        initControl()
        initDraggablePoint()
        if (isShowNews) {
            getData()
            sliderLayout.visibility = View.VISIBLE
        }
    }

    private fun initControl() {
        buttonLogin?.setOnClickListener {
            if (playerFragment != null) {
                playerFragment?.pause()
            }
            val intent = Intent(activity, UserLoginActivity::class.java)
            requireActivity().startActivityForResult(intent, Constant.CODE_LOGIN)
        }
        layoutRefresh?.setOnRefreshListener {
            layoutRefresh.isRefreshing = false
            countRequestPlayError = 0
            closeSocket()
            autoLogin()
            sliderLayout?.removeAllSliders()
            interactiveFragment?.removeNewsView()
            getData()
        }
    }

    private fun initComponent() {
        initPlayer()
        checkShowInterActive()
    }

    private val objectSync = Any()
    fun checkShowInterActive() {
        synchronized(objectSync) {
            if (layoutRequireLogin == null) {
                return
            }
            val itemUser = MyApplication.getInstance().dataManager.itemUser
            if (itemUser == null) {
                layoutRequireLogin?.visibility = View.VISIBLE
                progressBar?.visibility = View.GONE
                if (interactiveFragment != null) {
                    logoutInteractive()
                }
            } else {
//            progressBar.setVisibility(View.VISIBLE);
                initInteractive()
                layoutRequireLogin?.visibility = View.GONE
            }
            isInitInteractive = true
        }
    }

    private val dX = 0f
    private val dY = 0f
    private val lastAction = 0
    var viewDraggablePointHeight = 0
    private var isFullLayoutInteractive = false

    @SuppressLint("ClickableViewAccessibility")
    private fun initDraggablePoint() {
        viewDraggablePoint?.viewTreeObserver?.addOnGlobalLayoutListener(object :
            OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                viewDraggablePoint?.viewTreeObserver?.removeOnGlobalLayoutListener(this)
                viewDraggablePointHeight = viewDraggablePoint.height //height is ready
                Loggers.e(
                    "setOnTouchListener",
                    "viewDraggablePointHeight = $viewDraggablePointHeight"
                )
            }
        })
        viewDraggablePoint.setOnClickListener { tooggleFullLayoutInteractive() }

        /*viewDraggablePoint.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
//                        dX = view.getX() - event.getRawX();
                        dY = view.getY() - event.getRawY();
                        lastAction = MotionEvent.ACTION_DOWN;
                        break;
                    case MotionEvent.ACTION_MOVE:
//                        view.setX(event.getRawX() + dX);
                        view.setY(event.getRawY() + dY);
                        lastAction = MotionEvent.ACTION_MOVE;
                        break;
                    case MotionEvent.ACTION_UP:
                        if (lastAction == MotionEvent.ACTION_DOWN) {
                            Toast.makeText(getActivity(), "Clicked!", Toast.LENGTH_SHORT).show();
                        }
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });*/
    }

    fun isFullLayoutInteractive(): Boolean {
        if (isFullLayoutInteractive) {
            tooggleFullLayoutInteractive()
            return true
        }
        return false
    }

    private fun tooggleFullLayoutInteractive() {
        if (layoutInteractive == null) {
            return
        }
        val screenSize = ScreenSize(
            requireActivity()
        )
        //        int heightFull = screenSize.getHeight() - ((BaseActivity) getActivity())
//        .getStatusBarHeight();
        val heightFull = screenSize.height - topSpaceHeight
        val heightMinimize = bottomSheetWebviewHeight
        val heightIncrease = if (isFullLayoutInteractive) heightMinimize else heightFull
        val anim = ValueAnimator.ofInt(
            layoutInteractive!!.measuredHeight,
            heightIncrease
        )
        anim.addUpdateListener { valueAnimator ->
            val value = valueAnimator.animatedValue as Int
            val layoutParams = layoutInteractive!!.layoutParams
            layoutParams.height = value
            layoutInteractive?.layoutParams = layoutParams
        }
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                isFullLayoutInteractive = heightIncrease == heightFull
            }

            override fun onAnimationStart(animation: Animator) {
                super.onAnimationStart(animation)
                if (playerFragment != null) {
                    if (!isFullLayoutInteractive) {
                        playerFragment?.releasePlayer()
                        imgArow.rotation = 0f
                    } else {
                        countRequestPlayError = 0
                        playerFragment?.playLink()
                        imgArow.rotation = 180f
                    }
                }
            }
        })
        anim.duration = 250
        anim.start()
    }

    private var uploadMessage: ValueCallback<Array<Uri>>? = null
    private var mUploadMessage: ValueCallback<Uri?>? = null
    private val REQUEST_SELECT_FILE = 3001
    private val FILECHOOSER_RESULTCODE = 3002
    private val REQUEST_PERMISSION_CAMERA_CODE = 2001
    private val REQUEST_PERMISSION_AUDIO_CODE = 2002
    private val TYPE_IMAGE = 1
    private val TYPE_VIDEO = 2
    private val TYPE_AUDIO = 3
    private var takeMediaCameraUri: Uri? = null
    var webviewUrl: String? = null
    private var interactiveFragment: InteractiveFragment? = null
    private var isClickable = true

    private fun initInteractive() {
        if (interactiveFragment != null) {
            if (interactiveFragment!!.userId == null || interactiveFragment!!.userName == null || interactiveFragment!!.PORT == null) {
                val itemUser = MyApplication.getInstance().dataManager.itemUser
                updateInteractive(itemUser)
            }
            return
        }
        val itemUser = MyApplication.getInstance().dataManager.itemUser
        if (itemUser == null || MyApplication.getInstance().isEmpty(itemUser.accessToken)) {
            return
        }
        if (Constant.REQUIRE_CONFIRM && itemUser.userStatus == Constant.USER_STATUS_NOT_CONFIRM) {
            if (isOnScreen) {
//                LoadingDialogUtils.Companion.showLoadingDialog(requireContext(), true, null);
                showDialogConfirm(requireActivity(), itemUser, object : OnDialogButtonListener {
                    override fun onLeftButtonClick() {
                        showInteractiveFragment(itemUser)
                    }

                    override fun onRightButtonClick() {
                        showInteractiveFragment(itemUser)
                        showLoadingDialog(requireContext(), true, null)
                        val str =
                            "Tài khoản của tôi là " + itemUser.fullname + ", mã số " + itemUser.id + ". Chương trình vui lòng xác thực tài khoản giúp tôi!"
                        coppyToClipboard(requireActivity(), str)
                        openMessenger(requireActivity())
                    }
                })
            }
            return
        }
        showInteractiveFragment(itemUser)
    }

    private fun showInteractiveFragment(itemUser: ItemUser) {
        progressBar?.visibility = View.GONE
        mFragmentManager = childFragmentManager
        val fragmentTransaction = mFragmentManager!!.beginTransaction()
        interactiveFragment = InteractiveFragment()
        val bundle = Bundle()
        bundle.putString(InteractiveConstant.USER_ID, itemUser.id.toString())
        bundle.putString(InteractiveConstant.USER_NAME, itemUser.fullname)
        bundle.putString(InteractiveConstant.USER_TOKEN, itemUser.accessToken)
        bundle.putString(InteractiveConstant.USER_AVATAR, itemUser.avatar)
        bundle.putString(InteractiveConstant.USER_STATUS, itemUser.userStatus)
        //        bundle.putString(InteractiveConstant.USER_STATUS, "1");
        bundle.putString(InteractiveConstant.GROUP_ID, itemUser.groupId)
        bundle.putString(InteractiveConstant.GROUP_NAME, itemUser.groupName)
        if (!SharedPreferencesManager.isTest(context) || MyApplication.getInstance().isEmpty(
                SharedPreferencesManager.getPortGame(
                    context
                )
            )
        ) {
            portSocketConnect = portSocket
        } else {
            portSocketConnect = SharedPreferencesManager.getPortGame(
                context
            )
        }
        bundle.putString(InteractiveConstant.PORT, portSocketConnect)
        bundle.putString(InteractiveConstant.SHARE, urlShare)
        bundle.putString(InteractiveConstant.APP_ID, appID)
        bundle.putBoolean(InteractiveConstant.ROLE, "admin" == itemUser.role)
        bundle.putBoolean(InteractiveConstant.SHOW_NEWS, isShowNews)
        interactiveFragment?.arguments = bundle
        //        fragmentTransaction.add(R.id.frameLayout, playerFragment);
        interactiveFragment?.onClickListenner = object : InteractiveFragment.OnClickListenner {
            override fun onClickToggle() {
                tooggleFullLayoutInteractive()
            }

            override fun onClickViews() {
//                showNews(!isShowNews)
//                var fragment = AccountFragment()
//                val bundle = Bundle()
//                bundle.putString(Constant.DATA, itemUser.fullname)
//                fragment?.arguments = bundle
//                fragment.show(requireActivity().supportFragmentManager,"")
//                Handler(Looper.getMainLooper()).postDelayed({showWebGame()},200)

            }

            override fun onClickMessenger() {
                openMessenger(requireActivity())
            }

            override fun onClickConfirm() {
                showDialogConfirm(requireActivity(), itemUser, null)
            }

            override fun onConnectSocket(isConnect: Boolean) {
            }

            override fun onClickGroup() {
                if (!isClickable) {
                    return
                }
                isClickable = false
                Handler(Looper.getMainLooper()).postDelayed({ isClickable = true }, 500)
                val fragment = ListGroupFragment()
                val fragmentTransaction = childFragmentManager.beginTransaction()
                fragmentTransaction.setCustomAnimations(
                    R.anim.fragment_right_to_left,
                    R.anim.fragment_right_to_left_exit,
                    R.anim.fragment_left_to_right,
                    R.anim.fragment_left_to_right
                )
                //                showFragment(fragment);
                Handler(Looper.getMainLooper()).postDelayed({
                    fragment.show(
                        fragmentTransaction,
                        "LIST_GROUP"
                    )
                }, 200)
                fragment.onGroupListener = object : OnGroupListener {
                    override fun onRequireConnectSocket() {
                        reconnectSocket()
                    }
                }
            }

            override fun onClickSetUp() {
                val fragment = SetUpDialogFragment()
                fragment.onResultListener = object : OnResultListener {
                    override fun onResult(isSuccess: Boolean, msg: String?) {
                        if (isSuccess) {
                            if (!SharedPreferencesManager.isTest(context)) {
                                updatePlayer(linkPlay)
                                updatePort(portSocket)
                            } else {
                                if (!MyApplication.getInstance().isEmpty(
                                        SharedPreferencesManager.getPortGame(
                                            context
                                        )
                                    )
                                ) {
                                    portSocketConnect = SharedPreferencesManager.getPortGame(
                                        context
                                    )
                                    updatePort(portSocketConnect)
                                }
                                if (!MyApplication.getInstance().isEmpty(
                                        SharedPreferencesManager.getUrlStream(
                                            context
                                        )
                                    )
                                ) {
                                    updatePlayer(SharedPreferencesManager.getUrlStream(context))
                                }
                            }
                        }
                    }
                }
                fragment.show(childFragmentManager, "SET UP")
            }

            override fun onClickShareFacebook() {
                val item = ServiceUtilTT.itemInteractiveConfig
                if (item == null || item.urlShare == null || item.urlShare.trim { it <= ' ' } == "") {
                    return
                }
                val shareFacebookUtil = ShareFacebookUtil(activity)
                shareFacebookUtil.shareFacebook(item.urlShare)
            }

            override fun onClickMyVoucher() {
                closeSocket()
                openMyVoucher(activity)

            }

            override fun onClickSignOut() {
                logoutInteractive()
            }
        }
        try {
            fragmentTransaction.add(frameLayoutInteractive!!.id, interactiveFragment!!)
            fragmentTransaction.commitAllowingStateLoss()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun reconnectSocket() {
        if (interactiveFragment != null) {
            interactiveFragment?.reconnect()
        }
    }

    fun logoutInteractive() {
        try {
            MyApplication.getInstance().dataManager.itemUser = null
            SharedPreferencesManager.saveUser(activity, null, null)
            activity?.runOnUiThread {
                try {
                    if (interactiveFragment != null) {
                        interactiveFragment?.closeSocket()
                    }
                    mFragmentManager?.beginTransaction()?.remove(interactiveFragment!!)
                        ?.commitAllowingStateLoss()
                    interactiveFragment = null
                    if (activity is MainActivity) {
                        (activity as MainActivity?)?.updateUIChangeAcount()
                    } else {
                        checkShowInterActive()
                    }
                    FirebaseAuth.getInstance().signOut()
                    if (AccessToken.getCurrentAccessToken() == null) {
                        return@runOnUiThread
                    }
                    GraphRequest(AccessToken.getCurrentAccessToken(),
                        "/me/permissions/",
                        null, HttpMethod.DELETE,
                        { graphResponse: GraphResponse? ->
                            LoginManager.getInstance().logOut()
                        }).executeAndWait()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    private var bottomSheetWebviewHeight = 0
    private var playerHeight = 0
    private var hasInited = false
    private fun setLayoutBottomSheetHeight(screenSize: ScreenSize, height: Int) {
        if (hasInited) {
            return
        }
        hasInited = true
        val statusBarHeight = statusBarHeight
        val viewBelowPlayerLocation = IntArray(2)
        viewBelowPlayer.getLocationOnScreen(viewBelowPlayerLocation)
        val belowPlayerHeight = viewBelowPlayerLocation[1]
        val viewBottomLocation = IntArray(2)
        viewBottom.getLocationOnScreen(viewBottomLocation)
        val bottomHeight = viewBottomLocation[1]
        Loggers.e("setLayoutBottomSheetHeight_check", "$belowPlayerHeight _ $bottomHeight")

//        int distance = screenSize.getHeight() - topSpaceHeight - height - topHeightPlayer;
        val distance = bottomHeight - belowPlayerHeight
        Loggers.e(
            "initPlayer",
            " frameLayout distance = " + distance + " _ heightPlayer = " + height + " _ " +
                    "statusBarHeight = " + statusBarHeight
        )
        bottomSheetWebviewHeight = distance
        val lp = layoutInteractive!!.layoutParams as RelativeLayout.LayoutParams
        lp.height = distance
        layoutInteractive?.layoutParams = lp
        Loggers.e(
            "webviewUrl", "playerHeight = " + playerHeight + " __ bottomSheetWebviewHeight " +
                    "= " + bottomSheetWebviewHeight
        )
    }

    private var allowRunBackground = false
    fun setAllowRunBackground(allowRunBackground: Boolean) {
        this.allowRunBackground = allowRunBackground
    }

    private fun initFrameLayoutHeight() {
        if (activity == null) {
            return
        }
        //        linkPlay = itemObj.getLiveStream();
        val screenSize = ScreenSize(
            requireActivity()
        )
        val height = screenSize.width * 9 / 16
        Loggers.e(
            "LiveStreamFragment_initPlayer",
            " height = $height _linkPlay = $linkPlay"
        )
        val layoutParams = frameLayout!!.layoutParams as RelativeLayout.LayoutParams
        layoutParams.height = height
        frameLayout?.layoutParams = layoutParams
        frameLayout?.visibility = View.VISIBLE
        playerHeight = height
        Handler(Looper.getMainLooper()).postDelayed({
            setLayoutBottomSheetHeight(
                screenSize,
                height
            )
        }, 200)
    }

    private fun initPlayer() {
        val fragmentManager = childFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        playerFragment = PlayerFragment()
        val bundle = Bundle()
        bundle.putString(Constant.TAB_TAG, currentTabTag)
        if (!SharedPreferencesManager.isTest(context) || MyApplication.getInstance().isEmpty(
                SharedPreferencesManager.getUrlStream(
                    context
                )
            )
        ) {
            bundle.putString(Constant.DATA, linkPlay)
        } else {
            bundle.putString(
                Constant.DATA, SharedPreferencesManager.getUrlStream(
                    context
                )
            )
        }
        bundle.putString(Constant.TYPE, type)
        bundle.putBoolean(Constant.IS_TAB_SELECTED, isTabSelected)
        bundle.putBoolean(Constant.IS_LIVE_STREAM, true)
        bundle.putInt(Constant.FRAME_PLAYER_HEIGHT, playerHeight)
        bundle.putBoolean(Constant.IS_RUN_PLAY, isAutoPlay)
        if (playImagePlaceholder != null && playImagePlaceholder!!.trim { it <= ' ' } != "") {
            bundle.putBoolean(Constant.IS_SHOW_IMG_ART, true)
            bundle.putString(Constant.IMAGE, playImagePlaceholder)
        }
        //            bundle.putString(Constant.IMAGE, "https://daugiatruyenhinh.com/upload/banner/file/istudio_placeholder.jpg");
        playerFragment?.arguments = bundle
        playerFragment?.setOnPlayerStateChangeListener(object : OnPlayerStateChangeListener {
            override fun onStateEnd() {}
            override fun onStateReady() {
                countRequestPlayError = 0
            }

            override fun onPlayerError() {
                if (activity != null) {
                    activity?.runOnUiThread { autoTryResumePlayError() }
                }
            }
        })
        //        fragmentTransaction.add(R.id.frameLayout, playerFragment);
        fragmentTransaction.add(frameLayout!!.id, playerFragment!!)
        fragmentTransaction.commit()
        Handler(Looper.getMainLooper()).postDelayed({
            playerFragment?.setAllowRunBackground(
                allowRunBackground
            )
        }, 200)
    }

    private var handlerRequestPlay: Handler? = null
    private var countRequestPlayError = 0
    private val isConnect = false
    private val runnableRequestPlay = Runnable {
        if (!isOnScreen) {
            resumePlay()
            countRequestPlayError++
            Loggers.e("RESUMEPLAY", "" + countRequestPlayError + isConnect)
        }
    }

    private fun autoTryResumePlayError() {
        if (!isOnScreen) {
            return
        }
        Loggers.e("RESUMEPLAY", "" + countRequestPlayError + isConnect)
        if (countRequestPlayError < 20) {
            resumePlay()
            countRequestPlayError++
            Loggers.e("RESUMEPLAY", "" + countRequestPlayError + isConnect)
        } else if (countRequestPlayError < 40) {
            if (handlerRequestPlay == null) {
                handlerRequestPlay = Handler(Looper.getMainLooper())
            }
            handlerRequestPlay?.postDelayed(runnableRequestPlay, 5000)
        } else if (countRequestPlayError < 60) {
            if (handlerRequestPlay == null) {
                handlerRequestPlay = Handler(Looper.getMainLooper())
            }
            handlerRequestPlay?.postDelayed(runnableRequestPlay, 30000)
        } else if (countRequestPlayError < 80) {
            if (handlerRequestPlay == null) {
                handlerRequestPlay = Handler(Looper.getMainLooper())
            }
            handlerRequestPlay?.postDelayed({}, 60000)
        } else {
            if (handlerRequestPlay == null) {
                handlerRequestPlay = Handler(Looper.getMainLooper())
            }
            handlerRequestPlay?.postDelayed(runnableRequestPlay, 120000)
        }
    }

    fun resumePlay() {
        if (playerFragment == null || MyApplication.getInstance()
                .isEmpty(playerFragment!!.linkPlay)
        ) {
            return
        }
        playerFragment?.setTabSelected(isTabSelected)
        playerFragment?.playLink()
        //        playerFragment.resumeWebview();
    }

    fun resumeWebview() {
//        if (webView != null && !MyApplication.getInstance().isEmpty(webviewUrl)) {
//            webView.loadUrl(webviewUrl);
//            webView.requestFocus(View.FOCUS_DOWN);
//        }
    }

    fun pauseFragment() {
        if (playerFragment != null) {
//            playerFragment.releasePlayer();
            playerFragment?.pause()
        }
    }

    fun stopTryAutoConnect() {
        if (playerFragment != null) {
//            playerFragment.releasePlayer();
            playerFragment?.releasePlayer()
        }
    }

    override fun onDestroy() {
        if (myHttpRequest != null) {
            myHttpRequest?.cancel()
        }
        pauseFragment()
        super.onDestroy()
    }


    private fun checkIntentView(acceptType: String): Intent? {
        var mediaType = 0
        if (acceptType.contains("image")) {
            mediaType = TYPE_IMAGE
        } else if (acceptType.contains("camera")) {
            mediaType = TYPE_VIDEO
        } else if (acceptType.contains("audio")) {
            mediaType = TYPE_AUDIO
        }
        if (!checkPermission(mediaType)) {
            return null
        }
        val intent: Intent
        if (acceptType.contains("image")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "image/*"
            val takeCameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            takeMediaCameraUri = createMediaCameraUri(TYPE_IMAGE)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else if (acceptType.contains("camera")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "video/*"
            val takeCameraIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
            takeMediaCameraUri = createMediaCameraUri(TYPE_VIDEO)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else if (acceptType.contains("audio")) {
            val contentSelectionIntent = Intent(Intent.ACTION_GET_CONTENT)
            contentSelectionIntent.addCategory(Intent.CATEGORY_OPENABLE)
            contentSelectionIntent.type = "audio/*"
            val takeCameraIntent = Intent(MediaStore.Audio.Media.RECORD_SOUND_ACTION)
            takeMediaCameraUri = createMediaCameraUri(TYPE_AUDIO)
            takeCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, takeMediaCameraUri)
            val galleryIntent = Intent()
            galleryIntent.type = "audio/*"
            galleryIntent.action = Intent.ACTION_GET_CONTENT
            val intentArray = arrayOf(takeCameraIntent, galleryIntent)
            intent = Intent(Intent.ACTION_CHOOSER)
            intent.putExtra(Intent.EXTRA_INTENT, contentSelectionIntent)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
            intent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray)
        } else {
            intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.putExtra(Intent.EXTRA_TITLE, getString(R.string.select_task))
        }
        return intent
    }

    private fun createMediaCameraUri(mediaType: Int): Uri {
        var appName = getString(R.string.app_name)
        appName = appName.replace(" ", "")
        val root = Environment.getExternalStorageDirectory()
        val dir = File(root.absolutePath + "/" + appName + "/")
        if (!dir.exists()) {
            dir.mkdirs()
        }
        var name = ""
        name = if (mediaType == TYPE_IMAGE) {
            "Img_" + Calendar.getInstance(Locale.ENGLISH).timeInMillis + ".jpg"
        } else if (mediaType == TYPE_VIDEO) {
            "Video_" + Calendar.getInstance(Locale.ENGLISH).timeInMillis + ".mp4"
        } else if (mediaType == TYPE_AUDIO) {
            "Audio_" + Calendar.getInstance(Locale.ENGLISH).timeInMillis + ".m4a"
        } else {
            "Noname_" + Calendar.getInstance(Locale.ENGLISH).timeInMillis
        }
        val fileName = dir.absolutePath + "/" + name
        val file = File(fileName)
        return Uri.fromFile(file)
    }

    private val permissionCameraList =
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)

    private fun checkPermission(mediaType: Int): Boolean {
        val permissionStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val permissionCamera = Manifest.permission.CAMERA
        val permissionRecordAudio = Manifest.permission.RECORD_AUDIO
        if (mediaType == TYPE_IMAGE || mediaType == TYPE_VIDEO) {
            if (ContextCompat.checkSelfPermission(
                    requireActivity(),
                    permissionStorage
                ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                    requireActivity(), permissionCamera
                ) != PackageManager.PERMISSION_GRANTED
            ) {
//                ActivityCompat.requestPermissions(getActivity(), permissionCameraList,
//                REQUEST_PERMISSION_CAMERA_CODE);
                requestPermissions(permissionCameraList, REQUEST_PERMISSION_CAMERA_CODE)
                return false
            }
        } else if (mediaType == TYPE_AUDIO) {
            if (ContextCompat.checkSelfPermission(
                    requireActivity(),
                    permissionRecordAudio
                ) != PackageManager.PERMISSION_GRANTED
            ) {
//                ActivityCompat.requestPermissions(getActivity(), new
//                String[]{permissionRecordAudio}, REQUEST_PERMISSION_AUDIO_CODE);
                requestPermissions(
                    arrayOf(permissionRecordAudio),
                    REQUEST_PERMISSION_AUDIO_CODE
                )
                return false
            }
        }
        return true
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CAMERA_CODE || requestCode == REQUEST_PERMISSION_AUDIO_CODE) {
            if (!checkGrantPermission(grantResults, permissions)) {
//                ((BaseActivity) getActivity()).showToast(R.string.msg_require_accept_permission);
                Toast.makeText(context, R.string.msg_require_accept_permission, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun checkGrantPermission(grantResults: IntArray, permissions: Array<String>): Boolean {
        for (i in grantResults.indices) {
            if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    fun hideViewDraggable(): Boolean {
        if (isFullLayoutInteractive) {
            tooggleFullLayoutInteractive()
            return true
        }
        return false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        var intent = intent
        super.onActivityResult(requestCode, resultCode, intent)
        if (requestCode == Constant.CODE_LOGIN) {
            if (resultCode == Activity.RESULT_OK) {
                if (layoutRequireLogin == null) {
                    return
                }
                MyApplication.getInstance().dataManager.isNeedRefreshLeftMenu = true
                if (layoutRequireLogin != null) {
                    layoutRequireLogin?.visibility = View.GONE
                    if (activity is MainActivity) {
                        (activity as MainActivity?)?.updateUIChangeAcount()
                    } else {
                        checkShowInterActive()
                    }
                }
            }
            return
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (requestCode == REQUEST_SELECT_FILE) {
                if (uploadMessage == null) {
                    return
                }
                if (intent == null) {
                    intent = Intent()
                }
                if (takeMediaCameraUri != null && intent.data == null) {
                    intent.data = takeMediaCameraUri
                }
                uploadMessage?.onReceiveValue(FileChooserParams.parseResult(resultCode, intent))
                uploadMessage = null
                takeMediaCameraUri = null
            }
        } else if (requestCode == FILECHOOSER_RESULTCODE) {
            if (null == mUploadMessage) {
                return
            }
            // Use MainActivity.RESULT_OK if you're implementing WebView inside Fragment
            // Use RESULT_OK only if you're implementing WebView inside an Activity
            val result =
                if (intent == null || resultCode != Activity.RESULT_OK) null else intent.data
            mUploadMessage?.onReceiveValue(result)
            mUploadMessage = null
        } else {
//            ((BaseActivity) getActivity()).showToast(R.string.msg_error_choose_file);
            Toast.makeText(context, R.string.msg_error_choose_file, Toast.LENGTH_SHORT).show()
        }
    }

    private val statusBarHeight: Int
        private get() {
            val res = resources.getIdentifier("status_bar_height", "dimen", "android")
            var statusBarHeight = 0
            if (res != 0) {
                statusBarHeight = resources.getDimensionPixelSize(res)
            }
            return statusBarHeight
        }

    private fun autoLogin() {
        ServiceUtilTT.autoLogin(requireContext(), object : OnLoginFormListener {
            override fun onLogin(hasLogin: Boolean) {
                if (isDetached) {
                    return
                }
                setNeedUpdateFcmTokenForUser(activity!!, true)
                updateFcmTokenUser(activity!!)
                activity?.runOnUiThread {
                    val item = MyApplication.getInstance().dataManager.itemUser
                    updateInteractive(item)
                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
        isOnScreen = false
        if (onShowListener != null) {
            onShowListener?.onPause()
        }
        if (playerFragment != null) {
            playerFragment?.releasePlayer()
        }
        try {
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private var isOnScreen = false
    override fun onResume() {
        super.onResume()
        hideLoadingDialog()
        isOnScreen = true
        if (onShowListener != null) {
            onShowListener?.onResume()
        }
        if (playerFragment != null) {
            countRequestPlayError = 0
            playerFragment?.playLink()
        }
        checkShowInterActive()
        try {
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    var onShowListener: OnShowListener? = null
    fun updatePlayer(url: String?) {
        linkPlay = url
        Loggers.e("LINKPORT_UDATELINK", linkPlay + "*****" + portSocket)
        var link = url
        if (SharedPreferencesManager.isTest(context) && !MyApplication.getInstance().isEmpty(
                SharedPreferencesManager.getUrlStream(
                    context
                )
            )
        ) {
            link = SharedPreferencesManager.getUrlStream(context)
        }
        if (link == null || link.trim { it <= ' ' } == "") {
            return
        }
        if (playerFragment == null) {
            return
        }
        if (link != playerFragment!!.linkPlay) {
            countRequestPlayError = 0
            playerFragment?.linkPlay = url
            playerFragment?.playLink()
        }
    }

    fun updatePort(port: String?) {
        portSocket = port
        Loggers.e("LINKPORT_UDATEPORT", linkPlay + "*****" + portSocket)
        var PORT = port
        if (SharedPreferencesManager.isTest(context) && !MyApplication.getInstance().isEmpty(
                SharedPreferencesManager.getPortGame(
                    context
                )
            )
        ) {
            PORT = SharedPreferencesManager.getPortGame(context)
        }
        if (PORT == null || PORT.trim { it <= ' ' } == "") {
            return
        }
        if (interactiveFragment == null) {
            return
        }
        portSocketConnect = PORT
        if (PORT.trim { it <= ' ' } != interactiveFragment!!.PORT) {
            interactiveFragment?.PORT = port!!.trim { it <= ' ' }
            interactiveFragment?.initSocket()
        }
    }

    fun updateImagePlacholder(urlImagePlaceholder: String?) {
        if (!isOnScreen || isDetached || playerFragment == null || urlImagePlaceholder == null || urlImagePlaceholder == playerFragment!!.imageThumbnailAudioUrl) {
            return
        }
        playerFragment?.setAndLoadImagePlacholder(urlImagePlaceholder)
    }

    fun updateInteractive(item: ItemUser?) {
        if (interactiveFragment != null && item != null) {
            interactiveFragment?.userId = item.id.toString()
            interactiveFragment?.userName = item.fullname
            interactiveFragment?.isAdmin = "admin" == item.role
            interactiveFragment?.userToken = item.accessToken
            interactiveFragment?.userStatus = item.userStatus
            interactiveFragment?.avatar = item.avatar
            interactiveFragment?.PORT = portSocketConnect
            interactiveFragment?.avatar = item.avatar
            interactiveFragment?.setData()
            interactiveFragment?.initSocket()
        }
    }

    fun updateShareFacebook(urlShare: String?) {
        if (interactiveFragment == null) {
            return
        }
        if (urlShare == null || urlShare.trim { it <= ' ' } == "") {
            interactiveFragment?.showButtonShareFacebook(false)
        } else {
            interactiveFragment?.showButtonShareFacebook(true)
        }
    }

    fun closeSocket() {
        if (interactiveFragment != null) {
            interactiveFragment?.closeSocket()
        }
    }

    fun initSocket() {
        if (interactiveFragment != null) {
            interactiveFragment?.initSocket()
        }
    }

    fun showFragment(fragment: Fragment?) {
        val fragmentTransaction = childFragmentManager.beginTransaction()
        fragmentTransaction.setCustomAnimations(
            R.anim.fragment_right_to_left,
            R.anim.fragment_right_to_left_exit,
            R.anim.fragment_left_to_right,
            R.anim.fragment_left_to_right
        )
        fragmentTransaction.addToBackStack("")
        fragmentTransaction.add(R.id.frameLayoutFullscreen, fragment!!)
        fragmentTransaction.commit()
    }

    private fun requestUpdateState(isOnline: Boolean, id: String) {
        if (!MyApplication.getInstance().isNetworkConnect) {
            return
        }
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        }
        val requestParams = RequestParams()
        val state = if (isOnline) "online" else "offline"
        requestParams.put("state", state)
        requestParams.put("userid", id)
        val api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GROUP_UPDATE_STATUS)
        myHttpRequest?.request(false, api, requestParams, object : ResponseListener {
            override fun onFailure(statusCode: Int) {}
            override fun onSuccess(statusCode: Int, responseString: String) {
                Loggers.e("MyHttpRequest_url", api)
                Loggers.e("MyHttpRequest_result", responseString)
            }
        })
    }

    fun setOnScreenInteractive(isShow: Boolean) {
        if (interactiveFragment != null) {
            interactiveFragment?.isOnScreen = isShow
        }
    }

    private var portSocket: String? = null
    private var portSocketConnect: String? = null
    private var urlShare: String? = null
    private var playImagePlaceholder: String? = null

    companion object {
        var isShowNews = false
    }

    var isLoading: Boolean = false
    var hasData: Boolean = false
    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = true
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(getString(R.string.msg_network_error))
            return
        }
        progressBar?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        } else {
            myHttpRequest?.cancel()
        }

        myHttpRequest?.request(
            false,
            ServiceUtilTT.validAPIDGTH(context, Constant.API_APP_HOME),
            null, object :
//        myHttpRequest!!.request(false, ServiceUtil.validAPI(Constant.API_HOME), null, object :
                MyHttpRequest.ResponseListener {
                override fun onFailure(statusCode: Int) {
                    if (isDetached) {
                        return
                    }
                    isLoading = false
                    activity?.runOnUiThread {
                        showErrorNetwork(getString(R.string.msg_network_error))
                    }
                }

                override fun onSuccess(statusCode: Int, responseString: String?) {
                    if (isDetached) {
                        return
                    }
                    activity?.runOnUiThread { progressBar?.visibility = View.GONE }
                    isLoading = false
                    handleData(responseString)
                }
            })
    }

    private fun showErrorNetwork(string: String) {
        progressBar?.visibility = View.GONE

    }


    val KEY_HANDLE_SLIDER = "KEY_HANDLE_SLIDER"
    val KEY_HANDLE_HOME_LAYOUT = "KEY_HANDLE_HOME_LAYOUT"

    fun handleData(responseStr: String?) {
        BackgroundExecutor.execute(object :
            BackgroundExecutor.Task(KEY_HANDLE_HOME_LAYOUT, 0L, KEY_HANDLE_HOME_LAYOUT) {
            override fun execute() {
                if (responseStr.isNullOrEmpty()) {
                    showErrorNetwork(getString(R.string.msg_empty_data))
                    return
                }
                val responseString = responseStr.trim()
                val jsonObject = JsonParser.getJsonObject(responseString);
                if (jsonObject == null) {
                    showErrorNetwork(getString(R.string.msg_empty_data))
                    return
                }
                val errorCode = JsonParser.getInt(jsonObject, Constant.CODE);
                if (errorCode != Constant.SUCCESS) {
                    val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
                    showErrorNetwork(message ?: getString(R.string.msg_empty_data))
                    return
                }
                val resultArr = JsonParser.getJsonArray(jsonObject, Constant.RESULT)
                if (resultArr == null || resultArr.length() == 0) {
                    showErrorNetwork(getString(R.string.msg_empty_data))
                    return
                }
                progressBar?.visibility = View.GONE
                hasData = true
                for (i in 0 until resultArr.length()) {
                    val jsonObj = resultArr.getJSONObject(i) ?: continue
                    UiThreadExecutor.runTask(KEY_HANDLE_HOME_LAYOUT, Runnable {
                        activity?.runOnUiThread {
                            initHomeLayout(jsonObj)
                        }
                    }, 0)
                }
            }
        })
    }

    fun initSlide(itemList: ArrayList<ItemNews>?) {
        if (itemList == null || itemList.size == 0) {
            sliderLayout.visibility = View.GONE
            return
        }
//
//        val screenSize = ScreenSize(requireActivity())
//        val lp: ViewGroup.LayoutParams = sliderLayout.layoutParams as ViewGroup.LayoutParams
//        lp.height = screenSize.width * 9 / 16
////        lp.height = screenSize.width * 11 / 10
        sliderLayout.layoutParams = frameLayout.layoutParams
        for (itemObj in itemList) {
            val sliderView: MySliderView = object : MySliderView(activity) {
                override fun getView(): View {
                    val view = super.getView()
                    val layoutDescrition = view.glide_slider_description_layout
                    layoutDescrition.removeAllViews()
                    layoutDescrition.setBackgroundColor(Color.TRANSPARENT)
                    return view
                }
            }
            sliderView.image(itemObj.image)
                .setProgressBarVisible(true)
                .setOnSliderClickListener {
                    handleSlideClicked(itemObj)
                }
            sliderLayout.addSlider(sliderView)
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Accordion)
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom)
        sliderLayout.pagerIndicator.setDefaultIndicatorColor(
            ContextCompat.getColor(
                requireActivity(), R
                    .color.indicator_selected
            ), ContextCompat.getColor(
                requireActivity(), R.color
                    .indicator_unselected
            )
        )
        sliderLayout.setDuration(5000)
        sliderLayout.stopCyclingWhenTouch(false)
    }

    fun handleSlideClicked(itemObj: ItemNews) {
        if (itemObj.contentType.isNullOrEmpty()) {
            return
        }
        if (Constant.TYPE_LIVE.equals(itemObj.contentType!!.trim())) {
            (activity as? MainActivity)?.moveToTab(2)
            return
        }
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            GeneralUtils.goDetailNewsActivity(activity, itemObj)
        }, 200)
    }

    fun initHomeLayout(jsonObj: JSONObject?) {
        var type = JsonParser.getInt(jsonObj, "type")
        var cardType = JsonParser.getString(jsonObj, "card_type")
        val dataArray = JsonParser.getJsonArray(jsonObj, Constant.DATA)
        if (activity == null || cardType == null) {
            return
        }
        if (type == 0) {
            type = Constant.STYLE_HORIZONTAL
        }
        val itemList = JsonParser.parseItemNewsList(requireActivity(), dataArray)
        if (itemList == null || itemList.size == 0) {
            return
        }
        if (cardType.equals(Constant.CARD_TYPE_BANNER)) {
            initSlide(itemList)
            return
        }
        interactiveFragment?.initHomeLayout(jsonObj)
    }


    var isFixShowNews = false
    fun showNews(isShowNewsm: Boolean) {
        if (isShowNews.equals(isShowNewsm)) {
            return
        }
        isShowNews = isShowNewsm
        if (isShowNews) {
            if (!hasData) {
                getData()
            }
            playerFragment?.releasePlayer()
            sliderLayout?.visibility = View.VISIBLE
            interactiveFragment?.showNews(true)
        } else {
            if (playerFragment != null) {
                countRequestPlayError = 0
                playerFragment?.playLink()
            }
            sliderLayout?.visibility = View.GONE
            interactiveFragment?.showNews(false)
        }
    }
}