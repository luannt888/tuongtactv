package vn.mediatech.istudiocafe.fragment

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.NumberPicker
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_cart.*
import kotlinx.android.synthetic.main.fragment_cart.buttonClose
import kotlinx.android.synthetic.main.item_order_product_detail.view.*
import kotlinx.android.synthetic.main.layout_ship_time_picker_dialog.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.activity.BaseActivity
import vn.mediatech.istudiocafe.activity.DetailProductActivity
import vn.mediatech.istudiocafe.activity.MainActivity
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.Loggers
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.listener.OnCancelListener
import vn.mediatech.istudiocafe.listener.OnDialogButtonListener
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.util.TextUtil
import java.lang.reflect.Field
import java.text.DecimalFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class CartFragment : Fragment() {
    var isLoading: Boolean = false
    var myHttpRequest: MyHttpRequest? = null
    var preOrientation = 0
    var onCancelListener: OnCancelListener? = null
    var itemUser: ItemUser? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        preOrientation = resources.configuration.orientation
//        hideSystemUI()
        return inflater.inflate(R.layout.fragment_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
        val lp: RelativeLayout.LayoutParams = layoutHeader.layoutParams as RelativeLayout.LayoutParams
        lp.topMargin = (activity as BaseActivity).getStatusBarHeight()
    }

    fun initData() {
        itemUser = MyApplication.getInstance().dataManager.itemUser
        textPersonFullName.text = itemUser!!.fullname
        textPersonPhone.text = itemUser!!.phone
        textPersonAddress.text = itemUser!!.address
        setData()
    }

    fun setData() {
        val itemCartList = MyApplication.getInstance().dataManager.itemCartList
        if (itemCartList.size == 0) {
            (activity as MainActivity).removeCartFragment()
            return
        }
        layoutProductList.removeAllViews()

        val layoutInflater: LayoutInflater = activity?.getSystemService(Context
                .LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var totalPrice = 0
        for (i in 0 until itemCartList.size) {
            val itemObj = itemCartList.get(i)
            val price = itemObj.quantity * itemObj.basePrice
            totalPrice += price
            val layout: View = layoutInflater.inflate(R.layout.item_order_product_detail,
                    layoutProductList, false)
            layout.textQuantity.text = "${itemObj.quantity}"
            layout.textTitle.text = "${itemObj.name}"
            layout.textPriceInfo.text = TextUtil.formatMoney(price)
            if (i == itemCartList.size - 1) {
                layout.viewLineBottom.visibility = View.GONE
            }
            layoutProductList.addView(layout)
            layout.setOnClickListener {
//                (activity as BaseActivity).goDetailProductActivity(itemObj, null)
                val bundle = Bundle()
                bundle.putParcelable(Constant.DATA, itemObj)
                (activity as BaseActivity).gotoActivityForResult(DetailProductActivity::class.java, bundle, Constant.REQUEST_CODE_DETAIL_PRODUCT)
            }
        }
        textTotalCostTemp.text = TextUtil.formatMoney(totalPrice)
        val shipCost = 0
        textShipCost.text = if (shipCost > 0) TextUtil.formatMoney(shipCost) else getString(R.string.free)
//        textTotalCost.text = TextUtil.formatMoney(totalPrice + shipCost)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            if (isDetached) {
                return@Runnable
            }
            textTotalCost.animateText(TextUtil.formatMoney(totalPrice + shipCost))
        }, 500)
    }

    fun initControl() {
        buttonClose.setOnClickListener {
            (activity as MainActivity).removeCartFragment()
        }
        buttonTrash?.setOnClickListener {
            (activity as BaseActivity).showDialog(true, R.string.notification, R.string.msg_delete_cart, R.string.delete, R.string.close, object : OnDialogButtonListener {
                override fun onLeftButtonClick() {
                    MyApplication.getInstance().dataManager.itemCartList.clear()
                    onCancelListener?.onCancel(true)
                    (activity as MainActivity).removeCartFragment()
                }

                override fun onRightButtonClick() {
                }
            })
        }
        radioShipTimeLater.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                showShipTimeDatePicker()
            }, 200)
        }
        buttonChangeAddress.setOnClickListener {
            (activity as MainActivity).showPersonFragment()
            (activity as MainActivity).removeCartFragment()
        }
        buttonOrder.setOnClickListener {
            (activity as BaseActivity).showDialog(getString(R.string.msg_comming_soon))
        }
    }

    fun showShipTimeDatePicker() {
        val view = layoutInflater.inflate(R.layout.layout_ship_time_picker_dialog, null)
        val bottomSheetDialog = BottomSheetDialog(requireContext(), R.style.BottomSheetDialog)
        bottomSheetDialog.setContentView(view)
        val pickerDate = bottomSheetDialog.pickerDate
        val pickerHour = bottomSheetDialog.pickerHour

        val dateList = ArrayList<String>()
        val sdfDisplay = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)

        for (i in 0 until 3) {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DATE, i)
            val dateDisplay = sdfDisplay.format(calendar.getTime())
            val dateStr = if (i == 0) getString(R.string.today) else dateDisplay
            dateList.add(dateStr)
        }
        initNumberPickerStyle(pickerDate, dateList)

        val hourList = ArrayList<String>()
        /*for (i in 0 until 24) {
            val numberFormat: NumberFormat = DecimalFormat("00")
            val timeHourStr = numberFormat.format(i)
            hourList.add(timeHourStr)
        }*/
        initNumberPickerHourData(0, pickerHour, hourList)
        initNumberPickerStyle(pickerHour, hourList)

        pickerDate.setOnValueChangedListener(object : NumberPicker.OnValueChangeListener {
            override fun onValueChange(numberPicker: NumberPicker?, oldVal: Int, newVal: Int) {
                if (oldVal != 0 && newVal != 0) {
                    return
                }
                initNumberPickerHourData(newVal, pickerHour, hourList)
            }
        })

        bottomSheetDialog.buttonClose.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.buttonDone.setOnClickListener {
            bottomSheetDialog.dismiss()
            val date = dateList.get(pickerDate.value)
            val hour = hourList.get(pickerDate.value)
            radioShipTimeLater.text = getString(R.string.ship_time_later) + "\n$date $hour"
        }
        bottomSheetDialog.setOnDismissListener {
            if (radioShipTimeLater.text.toString().equals(getString(R.string.ship_time_later))) {
                radioShipTimeNow.isChecked = true
            }
        }
        bottomSheetDialog.show()
    }

    fun initNumberPickerHourData(newVal: Int, pickerHour: NumberPicker, hourList: ArrayList<String>) {
        var minHour = 8
//        var minTime = 0
        if (newVal == 0) {
            val currentCalendar = Calendar.getInstance()
            val currentHour = currentCalendar.get(Calendar.HOUR_OF_DAY)
            val currentMinute = currentCalendar.get(Calendar.MINUTE)
            val currentSecond = currentCalendar.get(Calendar.SECOND)
            minHour = currentHour + 1
//            minTime = currentMinute
            Loggers.e("pickerDate", "$currentHour _ $currentMinute _ $currentSecond")
        }

        hourList.clear()
        for (i in minHour until 23) {//max is 23hour
            val numberFormat: NumberFormat = DecimalFormat("00")
            val timeHourStr = numberFormat.format(i)
            for (j in 0..1) {
                val minute = 30 * j//30 minute
                val timeMinuteStr = numberFormat.format(minute)
                val time = "$timeHourStr:$timeMinuteStr"
                hourList.add(time)
            }
        }
        val listArr: Array<String> = hourList.toTypedArray()
        pickerHour.minValue = 0
        pickerHour.maxValue = listArr.size - 1
        pickerHour.displayedValues = listArr
    }

    fun initNumberPickerStyle(picker: NumberPicker, list: ArrayList<String>) {
        val listArr: Array<String> = list.toTypedArray()
        picker.minValue = 0
        picker.maxValue = listArr.size - 1
        picker.displayedValues = listArr
        picker.wrapSelectorWheel = false
        changeNumberPickerDividerColor(picker, ContextCompat.getColor(requireContext(), R.color.gray))
        setNumberPickerDividerHeight(picker, 1)
    }

    private fun changeNumberPickerDividerColor(picker: NumberPicker, color: Int) {
        try {
            val mField: Field = NumberPicker::class.java.getDeclaredField("mSelectionDivider")
            mField.setAccessible(true)
            val colorDrawable = ColorDrawable(color)
            mField.set(picker, colorDrawable)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun setNumberPickerDividerHeight(picker: NumberPicker, height: Int) {
        val fields = NumberPicker::class.java.declaredFields
        for (f in fields) {
            if (f.name == "mSelectionDividerHeight") {
                f.isAccessible = true
                try {
                    f[picker] = height
                } catch (e: IllegalAccessException) {
                    e.printStackTrace()
                }
                break
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        if (requestCode == Constant.REQUEST_CODE_DETAIL_PRODUCT) {
            setData()
        }
    }

    fun showSystemUI() {
//        requireContext().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_VISIBLE
//        (activity as BaseActivity).setStatusbarColor("#00000000", false)
//        Toast.makeText(activity, "showSystemUI", Toast.LENGTH_SHORT).show()
    }

    fun hideSystemUI() {
//        requireContext().window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        // Hide the nav bar and status bar
//                or View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_FULLSCREEN)
//        requireContext().window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
    }

    override fun onDestroy() {
        myHttpRequest?.cancel()
        super.onDestroy()
    }

}