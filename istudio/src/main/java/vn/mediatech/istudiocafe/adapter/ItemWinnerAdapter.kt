package vn.mediatech.istudiocafe.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_winner.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemWiner
import vn.mediatech.istudiocafe.util.ScreenSize
import java.util.*

class ItemWinnerAdapter(
    val context: Context, val itemList: ArrayList<ItemWiner>, val type: Int, val style: Int, val
    numberColumn: Int
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var isBlackTheme = false


    override fun getItemViewType(position: Int): Int {
        val viewType = style
        return position
    }

    fun initViewSize(viewType: Int) {
        /*val height: Int = activity.resources.getDimensionPixelSize(R.dimen.dimen_150)
        when (viewType) {
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = height * 2 / 3
                imageHeight = height
            }
        }*/
        val screenSize = ScreenSize(context)
        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
            screenSize.width
        imageWidth = (screenWidth  - context.resources.getDimensionPixelSize(R.dimen.dimen_10)*(numberColumn+1))/numberColumn
        imageHeight = imageWidth
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_winner,
            parent, false
        )
        return FrameViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemWiner = itemList.get(position)
        if (holder is FrameViewHolder) {
            bindDataFrameView(holder, itemObj, position)
        }
    }

    fun bindDataFrameView(holder: FrameViewHolder, itemObj: ItemWiner, position: Int) {
        if (!itemObj.avatar.isNullOrEmpty()) {
            holder.layoutRoot.visibility = View.VISIBLE
            holder.imgThumbnail.visibility = View.VISIBLE
            MyApplication.getInstance().loadImageCircle(
                context,
                holder.imgThumbnail,
                itemObj.avatar,
                R.drawable.avatar_placeholder
            )
        } else {
            holder.layoutRoot.visibility = View.GONE
            holder.imgThumbnail.visibility = View.GONE
        }
        var index: String? = null
        val pos = position + 1
        if (pos < 10) {
            index = "0" + pos
        } else {
            index = "" + pos
        }
        holder.textIndex.text = index
        holder.textName.text = itemObj.fullName
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
//            holder?.layoutRoot?.isEnabled = false
//            Handler(Looper.getMainLooper()).postDelayed({holder?.layoutRoot?.isEnabled = true},3000)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    inner class FrameViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgThumbnail = itemView.imgAvatar
        val textIndex = itemView.textIndex
        val textName = itemView.textName

        init {
            if (isBlackTheme) {
                textName.setTextColor(ContextCompat.getColor(context, R.color.white))
            } else {
                textName.setTextColor(ContextCompat.getColor(context, R.color.black_tt))
            }
            val lp = imgThumbnail.layoutParams
            lp?.width = imageWidth
            lp?.height = imageHeight
        }
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemWiner, position: Int)
    }


}