package vn.mediatech.istudiocafe.adapter

import android.app.Activity
import android.graphics.Color
import android.graphics.Outline
import android.text.SpannableString
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import kotlinx.android.synthetic.main.item_event_view.view.textCategory
import kotlinx.android.synthetic.main.item_event_view.view.textPriceInfo
import kotlinx.android.synthetic.main.item_event_view.view.textTime
import kotlinx.android.synthetic.main.item_news_grid_view_tt.view.*
import kotlinx.android.synthetic.main.item_news_list_box_view_tt.view.imagePlayThumbnail
import kotlinx.android.synthetic.main.item_news_list_box_view_tt.view.textComment
import kotlinx.android.synthetic.main.item_news_list_box_view_tt.view.textShare
import kotlinx.android.synthetic.main.item_news_list_box_view_tt.view.textView
import kotlinx.android.synthetic.main.item_news_list_view_tt.view.*
import kotlinx.android.synthetic.main.item_product_grid_view.view.*
import kotlinx.android.synthetic.main.item_product_grid_view.view.buttonOrder
import kotlinx.android.synthetic.main.item_voucher.view.*
import kotlinx.android.synthetic.main.item_voucher.view.imageThumbnail
import kotlinx.android.synthetic.main.item_voucher.view.layoutImage
import kotlinx.android.synthetic.main.item_voucher.view.layoutRoot
import kotlinx.android.synthetic.main.item_voucher.view.textTitle
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemNews
import vn.mediatech.istudiocafe.util.ScreenSize
import vn.mediatech.istudiocafe.util.ServiceUtilTT
import vn.mediatech.istudiocafe.util.TextUtil
import java.util.*

class NewsAdapter(
    val activity: Activity, val itemList: ArrayList<ItemNews>, val style: Int, val
    numberColumn: Int, val isRelatesList: Boolean = false
) : RecyclerView.Adapter<ViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    var imageWidth: Int = 0
    var imageHeight: Int = 0
    var isShowButtonInfo = true
    var isBlackTheme = false

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemViewType(position: Int): Int {
        val itemObj = itemList.get(position)
        var viewType = style
        if (!isRelatesList) {
            if (viewType != Constant.STYLE_EVENT && viewType != Constant.STYLE_HORIZONTAL) {
                if (itemObj.type.equals(Constant.TYPE_NEWS_FEATURE)) {
                    viewType = Constant.STYLE_LISTVIEW_BOX;
                }
            }
        }
//        Loggers.e("getItemViewType: $position", "viewType = $viewType")
        return viewType
    }

    fun initViewSize(viewType: Int) {
        val screenSize = ScreenSize(activity)
        val screenWidth: Int = if (screenSize.width > screenSize.height) screenSize.height else
            screenSize.width
        val spacePx: Int = activity.resources.getDimensionPixelSize(R.dimen.space_item)
        when (viewType) {
            Constant.STYLE_GRIDVIEW -> {
                val whiteDistance: Int = spacePx * (numberColumn + 1)
                imageWidth = (screenWidth - whiteDistance) / numberColumn
                imageHeight = imageWidth * 5 / 8
            }
            Constant.STYLE_LISTVIEW -> {
                val whiteDistance: Int = spacePx * 2
                imageWidth = (screenWidth - whiteDistance) / 3
                imageHeight = imageWidth * 7 / 10
            }
            Constant.STYLE_LISTVIEW_BOX -> {
                val whiteDistance: Int = spacePx * 2
                imageWidth = (screenWidth - whiteDistance)
                imageHeight = imageWidth * 5 / 8
            }
            Constant.STYLE_HORIZONTAL -> {
                imageWidth = screenWidth * 9 / 20;
                imageHeight = imageWidth * 5 / 8
            }
            Constant.STYLE_EVENT -> {
                val whiteDistance: Int = spacePx * (numberColumn + 1)
                imageWidth = (screenWidth - whiteDistance) / numberColumn
                imageHeight = imageWidth * 2 / 5
            }
            Constant.STYLE_HOME_MENU -> {
                val whiteDistance: Int = spacePx * (numberColumn + 1)
                imageWidth = (screenWidth - whiteDistance) / numberColumn
                imageHeight = imageWidth
            }
            Constant.STYLE_PRODUCT -> {
                val whiteDistance: Int = spacePx * (numberColumn + 1)
                imageWidth = (screenWidth - whiteDistance) / numberColumn
                imageHeight = imageWidth * 5 / 8
            }
            Constant.STYLE_GALLERY_VERTICAL -> {
                imageWidth = screenWidth * 21 / 50;
                imageHeight = imageWidth * 7 / 5
            }
            Constant.STYLE_VOUCHER -> {
                val whiteDistance: Int = spacePx * 2
                imageWidth = (screenWidth - whiteDistance) / 3
                imageHeight = imageWidth
            }
        }
    }

    fun setFullSpan(view: View, isFullSpan: Boolean) {
        if (view.layoutParams is StaggeredGridLayoutManager.LayoutParams) {
            (view.layoutParams as StaggeredGridLayoutManager.LayoutParams).isFullSpan = isFullSpan
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initViewSize(viewType)
        when (viewType) {
            Constant.STYLE_GRIDVIEW -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_news_grid_view_tt,
                    parent, false
                )
                setFullSpan(view, false)
                return GridViewHolder(view, false)
            }
            Constant.STYLE_HORIZONTAL -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_news_grid_view_tt,
                    parent, false
                )
                setFullSpan(view, false)
                return GridViewHolder(view, true)
            }
            Constant.STYLE_LISTVIEW -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_news_list_view_tt,
                    parent, false
                )
                setFullSpan(view, false)
                return ListViewHolder(view)
            }
            Constant.STYLE_LISTVIEW_BOX -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_news_list_box_view_tt,
                    parent, false
                )
                setFullSpan(view, true)
                return ListBoxViewHolder(view)
            }
            Constant.STYLE_EVENT -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_event_view,
                    parent, false
                )
                setFullSpan(view, false)
                return EventViewHolder(view)
            }
            Constant.STYLE_HOME_MENU -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_home_menu_view,
                    parent, false
                )
                setFullSpan(view, false)
                return HomeMenuViewHolder(view)
            }
            Constant.STYLE_PRODUCT -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_product_grid_view,
                    parent, false
                )
                setFullSpan(view, false)
                return ProductGridViewHolder(view)
            }
            Constant.STYLE_GALLERY_VERTICAL -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_gallery_vertical_view,
                    parent, false
                )
                setFullSpan(view, false)
                return GalleryVerticalViewHolder(view)
            }
            Constant.STYLE_VOUCHER -> {
                val view = LayoutInflater.from(activity).inflate(
                    R.layout.item_voucher,
                    parent, false
                )
                setFullSpan(view, false)
                return VoucherHolder(view)
            }
        }
        //default
        val view = LayoutInflater.from(activity).inflate(
            R.layout.item_news_grid_view_tt, parent,
            false
        )
        setFullSpan(view, false)
        return GridViewHolder(view, false)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemObj: ItemNews = itemList.get(position)
        if (holder is GridViewHolder) {
            bindDataGridView(holder, itemObj, position)
        } else if (holder is ListViewHolder) {
            bindDataListView(holder, itemObj, position)
        } else if (holder is ListBoxViewHolder) {
            bindDataListBoxView(holder, itemObj, position)
        } else if (holder is EventViewHolder) {
            bindDataEventView(holder, itemObj, position)
        } else if (holder is HomeMenuViewHolder) {
            bindDataHomeMenuView(holder, itemObj, position)
        } else if (holder is ProductGridViewHolder) {
            bindDataProductGridView(holder, itemObj, position)
        } else if (holder is GalleryVerticalViewHolder) {
            bindDataGalleryVerticalView(holder, itemObj, position)
        } else if (holder is VoucherHolder) {
            bindDataVoucherView(holder, itemObj, position)
        }
    }

    fun bindDataGridView(holder: GridViewHolder, itemObj: ItemNews, position: Int) {
        holder.textTitle.text = itemObj.name
        holder.textCategory.text = itemObj.categoryName
        holder.textView.text = itemObj.view
        holder.textComment.text = itemObj.comment
        holder.imagePlayThumbnail.visibility =
            if (!itemObj.contentType.isNullOrEmpty() && itemObj.contentType.equals(
                    Constant
                        .TYPE_VIDEO
                )
            ) View.VISIBLE else View.GONE
        holder.textLive.visibility = if (itemObj.isLive!!) View.VISIBLE else View.GONE
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)
        holder.textComment.setOnClickListener {
            onItemClickListener?.onCommentClick(itemObj, position)
        }
        holder.textShare.setOnClickListener {
            onItemClickListener?.onShareClick(itemObj, position)
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
    }

    fun bindDataListView(holder: ListViewHolder, itemObj: ItemNews, position: Int) {
        holder.textTitle.text = itemObj.name
        holder.textTime.text = itemObj.timeAgo

        if (itemObj.categoryName.isNullOrEmpty()) {
            holder.textCategory.visibility = View.GONE
        } else {
            holder.textCategory.text = String.format(
                Locale.ENGLISH,
                activity.getString(R.string.event_category_format),
                itemObj.categoryName
            )
            holder.textCategory.visibility = View.VISIBLE
        }

        holder.imagePlayThumbnail.visibility =
            if (!itemObj.contentType.isNullOrEmpty() && itemObj.contentType.equals(
                    Constant
                        .TYPE_VIDEO
                )
            ) View.VISIBLE else View.GONE
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
        holder.layoutRoot.setOnLongClickListener {
            onItemClickListener?.onLongClick(itemObj, position, holder.layoutBottom)
            true
        }
    }

    fun bindDataListBoxView(holder: ListBoxViewHolder, itemObj: ItemNews, position: Int) {
        holder.textTitle.text = itemObj.name
        holder.textCategory.text = itemObj.categoryName
        holder.textView.text = itemObj.view
        holder.textComment.text = itemObj.comment
        holder.textTime.text = itemObj.timeAgo
        holder.imagePlayThumbnail.visibility =
            if (!itemObj.contentType.isNullOrEmpty() && itemObj.contentType.equals(
                    Constant
                        .TYPE_VIDEO
                )
            ) View.VISIBLE else View.GONE
        holder.textCategory.visibility =
            if (!itemObj.categoryName.isNullOrEmpty()) View.VISIBLE else View.GONE
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)
        holder.textComment.setOnClickListener {
            onItemClickListener?.onCommentClick(itemObj, position)
        }
        holder.textShare.setOnClickListener {
            onItemClickListener?.onShareClick(itemObj, position)
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
    }

    fun bindDataEventView(holder: EventViewHolder, itemObj: ItemNews, position: Int) {
        if (itemObj.name.isNullOrEmpty()) {
            holder.textTitle.visibility = View.GONE
        } else {
            holder.textTitle.text = itemObj.name
            holder.textTitle.visibility = View.VISIBLE
        }
        if (itemObj.time.isNullOrEmpty()) {
            holder.textTime.visibility = View.GONE
        } else {
            holder.textTime.text = itemObj.time
            holder.textTime.visibility = View.VISIBLE
        }
        if (itemObj.categoryName.isNullOrEmpty()) {
            holder.textCategory.visibility = View.GONE
        } else {
            holder.textCategory.text = String.format(
                Locale.ENGLISH,
                activity.getString(R.string.event_category_format),
                itemObj.categoryName
            )
            holder.textCategory.visibility = View.VISIBLE
        }
        val price =
            if (itemObj.basePrice == 0) activity.getString(R.string.free) else TextUtil.formatMoney(
                itemObj.basePrice
            )
        holder.textPrice.text = price

        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
        holder.buttonOrder.setOnClickListener {
            onItemClickListener?.onOrderClick(itemObj, position, holder)
        }
    }

    fun bindDataHomeMenuView(holder: HomeMenuViewHolder, itemObj: ItemNews, position: Int) {
        if (itemObj.name.isNullOrEmpty()) {
            holder.textTitle.visibility = View.GONE
        } else {
            holder.textTitle.text = itemObj.name
            holder.textTitle.visibility = View.VISIBLE
        }
        MyApplication.getInstance().loadImageCircle(activity, holder.imageThumbnail, itemObj.image)

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
    }

    fun bindDataProductGridView(holder: ProductGridViewHolder, itemObj: ItemNews, position: Int) {
        if (itemObj.name.isNullOrEmpty()) {
            holder.textTitle.visibility = View.GONE
        } else {
            holder.textTitle.text = itemObj.name
            holder.textTitle.visibility = View.VISIBLE
        }
//        holder.textPrice.text = TextUtil.formatMoney(itemObj.basePrice)
        holder.rate.rating = itemObj.rate!!
        if (itemObj.reviews.isNullOrEmpty()) {
            holder.textReviews.visibility = View.GONE
        } else {
            holder.textReviews.text = itemObj.reviews
            holder.textReviews.visibility = View.VISIBLE
        }
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)
        initUserReviewsData(holder.layoutImageReviews, itemObj)

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
        holder.buttonOrder.setOnClickListener {
            onItemClickListener?.onOrderClick(itemObj, position, holder)
        }
    }

    fun initUserReviewsData(layoutImageReviews: RelativeLayout, itemObj: ItemNews) {
        val itemUserReviewList = itemObj.itemUserReviewList ?: return
        layoutImageReviews.removeAllViews()
        for (i in 0 until itemUserReviewList.size) {
            val itemUserReview = itemUserReviewList.get(i)
            val paddingStart = activity.resources.getDimensionPixelSize(R.dimen.dimen_15) * i
            val image = ImageView(activity)
            image.scaleType = ImageView.ScaleType.CENTER_CROP
            image.setBackgroundResource(R.drawable.bg_user_review)
            image.layoutParams = RelativeLayout.LayoutParams(50, 50)
            image.x = paddingStart * 1.0f
            MyApplication.getInstance().loadImageCircle(activity, image, itemUserReview.avatar)
            layoutImageReviews.addView(image)
        }
    }

    fun bindDataGalleryVerticalView(
        holder: GalleryVerticalViewHolder,
        itemObj: ItemNews,
        position: Int
    ) {
        if (itemObj.name.isNullOrEmpty()) {
            holder.textTitle.visibility = View.GONE
        } else {
            holder.textTitle.text = itemObj.name
            holder.textTitle.visibility = View.VISIBLE
        }

        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
    }

    fun formatNumber(input: String?): Int {
        if (input.isNullOrEmpty()) {
            return 0
        }
        var number = 0
        try {
            number = input.toInt()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return number
    }

    fun bindDataVoucherView(holder: VoucherHolder, itemObj: ItemNews, position: Int) {
        holder.textTitle.text = itemObj.name
        val price = itemObj.price!!
        holder.textPrice.text = TextUtil.formatMoney(price)
        holder.textFirstPrice.text = TextUtil.formatMoney(itemObj.basePrice)
        /*var percent = if (itemObj.basePrice == 0) 0 else (price * 100f / itemObj.basePrice).toInt()
        when {
            percent <= 0 -> percent = -100
            percent < 100 -> percent -= 100
            percent == 100 -> percent = 0
        }*/
        val percent = ServiceUtilTT.calculatorPercent(price, itemObj.basePrice)
//        val percent = 0
        holder.textPercent.text =
            String.format(Locale.ENGLISH, activity.getString(R.string.percent), percent)
        holder.textExprireDate.text = itemObj.expireDate
        MyApplication.getInstance().loadImage(activity, holder.imageThumbnail, itemObj.image)

        val firstPrice = holder.textFirstPrice.text.toString()
        val firstPriceSpan = SpannableString(firstPrice)
        firstPriceSpan.setSpan(
            StrikethroughSpan(),
            0,
            firstPrice.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        holder.textFirstPrice.text = firstPriceSpan

        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position, holder)
        }
        holder.layoutRoot.setOnLongClickListener {
            onItemClickListener?.onLongClick(itemObj, position, holder.layoutRoot)
            true
        }
    }

    /*fun formatMoney(number: Int?): String {
        if (number == null) {
            return "0 đ"
        }
        val format: NumberFormat = NumberFormat.getCurrencyInstance()
        format.setMaximumFractionDigits(0)
        format.setCurrency(Currency.getInstance("VND"))
        return format.format(number)
    }*/

    inner class GridViewHolder(itemView: View, isHorizontal: Boolean) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val imagePlayThumbnail = itemView.imagePlayThumbnail
        val textTitle = itemView.textTitle
        val textCategory = itemView.textCategory
        val layoutButtonInfo = itemView.layoutButtonInfo
        val textView = itemView.textView
        val textComment = itemView.textComment
        val textShare = itemView.textShare
        val layoutImage = itemView.layoutImage
        val textLive = itemView.textLive

        init {
            if (isHorizontal) {
                val params = layoutRoot.layoutParams as RecyclerView.LayoutParams
//                params.width = imageWidth
                params.width = RecyclerView.LayoutParams.WRAP_CONTENT
                layoutRoot.layoutParams = params
                layoutRoot.setBackgroundColor(ContextCompat.getColor(activity, R.color.transparent))
            }

            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
            setRound(layoutImage, 3f)
//            setRound(itemView)
            /*if (style == Constant.STYLE_GALLERY) {
                textCategory.visibility = View.GONE
                isShowButtonInfo = false
            }*/
            if (!isShowButtonInfo) {
                layoutButtonInfo.visibility = View.GONE
            }
            if(isBlackTheme){
                textTitle.setTextColor(ContextCompat.getColor(activity,R.color.white))
//                textTime.setTextColor(ContextCompat.getColor(activity,R.color.white2_tt))
                textCategory.setTextColor(ContextCompat.getColor(activity,R.color.white3_tt))
            } else{
                textTitle.setTextColor(ContextCompat.getColor(activity,R.color.black_tt))
//                textTime.setTextColor(ContextCompat.getColor(activity,R.color.gray))
                textCategory.setTextColor(ContextCompat.getColor(activity,R.color.black4))
            }
        }
    }

    inner class ListViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val imagePlayThumbnail = itemView.imagePlayThumbnail
        val textTitle = itemView.textTitle
        val textCategory = itemView.textCategory
        val textTime = itemView.textTime
        val layoutImage = itemView.layoutImage
        val layoutBottom = itemView.layoutBottom

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
//            setRound(itemView)
            setRound(layoutImage, 3f)
            if (isRelatesList) {
                textTitle.setTextColor(ContextCompat.getColor(activity, R.color.black_tt))
                layoutRoot.setBackgroundColor(Color.parseColor("#e9e9e9"))
            }
            if(isBlackTheme){
                layoutRoot.setBackgroundColor(ContextCompat.getColor(activity, R.color.transparent))
                textTitle.setTextColor(ContextCompat.getColor(activity,R.color.white))
                textTime.setTextColor(ContextCompat.getColor(activity,R.color.white2_tt))
                textCategory.setTextColor(ContextCompat.getColor(activity,R.color.white3_tt))
            } else{
                textTitle.setTextColor(ContextCompat.getColor(activity,R.color.black_tt))
                textTime.setTextColor(ContextCompat.getColor(activity,R.color.gray))
                textCategory.setTextColor(ContextCompat.getColor(activity,R.color.black4))
            }
        }
    }

    inner class ListBoxViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val imagePlayThumbnail = itemView.imagePlayThumbnail
        val textTitle = itemView.textTitle
        val textTime = itemView.textTime
        val textCategory = itemView.textCategory
        val textView = itemView.textView
        val textComment = itemView.textComment
        val textShare = itemView.textShare
        val layoutImage = itemView.layoutImage

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
            if(isBlackTheme){
                textTitle.setTextColor(ContextCompat.getColor(activity,R.color.white))
                textTime.setTextColor(ContextCompat.getColor(activity,R.color.white2_tt))
                textCategory.setTextColor(ContextCompat.getColor(activity,R.color.white3_tt))
            } else{
                textTitle.setTextColor(ContextCompat.getColor(activity,R.color.black_tt))
                textTime.setTextColor(ContextCompat.getColor(activity,R.color.gray))
                textCategory.setTextColor(ContextCompat.getColor(activity,R.color.black4))
            }
//            setRound(itemView)
        }
    }

    inner class EventViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val textTitle = itemView.textTitle
        val textTime = itemView.textTime
        val textCategory = itemView.textCategory
        val textPrice = itemView.textPriceInfo
        val buttonOrder = itemView.buttonOrder
        val layoutImage = itemView.layoutImage

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
            setRound(itemView)
        }
    }

    inner class HomeMenuViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val textTitle = itemView.textTitle
        val layoutImage = itemView.layoutImage

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
            setRound(itemView)
        }
    }

    inner class ProductGridViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val textTitle = itemView.textTitle
        val textPrice = itemView.textPriceInfo
        val layoutImage = itemView.layoutImage
        val rate = itemView.rate
        val textReviews = itemView.textReviews
        val buttonOrder = itemView.buttonOrder
        val layoutImageReviews = itemView.layoutImageReviews

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
            setRound(itemView)
        }
    }

    inner class GalleryVerticalViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imageThumbnail = itemView.imageThumbnail
        val textTitle = itemView.textTitle
        val layoutImage = itemView.layoutImage

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
            setRound(itemView)
        }
    }

    inner class VoucherHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val layoutImage = itemView.layoutImage
        val imageThumbnail = itemView.imageThumbnail
        val textTitle = itemView.textTitle
        val textPrice = itemView.textPrice
        val textFirstPrice = itemView.textFirstPrice
        val textPercent = itemView.textPercent
        val textExprireDate = itemView.textExprireDate

        init {
            val lp: LinearLayout.LayoutParams = layoutImage.layoutParams as LinearLayout
            .LayoutParams
            lp.width = imageWidth
            lp.height = imageHeight
            layoutImage.layoutParams = lp
            setRound(imageThumbnail)
        }
    }

    fun setRound(view: View, cornerRadiusDP: Float = 7f) {
        view.setOutlineProvider(object : ViewOutlineProvider() {
            override fun getOutline(view: View, outline: Outline) {
                val cornerRadius = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    cornerRadiusDP, activity.resources.getDisplayMetrics()
                )
//                if (isRoundBottomStyleListBox) {
//                    outline.setRoundRect(0, 0, view.width, (view.height + cornerRadius).toInt()
//                    , cornerRadius)
//                } else {
                outline.setRoundRect(0, 0, view.width, view.height, cornerRadius)
//                }
            }
        })
        view.setClipToOutline(true)
    }

    interface OnItemClickListener {
        fun onClick(itemObject: ItemNews, position: Int, holder: ViewHolder)
        fun onLongClick(itemObject: ItemNews, position: Int, view: View)
        fun onCommentClick(itemObject: ItemNews, position: Int)
        fun onShareClick(itemObject: ItemNews, position: Int)
        fun onOrderClick(itemObject: ItemNews, position: Int, holder: ViewHolder)
    }
}