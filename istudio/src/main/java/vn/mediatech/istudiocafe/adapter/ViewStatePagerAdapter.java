package vn.mediatech.istudiocafe.adapter;

import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class ViewStatePagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<String> tabTitleList;
    private List<Fragment> fragmentList;

    public ViewStatePagerAdapter(FragmentManager fm, List<Fragment> fragments, ArrayList<String> tabTitleList) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        this.fragmentList = fragments;
        this.tabTitleList = tabTitleList;
    }

    @Override
    public Fragment getItem(int position) {
        if (fragmentList != null && position < fragmentList.size()) {
            return fragmentList.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        if (fragmentList != null) {
            return fragmentList.size();
        }
        return 0;
    }

    public int getTabTitleCount() {
        if (tabTitleList != null) {
            return tabTitleList.size();
        }
        return 0;
    }

    public void setTabTitleList(ArrayList<String> tabTitleList) {
        this.tabTitleList = tabTitleList;
    }

    public void addTabPage(Fragment fragment, String tabTitle) {
        if (fragment == null || tabTitle == null) {
            return;
        }
        if (fragmentList == null) {
            fragmentList = new ArrayList<>();
        }
        if (tabTitleList == null) {
            tabTitleList = new ArrayList<>();
        }
        fragmentList.add(fragment);
        tabTitleList.add(tabTitle);
//        notifyDataSetChanged();
    }

    public void updateTabTitle(int position, String tabTitle) {
        if (fragmentList == null || tabTitle == null || tabTitleList.size() <= position) {
            return;
        }
        tabTitleList.remove(position);
        tabTitleList.add(position, tabTitle);
        notifyDataSetChanged();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (tabTitleList == null || tabTitleList.size() <= position) {
            return null;
        }
        return tabTitleList.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
    }
}