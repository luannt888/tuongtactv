package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemPhoto(val title: String?, val image: String?, var time: String? = null) : Parcelable