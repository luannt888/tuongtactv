package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemWiner(
        @SerializedName("id")val id: String?,
        @SerializedName("avatar")val avatar: String?,
        @SerializedName("show_date")val showDate: String?,
        @SerializedName("full_name")var fullName: String? = null) : Parcelable
/*show_date: "2021-05-18",
id: "525",
avatar: "https://graph.facebook.com/2999532883651035/picture?type=large",
full_name: "Lê Việt Hoàng"*/