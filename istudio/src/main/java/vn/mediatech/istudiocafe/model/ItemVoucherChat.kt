package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemVoucherChat(
        @SerializedName("id") val id: Int?,
        @SerializedName("user_name") val userName: String?,
        @SerializedName("user_avatar") val userAvatar: String?,
        @SerializedName("type") val type: Int?,
        @SerializedName("content") val content: String?,
        @SerializedName("image") val image: String?,
        @SerializedName("created_at") val createdAt: String?
) : Parcelable