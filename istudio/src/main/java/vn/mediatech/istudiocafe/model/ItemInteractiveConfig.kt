package vn.mediatech.istudiocafe.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ItemInteractiveConfig(
        @SerializedName("id") val id: Int?,
        @SerializedName("appid") val appId: String?,
        @SerializedName("type_show_home") var typeShowHome: String? = "0",// 0:INVISIBLE, 1:VISIBLE
        @SerializedName("type_show_detail") var typeShowDetail: String?  = "0",
        @SerializedName("type_show_news") var typeShowNews: String?  = "0",
        @SerializedName("port_socket") val portSocket: String?,
        @SerializedName("url_livestream") val urlLivestream: String?,
        @SerializedName("url_share_livestream") val urlShare: String?,
        @SerializedName("port_support") val portSupport: String?,
        @SerializedName("background") val imagePlaceholderUrl: String?,
        @SerializedName("icon") val icon: String?,
        @SerializedName("base_url") val baseUrl: String?,
        @SerializedName("menu") val menu: Array<ItemMenuInteractive>?,
        @SerializedName("time_period") var time_period:Int?
) : Parcelable
/*
id: "1",
appid: "bacgiang",
type_show_home: "1",
type_show_detail: "1",
port_socket: "wss://api.daugiatruyenhinh.com:8088/interactive?",
url_livestream: "rtmp://rtmp.mediatech.vn/mdtvlive/group4toadam?vhost=__defaultVhost__",
icon: "https://daugiatruyenhinh.com/upload/advertise/thumb/pop-up-hien-thi.png"*/
