package vn.mediatech.istudiocafe.model

data class ItemVersion(val versionCode: Int?, val title: String?, val message: String?, val url: String?, val isUpdate: Boolean?)