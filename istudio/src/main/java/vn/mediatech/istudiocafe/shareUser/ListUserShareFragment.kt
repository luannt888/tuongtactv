package vn.mediatech.istudiocafe.shareUser

import android.graphics.Color
import android.os.*
import android.view.*
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.skydoves.powermenu.*
import kotlinx.android.synthetic.main.fragment_list_user_share.*
import kotlinx.android.synthetic.main.item_checkbox.*
import vn.mediatech.interactive.app.*
import vn.mediatech.voicecontrol.listener.OnShowDismissListener
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication
import vn.mediatech.istudiocafe.model.ItemUser
import vn.mediatech.istudiocafe.service.MyHttpRequest
import vn.mediatech.istudiocafe.service.RequestParams
import vn.mediatech.istudiocafe.util.*
import java.util.*
import kotlin.collections.ArrayList


class ListUserShareFragment : BottomSheetDialogFragment() {
    private var isLoadMore: Boolean = false
    private var page: Int = 1
    private var limit: Int = 20
    var isLoading: Boolean = false
    var isViewCreated: Boolean = false
    var savedInstanceState: Bundle? = null
    var bottomSheetDialog: BottomSheetDialog? = null
    var myHttpRequest: MyHttpRequest? = null
    var onShowDismissListener: OnShowDismissListener? = null
    var itemUser: ItemUser? = null
    var itemList: ArrayList<ItemUserShare> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_user_share, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        dialog?.setCanceledOnTouchOutside(true)
        dialog?.setOnShowListener {
            bottomSheetDialog = it as BottomSheetDialog
            bottomSheetDialog!!.behavior.skipCollapsed = true
            bottomSheetDialog!!.behavior.isDraggable = false
            bottomSheetDialog!!.behavior.state = BottomSheetBehavior.STATE_EXPANDED
            val bottomSheet = bottomSheetDialog!!.findViewById<View>(
                com.google.android.material.R.id.design_bottom_sheet
            )
                ?: return@setOnShowListener
            bottomSheet.setBackgroundColor(Color.TRANSPARENT)
        }
        isViewCreated = true
        onShowDismissListener?.onShow()
        checkRefresh()
        return
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.run {
            putParcelable(Constant.USERNAME, itemUser)
        }
        super.onSaveInstanceState(outState)
    }

    fun checkRefresh() {
        if (!isViewCreated) {
            Handler(Looper.getMainLooper()).postDelayed(Runnable {
                checkRefresh()
            }, 2000)
            return
        }
        init()
    }

    private fun init() {
        initUI()
        initData()
        initControl()
    }

    fun initUI() {
//        if (activity is BaseActivity) {
//            val lp: RelativeLayout.LayoutParams =
//                buttonBack.layoutParams as RelativeLayout.LayoutParams
//            lp.topMargin = (activity as BaseActivity).getStatusBarHeight()
//        }
    }


    fun initData() {
        if (savedInstanceState != null) {
            itemUser = savedInstanceState!!.getParcelable(Constant.USERNAME)
        } else {
            itemUser = MyApplication.getInstance().dataManager.itemUser
        }
//        val bundle = arguments
        getData()
    }

    fun showErrorNetwork(message: String?) {
        if (message == null) {
            return
        }
        activity?.runOnUiThread {
            try {
                if (itemList.size == 0) {
                    textNotify.visibility = View.VISIBLE
                    textNotify.text = message
                }
                progressBar.visibility = View.GONE
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun getData() {
        if (isLoading || isDetached) {
            return
        }
        isLoading = false
        if (!MyApplication.getInstance().isNetworkConnect) {
            isLoading = false
            showErrorNetwork(activity?.getString(R.string.msg_network_error))
            return
        }
        progressBar?.visibility = View.VISIBLE
        textNotify?.visibility = View.GONE
        if (myHttpRequest == null) {
            myHttpRequest = MyHttpRequest(activity)
        }
        val requestParams = RequestParams()
//        requestParams.put("page", "${page}")
//        requestParams.put("limit", "${limit}")
        requestParams.put("presenter_id", "${itemUser!!.id}")
//        requestParams.put("presenter_id", "345")
        var api: String? = null
        api = ServiceUtilTT.validAPIDGTH(context, Constant.API_GET_LIST_SHARE_USER)
        myHttpRequest!!.request(false, api, requestParams, object : MyHttpRequest.ResponseListener {
            override fun onFailure(statusCode: Int) {
                if (isDetached) {
                    return
                }
                isLoading = false
                showErrorNetwork(activity?.getString(R.string.msg_network_error))
            }

            override fun onSuccess(statusCode: Int, responseString: String?) {
                if (isDetached) {
                    return
                }
                handleData(responseString)
                activity?.runOnUiThread {
                    try {
                        progressBar.visibility = View.GONE
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                isLoading = false
            }
        })
    }

    fun handleData(responseStr: String?) {
        if (responseStr.isNullOrEmpty()) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val responseString = responseStr.trim()
        val jsonObject = JsonParser.getJsonObject(responseString)
        if (jsonObject == null) {
            showErrorNetwork(activity?.getString(R.string.msg_empty_data))
            return
        }
        val errorCode = JsonParser.getInt(jsonObject, Constant.CODE)
        if (errorCode != Constant.SUCCESS) {
            val message = JsonParser.getString(jsonObject, Constant.MESSAGE)
//            activity?.runOnUiThread { message?.let { showErrorNetwork(it) } }
            activity?.runOnUiThread { showErrorNetwork(activity?.getString(R.string.msg_empty_data)) }
            return
        }
        val resultObj = JsonParser.getString(jsonObject, Constant.RESULT)
        val gson = Gson()
        val gsonType = object : TypeToken<ArrayList<ItemUserShare>>() {}.type
        var newList: ArrayList<ItemUserShare> = ArrayList()
        try {
            val list: ArrayList<ItemUserShare>? = gson.fromJson(resultObj, gsonType)
            newList.addAll(list!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        activity?.runOnUiThread {
            try {
                itemList.clear()
                if (adapter != null) {
                    itemList.addAll(newList)
                    adapter?.notifyDataSetChanged()
                } else {
                    itemList.addAll(newList)
                    initAdapter()
                }
                setData()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun setData() {
        if (itemList.size == 0) {
            textNotify.visibility = View.VISIBLE
        } else {
            textNotify.visibility = View.GONE
        }
    }

    var adapter: ItemUserShareAdapter? = null
    private fun initAdapter() {
//        itemList.get(0).statusInteractive = Constant.STATUS_SHARE_USER_NOT_INTERACTIVE
//        itemList.get(1).statusInteractive = Constant.STATUS_SHARE_USER_SUCCESS
//        itemList.get(2).statusInteractive = Constant.STATUS_SHARE_USER_NOT_CONFIRM
        adapter = ItemUserShareAdapter(requireContext(), itemList)
        val layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        recyclerView.scheduleLayoutAnimation()
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }


    fun scrollToBottom() {
//        scrollContent.fullScroll(View.FOCUS_DOWN)
    }

    fun initControl() {
        textNotify.setOnClickListener {
            progressBar.visibility = View.VISIBLE
            getData()
        }
        buttonBack.setOnClickListener {
            dismiss()
        }
        layoutRefresh.setOnRefreshListener {
            refreshData()
        }
    }

    private fun refreshData() {
        page = 1
        adapter?.notifyDataSetChanged()
        progressBar.visibility = View.VISIBLE
        layoutRefresh.isRefreshing = false
        getData()
    }

    override fun onDestroyView() {
        myHttpRequest?.cancel()
        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
    }
}