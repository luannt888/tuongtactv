package vn.mediatech.istudiocafe.shareUser

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import kotlinx.android.synthetic.main.item_user_share.view.*
import vn.mediatech.istudiocafe.R
import vn.mediatech.istudiocafe.app.Constant
import vn.mediatech.istudiocafe.app.MyApplication

class ItemUserShareAdapter(
    val context: Context,
    val list: ArrayList<ItemUserShare>
) : Adapter<ItemUserShareAdapter.MyViewHolder>() {
    var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.item_user_share,
            parent, false
        )
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position >= list.size) {
            return
        }
        val itemObj: ItemUserShare = list.get(position)
        MyApplication.getInstance().loadImageCircle(
            context,
            holder.imgAvatar,
            itemObj.avatar,
            R.drawable.ic_avatar
        )
        holder.textName.setText(itemObj.name)
        holder.textId.setText("ID: ${itemObj.userId}")
        holder.textStatus.visibility = View.GONE
        if (Constant.STATUS_SHARE_USER_SUCCESS.equals(itemObj.statusInteractive)) {
            holder.textStatus.setText(itemObj.statusText)
            holder.textStatus.setBackgroundResource(R.drawable.bg_rec_round_conner_blue)
            holder.textStatus.visibility = View.VISIBLE
        }else if (Constant.STATUS_SHARE_USER_NOT_INTERACTIVE.equals(itemObj.statusInteractive)) {
            holder.textStatus.setText(itemObj.statusText)
            holder.textStatus.setBackgroundResource(R.drawable.bg_rec_round_conner_yelow)
            holder.textStatus.visibility = View.VISIBLE
        } else if (Constant.STATUS_SHARE_USER_NOT_CONFIRM.equals(itemObj.statusInteractive)) {
            holder.textStatus.setText(itemObj.statusText)
            holder.textStatus.setBackgroundResource(R.drawable.bg_rec_round_conner_grey)
            holder.textStatus.visibility = View.VISIBLE
        }
        holder.layoutRoot.setOnClickListener {
            onItemClickListener?.onClick(itemObj, position)
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class MyViewHolder(itemView: View) : ViewHolder(itemView) {
        val layoutRoot = itemView.layoutRoot
        val imgAvatar = itemView.imgAvatar
        val textId = itemView.textId
        val textName = itemView.textName
        val textStatus = itemView.textStatus
    }

    interface OnItemClickListener {
        fun onClick(item: ItemUserShare, position: Int)
    }

}