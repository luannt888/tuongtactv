/**
 * Automatically generated file. DO NOT MODIFY
 */
package vn.mediatech.istudiocafe;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "vn.mediatech.istudiocafe";
  public static final String BUILD_TYPE = "debug";
  public static final int VERSION_CODE = 10;
  public static final String VERSION_NAME = "1.3.22";
}
